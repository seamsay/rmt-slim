# see https://hub.docker.com/_/gcc/
image: abrown41/rmt-debian-lite:python

# Reuse pip cache between jobs to speed up
cache:                      # Pip's cache doesn't store the python packages
  paths:                    # https://pip.pypa.io/en/stable/topics/caching/
    - .cache/pip

before_script:
  - apt update && apt upgrade -y
  - python3 -V               # Print out python version for debugging
  - python3 -m venv venv
  - source venv/bin/activate
  - python -m pip install .
  - python -m pip install -r requirements-dev.txt

variables:  # Change pip's cache directory to be inside the project directory since we can only cache local items.
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  RUN_KELVIN: "false"

stages:
  - test
  - build
  - regress
  - docs 
  - deploy

pytest-and-coverage:
  stage: test
  script:
    - coverage run -m pytest
    - coverage report -m
    - coverage xml
    - flake8
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura 
        path: coverage.xml
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"'
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'
    - if: '$CI_COMMIT_BRANCH' 

# kelvin tests are disabled for now with rules
build:kelvin:
  before_script: []
  tags: 
    - gcc
    - linux
    - singularity
    - qub
  stage: build
  script:
    - pwd
    - ssh login4 "cd $PWD ; $HOME/gitlab-runner/scripts/gnu_compile.sh"
  artifacts:
    paths:
      - bin
    expire_in : 24 hours
  rules:
    - if: $RUN_KELVIN == "true"

# kelvin tests are disabled for now with if: false
test:kelvin:
  before_script: []
  tags: 
    - gcc
    - linux
    - singularity
    - qub
  allow_failure: true
  needs: ["build:kelvin"]
  stage: regress
  script:
    - pwd
    - ssh login4 "cd $PWD && salloc --nodes=1 --tasks-per-node=128 --time=00:30:00 $HOME/gitlab-runner/scripts/run_regression_test.sh $PWD/bin/rmt.x"
  rules:
    - if: $RUN_KELVIN == "true"


build:docker:
  stage: build
  before_script: []
  script:
    - cat /etc/os-release
    - export CC=$(which mpicc)
    - export CXX=$(which mpicxx)
    - export FC=$(which mpifort)
    - cmake source -D RMT_TESTING=ON
    - make
  artifacts:
    paths: 
      - bin
    expire_in: 24 hours

test:docker:
  stage: regress
  needs: ["build:docker"]
  script:
    - export RMT_EXEC=$PWD/bin/rmt.x
    - export CONVERTHD_EXEC=$PWD/bin/convertHD
    - pytest tests/testregress.py tests/utilities_tests/test_analyse_hd.py
  dependencies: 
    - build:docker

test:unit:
  stage: regress
  before_script: []
  needs:
    - build:docker
  script:
    # RMT logging is written to stdout but is not useful for testing.
    # Since all test logging is written to stderr we can safely ignore stdout.
    - bin/RMT-tester >/dev/null

build-docs:
  stage: docs
  script:
    - apt -y install doxygen
    - export CC=$(which mpicc)
    - export CXX=$(which mpicxx)
    - export FC=$(which mpifort)
    - export FFLAGS="-std=f2008 -pedantic-errors"
    - cmake source 
    - make doc_doxygen
    - make -C docs html
  artifacts:
    paths:
      - docs/html
    expire_in: 1 week
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'

pages:
  stage: deploy
  before_script: []
  script:
    - mv docs/html public/
  dependencies:
    - build-docs
  artifacts:
    paths:
    - public/
    expire_in: 1 year
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
