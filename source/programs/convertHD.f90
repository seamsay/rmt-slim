! Copyright 2023
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
PROGRAM convertHD
    USE ISO_FORTRAN_ENV, ONLY: stdout => OUTPUT_UNIT, INT32, REAL64

    USE hamiltonian_input_file, ONLY: H_file, L_block, parse_H_file, deallocate_H_file
    USE rmt_serialiseHD, ONLY: serialise_f90, serialise_json
    USE rmt_utilities, ONLY: int_to_char, real_to_es_char

    IMPLICIT NONE

    TYPE output_format
        INTEGER(SELECTED_INT_KIND(2)) :: code
    END TYPE output_format

    INTERFACE OPERATOR(==)
        PROCEDURE equal_output_format_output_format
    END INTERFACE

    TYPE(output_format), PARAMETER :: JSON = output_format(1)
    TYPE(output_format), PARAMETER :: F90 = output_format(2)

    TYPE arguments
        CHARACTER(len=:), ALLOCATABLE :: path
        TYPE(output_format) :: output_format = JSON
    END TYPE arguments

    TYPE(arguments) :: cli
    TYPE(H_file) :: contents

    cli = parse_arguments()
    contents = parse_H_file(cli%path)

    IF (cli%output_format == JSON) THEN
        WRITE (stdout, '(a)') serialise_json(contents)
    ELSE IF (cli%output_format == F90) THEN
        WRITE (stdout, '(a)') serialise_f90(contents)
    ELSE
        ERROR STOP "INTERNAL: Should have been caught by argument parser."
    END IF

    CALL deallocate_H_file(contents)
    DEALLOCATE (cli%path)
CONTAINS
    FUNCTION get_argument(i) RESULT(VALUE)
        CHARACTER(len=:), ALLOCATABLE :: VALUE
        INTEGER, INTENT(IN) :: i

        INTEGER :: length

        CALL GET_COMMAND_ARGUMENT(i, length=length)
        ALLOCATE (CHARACTER(len=length) :: VALUE)
        CALL GET_COMMAND_ARGUMENT(i, VALUE=VALUE)
    END FUNCTION

    PURE FUNCTION uppercase(s)
        CHARACTER(len=*), INTENT(IN) :: s
        CHARACTER(len=LEN_TRIM(s)) :: uppercase

        INTEGER :: i

        DO i = 1, LEN(uppercase)
            IF ('a' <= s(i:i) .AND. s(i:i) <= 'z') THEN
                uppercase(i:i) = ACHAR(IACHAR('A') + IACHAR(s(i:i)) - IACHAR('a'))
            ELSE
                uppercase(i:i) = s(i:i)
            END IF
        END DO
    END FUNCTION

    SUBROUTINE print_help
        CHARACTER(len=:), ALLOCATABLE :: name
        name = get_argument(0)

        WRITE (stdout, '(a, a, a)') "usage: ", name, "[-o|--output-format JSON|F90] <path to H file>"
        WRITE (stdout, '()')
        WRITE (stdout, '(a)') "Convert a H file to other formats."
        WRITE (stdout, '()')
        WRITE (stdout, '(a)') "ARGUMENTS"
        WRITE (stdout, '()')
        WRITE (stdout, '(a)') "    -h|--help"
        WRITE (stdout, '(a)') "        Print this help message."
        WRITE (stdout, '(a)') "    -o|--output-format"
        WRITE (stdout, '(a)') "        The format to convert the H file to."
        WRITE (stdout, '(a)') "        Default: JSON"
        WRITE (stdout, '()')
        WRITE (stdout, '(a)') "        JSON: A simple JSON format storing derived types as objects and n-dimensional arrays in"
        WRITE (stdout, '(a)') "              an object containing the shape in an array and the values in a column-major array."
        WRITE (stdout, '()')
        WRITE (stdout, '(a)') "        F90: A format that can be used to create the derived type representing the H file in RMT."
    END SUBROUTINE

    FUNCTION parse_arguments() RESULT(args)
        TYPE(arguments) :: args

        INTEGER :: i
        CHARACTER(len=:), ALLOCATABLE :: argument, uppercased_argument

        i = 0
        DO WHILE (i < COMMAND_ARGUMENT_COUNT())
            i = i + 1
            argument = get_argument(i)
            uppercased_argument = uppercase(argument)

            IF (uppercased_argument == "--HELP" .OR. argument == "-h") THEN
                CALL print_help
                STOP
            ELSE IF (uppercased_argument == "--OUTPUT-FORMAT" .OR. argument == "-o") THEN
                i = i + 1
                argument = get_argument(i)
                uppercased_argument = uppercase(argument)

                IF (uppercased_argument == "JSON") THEN
                    args%output_format = JSON
                ELSE IF (uppercased_argument == "F90") THEN
                    args%output_format = F90
                ELSE
                    CALL print_help
                    ERROR STOP "Output format not recognised: "//argument
                END IF
            ELSE IF (argument(1:1) /= "-") THEN
                args%path = argument
            ELSE
                CALL print_help
                ERROR STOP "Argument not recognised: "//argument
            END IF
        END DO

        IF (.NOT. ALLOCATED(args%path)) THEN
            CALL print_help
            ERROR STOP "No path to the H file was provided."
        END IF
    END FUNCTION

    PURE FUNCTION equal_output_format_output_format(l, r) RESULT(equal)
        TYPE(output_format), INTENT(IN) :: l, r
        LOGICAL :: equal

        equal = l%code == r%code
    END FUNCTION
END PROGRAM convertHD
