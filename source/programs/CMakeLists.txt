cmake_minimum_required(VERSION 3.0)
project(RMT Fortran)

# Use module files
include_directories(${CMAKE_BINARY_DIR}/modules/mod)

# Programs in the RMT package
add_executable(rmt.x       tdse.f90)
add_executable(reform      reform.f90)
add_executable(field_check field_check.f90)
add_executable(GetProcInfo GetProcInfo.f90)
add_executable(convertHD   convertHD.f90)


# Link all executables against common subprograms
target_link_libraries(rmt.x       modules ${LAPACK_LIBRARIES})
target_link_libraries(reform      modules ${LAPACK_LIBRARIES})
target_link_libraries(field_check modules ${LAPACK_LIBRARIES})
target_link_libraries(GetProcInfo modules ${LAPACK_LIBRARIES})
target_link_libraries(convertHD   modules ${LAPACK_LIBRARIES})
