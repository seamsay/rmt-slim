! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> \authors MAL, A Brown, Z Masin, D Clarke, G Armstrong, J Benda
!> \date    2006 - 2018
!>
!> \brief Reads the appropriate target structure and Hamiltonian files for the selected (atomic!> / molecular) mode and then sets up the large Hamiltonian for a time-dependent calculation.
!>
!> In the case of atomic calculation, the files read are the 'H' file and 'D*' files produced by atomic
!> packages R-Matrix I / II.
!>
!> In the case of molecular calculation, the only file needed is 'molecular_data' produced by the molecular
!> package UKRmol+ (rmt_interface).
!>
!> While the atomic and molecular input data are mostly compatible, the dipole operator matrix elements
!> present in 'D*' and in 'molecular_data' differ by sign. The atomic dipoles adhere to the proper
!> definition, i.e. that the dipole moment points from negative to positive charge. On the contrary,
!> the notion of dipole in the molecular package is more "geometric", in the sense that the dipole operator
!> is identified with coordinate operator only, disregarding the charge and resulting in opposite directions.
!>
!> There is also another inconsistency between atomic and molecular calculations, in the outer region.
!> Molecular code considers the projectile wave function to be expanded in terms of real spherical harmonics,
!> while the atomic code uses complex spherical harmonics with Fano-Racah phase convention (i.e. standard
!> spherical harmonics with additional factor of \f$ \mathrm{i}^l \f$, as common in the partial wave expansion).
!>
!> \note DDAC (March 2017) - Modified for CP fields
!> \note GSJA (November 2017) - Modified for atomic-molecular merge
!>

MODULE readhd

    USE precisn,                   ONLY: wp, longint
    USE rmt_assert,                ONLY: assert
    USE angular_momentum,          ONLY: cg, dcg, &
                                         parity_allowed, &
                                         satisfies_triangle_rule
    USE communications_parameters, ONLY: outer_group_id, &
                                         inner_group_id
    USE mpi_communications,        ONLY: mpi_comm_region, &
                                         get_my_group_id, &
                                         get_my_group_pe_id
    USE initial_conditions,        ONLY: gs_finast, &
                                         numsols => no_of_field_confs, &
                                         dipole_velocity_output, &
                                         lplusp, &
                                         set_ML_max, &
                                         ML_max, &
                                         xy_plane_desired, &
                                         adjust_gs_energy, &
                                         gs_energy_desired, &
                                         remove_eigvals_threshold, &
                                         neigsrem_1st_sym, &
                                         neigsrem_higher_syms, &
                                         disk_path, &
                                         RMatrixI_format_id, &
                                         RMatrixII_format_id, &
                                         dipole_format_id, &
                                         molecular_target,&
                                         debug, &
                                         LS_coupling_id,   &
                                         jK_coupling_id,   &
                                         coupling_id
    USE hamiltonian_input_file,                   ONLY: log_H_parameters, &
                                         log_atomic_structure, &
                                         log_debug_info, &
                                         log_total_channels, &
                                         log_wmat, &
                                         read_H_parameters2, &
                                         read_H_file2
    USE MPI

    IMPLICIT NONE

    ! Print_wmat allows for a check that the chosen parameters are indeed appropriate
    ! Surface amplitudes are written to wmatout

    LOGICAL, PARAMETER           :: print_wmat = .false.

    ! ZM additional variables from the molecular_data input
    INTEGER, SAVE                :: nfdm_molecular
    REAL(wp), SAVE               :: deltar_molecular
    INTEGER, SAVE                :: n_rg         ! total number of non-zero real Gaunt coefficients needed for th WD and WP potentials.
    INTEGER, ALLOCATABLE, SAVE   :: lm_rg(:, :)  ! l,m values for each non-zero real Gaunt coefficient rg
    INTEGER, ALLOCATABLE, SAVE   :: ichl(:, :)   ! indices of target states corresponding to each l,m channel
    REAL(wp), ALLOCATABLE        :: rg(:)        ! non-zero real Gaunt coefficients
    REAL(wp), ALLOCATABLE        :: r_points(:)  ! points at which the amplitudes have been evaluated

    COMPLEX(wp), ALLOCATABLE     :: wf(:, :)
    REAL(wp), ALLOCATABLE        :: wamp2(:, :, :, :) ! channel amplitudes evaluated for the inner region finite difference points
    INTEGER, SAVE                :: nelc, nz, lrang2, lamax, two_electron_nchan, ntarg, ntarg_dication
    REAL(wp), SAVE               :: bbloch, rmatr
    REAL(wp), ALLOCATABLE, SAVE  :: etarg(:)
    INTEGER, ALLOCATABLE, SAVE   :: ltarg(:), starg(:)
    INTEGER, ALLOCATABLE, SAVE   :: ltarg_dication(:), starg_dication(:)

    INTEGER, ALLOCATABLE, SAVE   :: L_block_lrgl(:), L_block_nspn(:), L_block_npty(:), LML_block_Lblk(:), &
                                    LML_block_lrgl(:), LML_block_nspn(:), LML_block_npty(:), LML_block_ml(:)
    INTEGER, ALLOCATABLE, SAVE   :: L_block_nchan(:), mnp1(:), states_per_L_block(:), states_per_LML_block(:), &
                                    LML_block_nchan(:)

    ! Flags used in dipole_coupled (any bit combination is allowed).
    ! They indicate which component of the dipole (none, one or more of them) couples the individual symmetries.
    INTEGER, PARAMETER           :: nocoupling = INT(b'000')
    INTEGER, PARAMETER           :: downcoupling = INT(b'001')
    INTEGER, PARAMETER           :: samecoupling = INT(b'010')
    INTEGER, PARAMETER           :: upcoupling = INT(b'100')

    INTEGER, ALLOCATABLE, SAVE   :: L_block_nconat(:, :), L_block_l2p(:, :), lrgt(:, :), LML_block_l2p(:, :), m2p(:, :)
    REAL(wp), ALLOCATABLE, SAVE  :: wmat(:, :, :), eig(:, :)

    ! Dipole matrix storage arrays. There are 'no_of_L_blocks' unique dipole blocks. A dipole block b is linking wave-function
    ! segments 'iidip(b)' to 'ifdip(b)' and is stored in 'dipsto(:, :, b)'. Or, from the other end, wave-function segments
    ! 'i' and 'j' are linked by block 'b = block_ind(i, j, comp)' where 'comp' is a specific component of the dipole operator.
    ! The index 'comp' only matters in molecular mode.
    REAL(wp), ALLOCATABLE, SAVE  :: dipsto(:, :, :)
    REAL(wp), ALLOCATABLE, SAVE  :: dipsto_v(:, :, :)
    INTEGER, ALLOCATABLE, SAVE   :: iidip(:), ifdip(:)
    INTEGER, ALLOCATABLE, SAVE   :: block_ind(:, :, :)

    INTEGER, ALLOCATABLE, SAVE   :: dipole_coupled(:, :)
    INTEGER, ALLOCATABLE, SAVE   :: i_couple_to(:,:)    ! allocated in distribute_hd_blocks.f90
    INTEGER, allocatable, SAVE   :: my_no_of_couplings(:)
    REAL(wp), ALLOCATABLE, SAVE  :: L_block_cf(:, :, :, :)
    REAL(wp), ALLOCATABLE, SAVE  :: LML_block_cf(:, :, :, :)
    INTEGER, ALLOCATABLE, SAVE   :: L_block_post(:)
    INTEGER, ALLOCATABLE, SAVE   :: LML_block_nconat(:, :)
    INTEGER, ALLOCATABLE, SAVE   :: LML_block_post(:)

    REAL(wp), ALLOCATABLE, SAVE  :: crlv(:, :)
    REAL(wp), ALLOCATABLE, SAVE  :: crlv_v(:, :)

    REAL(wp), ALLOCATABLE, SAVE  :: wp_read_store(:, :)
    REAL(wp), ALLOCATABLE, SAVE  :: wd_read_store(:, :)
    REAL(wp), ALLOCATABLE, SAVE  :: wd_read_store_v(:, :)

    REAL(wp), ALLOCATABLE, SAVE  :: LML_wp_read_store(:, :)
    REAL(wp), ALLOCATABLE, SAVE  :: LML_wd_read_store(:, :)
    REAL(wp), ALLOCATABLE, SAVE  :: LML_wd_read_store_v(:, :)

    INTEGER, SAVE                :: nchmx, nstmx, inast, no_of_L_blocks, fintr, no_of_LML_blocks

    INTEGER, ALLOCATABLE, SAVE   :: nchn(:), kept(:), nstk(:)

    ! Allocation should be fixed - currently allocated to size 580
    INTEGER, ALLOCATABLE, SAVE   :: neigsrem(:), lstartm1(:, :)

    INTEGER, SAVE                :: max_L_block_size
    INTEGER, SAVE                :: L_block_tot_nchan, LML_block_tot_nchan, lmaxp1

    INTEGER, SAVE                :: bspl_nc, bspl_k9, bspl_nb
    INTEGER, SAVE                :: bspl_ndim, bspl_nt
    INTEGER, SAVE :: mat_size_wp
    INTEGER, SAVE :: mat_size_wd
    REAL(wp), ALLOCATABLE, SAVE  :: bspl_t(:)
    REAL(wp), ALLOCATABLE, SAVE  :: splwave(:, :, :)
    REAL(wp), ALLOCATABLE, SAVE  :: cg_store(:, :)

    ! Region III data
    INTEGER, ALLOCATABLE, SAVE   :: ion_targ(:), ion_l(:), ion_s(:)
    INTEGER, ALLOCATABLE, SAVE   :: first_l(:), second_l(:)
    INTEGER, ALLOCATABLE, SAVE   :: two_electron_l(:), tot_l(:), tot_ml(:)

    PRIVATE
    PUBLIC cg_store
    PUBLIC wmat, rmatr, wf
    PUBLIC no_of_L_blocks, mnp1, eig, etarg, L_block_post, L_block_nchan, LML_block_nchan, ntarg, no_of_LML_blocks
    PUBLIC L_block_npty, L_block_lrgl, ltarg, L_block_l2p, L_block_nconat, crlv
    PUBLIC L_block_cf, lamax, L_block_nspn, LML_block_lrgl, LML_block_npty, LML_block_nspn, LML_block_Lblk, LML_block_ml
    PUBLIC set_ML_max, ML_max
    PUBLIC LML_block_post, LML_block_nconat, LML_block_l2p, m2p, LML_block_cf
    PUBLIC nchn, kept, nstk, crlv_v
    PUBLIC inast, nstmx, nchmx, nz, nelc
    PUBLIC L_block_tot_nchan, LML_block_tot_nchan, max_L_block_size, fintr
    PUBLIC bspl_nc, bspl_k9, bspl_nb, bspl_ndim, bspl_nt, bspl_t
    PUBLIC splwave
    PUBLIC iidip, ifdip, dipsto, dipsto_v

    PUBLIC nocoupling, downcoupling, samecoupling, upcoupling

    PUBLIC ntarg_dication, ltarg_dication, starg_dication

    PUBLIC wp_read_store
    PUBLIC wd_read_store
    PUBLIC wd_read_store_v
    PUBLIC LML_wp_read_store
    PUBLIC LML_wd_read_store
    PUBLIC LML_wd_read_store_v

    PUBLIC my_no_of_couplings, i_couple_to
    PUBLIC dipole_coupled, block_ind
    PUBLIC neigsrem, states_per_L_block, states_per_LML_block
    PUBLIC lmaxp1, lstartm1

    PUBLIC finind_L, finind_LML
    PUBLIC inner_master_reads_spline_files
    PUBLIC deallocate_read_spline_files
    PUBLIC deallocate_rank0_read_hd_files1
    PUBLIC deallocate_rank0_read_hd_files
    PUBLIC deallocate_readhd
    PUBLIC init_read_in_and_distribute
    PUBLIC read_psi_files
    PUBLIC reform_reads
    PUBLIC print_dipole_coupling

    PUBLIC deltar_molecular, nfdm_molecular, r_points
    PUBLIC n_rg, rg, lm_rg, ichl

    PUBLIC mat_size_wp
    PUBLIC mat_size_wd
    PUBLIC wamp2

    PUBLIC ion_targ, ion_l, ion_s
    PUBLIC first_l, second_l, two_electron_l
    PUBLIC tot_l, tot_ml, two_electron_nchan

CONTAINS

    !> \brief   Dipole coupling table
    !> \authors Jakub Benda
    !> \date    2018
    !>
    !> Produces a table with all dipole couplings between the angular states. Each element
    !> of the table is represented as three characters, each of which is one of 'u', 'd', 's', '_'.
    !> In that order they stand for "up", "down", "same" and "none" coupling. For example,
    !> if the simulated system is an H2O2 molecule (point group C2), the table will look
    !> like this:
    !>
    !> \verbatim
    !>    __s  ud_
    !>    ud_  ___
    !> \endverbatim
    !>
    !> In this example there are just two eigenstates of the symmetry, \f$ A \f$ and \f$ B \f$.
    !> \f$ A \f$ is coupled to itself by \f$ z \f$, \f$ B \f$ is not coupled to itself by any
    !> component, and the cross coupling is due to both \f$ x \f$ and \f$ y \f$.
    !>
    SUBROUTINE print_dipole_coupling

        IMPLICIT NONE

        INTEGER :: i, j

        WRITE (*, '(/,5X,"Dipole coupling modes (up, same, down):")')
        DO j = 1, no_of_LML_blocks
            WRITE (*, '(5X)', ADVANCE="no")
            DO i = 1, no_of_LML_blocks
                WRITE (*, '(8A4)', ADVANCE="no") &
                    MERGE('u', '_', IAND(dipole_coupled(i, j), upcoupling) /= 0)// &
                    MERGE('s', '_', IAND(dipole_coupled(i, j), samecoupling) /= 0)// &
                    MERGE('d', '_', IAND(dipole_coupled(i, j), downcoupling) /= 0)
            END DO
            WRITE (*, *)
        END DO

    END SUBROUTINE print_dipole_coupling

!---------------------------------------------------------------------------

    !> Stripped down version of Inner_Master_Reads_Input files to provide info for wavefunction reconstruction
    SUBROUTINE reform_reads

        IF (molecular_target) THEN
            CALL inner_master_reads_molecular_input_files
        ELSE
            ! Read file H.dat to get information on numbers of states and channels for each symmetry
            CALL read_H_parameters()

            ! Read file H.dat to fill arrays of data eg energy evals and surface amplitudes
            CALL read_H_file(adjust_gs_energy, gs_energy_desired)  !READ ALL RELEVANT DATA FROM THE H-FILE
        END IF

    END SUBROUTINE reform_reads

!---------------------------------------------------------------------------

    SUBROUTINE inner_master_reads_atomic_input_files

        ! Only the inner region master calls this routine and reads the files

        ! Read file H.dat to get information on numbers of states and channels for each symmetry
        CALL read_H_parameters()

        ! Read file H.dat to fill arrays of data eg energy evals and surface amplitudes
        CALL read_H_file(adjust_gs_energy, gs_energy_desired)  !READ ALL RELEVANT DATA FROM THE H-FILE

        ! Read file d00.dat and d.dat to get dipole matrix elements
        IF (dipole_format_id .EQ. RMatrixI_format_id) THEN
            CALL read_D_file_RMatrixI(0)  !READ ALL RELEVANT DATA FROM THE D-FILE
        ELSEif (dipole_format_id .EQ. RMatrixII_format_id) THEN
            CALL read_D_file()  !READ ALL RELEVANT DATA FROM THE D-FILE
        END IF

        ! Read the B-Spline files
        CALL read_spline_files_info

        CALL setup_cg_store

    END SUBROUTINE inner_master_reads_atomic_input_files

!---------------------------------------------------------------------------

    SUBROUTINE inner_master_reads_molecular_input_files

        ! Only the inner region master calls this routine and reads the file molecular_data.

        CALL read_molecular_data(crlv, fintr, dipsto, iidip, ifdip, n_rg, rg, lm_rg, nelc, nz, lrang2, lamax, ntarg, inast, &
                                 no_of_L_blocks, nchmx, nstmx, lmaxp1, LML_block_tot_nchan, rmatr, bbloch, etarg, ltarg, starg, &
                                 LML_block_lrgl, LML_block_nspn, LML_block_npty, LML_block_nchan, mnp1, LML_block_nconat,&
                                 LML_block_l2p, m2p, eig, wmat, LML_block_cf, &
                                 ichl, nfdm_molecular, deltar_molecular, r_points, wamp2)

        CALL remove_states

    END SUBROUTINE inner_master_reads_molecular_input_files

!---------------------------------------------------------------------------

    !> \brief   Import of molecular data
    !> \authors Z Masin, A Brown, J Benda
    !> \date    2017 - 2020
    !>
    !> Reads all symmetry-related data, channel data, R-matrix amplitudes and dipoles from the file molecular_data: analogue
    !> of Read_H_Parameters, Read_H_File and Read_Spline_Files_data. The amplitude data for all symmetries is read into the single
    !> array wamp2. The values in wamp2 correspond to the amplitudes explicitly evaluated for the nfdm inner region points.
    !>
    SUBROUTINE read_molecular_data(crlv, fintr, dipsto, iidip, ifdip, n_rg, rg, lm_rg, nelc, nz, lrang2, lamax, ntarg, inast, &
                                   no_of_L_blocks, nchmx, nstmx, lmaxp1, LML_block_tot_nchan, rmatr, bbloch, etarg, ltarg, starg, &
                                   LML_block_lrgl, LML_block_nspn, LML_block_npty, LML_block_nchan, mnp1, LML_block_nconat,&
                                   LML_block_l2p, m2p, eig, wmat, LML_block_cf, &
                                   ichl, nfdm, delta_r, r_points, wamp2)

        USE electric_field,     ONLY: get_field_components
        USE initial_conditions, ONLY: debug

        IMPLICIT NONE

        INTEGER, INTENT(OUT)  :: nelc, nz, lrang2, lamax, ntarg, inast, nchmx, nstmx, lmaxp1, LML_block_tot_nchan, fintr, n_rg
        INTEGER, INTENT(OUT)  :: nfdm, no_of_L_blocks
        REAL(wp), INTENT(OUT) :: rmatr, bbloch, delta_r
        REAL(wp), ALLOCATABLE, INTENT(OUT) :: etarg(:), r_points(:), wamp2(:, :, :, :)
        INTEGER, ALLOCATABLE, INTENT(OUT)  :: LML_block_l2p(:, :), m2p(:, :), LML_block_lrgl(:)
        INTEGER, ALLOCATABLE, INTENT(OUT)  ::  LML_block_nspn(:), LML_block_npty(:), LML_block_nchan(:), mnp1(:)
        INTEGER, ALLOCATABLE, INTENT(OUT)  :: LML_block_nconat(:, :), iidip(:), ifdip(:)
        INTEGER, ALLOCATABLE, INTENT(OUT)  :: ltarg(:), starg(:), lm_rg(:, :), ichl(:, :)
        REAL(wp), ALLOCATABLE, INTENT(OUT) :: eig(:, :), wmat(:, :, :), LML_block_cf(:, :, :, :), dipsto(:, :, :), rg(:)
        REAL(wp), ALLOCATABLE, INTENT(OUT) :: crlv(:, :)

        CHARACTER(LEN=*), PARAMETER :: path = 'molecular_data'
        REAL(wp), ALLOCATABLE :: tmp(:, :), t_wamp2(:, :, :), t_eig(:), t_wmat(:, :), t_cf(:, :, :)
        INTEGER, ALLOCATABLE  :: t_nconat(:), t_l2p(:), t_m2p(:), t_ichl(:)
        INTEGER(longint) :: readp
        INTEGER  :: iidip_tmp(4*3), ifdip_tmp(4*3), mmdip_tmp(4*3), intr, i, j, m, cnt, s1, s2, err, lu, mblk
        INTEGER  :: block_ind_irr(8,8,3), dipole_coupled_irr(8,8), m_sph_to_cart(-1:+1)
        INTEGER  :: t_lrgl, t_nspn, t_npty, t_nchan, t_mnp1, rbsize
        INTEGER  :: irr1, irr2, n_blocks, coupling, gs_irr, mapping(8)
        INTEGER  :: m_coupling(-1:+1) = (/ DownCoupling, SameCoupling, UpCoupling /)
        LOGICAL  :: symmetry_needed(8), blocks_needed(8, 8), need, changed, field_components(3)

        m_sph_to_cart = (/ 2, 3, 1 /)

        WRITE (*, '(/,10X,"read_molecular_data: start")')
        WRITE (*, '(  10X,"============================",/)')

        OPEN (newunit=lu, FILE=disk_path//path, STATUS='old', FORM='unformatted', ACCESS='stream')

        ! bytesize of the current real type (needed for jumping in the file)
        CALL MPI_Sizeof(rmatr, rbsize, err)
        CALL assert(err == 0, 'Failed to obtain size of REAL from MPI_Sizeof.')

        !
        ! 1. Find out which dipole blocks are available in the molecular data file. Do some basic sanity checking.
        !    Construct the dipole coupling table given the non-zero components of the electric field in this calculation.
        !

        iidip_tmp = -1
        ifdip_tmp = -1
        mmdip_tmp = -1
        dipole_coupled_irr = nocoupling
        field_components = get_field_components()   ! non-zero field components
        nstmx = 0
        n_blocks = 0
        readp = 1

        DO m = -1, 1
            coupling = NoCoupling
            SELECT CASE (m)
            CASE (1);  IF (field_components(1)) coupling = UpCoupling
            CASE (0);  IF (field_components(3)) coupling = SameCoupling
            CASE (-1); IF (field_components(2)) coupling = DownCoupling
            END SELECT

            READ (lu, POS = readp) intr, s1, s2
            READ (lu) iidip_tmp(n_blocks + 1:n_blocks + intr)
            READ (lu) ifdip_tmp(n_blocks + 1:n_blocks + intr)
            INQUIRE (lu, POS = readp)
            readp = readp + rbsize * intr * INT(s1, longint) * s2    ! skip the dipole matrices

            WRITE (*, '(/,5X,"Dipole blocks for component m = ",i2)') m
            mmdip_tmp(n_blocks + 1:n_blocks + intr) = m
            nstmx = MAX(nstmx, s1, s2)
            DO i = n_blocks + 1, n_blocks + intr
                IF (iidip_tmp(i) <= 0 .OR. ifdip_tmp(i) <= 0) THEN
                    WRITE (*, '("Incorrect or missing data for a required dipole block: ",2i10)') i, intr
                    CALL assert(.false., 'Error: wrong or missing dipole data on the file molecular_data')
                END IF
                dipole_coupled_irr(iidip_tmp(i), ifdip_tmp(i)) = IOR(dipole_coupled_irr(iidip_tmp(i), ifdip_tmp(i)), coupling)
                dipole_coupled_irr(ifdip_tmp(i), iidip_tmp(i)) = IOR(dipole_coupled_irr(ifdip_tmp(i), iidip_tmp(i)), coupling)
                WRITE (*, '(5X,"[",i1,",",i1,"]")') iidip_tmp(i), ifdip_tmp(i)
            END DO
            n_blocks = n_blocks + intr
        END DO

        REWIND (lu)

        !
        ! 2. Starting with the ground state symmetry, iteratively include all other symmetries needed in the calculation
        !    given the earlier obtained table of non-zero couplings. Define the mapping between the available and the needed
        !    irreducible representations.
        !

        gs_irr = gs_finast
        symmetry_needed = .false.
        symmetry_needed(gs_irr) = .true.
        need = .false.
        changed = .true.
        DO WHILE (changed)
            changed = .false.
            DO i = 1, 8
                IF (symmetry_needed(i)) THEN
                    DO j = 1, 8
                        IF (dipole_coupled_irr(j, i) /= NoCoupling) THEN
                            need = .true.
                            IF (.NOT. symmetry_needed(j)) THEN
                                symmetry_needed(j) = .true.
                                changed = .true.
                            END IF
                        END IF
                    END DO
                END IF
            END DO
        END DO

        CALL assert (need, 'read_molecular_data: The initial state symmetry is not coupled by the field to any symmetry&
                          & - not even to itself!')

        WRITE (*, '(/,5X,"Wavefunction symmetries to be included:")')
        mapping = 0
        cnt = 0
        DO i = 1, 8
            IF (symmetry_needed(i)) THEN
                cnt = cnt + 1
                mapping(i) = cnt
                WRITE (*, '(5X,"IRR ",i1," with sequence number: ",i1)') i, cnt
            END IF
        END DO

        gs_finast = mapping(gs_irr)
        WRITE (*, '(/,5X,"Index of the symmetry where the ground state is: ",i1,/)') gs_finast

        no_of_L_blocks = count(symmetry_needed)
        no_of_LML_blocks = no_of_L_blocks ! no well defined magnetic sub-levels in molecular case

        !
        ! 5. Set up indexing for the final dipole matrix storage.
        !

        fintr = 0 !Total number of unique dipole blocks
        blocks_needed = .false.
        block_ind_irr = 0
        DO i = 1, n_blocks
            irr1 = iidip_tmp(i)
            irr2 = ifdip_tmp(i)
            mblk = mmdip_tmp(i)
            IF (.NOT. symmetry_needed(irr1) .OR. .NOT. symmetry_needed(irr2)) CYCLE
            IF (IAND(dipole_coupled_irr(irr1, irr2), m_coupling(mblk)) /= 0) THEN
                blocks_needed(irr1, irr2) = .true.
                fintr = fintr + 1
                block_ind_irr(irr1, irr2, m_sph_to_cart(mblk)) = fintr
                WRITE (*, '(5X,"A new dipole block [",I1,",",I1,"] (m = ",SP,I0,SS,") will be kept with index: ",I2)') &
                        irr1, irr2, mblk, fintr
            END IF
        END DO

        ! The array dipole_coupled will include the structure of the Hamiltonian matrix with the order of the wavefunction
        ! symmetries given by their sequence numbers (not IRR numbers).
        ALLOCATE (dipole_coupled(no_of_LML_blocks, no_of_LML_blocks), block_ind(no_of_LML_blocks, no_of_LML_blocks, 3), stat=err)
        CALL assert(err == 0, 'read_molecular_data: Memory allocation 0 failed.')

        dipole_coupled = nocoupling
        block_ind = 0
        DO i = 1, n_blocks
            irr1 = iidip_tmp(i)
            irr2 = ifdip_tmp(i)
            IF (blocks_needed(irr1, irr2)) THEN
                dipole_coupled(mapping(irr1), mapping(irr2)) = dipole_coupled_irr(irr1, irr2)
                dipole_coupled(mapping(irr2), mapping(irr1)) = dipole_coupled_irr(irr2, irr1)
                block_ind(mapping(irr1), mapping(irr2), :) = block_ind_irr(irr1, irr2, :)
            END IF
        END DO

        CALL print_dipole_coupling

        !
        ! 6. Read the actually needed subset of all dipole blocks and store them in memory in the final dipole blocks storage
        !    and also process the rest of the file.
        !

        ALLOCATE (dipsto(nstmx, nstmx, fintr), iidip(fintr), ifdip(fintr), stat=err)
        CALL assert(err == 0, 'read_molecular_data: Memory allocation 2 failed.')
        dipsto = 0.0_wp

        WRITE (*, '(/,5X,"Reading the required dipole blocks...")')
        nstmx = 0
        readp = 1
        DO m = -1, 1 !-1: y, 0: z, 1: x
            READ (lu, POS = readp) intr, s1, s2
            READ (lu) iidip_tmp(1:intr)
            READ (lu) ifdip_tmp(1:intr)
            INQUIRE (lu, POS = readp)
            WRITE (*, '(/,5x,"Component m = ",i0)') m
            WRITE (*, '(5x," - datafile contains ",i0," dipole transitions blocks")') intr
            WRITE (*, '(5x," - every such block is a ",i0,"x",i0," state-to-state matrix",/)') s1, s2
            DO i = 1, intr
                irr1 = iidip_tmp(i)
                irr2 = ifdip_tmp(i)
                IF (blocks_needed(irr1, irr2) .AND. IAND(dipole_coupled_irr(irr1, irr2), m_coupling(m)) /= 0) THEN
                    j = block_ind_irr(irr1, irr2, m_sph_to_cart(m))
                    WRITE (*, '(5X,"Dipole block ",i2,": [",i1,",",i1,"]")') j, irr1, irr2
                    READ (lu, POS = readp) dipsto(1:s1, 1:s2, j)
                    INQUIRE (lu, POS = readp)

                    ! minus sign is for electron charge (file contains only <r>, not <er>)
                    dipsto(1:s1, 1:s2, j) = -dipsto(1:s1, 1:s2, j)

                    !We stop using the IRRs of the dipole blocks and resort to the relative indices of the symmetries:
                    iidip(j) = mapping(irr1)
                    ifdip(j) = mapping(irr2)
                ELSE
                    ! skip this dipole matrix
                    WRITE (*, '(5X,"Dipole block   : [",i1,",",i1,"] (not used)")') irr1, irr2
                    readp = readp + rbsize * INT(s1, longint) * s2
                END IF
            END DO
        END DO !m
        WRITE (*, '(/,5X,"...done")')

        READ (lu, POS = readp) ntarg
        ALLOCATE (crlv(3*ntarg, ntarg), tmp(ntarg, ntarg), stat=err)
        CALL assert(err == 0, 'read_molecular_data: Memory allocation 5 failed.')

        ! read the target dipole elements
        ! minus sign is for electron charge (file contains only <r>, nor <er>)
        crlv = 0.0_wp
        READ (lu) tmp; FORALL (i=1:ntarg, j=1:ntarg) crlv(3*(i - 1) + 2, j) = -tmp(i, j) ! m = -1, down, y
        READ (lu) tmp; FORALL (i=1:ntarg, j=1:ntarg) crlv(3*(i - 1) + 3, j) = -tmp(i, j) ! m =  0, same, z
        READ (lu) tmp; FORALL (i=1:ntarg, j=1:ntarg) crlv(3*(i - 1) + 1, j) = -tmp(i, j) ! m = +1, up,   x

        ! Read the real Gaunt coupling coefficients (l1,m1,l2,m3,1,m).
        READ (lu) n_rg
        ALLOCATE (rg(n_rg), lm_rg(6, n_rg), stat=err)
        CALL assert(err == 0, 'read_molecular_data: Memory allocation 6 failed.')
        READ (lu) rg, lm_rg

        WRITE (*, '(/,5X,"Number of coupling coefficients: ",i10)') n_rg

        ALLOCATE (etarg(ntarg), ltarg(ntarg), starg(ntarg), stat=err)
        CALL assert(err == 0, 'read_molecular_data: Memory allocation 7 failed.')

        READ (lu) nelc, nz, lrang2, lamax, ntarg, inast, nchmx, nstmx, lmaxp1
        READ (lu) rmatr, bbloch
        READ (lu) etarg, ltarg, starg

        READ (lu) nfdm, delta_r

        ALLOCATE (LML_block_nconat(ntarg, no_of_L_blocks), LML_block_l2p(nchmx, &
        no_of_L_blocks), m2p(nchmx, no_of_L_blocks), &
                  eig(nstmx, no_of_L_blocks), wmat(nchmx, nstmx, no_of_L_blocks),&
                  LML_block_cf(nchmx, nchmx, lamax, no_of_L_blocks), &
                  ichl(nchmx, no_of_L_blocks), LML_block_lrgl(no_of_L_blocks), &
                  LML_block_nspn(no_of_L_blocks), LML_block_npty(no_of_L_blocks), &
                  LML_block_nchan(no_of_L_blocks), LML_block_Lblk(no_of_LML_blocks), mnp1(no_of_L_blocks),&
                  lstartm1(no_of_L_blocks, 2), r_points(nfdm + 1), &
                  wamp2(nchmx, nstmx, nfdm, no_of_L_blocks), stat=err)
        CALL assert(err == 0, 'read_molecular_data: Memory allocation 8 failed.')

        ALLOCATE (LML_block_ml(no_of_L_blocks), stat=err)
        CALL assert(err == 0, 'read_molecular_data: Memory allocation 8b failed.')

        ALLOCATE (t_nconat(ntarg), t_l2p(nchmx), t_m2p(nchmx), t_eig(nstmx), t_wmat(nchmx, nstmx), t_cf(nchmx, nchmx, lamax), &
                  t_ichl(nchmx), t_wamp2(nchmx, nstmx, nfdm), stat=err)
        CALL assert(err == 0, 'read_molecular_data: Memory allocation 8 failed.')

        READ (lu) r_points(1:nfdm + 1)

        LML_block_tot_nchan = 0
        lstartm1 = 0
        wamp2 = 0.0_wp
        DO i = 1, inast

            READ (lu) t_lrgl, t_nspn, t_npty, t_nchan, t_mnp1, t_nconat(1:ntarg), t_l2p(1:nchmx), t_m2p(1:nchmx)
            READ (lu) t_eig(1:nstmx), t_wmat(1:nchmx, 1:nstmx), t_cf(1:nchmx, 1:nchmx, 1:lamax)
            READ (lu) t_ichl(1:nchmx)

            READ (lu) s1, s2
            READ (lu) t_wamp2(1:s1, 1:s2, 1:nfdm)

            IF (symmetry_needed(t_lrgl)) THEN
                j = mapping(t_lrgl) !relative index of the present symmetry
                WRITE (*, '(/,5X,"Saving data for IRR: ",i1," under sequence number: ",i1)') t_lrgl, j
                WRITE (*, '(  5X,"===============================================")')
                LML_block_Lblk(j) = j
                LML_block_lrgl(j) = t_lrgl
                LML_block_ml(j) = 0        ! not used in molecular calculations
                LML_block_nspn(j) = t_nspn
                LML_block_npty(j) = t_npty
                LML_block_nchan(j) = t_nchan
                mnp1(j) = t_mnp1
                LML_block_nconat(1:ntarg, j) = t_nconat(1:ntarg)
                LML_block_l2p(1:nchmx, j) = t_l2p(1:nchmx)
                m2p(1:nchmx, j) = t_m2p(1:nchmx)
                eig(1:nstmx, j) = t_eig(1:nstmx)
                wmat(1:nchmx, 1:nstmx, j) = t_wmat(1:nchmx, 1:nstmx)
                LML_block_cf(1:nchmx, 1:nchmx, 1:lamax, j) = t_cf(1:nchmx, 1:nchmx, 1:lamax)
                ichl(1:nchmx, j) = t_ichl(1:nchmx)
                wamp2(1:s1, 1:s2, 1:nfdm, j) = t_wamp2(1:s1, 1:s2, 1:nfdm)

                lstartm1(j, LML_block_npty(j) + 1) = LML_block_tot_nchan ! WARNING: this indexing is different than in the atomic
                                                                 ! case where we had total L instead of j.
                LML_block_tot_nchan = LML_block_tot_nchan + LML_block_nchan(j) ! to get total number channels

                WRITE (*, '(5X,"Spin: ",i1)') LML_block_nspn(j)
                WRITE (*, '(5X,"Parity: ",i1)') LML_block_npty(j)
                WRITE (*, '(5X,"Number of channels: ",i5)') LML_block_nchan(j)
                WRITE (*, '(5X,"Number of N+1 states: ",i15)') mnp1(j)
                WRITE (*, '(/,5X,"Number of continuum channels per target state: ")')
                DO m = 1, ntarg
                    WRITE (*, '(5X,i5,1X,i10)') m, LML_block_nconat(m, j)
                END DO

                IF (debug) THEN
                    WRITE (*, '(/,5X,"Index of the target state and the corresponding l,m quantum numbers for each channel: ")')
                    DO m = 1, LML_block_nchan(j)
                        WRITE (*, '(5X,i5,1X,i3,1X,i4)') ichl(m, j), LML_block_l2p(m, j), m2p(m, j)
                    END DO
                    WRITE (*, '(5X,"...done")')
                END IF
            END IF
        END DO !i

        lmaxp1 = no_of_L_blocks ! This is a modification wrt atomic case where lmaxp1 was the largest total L augmented
                                ! by 1. In the molecular case this must be the number of symmetries included.

        IF (adjust_gs_energy) THEN
            WRITE (*, '(/,5X,"Adjusting energy of the initial state in IRR: ",i1)') gs_irr
            j = mapping(gs_irr)
            WRITE (*, '(5X,"Original value: ",e25.15)') eig(1, j)
            eig(1, j) = gs_energy_desired + etarg(1)
            WRITE (*, '(5X,"Adjusted value: ",e25.15)') eig(1, j)
        END IF

        CLOSE (lu)

        ! Deprecated arrays - will be removed after renaming of "_new" arrays
        ! In atomic case both L_block_* and LML_block_* arrays are used, we
        ! might need this duplication to make the molecular code work.
        ALLOCATE (L_block_nchan(no_of_L_blocks)); L_block_nchan = LML_block_nchan
        ALLOCATE (L_block_lrgl(no_of_L_blocks)); L_block_lrgl = LML_block_lrgl
        ALLOCATE (L_block_nspn(no_of_L_blocks)); L_block_nspn = LML_block_nspn
        ALLOCATE (L_block_npty(no_of_L_blocks)); L_block_npty = LML_block_npty
        ALLOCATE (L_block_nconat(ntarg, no_of_L_blocks)); L_block_nconat = LML_block_nconat
        ALLOCATE (L_block_cf(nchmx, nchmx, lamax, no_of_L_blocks)); L_block_cf = LML_block_cf
        ALLOCATE (L_block_l2p(nchmx, no_of_L_blocks)); L_block_l2p = LML_block_l2p

        CALL set_reduced_input_parameters(lamax)

        WRITE (*, '(/,10X,"read_molecular_data: finished")')
        WRITE (*, '(  10X,"=============================",/)')

    END SUBROUTINE read_molecular_data

!---------------------------------------------------------------------------

    SUBROUTINE remove_states

        IMPLICIT NONE

        INTEGER :: i, i1, j, err
        INTEGER :: neigsrem_i

        OPEN (UNIT=7,FILE='molecular_structure',STATUS='REPLACE')

        ALLOCATE (states_per_L_block(-1:no_of_L_blocks + 2), &
                  states_per_LML_block(-1:no_of_L_blocks + 2), &
                  L_block_post(0:inast), LML_block_post(0:inast), neigsrem(no_of_L_blocks), &
                  stat=err)
        CALL assert(err .EQ. 0, 'Allocation error Remove_States')

        L_block_post = 0
        states_per_L_block = 0

        DO i = 1, no_of_L_blocks
            ! Initialize neigsrem - number of eigenvalues to remove in the inner region for each symmetry
            neigsrem(i) = 0

            ! Using wmat determine how many eigenstates to remove from this symmetry
            ! Need to keep any states with large contributions to surface amplitudes

            IF (remove_eigvals_threshold) THEN
                CALL calculate_neigsrem_i(neigsrem_i, i)
                neigsrem(i) = neigsrem_i
            ELSE
                IF (i == 1) THEN
                    neigsrem(i) = neigsrem_1st_sym
                ELSE
                    neigsrem(i) = neigsrem_higher_syms
                END IF
            END IF
            WRITE (7, *)
            WRITE (7, *) "      L       S      PI  #chans #states"
            WRITE (7, "(5I8)") LML_block_lrgl(i), LML_block_nspn(i), LML_block_npty(i), LML_block_nchan(i), mnp1(i)
            WRITE (7, *) '# of connected chans per target state'
            WRITE (7, *) (L_block_nconat(j, i), j=1, ntarg)
            WRITE (7, *) 'neigrem for sym', i, '=', neigsrem(i)

            ! Set arrays that use neigsrem
            states_per_L_block(i) = mnp1(i) - neigsrem(i)
            ! number states retained in each LSP sym block
            L_block_post(i) = L_block_post(i - 1) + mnp1(i) - neigsrem(i)
            !removing neigsrem highest eigenvalues from each sym block

            WRITE (7, *) 'POSTI', L_block_post(i)
            WRITE (7, *) (eig(j, i) - etarg(1), j=1, mnp1(i) - neigsrem(i))
            WRITE (7, *) 'EXCLUDED'
            WRITE (7, *) (eig(j, i) - etarg(1), j=mnp1(i) - neigsrem(i) + 1, mnp1(i))

            IF (print_wmat) THEN
                DO i1 = 1, LML_block_nchan(i)
                    DO j = 1, mnp1(i)
                        WRITE (81, *) i, i1, j, wmat(i1, j, i)
                    END DO
                END DO
            END IF
        END DO !i

        CLOSE(7)

        ! Determine max_L_block_size - dimension of largest LSP block
        CALL print_var_to_screen('States_Per_L_Block',1, no_of_L_blocks, States_Per_L_Block(1:))

        max_L_block_size = maxval(states_per_L_block)
        PRINT *, 'Max_L_Block_Size = ', max_L_block_size

        states_per_LML_block = states_per_L_block

        LML_block_post = L_block_post

    END SUBROUTINE remove_states

!---------------------------------------------------------------------------

    SUBROUTINE inner_master_reads_spline_files

        CALL read_spline_files_data

    END SUBROUTINE

!---------------------------------------------------------------------------

    SUBROUTINE distribute_ham_data(i_am_inner_master, &
                                   i_am_in_inner_region, &
                                   r_at_region_bndry, &
                                   number_channels, &
                                   Z_minus_N)

        LOGICAL, INTENT(IN)    :: i_am_inner_master
        LOGICAL, INTENT(IN)    :: i_am_in_inner_region
        REAL(wp), INTENT(OUT)  :: r_at_region_bndry
        INTEGER, INTENT(OUT)   :: number_channels
        REAL(wp), INTENT(OUT)  :: Z_minus_N

        ! First processor sends H and D information to other processors
        ! DEPENDS ON PARALLELISM SCHEME USED
        CALL bcast_ham_data(i_am_inner_master)

        IF (i_am_in_inner_region) THEN

            CALL bcast_states_per_L_block(i_am_inner_master)
            ! DDAC ADDED BELOW BCAST
            CALL bcast_states_per_LML_block(i_am_inner_master)

        END IF

        ! set r_at_bndry = rmatr which has been read and set in Read_H_Parameters
        ! This is the value of r at the inter-region boundary
        r_at_region_bndry = rmatr

        ! set Z_minus_N = nz - nelc which have been read in Read_H_Parameters
        Z_minus_N = REAL(nz - nelc, wp)

        ! set the number of channels for passing to the outer region:
        number_channels = LML_block_tot_nchan

!       number_channels2 = L_block_tot_nchan
        ! Check
!       CALL assert(number_channels.EQ.number_channels2,'Error in number_channels, Distribute_Ham_Data')


    END SUBROUTINE distribute_ham_data

!---------------------------------------------------------------------------

    !> READ THE H FILE AND DEFINE THE RANGE OF THE VARIABLES
    SUBROUTINE read_H_parameters()
        USE wall_clock, ONLY: hel_time
        USE initial_conditions, ONLY: double_ionization, reduced_L_max

        INTEGER :: err
        INTEGER :: L_max, last_lrgl, state_id_last
        REAL(wp):: tic, toc, timing

        tic = hel_time()

        CALL read_h_parameters2(path = disk_path//"H", &
                                reduced_L_max = reduced_L_max, &
                                coupling_id = coupling_id, &
                                lplusp = lplusp, &
                                set_ML_max = set_ML_max, &
                                xy_plane_desired = xy_plane_desired(1), &
                                nelc = nelc, &
                                nz = nz, &
                                lrang2 = lrang2, &
                                lamax = lamax, &
                                ntarg = ntarg, &
                                rmatr = rmatr, &
                                bbloch = bbloch, &
                                etarg = etarg, &
                                ltarg = ltarg, &
                                starg = starg, &
                                nchmx = nchmx, &
                                nstmx = nstmx, &
                                ML_max = ML_max, &
                                lmaxp1 = lmaxp1, &
                                no_of_L_blocks = no_of_L_blocks, &
                                no_of_LML_blocks = no_of_LML_blocks, &
                                kept = kept, &
                                nstk = nstk, &
                                nchn = nchn, &
                                L_max = L_max, &
                                last_lrgl = last_lrgl, &
                                inast = inast)

        CALL log_H_parameters(nchmx, &
                              lplusp, &
                              L_max, &
                              ML_max, &
                              no_of_L_blocks, &
                              no_of_LML_blocks, &
                              coupling_id)

        IF (double_ionization) THEN
            !He input:
            ntarg_dication = 1
            ALLOCATE (ltarg_dication(ntarg_dication), starg_dication(ntarg_dication), stat=err)
            ltarg_dication = 0; starg_dication = 0

            !   Ne input:
            !ntarg_dication = 3
            !ALLOCATE (ltarg_dication(ntarg_dication), starg_dication(ntarg_dication), stat=err)
            !ltarg_dication = (/1, 2, 0/); starg_dication = (/1, 0, 0/)

            CALL setup_region_three_channels(.TRUE., ntarg_dication, ltarg_dication, starg_dication, last_lrgl, state_id_last)
            WRITE (*, *) "Number of two-electron channels : ", state_id_last
            CALL setup_region_three_channels(.FALSE., ntarg_dication, ltarg_dication, starg_dication, last_lrgl, state_id_last)
            two_electron_nchan = state_id_last
        END IF

        toc = hel_time()
        timing = toc - tic
        PRINT *, 'Finished Read_H_Parameters: (secs)', timing
        PRINT *, '==========================================='
        PRINT *, ""
    END SUBROUTINE read_H_parameters

!---------------------------------------------------------------------------

    !> Read in H-file
    SUBROUTINE read_H_file(adjust_gs_energy, gs_energy_desired)

        USE initial_conditions, ONLY: reduced_lamax, reduced_L_max, surf_amp_threshold_value
        USE wall_clock, ONLY: hel_time
        LOGICAL, INTENT(IN) :: adjust_gs_energy
        REAL(wp), INTENT(IN) :: gs_energy_desired
        REAL(wp) :: tic, toc, timing

        tic = hel_time()

        CALL read_H_file2(adjust_gs_energy = adjust_gs_energy, &
                          gs_finast = gs_finast, &
                          gs_energy_desired = gs_energy_desired, &
                          remove_eigvals_threshold = remove_eigvals_threshold, &
                          no_of_L_blocks = no_of_L_blocks, &
                          no_of_LML_blocks = no_of_LML_blocks, &
                          nchmx = nchmx, &
                          nstmx = nstmx, &
                          lplusp = lplusp, &
                          set_ML_max = set_ML_max, &
                          ML_max = ML_max, &
                          reduced_L_max = reduced_L_max, &
                          reduced_lamax = reduced_lamax, &
                          coupling_id = coupling_id, &
                          xy_plane_desired = xy_plane_desired(1), &
                          neigsrem_1st_sym = neigsrem_1st_sym, &
                          neigsrem_higher_syms = neigsrem_higher_syms, &
                          surf_amp_threshold_value = surf_amp_threshold_value, &
                          lamax = lamax, &
                          L_block_tot_nchan = L_block_tot_nchan, &
                          L_block_lrgl = L_block_lrgl, &
                          L_block_nspn = L_block_nspn, &
                          L_block_npty = L_block_npty, &
                          L_block_nchan = L_block_nchan, &
                          mnp1 = mnp1, &
                          neigsrem = neigsrem, &
                          L_block_post = L_block_post, &
                          states_per_L_block = states_per_L_block, &
                          max_L_block_size = max_L_block_size, &
                          L_block_nconat = L_block_nconat, &
                          L_block_l2p = L_block_l2p, &
                          lrgt = lrgt, &
                          eig = eig, &
                          wmat = wmat, &
                          L_block_cf = L_block_cf, &
                          LML_block_lrgl = LML_block_lrgl, &
                          LML_block_nspn = LML_block_nspn, &
                          LML_block_npty = LML_block_npty, &
                          LML_block_nchan = LML_block_nchan, &
                          LML_block_tot_nchan = LML_block_tot_nchan, &
                          LML_block_ml = LML_block_ml, &
                          LML_block_Lblk = LML_block_Lblk, &
                          LML_block_post = LML_block_post, &
                          states_per_LML_block = states_per_LML_block, &
                          LML_block_nconat = LML_block_nconat, &
                          LML_block_l2p = LML_block_l2p, &
                          m2p = m2p, &
                          lstartm1 = lstartm1, &
                          LML_block_cf = LML_block_cf)

        CALL log_atomic_structure(etarg(1), &
                                  L_block_lrgl, &
                                  L_block_nspn, &
                                  L_block_npty, &
                                  L_block_nchan, &
                                  mnp1, &
                                  neigsrem, &
                                  L_block_post, &
                                  L_block_nconat, &
                                  eig)

        IF (print_wmat) CALL log_wmat(disk_path//"wmatout", L_block_nchan, mnp1, wmat)

        CALL log_total_channels(L_block_tot_nchan, LML_block_tot_nchan)

        IF (debug) CALL log_debug_info(states_per_L_block, &
                                       L_block_lrgl, &
                                       L_block_npty, &
                                       states_per_LML_block, &
                                       LML_block_lrgl, &
                                       LML_block_nspn, &
                                       LML_block_npty, &
                                       LML_block_ml, &
                                       LML_block_nchan, &
                                       LML_block_post, &
                                       lstartm1)

        toc = hel_time()
        timing = toc - tic
        PRINT *, 'Finished Read_H_File: (secs)', timing
        PRINT *, '==========================================='
        PRINT *, ""

    END SUBROUTINE read_H_file

!---------------------------------------------------------------------------

    SUBROUTINE set_reduced_input_parameters(lamax)

      USE initial_conditions, ONLY: reduced_lamax

      INTEGER, INTENT(INOUT) :: lamax
      LOGICAL                :: keep_H_file_lamax

      CALL assert(reduced_lamax >= -1, 'Reset reduced_lamax to a positive value')
      keep_H_file_lamax = (reduced_lamax == -1) .OR. (reduced_lamax > lamax)
      lamax = MERGE(lamax, reduced_lamax, keep_H_file_lamax)

    END SUBROUTINE set_reduced_input_parameters



!---------------------------------------------------------------------------

    SUBROUTINE print_var_to_screen(var_name,i,j,var)
        INTEGER, INTENT(IN)           :: i, j
        CHARACTER(LEN=*), INTENT(IN) :: var_name
        INTEGER, INTENT(IN)           :: var(i:j)
        INTEGER                       :: ii

        PRINT *, var_name
        DO ii = i, j
            PRINT *, ii, var(ii)
        END DO

    END SUBROUTINE print_var_to_screen

!---------------------------------------------------------------------------

    SUBROUTINE calculate_neigsrem_i(neigsrem_i, ist_value)

        USE initial_conditions, ONLY: surf_amp_threshold_value

        INTEGER, INTENT(OUT) :: neigsrem_i
        INTEGER, INTENT(IN)  :: ist_value

        INTEGER :: ii, jj, max_desired_index
        INTEGER :: desired_index(1:L_block_nchan(ist_value))
        LOGICAL :: found_desired_index

        ! Search through surface amplitude array wmat from last state to
        ! first state for this symmetry and find first index corresponding
        ! to a large value in wmat -- do for each channel then select
        ! maximum value

        desired_index = 0

        DO ii = 1, L_block_nchan(ist_value)
            found_desired_index = .false.
            desired_index(ii) = mnp1(ist_value)
            DO jj = mnp1(ist_value), 1, -1
                IF (.NOT. (found_desired_index)) THEN
                    IF (ABS(wmat(ii, jj, ist_value)) .GT. surf_amp_threshold_value) THEN
                        found_desired_index = .true.
                        desired_index(ii) = jj
                    END IF
                END IF
            END DO
        END DO

        max_desired_index = MAXVAL(desired_index)

        neigsrem_i = mnp1(ist_value) - max_desired_index

    END SUBROUTINE calculate_neigsrem_i

!---------------------------------------------------------------------------

    !> READ IN DIPOLE ELEMENTS FROM FILE D from RMatrixI Format
    SUBROUTINE read_D_file_RMatrixI(lplusp)

        INTEGER, INTENT(IN)   :: lplusp
        INTEGER  :: lgsf, lgsi, lgli, lglf, lgpi, lgpf, ni, nf, err, intr
        INTEGER  :: i, noterm, isi, jsi, inch, jnch, icount
        INTEGER  :: blocki,blockf,lgmli,lgmlf
        INTEGER  :: lptermi,lptermf,stride
        REAL(wp) :: cgc
        INTEGER  :: lptesti, lptestf

        REAL(wp), ALLOCATABLE :: temp_dipsto(:, :)
        REAL(wp), ALLOCATABLE :: temp_dipsto_v(:, :)
        REAL(wp), ALLOCATABLE :: wp_read(:, :)
        REAL(wp), ALLOCATABLE :: wd_read(:, :)
        REAL(wp), ALLOCATABLE :: wd_read_v(:, :)

        INTEGER :: N1
        INTEGER :: N2
        INTEGER :: M1
        INTEGER :: M2
        INTEGER :: M
        INTEGER :: N

        INTEGER :: ILrgL
        INTEGER :: IPty
        INTEGER :: ISpn
        INTEGER :: JLrgL

        INTEGER :: nchanBegin
        INTEGER :: nchanEnd
        INTEGER :: mchanBegin
        INTEGER :: mchanEnd

        INTEGER :: ML_Dipole_Coupling_Diff
        LOGICAL :: read_dipsto

        INTEGER :: sym_one
        INTEGER :: sym_two
        INTEGER :: target_one
        INTEGER :: target_two
        INTEGER :: ncounter_one
        INTEGER :: ncounter_two
        INTEGER :: channel_counter_one
        INTEGER :: channel_counter_two
        INTEGER :: channel_insym_counter_one
        INTEGER :: channel_insym_counter_two

        CHARACTER(LEN=3) :: transition_number

        If (coupling_id.eq.LS_coupling_id) THEN
            ML_Dipole_Coupling_Diff=1
        ELSE
            ML_Dipole_Coupling_Diff=2 !because J is doubled in JK coupling in RMT for half integers
        END IF

        !Calculate mat_size here
        !wp corresponds to projectile_lcoupled
        !wd corresponds to target_coupled

        channel_counter_one=0
        channel_counter_two=0
        channel_insym_counter_one=0
        channel_insym_counter_two=0
        mat_size_wd=0
        mat_size_wp=0

        !Loop over sym 1
        !Loop over sym 2
        DO sym_one=1,no_of_LML_blocks
           channel_insym_counter_one=0
        DO target_one=1,ntarg
        DO ncounter_one=1,LML_block_nconat(target_one,sym_one)
            channel_counter_one=channel_counter_one+1
            channel_insym_counter_one=channel_insym_counter_one+1

            channel_counter_two=0
            DO sym_two=1,no_of_LML_blocks
            channel_insym_counter_two=0
            DO target_two=1,ntarg
            DO ncounter_two=1,LML_block_nconat(target_two,sym_two)
                channel_counter_two=channel_counter_two+1
                channel_insym_counter_two=channel_insym_counter_two+1

                IF (abs(LML_block_lrgl(sym_one)-LML_block_lrgl(sym_two)).le.ML_Dipole_Coupling_Diff) THEN
                    IF (LML_block_npty(sym_one).NE.LML_block_npty(sym_two)) THEN
                    IF (abs(LML_block_ml(sym_one)-LML_block_ml(sym_two)).le.ML_Dipole_Coupling_Diff) THEN

                    !Do wp Test
                    IF (abs(LML_block_l2p(channel_insym_counter_one,sym_one)        &
                        -LML_block_l2p(channel_insym_counter_two,sym_two)).le.1) THEN
                        mat_size_wp=max(mat_size_wp,abs(channel_counter_one-channel_counter_two))
                    END IF

                    !Do wd Test
                    mat_size_wd=max(mat_size_wd,abs(channel_counter_one-channel_counter_two))
                    END IF
                    END IF
                    END IF

                !End loop
                !End loop
                END DO
                END DO

            !End loop
            !End loop
            END DO
            END DO

        !End loop
        !End loop
        END DO
        END DO

        IF (debug) THEN
            print *,'mat_size_wp = ',mat_size_wp
            print *,'mat_size_wd = ',mat_size_wd
        END IF

        !Allocate arrays in lrpost
        ! IMPORANT: Check for factors of -1 later

        OPEN (UNIT=21, FILE=disk_path//'D00', STATUS='old', FORM='unformatted')

        ! Allocate target dipoles - these aren't used, but need to be there for bcasts
        ALLOCATE (crlv(ntarg, ntarg), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in crlv')

        ! Note form of d00 is Li - L(i-1) - Not necessarily true for RMatrixI
        READ (21) intr  ! number of block transitions
        
        IF (debug)  PRINT *, 'intr=', intr

        ALLOCATE (iidip(intr), ifdip(intr), dipsto(nstmx, nstmx, intr), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in dip')

        ALLOCATE (dipole_coupled(no_of_LML_blocks,no_of_LML_blocks),stat=err)
        CALL Assert(err.eq.0,'Allocation error in dipole_coupled')
        
        IF (debug) THEN
            write(6,*) 'dipole_coupling allocated with size',No_Of_LML_blocks*no_of_LML_Blocks
        END IF 

        ALLOCATE (block_ind(No_Of_L_Blocks,No_Of_L_Blocks,3),stat=err)
        CALL Assert(err.eq.0,'Allocation error in dipole_coupled')



        ALLOCATE (wp_read_store(L_block_tot_nchan, L_block_tot_nchan), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in wp_read_store')
        ALLOCATE (wd_read_store(L_block_tot_nchan, L_block_tot_nchan), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store')
        ALLOCATE (wd_read_store_v(L_block_tot_nchan, L_block_tot_nchan), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store_v')

        ALLOCATE (LML_wp_read_store(-mat_size_wp:mat_size_wp, LML_block_tot_nchan), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in wp_read_store')
        ALLOCATE (LML_wd_read_store(-mat_size_wd:mat_size_wd, LML_block_tot_nchan), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store')
        ALLOCATE (LML_wd_read_store_v(-mat_size_wd:mat_size_wd, LML_block_tot_nchan), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store_v')

        ALLOCATE (wp_read(maxval(L_block_nchan), maxval(L_block_nchan)), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in wp_read_store')
        ALLOCATE (wd_read(maxval(L_block_nchan), maxval(L_block_nchan)), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store')
        ALLOCATE (wd_read_v(maxval(L_block_nchan), maxval(L_block_nchan)), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in wd_read_store_v')

        wp_read_store = 0.0_wp
        wd_read_store = 0.0_wp
        wd_read_store_v = 0.0_wp
        LML_wp_read_store = 0.0_wp
        LML_wd_read_store = 0.0_wp
        LML_wd_read_store_v = 0.0_wp
        wp_read = 0.0_wp
        wd_read = 0.0_wp
        wd_read_v = 0.0_wp
        crlv = 0.0_wp

        IF (dipole_velocity_output) THEN
            ALLOCATE (dipsto_v(nstmx, nstmx, intr), stat=err)
            CALL assert(err .EQ. 0, 'Allocation error in dip_v')
            dipsto_v = 0.0_wp
        END IF


        icount = 0
        dipole_coupled = NoCoupling
        DO i = 1, intr

            ! Open Dipole file
            WRITE (transition_number, FMT='(I3.3)') i
            IF (debug) THEN
                PRINT *, 'Loading Transition Number: '//transition_number
            END IF

            OPEN (UNIT=22, FILE=disk_path//'D'//transition_number, STATUS='old', FORM='unformatted')

            ! i refers now to the LEFT-HAND SIDE
            ! f to the RIGHT-HAND SIDE

            !Read line from D00
            READ (21, IOSTAT=err) lgsf, lglf, lgpf, lgsi, lgli, lgpi
            IF (debug) WRITE (*, *) 'd00 Line:', lgsf, lglf, lgpf, lgsi, lgli, lgpi
            IF (err < 0) THEN
                PRINT *, 'End of file reached'
                EXIT
            END IF
            CALL assert(err .LE. 0, 'Problems my dear')

            !Check that this dipole transition is needed
            if (coupling_id.eq.LS_coupling_id) then
                lptesti = MOD(lgli + lgpi, 2)
                lptestf = MOD(lglf + lgpf, 2)
            ELSE
                lptesti = MOD(lgli/2 + lgpi, 2)
                lptestf = MOD(lglf/2 + lgpf, 2)
            END IF
            read_dipsto=((ML_Max.eq.0).AND.(lplusp==lptesti).AND.(lplusp==lptestf)).OR.(ML_Max > 0)
            IF ((lgpf <= 1) .AND. (lgpi <= 1)) THEN
                IF (read_dipsto) THEN
                    icount = icount + 1

                    !Find which symettries this transition is between
                    CALL finind_L(lgsf, lglf, lgpf, nf)
                    CALL finind_L(lgsi, lgli, lgpi, ni)
                    if ((ni==-1) .or. (nf==-1)) then
                        cycle
                    end if
                    iidip(icount) = ni
                    ifdip(icount) = nf
                    block_ind(ni,nf,:) = icount
                    block_ind(nf,ni,:) = 0
                    IF (debug) WRITE (*, *) 'Dipoles between symmetry: ', nf, ni

!
! LENGTH GAUGE USED THROUGHOUT
!
                    READ (22) noterm, isi, inch, ILrgL, IPty, ISpn, jsi, jnch, JLrgL !Line 1 of DNN file
                    CALL assert(lgpi .EQ. IPty, "ERROR in RMATRXI dipole reads. Parity doesn't agree")
!           WRITE(6,*) noterm,isi,inch,ILrgL,ISpn,jsi,jnch,JLrgL,IPty
                    IF (debug) PRINT *, 'ParityCheck: ', lgpf, lgpi, IPty

                    ALLOCATE (temp_dipsto(isi, jsi))
                    ALLOCATE (temp_dipsto_v(isi, jsi))
                    temp_dipsto = 0.0_wp
                    temp_dipsto_v = 0.0_wp

                    N2 = 0
                    DO WHILE (N2 .LT. jsi)
                        N1 = N2 + 1
                        N2 = MIN(N2 + noterm, jsi)
                        M2 = 0
                        DO WHILE (M2 .LT. isi)
                            M1 = M2 + 1
                            M2 = MIN(M2 + noterm, isi)
                            IF (dipole_velocity_output) THEN
                                READ (22) ((temp_dipsto(M, N), M=M1, M2), N=N1, N2), ((temp_dipsto_v(M, N), M=M1, M2), N=N1, N2)
                            ELSE
                                READ (22) ((temp_dipsto(M, N), M=M1, M2), N=N1, N2)
                            END IF
                        END DO
                    END DO

                    ! Reorder dipole matrix, (RMatrixI provides states in order from
                    ! highest to lowest eigenvalue. RMT expects these in order from
                    ! lowest to highest. The H file has already been reordered in
                    ! MakeSplineWaves. A corresponding reordering needs to happen here
                    ! for the dipoles
                    DO M = 1, isi
                        DO N = 1, jsi
                            dipsto(M, N, icount) = temp_dipsto(isi + 1 - M, jsi + 1 - N)
                            IF (dipole_velocity_output) THEN
                                dipsto_v(M, N, icount) = temp_dipsto_v(isi + 1 - M, jsi + 1 - N)
                            END IF
                        END DO
                    END DO

                    IF (coupling_id.EQ.LS_coupling_id) THEN
                       IF (JLrgL.EQ.ILrgL) THEN
                          dipsto(:,:,icount) = -1.0_wp*dipsto(:,:,icount)
                       END IF
                    END IF
                    ! deallocate temp dipole matrices
                    DEALLOCATE (temp_dipsto)
                    DEALLOCATE (temp_dipsto_v)

                    ! skip through RMatrixI buttle correction data
                    READ (22)
                    READ (22)
                    READ (22)
                    READ (22)

                    ! Read outer region dipole - we read velocity gauge whether or not
                    ! required. These matrices can be neglected later.
                    wp_read = 0.0_wp
                    wd_read = 0.0_wp
                    wd_read_v = 0.0_wp
                    Read (22) ((wp_read(M, N), N=1, jnch), M=1, inch), &
                        ((wd_read(M, N), N=1, jnch), M=1, inch), &
                        ((wd_read_v(M, N), N=1, jnch), M=1, inch)

                    ! Multiply all reduced dipoles by clebsch-gordan coefficient here
            ! Multiplication by cgc moved to live communications

                    ! Set up for putting individual transition outer region dipole matrices
                    ! together in matrix for entire system
                    CALL assert((L_block_nchan(ni) .EQ. inch), 'error in inchan(ni) and inch')
                    CALL assert((L_block_nchan(nf) .EQ. jnch), 'error in inchan(nf) and jnch')
                    IF (ni .EQ. 1) THEN
                        nchanBegin = 1
                    ELSE
                        nchanBegin = SUM(L_block_nchan(1:(ni - 1))) + 1
                    END IF
                    IF (nf .EQ. 1) THEN
                        mchanBegin = 1
                    ELSE
                        mchanBegin = SUM(L_block_nchan(1:(nf - 1))) + 1
                    END IF
                    nchanEnd = SUM(L_block_nchan(1:ni))
                    mchanEnd = SUM(L_block_nchan(1:nf))

                    CALL assert(((nchanEnd - nchanBegin + 1) .EQ. inch), 'error in nchans and inch')
                    CALL assert(((mchanEnd - mchanBegin + 1) .EQ. jnch), 'error in mchans and jnch')

                    ! Write outer region transition matrix to big outer region matrix
                    wp_read_store(nchanBegin:nchanEnd, mchanBegin:mchanEnd) = (wp_read(1:inch, 1:jnch))
                    wp_read_store(mchanBegin:mchanEnd, nchanBegin:nchanEnd) =  1.0_wp*transpose(wp_read(1:inch, 1:jnch))

                    wd_read_store(nchanBegin:nchanEnd, mchanBegin:mchanEnd) = (wd_read(1:inch, 1:jnch))
                    wd_read_store(mchanBegin:mchanEnd, nchanBegin:nchanEnd) =  1.0_wp*transpose(wd_read(1:inch, 1:jnch))

                    wd_read_store_v(nchanBegin:nchanEnd, mchanBegin:mchanEnd) = (wd_read_v(1:inch, 1:jnch))
                    wd_read_store_v(mchanBegin:mchanEnd, nchanBegin:nchanEnd) =  1.0_wp*transpose(wd_read_v(1:inch, 1:jnch))

                    IF (coupling_id.eq.LS_coupling_id) then
                        IF (xy_plane_desired(1)) THEN
                            stride = 2
                            lptermi = MOD(lgli + lgpi, 2)
                            lptermf = MOD(lglf + lgpf, 2)
                        ELSE
                            stride = 1
                            lptermi = 0
                            lptermf = 0
                        END IF
                    ELSE
                        IF (xy_plane_desired(1)) THEN
                            stride=4
                            IF (mod(ML_Max,2).eq.0) then
                                lptermi=2*MOD(lgli/2 + lgpi, 2)
                                lptermf=2*MOD(lglf/2 + lgpf, 2)
                            ELSE
                                lptermi=2*MOD(lgpi, 2)
                                lptermf=2*MOD(lgpf, 2)
                            END IF
                        ELSE
                            stride=2
                            lptermi=0
                            lptermf=0
                        END IF
                    END IF

                    DO lgmli=-min(lgli,ML_Max)+lptermi,min(lgli,ML_Max)-lptermi,stride
                        DO lgmlf=-min(lglf,ML_Max)+lptermf,min(lglf,ML_Max)-lptermf,stride
                            IF (lgmli==lgmlf.or.abs(lgmli-lgmlf)==ML_Dipole_Coupling_Diff) then
                            CALL finind_LML(lgsi,lgli,lgpi,lgmli,blocki)
                            CALL finind_LML(lgsf,lglf,lgpf,lgmlf,blockf)
                            IF (lglf < lgli) THEN
                               dipole_coupled(blockf,blocki) = DownCoupling
                               dipole_coupled(blocki,blockf) = UpCoupling
                            END IF
                            IF (lglf == lgli) THEN
                               dipole_coupled(blockf,blocki) = SameCoupling
                               dipole_coupled(blocki,blockf) = SameCoupling
                            END IF
                            IF (lglf > lgli) THEN
                               dipole_coupled(blockf,blocki) = UpCoupling
                               dipole_coupled(blocki,blockf) = DownCoupling
                            END IF
                            !JW Additions for Arbitrary polarisation
                            IF (coupling_id.eq.LS_coupling_id) then
                            !    cgc = -cg(lgli, 1, lglf, lgmli, lgmli-lgmlf, lgmlf) &
                                cgc = cg(lgli, 1, lglf, lgmli, lgmlf-lgmli, lgmlf) &
                                    /SQRT(REAL(lglf, wp) + REAL(lglf, wp) + 1._wp)
                            ELSE
                                cgc = dcg(lgli, 2, lglf, lgmli, lgmlf-lgmli, lgmlf) &
                                    /SQRT(REAL(lglf, wp) + 1._wp)
                            END IF
                            !IF (lgli == lglf) cgc = -cgc

!                            if (abs(cgc).gt.0.0000001_wp) then
                            IF (debug) THEN
                                print *,''
                                print *,'----------------------------------------------'
                                print *,lgli, 1, lglf, lgmli, lgmlf-lgmli, lgmlf
                                print *,cgc
                                print *,LML_block_npty(blocki),LML_block_npty(blockf)
                                print *,'----------------------------------------------'
                            END IF
!                            end if
                            ! Set up for putting individual transition outer region dipole matricies
                            ! together in matrix for entire system
                            CALL assert((LML_block_nchan(blocki) .EQ. inch), 'error in inchan(ni) and inch')
                            CALL assert((LML_block_nchan(blockf) .EQ. jnch), 'error in inchan(nf) and jnch')
                            IF (blocki .EQ. 1) THEN
                                nchanBegin = 1
                            ELSE
                                nchanBegin = SUM(LML_block_nchan(1:(blocki - 1))) + 1
                            END IF
                            IF (blockf .EQ. 1) THEN
                                mchanBegin = 1
                            ELSE
                                mchanBegin = SUM(LML_block_nchan(1:(blockf - 1))) + 1
                            END IF
                            nchanEnd = SUM(LML_block_nchan(1:blocki))
                            mchanEnd = SUM(LML_block_nchan(1:blockf))

                            CALL assert(((nchanEnd - nchanBegin + 1) .EQ. inch), 'error in nchans and inch')
                            CALL assert(((mchanEnd - mchanBegin + 1) .EQ. jnch), 'error in mchans and jnch')

                            ! Write outer region transition matrix to big outer region matrix

                            DO channel_counter_one=nchanBegin,nchanEnd
                            DO channel_counter_two=mchanBegin,mchanEnd

                            IF (abs(wp_read(channel_counter_one-nchanBegin+1,       &
                                channel_counter_two-mchanBegin+1)).GT.0.000000000001_wp) then
                               if (abs(channel_counter_one-channel_counter_two).gt.mat_size_wp) THEN
                                  print *,'error in wp size!',channel_counter_one,channel_counter_two,      &
                                  wp_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1)
                               END IF
                                IF (coupling_id.EQ.LS_coupling_id) THEN
                                    LML_wp_read_store(channel_counter_one-channel_counter_two, channel_counter_two) =    &
                                        (-1._wp)**(lgmlf-lgmli+1)*cgc*          &
                                        (wp_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                ELSE
                                    LML_wp_read_store(channel_counter_one-channel_counter_two, channel_counter_two) =    &
                                        (-1._wp)**((lgmlf-lgmli)/2+1)*cgc*      &
                                        (wp_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                END IF

                                !and do the transpose
                                LML_wp_read_store(channel_counter_two-channel_counter_one, channel_counter_one) =               &
                                    cgc*(wp_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))

                            END IF

                            IF (abs(wd_read(channel_counter_one-nchanBegin+1,       &
                                channel_counter_two-mchanBegin+1)).GT.0.000000000001_wp) then

                                IF (coupling_id.EQ.LS_coupling_id) THEN
                                    LML_wd_read_store(channel_counter_one-channel_counter_two, channel_counter_two) =    &
                                        (-1._wp)**(lgmlf-lgmli+1)*cgc*      &
                                        (wd_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                ELSE
                                    LML_wd_read_store(channel_counter_one-channel_counter_two, channel_counter_two) =    &
                                        (-1._wp)**((lgmlf-lgmli)/2+1)*cgc*      &
                                        (wd_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                END IF

                                LML_wd_read_store(channel_counter_two-channel_counter_one, channel_counter_one) =               &
                                    cgc*(wd_read(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))

                            END IF

                            IF (abs(wd_read_v(channel_counter_one-nchanBegin+1,         &
                                channel_counter_two-mchanBegin+1)).GT.0.000000000001_wp) then
                                IF (coupling_id.EQ.LS_coupling_id) THEN
                                    LML_wd_read_store_v(channel_counter_one-channel_counter_two, channel_counter_two) =  &
                                        (-1._wp)**(lgmlf-lgmli+1)*cgc*      &
                                        (wd_read_v(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                ELSE
                                    LML_wd_read_store_v(channel_counter_one-channel_counter_two, channel_counter_two) =  &
                                        (-1._wp)**((lgmlf-lgmli)/2+1)*cgc*      &
                                        (wd_read_v(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                                END IF
                                LML_wd_read_store_v(channel_counter_two-channel_counter_one, channel_counter_one) =             &
                                    cgc*(wd_read_v(channel_counter_one-nchanBegin+1, channel_counter_two-mchanBegin+1))
                            END IF

                            END DO
                            END DO


                            CALL assert(LML_block_npty(blocki).ne.LML_block_npty(blockf),       &
                            'Error in DBlock reading! Transition with no parity change!')

                         END IF
                      END DO
                   END DO
                END IF
            END IF
            ! Close DNN file for individual transition
            CLOSE (22)
        END DO
        ! Close D00 file
        CLOSE (21)
        fintr = icount


        ! We can now deallocate outer region matricies used for individual transitions
        DEALLOCATE (wp_read, stat=err)
        CALL assert(err .EQ. 0, 'deAllocation error in wp_read_store')
        DEALLOCATE (wd_read, stat=err)
        CALL assert(err .EQ. 0, 'deAllocation error in wd_read_store')
        DEALLOCATE (wd_read_v, stat=err)
        CALL assert(err .EQ. 0, 'deAllocation error in wd_read_store_v')

    END SUBROUTINE

!---------------------------------------------------------------------------

    !> READ IN DIPOLE ELEMENTS FROM FILE D from RMatrixII Format
    SUBROUTINE read_D_file()

        USE wall_clock, ONLY: hel_time

        INTEGER  :: lgsf, lgsi, lgli, lglf, lgpi, lgpf, ni, nf, err, intr
        INTEGER  :: blocki, blockf, lgmli, lgmlf
        INTEGER  :: nev, nod, i, noterm, isi, jsi, inch, jnch, icount
        REAL(wp) :: cgc, bbb
        INTEGER  :: lptesti, lptestf, j
        INTEGER  :: lptermi, lptermf, stride
        REAL(wp) :: tic, toc , timing

        tic = hel_time()
        CALL assert(coupling_id.eq.LS_coupling_id,'jK Coupling cant be used with RMatrixII input data')

        OPEN (UNIT=21, FILE=disk_path//'d00', STATUS='old', FORM='unformatted')
        OPEN (UNIT=22, FILE=disk_path//'d', STATUS='old', FORM='unformatted')


        ! Read target dipoles
        ALLOCATE (crlv(ntarg, ntarg), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in targ moms')
        IF (dipole_velocity_output) THEN
            ALLOCATE (crlv_v(ntarg, ntarg), stat=err)
            CALL assert(err .EQ. 0, 'Allocation error in targ moms')
        END IF

        READ (22) crlv(1:ntarg, 1:ntarg)  !length gauge reduced dipoles
        IF (dipole_velocity_output) THEN
            READ (22) crlv_v(1:ntarg, 1:ntarg) ! velocity dipoles
        ELSE
            READ (22)
        END IF

        IF (debug) THEN
            WRITE(*, *) 'length reduced targ moments'
            DO i = 1, ntarg
                DO j = 1, ntarg
                    WRITE (*, *) i, j, crlv(i, j)
                END DO
            END DO
        END IF

        ! Note form of d00 is Li - L(i-1)
        READ (21) intr  ! number of block transitions

        ! THESE STORAGE REQUIREMENTS CAN BE REDUCED BY READING D00, THEN D
        ALLOCATE (iidip(intr), ifdip(intr), dipsto(nstmx, nstmx, intr), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in dip')

        IF (dipole_velocity_output) THEN
            ALLOCATE (dipsto_v(nstmx, nstmx, intr), stat=err)
            CALL assert(err .EQ. 0, 'Allocation error in dip_v')
        END IF

        ALLOCATE (dipole_coupled(no_of_LML_blocks, no_of_LML_blocks), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in dipole_coupled')

        ALLOCATE (block_ind(no_of_L_blocks, no_of_L_blocks, 3), stat=err)
        CALL assert(err .EQ. 0, 'Allocation error in dipole_coupled')

        icount = 0
        dipole_coupled = NoCoupling

        DO i = 1, 3*intr       ! If only S targets
            READ (21, IOSTAT=err) lgsf, lglf, lgpf, lgsi, lgli, lgpi
            IF (debug) THEN
                WRITE (*, *) i
                WRITE (*, *) lgsf, lglf, lgpf, lgsi, lgli, lgpi
            END IF

            ! i refers now to the LEFT-HAND SIDE
            ! f to the RIGHT-HAND SIDE
            IF (err < 0) THEN ! end of file reached
                EXIT
            END IF
            CALL assert(err .LE. 0, 'END OF d FILE REACHED')
            lptesti = MOD(lgli + lgpi, 2)
            lptestf = MOD(lglf + lgpf, 2)
            IF ((lgpf <= 1) .AND. (lgpi <= 1)) THEN
                IF (ML_max .EQ. 0 .AND. (lplusp == lptesti) .AND. (lplusp == lptestf) .OR. ML_max .GT. 0) THEN
                    CALL finind_L(lgsf, lglf, lgpf, nf)
                    CALL finind_L(lgsi, lgli, lgpi, ni)
                    if ((ni==-1) .or. (nf==-1)) then
                        cycle
                    end if
                    icount = icount + 1
                    iidip(icount) = ni
                    ifdip(icount) = nf
                    block_ind(ni, nf, :) = icount
                    block_ind(nf, ni, :) = 0 !icount
                    cgc = 1.0_wp                        ! DDAC: Convert from red. dip. mat. elements to dip. mat. elements later.
                    nev = mnp1(ni)
                    nod = mnp1(nf)
                    IF (debug) WRITE (*, *)  'nrl=', nev, 'ncl=', nod

                    ! Suggestion from envoi - HUGO undo to check effect on norm
                    ! Testing gives slightly better result if kept in...
!                   IF (lgli == lglf) cgc=-cgc
!                   IF (lgli == lglf) cgc=0._wp

                    ! LENGTH GAUGE USED THROUGHOUT
                    READ (22) noterm, isi, inch, jsi, jnch
                    IF (debug) WRITE (*, *) noterm, isi, inch, jsi, jnch
                    READ (22) dipsto(1:nev, 1:nod, icount)
                    IF (dipole_velocity_output) THEN
                        READ (22) dipsto_v(1:nev, 1:nod, icount)
                        IF (debug) WRITE (*, *) 'Check ', dipsto(1, 1, icount)/cgc, dipsto_v(1, 1, icount)
                        dipsto_v(1:nev, 1:nod, icount) = cgc*dipsto_v(1:nev, 1:nod, icount)
                    ELSE
                        READ (22) bbb
                        IF (debug) WRITE (*, *) 'Check ', dipsto(1, 1, icount), bbb
                    END IF
                    dipsto(1:nev, 1:nod, icount) = cgc*dipsto(1:nev, 1:nod, icount)
                ELSE IF (ML_max .EQ. 0 .AND. (lplusp .NE. lptesti) .OR. ML_max .EQ. 0 .AND. (lplusp .NE. lptestf)) THEN

                    ! Values for ml not compatible
                    READ (22)
                    READ (22)
                    READ (22)
                END IF

                !  stride = 2 in xy plane since every other ML value is dipole accessible.
                !  stride = 1 in all other polarization planes since all ML values are accessible.
                !
                !  lptermi,lptermf determine the range of ML values retained for each L.
                !  These depend on both L and Parity, similar to the lpfact term used earlier.
                !   
                stride = MERGE(2,1,xy_plane_desired(1))
                lptermi = MERGE(MOD(lgli+lgpi,2),0,xy_plane_desired(1))
                lptermf = MERGE(MOD(lglf + lgpf,2),0,xy_plane_desired(1))

                IF (ML_max > 0 .OR. ML_max == 0 .AND. (lptesti == lplusp) .AND. (lptestf == lplusp)) THEN
                    DO lgmli = -MIN(lgli, ML_max) + lptermi, MIN(lgli, ML_max) - lptermi, stride
                        DO lgmlf = -MIN(lglf, ML_max) + lptermf, MIN(lglf, ML_max) - lptermf, stride
                            IF (lgmli == lgmlf .OR. ABS(lgmli - lgmlf) == 1) THEN
                                CALL finind_LML(lgsi, lgli, lgpi, lgmli, blocki)
                                CALL finind_LML(lgsf, lglf, lgpf, lgmlf, blockf)
                                IF (blocki == -1) WRITE (*, *) 'vals:', lgsi, lgli, lgpi, lgmli, blocki
                                IF (lglf < lgli) THEN
                                    dipole_coupled(blockf, blocki) = downcoupling
                                    dipole_coupled(blocki, blockf) = upcoupling
                                END IF
                                IF (lglf == lgli) THEN
                                    dipole_coupled(blockf, blocki) = samecoupling
                                    dipole_coupled(blocki, blockf) = samecoupling
                                END IF
                                IF (lglf > lgli) THEN
                                    dipole_coupled(blockf, blocki) = upcoupling
                                    dipole_coupled(blocki, blockf) = downcoupling
                                END IF
                            END IF
                        END DO
                    END DO
                END IF

            END IF
        END DO

        CLOSE (21)
        CLOSE (22)
        fintr = icount

        IF (debug) THEN
            IF (no_of_LML_blocks < 20) CALL print_dipole_coupling

            WRITE (*, *)  'fintr in Read_D_File = ', fintr
            WRITE (*, *)  'intr in Read_D_File = ', intr
        END IF

        toc = hel_time()
        timing = toc - tic
        PRINT *, 'Finished Read_D_File: (secs) ', timing
        PRINT *, '======================================'
        PRINT *,  ""
    END SUBROUTINE read_D_file

!---------------------------------------------------------------------------

    !> Read spline files
    SUBROUTINE read_spline_files_info

        IMPLICIT NONE

        INTEGER :: err

        OPEN (UNIT=34, FILE=disk_path//'Splinedata', FORM='unformatted')
        READ (34) bspl_nc, bspl_k9, bspl_nb, bspl_ndim, bspl_nt
        ALLOCATE (bspl_t(bspl_nt), stat=err)
        CALL assert(err .LE. 0, 'allocation error with bspl_t')
        READ (34) bspl_t
        CLOSE (34)

!       OPEN(UNIT=10, FILE=disk_path//'Splinewaves', FORM='unformatted')

!       ALLOCATE(splwave(nchmx*bspl_ndim,nstmx,no_of_L_blocks),stat=err)
!       IF (err /= 0) THEN
!          PRINT*, 'allocation error with splwave'
!          CALL helium_stop
!       END IF

!       splwave(:,:,:) = 0.0_wp

!       nco = 0
!       PRINT *,'HUGO - NCHN',nchn
!       PRINT *,'HUGO - KEPT',kept
!       PRINT *,'HUGO - NSTK',nstk
!       DO i = 1, inast
!           IF (kept(i) == 1) nco = nco+1
!           DO j = 1,nstk(i)
!               IF (kept(i) == 1) THEN
!                   PRINT *,'Check reading Splinewaves ',nco,j,bspl_ndim,nchn(nco)
!                   READ(10) splwave(1:nchn(nco)*bspl_ndim,j,nco)
!               ELSE
!                   READ(10)
!               END IF
!           END DO
!       END DO

!       CLOSE(10)

    END SUBROUTINE read_spline_files_info

!---------------------------------------------------------------------------

    SUBROUTINE read_spline_files_data

        IMPLICIT NONE

        INTEGER :: i, j, nco, err

        OPEN (UNIT=34, FILE=disk_path//'Splinedata', FORM='unformatted')
        READ (34) bspl_nc, bspl_k9, bspl_nb, bspl_ndim, bspl_nt
!       ALLOCATE(bspl_t(bspl_nt),stat = err)
!       IF (err /= 0) THEN
!           PRINT*, 'allocation error with bspl_t'
!           CALL helium_stop
!       END IF
!       READ(34) bspl_t
        CLOSE (34)

        OPEN (UNIT=10, FILE=disk_path//'Splinewaves', FORM='unformatted')

        ALLOCATE (splwave(nchmx*bspl_ndim, nstmx, no_of_L_blocks), stat=err)
        CALL assert(err .LE. 0, 'allocation error with splwave')

        splwave(:, :, :) = 0.0_wp

        nco = 0
!       PRINT *,'HUGO - NCHN',nchn
!       PRINT *,'HUGO - KEPT',kept
!       PRINT *,'HUGO - NSTK',nstk
        DO i = 1, inast
            IF (kept(i) == 1) nco = nco + 1
            DO j = 1, nstk(i)
                IF (kept(i) == 1) THEN
!                   PRINT *,'Check reading Splinewaves ',nco,j,bspl_ndim,nchn(nco)
                    READ (10) splwave(1:nchn(nco)*bspl_ndim, j, nco)
                ELSE
                    READ (10)
                END IF
            END DO
        END DO

        CLOSE (10)

    END SUBROUTINE read_spline_files_data

!---------------------------------------------------------------------------

    !> FIND SYMMETRY INDEX
    SUBROUTINE finind_L(lgsf, lglf, lgpf, nf)

        INTEGER, INTENT(IN)  :: lgsf, lglf, lgpf
        INTEGER, INTENT(OUT) :: nf
        INTEGER :: i

        nf = -1
        DO i = 1, no_of_L_blocks  !inast
            IF (lgsf == L_block_nspn(i)) THEN
                IF (lglf == L_block_lrgl(i)) THEN
                    IF (lgpf == L_block_npty(i)) THEN
                        nf = i
                    END IF
                END IF
            END IF
        END DO

    END SUBROUTINE finind_L

!---------------------------------------------------------------------------

    !> FIND SYMMETRY INDEX (LML BLOCKS) ADDED BY DDAC
    SUBROUTINE finind_LML(lgs, lgl, lgp, lgml, sym_no)

        INTEGER, INTENT(IN)  :: lgs, lgl, lgp, lgml
        INTEGER, INTENT(OUT) :: sym_no
        INTEGER :: i

        sym_no = -1

        DO i = 1, no_of_LML_blocks
            IF (lgs == LML_block_nspn(i)) THEN
                IF (lgl == LML_block_lrgl(i)) THEN
                    IF (lgp == LML_block_npty(i)) THEN
                        IF (lgml == LML_block_ml(i)) THEN
                            sym_no = i
                        END IF
                    END IF
                END IF
            END IF
        END DO

    END SUBROUTINE finind_LML

!---------------------------------------------------------------------------

    SUBROUTINE bcast_ham_data(rank0)

        USE wall_clock,         ONLY: hel_time
        USE initial_conditions, ONLY: double_ionization
        USE mpi_communications, ONLY: message_1st, &
                                      message_last, &
                                      first_pe_broadcasts_an_int, &
                                      first_pe_broadcasts_a_message, &
                                      pe0_bcasts_real1_to_a_group, &
                                      pe0_bcasts_int1_to_a_group, &
                                      pe0_bcasts_int2_to_a_group, &
                                      pe0_bcasts_real3_to_a_group, &
                                      pe0_bcasts_real4_to_a_group, &
                                      pe0_bcasts_real2_to_a_group

        LOGICAL, INTENT(IN) :: rank0
        INTEGER  :: ierr, n
        INTEGER  :: my_group_id

        INTEGER  :: intmessage(message_1st:message_last)
        REAL(wp) :: message(message_1st:message_last)
        REAL(wp) :: tic, toc, timing

        IF (Rank0) tic = hel_time()

        ! number of unique dipole element components to be stored
        n = MERGE(3, 1, molecular_target)

        ! First broadcast data read from Read_H_Parameters
        ! Only 1st PE has correct values before the bcast
        intmessage = 0
        intmessage(1) = nelc
        intmessage(2) = nz
        intmessage(3) = lrang2
        intmessage(4) = lamax
        intmessage(5) = ntarg
        intmessage(6) = nchmx
        intmessage(7) = nstmx
        intmessage(8) = inast
        intmessage(9) = no_of_L_blocks
        intmessage(10) = max_L_block_size
        intmessage(11) = L_block_tot_nchan
        intmessage(12) = fintr
        intmessage(13) = lmaxp1
        intmessage(14) = no_of_LML_blocks
        intmessage(15) = LML_block_tot_nchan
        IF (double_ionization) THEN
            intmessage(16) = two_electron_nchan
        END IF
        CALL first_pe_broadcasts_an_int(intmessage)
        nelc = intmessage(1)
        nz = intmessage(2)
        lrang2 = intmessage(3)
        lamax = intmessage(4)
        ntarg = intmessage(5)
        nchmx = intmessage(6)
        nstmx = intmessage(7)
        inast = intmessage(8)
        no_of_L_blocks = intmessage(9)
        max_L_block_size = intmessage(10)
        L_block_tot_nchan = intmessage(11)
        fintr = intmessage(12)
        lmaxp1 = intmessage(13)
        no_of_LML_blocks = intmessage(14)
        LML_block_tot_nchan = intmessage(15)
        IF (double_ionization) THEN
            two_electron_nchan = intmessage(16)
        END IF

        IF (molecular_target) THEN
            ! Broadcast the value of nfdm_molecular as read-in by the master. Its value
            ! is later checked against nfdm as expected by RMT.
            intmessage = 0
            intmessage(1) = nfdm_molecular
            intmessage(2) = n_rg
            intmessage(3) = GS_finast
            CALL first_pe_broadcasts_an_int(intmessage)
            nfdm_molecular = intmessage(1)
            n_rg = intmessage(2)
            gs_finast = intmessage(3)
        END IF

        ! Reals - rmatr, bbloch
        message = 0.0_wp
        message(1) = rmatr
        message(2) = bbloch
        CALL first_pe_broadcasts_a_message(message)
        rmatr = message(1)
        bbloch = message(2)

        IF (dipole_format_id .EQ. RMatrixI_format_id) THEN
           intmessage = 0.0_wp
           intmessage(1) = mat_size_wp
           intmessage(2) = mat_size_wd
           CALL first_pe_broadcasts_an_int(intmessage)
           mat_size_wp = intmessage(1)
           mat_size_wd = intmessage(2)
        END IF

        ! Determine if this processor is working in the inner or the outer region:
        CALL get_my_group_id(my_group_id)

!-------DDAC: BELOW SHOULD BE MODIFIED-----------------

        ! Outer region allocations

        IF ((.NOT. rank0) .AND. (my_group_id .EQ. outer_group_id)) THEN

            ALLOCATE (etarg(ntarg), ltarg(ntarg), crlv(n*ntarg, ntarg), mnp1(no_of_L_blocks), lstartm1(no_of_LML_blocks, 2), &
                      L_block_lrgl(no_of_L_blocks), L_block_nspn(no_of_L_blocks), L_block_npty(no_of_L_blocks), &
                      L_block_nchan(no_of_L_blocks), L_block_nconat(ntarg, no_of_L_blocks), L_block_l2p(nchmx, no_of_L_blocks), &
                      L_block_cf(nchmx, nchmx, lamax, no_of_L_blocks), L_block_post(0:no_of_L_blocks), &
                      LML_block_lrgl(no_of_LML_blocks), LML_block_nspn(no_of_LML_blocks), LML_block_npty(no_of_LML_blocks), &
                      LML_block_ml(no_of_LML_blocks), LML_block_nchan(no_of_LML_blocks), LML_block_Lblk(no_of_LML_blocks), &
                      LML_block_nconat(ntarg, no_of_LML_blocks), LML_block_cf(nchmx, nchmx, lamax, no_of_LML_blocks), &
                      LML_block_post(0:no_of_LML_blocks), LML_block_l2p(nchmx, no_of_LML_blocks), &
                      m2p(nchmx, no_of_LML_blocks), wmat(nchmx, nstmx, no_of_L_blocks), & 
                      stat = ierr)
            CALL assert(ierr .EQ. 0, 'allocation error 1 with send/receive Ham Data: outer region')

            IF (double_ionization) THEN
                ALLOCATE (ion_l(two_electron_nchan), ion_s(two_electron_nchan), first_l(two_electron_nchan), &
                          second_l(two_electron_nchan), two_electron_l(two_electron_nchan), & 
                          tot_l(two_electron_nchan), tot_ml(two_electron_nchan), stat = ierr)
                          CALL assert(ierr .EQ. 0, 'allocation error 1 with send/receive double ionization Ham Data: outer region')
            END IF

            !Allocate outer region dipole stores if reading RMatrixI format
            IF (dipole_format_id .EQ. RMatrixI_format_id) THEN
                ALLOCATE (wp_read_store(L_block_tot_nchan, L_block_tot_nchan), stat=ierr)
                CALL assert(ierr .EQ. 0, 'allocation error 4 with send/receive Ham Data: wp_read_store')
                ALLOCATE (wd_read_store(L_block_tot_nchan, L_block_tot_nchan), stat=ierr)
                CALL assert(ierr .EQ. 0, 'allocation error 5 with send/receive Ham Data: wd_read_store')
                ALLOCATE (wd_read_store_v(L_block_tot_nchan, L_block_tot_nchan), stat=ierr)
                CALL assert(ierr .EQ. 0, 'allocation error 6 with send/receive Ham Data: wd_read_store_v')
                ALLOCATE (LML_wp_read_store(-mat_size_wp:mat_size_wp, LML_block_tot_nchan), stat=ierr)
                CALL assert(ierr .EQ. 0, 'allocation error 4 with send/receive Ham Data: LML_wp_read_store')
                ALLOCATE (LML_wd_read_store(-mat_size_wd:mat_size_wd, LML_block_tot_nchan), stat=ierr)
                CALL assert(ierr .EQ. 0, 'allocation error 5 with send/receive Ham Data: LML_wd_read_store')
                ALLOCATE (LML_wd_read_store_v(-mat_size_wd:mat_size_wd, LML_block_tot_nchan), stat=ierr)
                CALL assert(ierr .EQ. 0, 'allocation error 6 with send/receive Ham Data: LML_wd_read_store_v')

                wp_read_store = 0.0_wp
                wd_read_store = 0.0_wp
                wd_read_store_v = 0.0_wp
                LML_wp_read_store = 0.0_wp
                LML_wd_read_store = 0.0_wp
                LML_wd_read_store_v = 0.0_wp
            END IF

            IF (molecular_target) THEN
                ALLOCATE (ichl(nchmx, inast), rg(n_rg), lm_rg(6, n_rg), stat=ierr)
                CALL assert(ierr .EQ. 0, 'allocation error 2 with send/receive Ham Data: outer region')
            END IF

            IF (dipole_velocity_output) THEN

                ALLOCATE (crlv_v(ntarg, ntarg), stat=ierr)
                CALL assert(ierr .EQ. 0, 'allocation error 3 with send/receive Ham Data: outer region')
            END IF

        END IF

        ! Now broadcast data read from the H and D files
        IF (rank0 .AND. debug) THEN
            PRINT *, 'Rank0 inast = ', inast, ' ntarg = ', ntarg
        END IF

        ! Outer region data
        IF ((rank0) .OR. (my_group_id .EQ. outer_group_id)) THEN
            CALL pe0_bcasts_real1_to_a_group(etarg, ntarg, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(ltarg, ntarg, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(L_block_lrgl, no_of_L_blocks, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(L_block_nspn, no_of_L_blocks, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(L_block_npty, no_of_L_blocks, outer_group_id)

            CALL pe0_bcasts_int1_to_a_group(LML_block_Lblk, no_of_LML_blocks, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(LML_block_lrgl, no_of_LML_blocks, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(LML_block_nspn, no_of_LML_blocks, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(LML_block_npty, no_of_LML_blocks, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(LML_block_ml, no_of_LML_blocks, outer_group_id)
            
            IF (double_ionization) THEN
                CALL pe0_bcasts_int1_to_a_group(ion_l, two_electron_nchan, outer_group_id)
                CALL pe0_bcasts_int1_to_a_group(ion_s, two_electron_nchan, outer_group_id)
                CALL pe0_bcasts_int1_to_a_group(first_l, two_electron_nchan, outer_group_id)
                CALL pe0_bcasts_int1_to_a_group(second_l, two_electron_nchan, outer_group_id)
                CALL pe0_bcasts_int1_to_a_group(two_electron_l, two_electron_nchan, outer_group_id)
                CALL pe0_bcasts_int1_to_a_group(tot_l, two_electron_nchan, outer_group_id)
                CALL pe0_bcasts_int1_to_a_group(tot_ml, two_electron_nchan, outer_group_id)
            END IF

            CALL pe0_bcasts_int1_to_a_group(L_block_nchan, no_of_L_blocks, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(LML_block_nchan, no_of_LML_blocks, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(mnp1, no_of_L_blocks, outer_group_id)
            CALL pe0_bcasts_int2_to_a_group(L_block_nconat, ntarg, no_of_L_blocks, outer_group_id)
            CALL pe0_bcasts_int2_to_a_group(LML_block_nconat, ntarg, no_of_LML_blocks, outer_group_id)
            CALL pe0_bcasts_int2_to_a_group(L_block_l2p, nchmx, no_of_L_blocks, outer_group_id)
            CALL pe0_bcasts_int2_to_a_group(LML_block_l2p, nchmx, no_of_LML_blocks, outer_group_id)
            CALL pe0_bcasts_int2_to_a_group(m2p, nchmx, no_of_LML_blocks, outer_group_id)
            IF (molecular_target) THEN
                CALL pe0_bcasts_int2_to_a_group(ichl, nchmx, no_of_L_blocks, outer_group_id)
                CALL pe0_bcasts_real1_to_a_group(rg, n_rg, outer_group_id)
                CALL pe0_bcasts_int2_to_a_group(lm_rg, 6, n_rg, outer_group_id)
            END IF
            CALL pe0_bcasts_real3_to_a_group(wmat, nchmx, nstmx, no_of_L_blocks, outer_group_id)
            CALL pe0_bcasts_real4_to_a_group(L_block_cf, nchmx, nchmx, lamax, no_of_L_blocks, outer_group_id)
            CALL pe0_bcasts_real4_to_a_group(LML_block_cf, nchmx, nchmx, lamax, no_of_LML_blocks, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(L_block_post(0:no_of_L_blocks), no_of_L_blocks + 1, outer_group_id)
            CALL pe0_bcasts_int1_to_a_group(LML_block_post(0:no_of_LML_blocks), no_of_LML_blocks + 1, outer_group_id)
            CALL pe0_bcasts_real2_to_a_group(crlv, n*ntarg, ntarg, outer_group_id)
            CALL pe0_bcasts_int2_to_a_group(lstartm1, no_of_LML_blocks, 2, outer_group_id)

            IF (dipole_velocity_output) THEN
                IF (dipole_format_id .EQ. RMatrixII_format_id) THEN
                    CALL PE0_Bcasts_Real2_to_a_Group(crlv_v, ntarg, ntarg, outer_group_id)
                END IF
            END IF

            !Broadcast all outer region dipoles read in using RMatrixI data if required
            IF (dipole_format_id .EQ. RMatrixI_format_id) THEN

               CALL pe0_bcasts_real2_to_a_group(wp_read_store, L_block_tot_nchan, L_block_tot_nchan, outer_group_id)
               CALL pe0_bcasts_real2_to_a_group(wd_read_store, L_block_tot_nchan, L_block_tot_nchan, outer_group_id)
               CALL pe0_bcasts_real2_to_a_group(wd_read_store_v, L_block_tot_nchan, L_block_tot_nchan, outer_group_id)
               CALL pe0_bcasts_real2_to_a_group(LML_wp_read_store, (2*mat_size_wp+1), LML_block_tot_nchan, outer_group_id)
               CALL pe0_bcasts_real2_to_a_group(LML_wd_read_store, (2*mat_size_wd+1), LML_block_tot_nchan, outer_group_id)
               CALL pe0_bcasts_real2_to_a_group(LML_wd_read_store_v, (2*mat_size_wd+1), LML_block_tot_nchan, outer_group_id)

            END IF

        END IF

        IF (Rank0) THEN
            toc = hel_time()
            timing = toc - tic
            PRINT *, 'Finished bcast_ham_data: (secs)', timing
            PRINT *, '========================================'
            PRINT *, ""
        END IF

    END SUBROUTINE bcast_ham_data

!---------------------------------------------------------------------------

    SUBROUTINE bcast_states_per_L_block(i_am_inner_master)

        LOGICAL, INTENT(IN) :: i_am_inner_master
        INTEGER             :: err, my_rank, my_rank_2, my_group_id!, no_of_L_blocks
        INTEGER, PARAMETER  :: inner_region_master_rank = 0

        ! We send in i_am_in_inner_region just for testing.

!       CALL assert (i_am_in_inner_region, 'Err 1 in Bcast_States_Per_L_Block')

        CALL get_my_group_id(my_group_id)
!       CALL assert &
!         (my_group_id == inner_group_id,     &
!         'Only inner group calls Bcast_States_Per_L_Block')

        ! Want    i_am_inner_master <=> my_rank == 0:
        CALL get_my_group_pe_id(my_rank)
!       CALL assert(i_am_inner_master .OR. my_rank /= 0,       &
!                  'Err 2a in Bcast_States_Per_L_Block')
!       CALL assert((.NOT. i_am_inner_master) .OR. my_rank == 0,  &
!                  'Err 2b in Bcast_States_Per_L_Block')

        CALL MPI_COMM_RANK(mpi_comm_region, my_rank_2, err)
!       CALL assert (my_rank == my_rank_2, 'Err 3 in Bcast_States_Per_L_Block')

        ! Should already have distributed L_block_last. Just in case:

!       CALL MPI_BCAST(L_block_last, 1, MPI_INTEGER, &
!                      inner_region_master_rank, mpi_comm_region, err)

        ! Now we can allocate states_per_L_block. Master already has it rt:
        IF (.NOT. i_am_inner_master) THEN
            ! ACB: changed allocation with zero entries above and below to simplify coupling later on
            ALLOCATE (states_per_L_block(-1:no_of_L_blocks + 2), stat=err) !ZM +1   !inast/=L_block_last in m/=0
            !ZM
            states_per_L_block = 0
            !Allocate (states_per_L_block(L_block_1st : L_block_last))
            CALL assert(err .EQ. 0, 'allocation error in Bcast_States_Per_L_Block')
        END IF

!       no_of_L_blocks = L_block_last - L_block_1st + 1

        ! Notice Inner_Region_Master_Rank
        ! bcasts. Only the inner master has the right values and bcasts:

        CALL MPI_BCAST(states_per_L_block, &
                       no_of_L_blocks + 4, & ! ACB : size of states_per_L_block has been increased to include zero blocks above and below
!                      no_of_L_blocks, &
                       MPI_INTEGER, &
                       inner_region_master_rank, &
                       mpi_comm_region, err)

    END SUBROUTINE bcast_states_per_L_block

!-------------------------------------------------------------------------

    !> DDAC ADDED BELOW SUBROUTINE: ANALOGUE OF Bcast_States_Per_L_block ABOVE
    SUBROUTINE bcast_states_per_LML_block(i_am_inner_master)

        LOGICAL, INTENT(IN) :: i_am_inner_master
        INTEGER             :: err, my_rank, my_rank_2, my_group_id!, no_of_LML_blocks
        INTEGER, PARAMETER  :: inner_region_master_rank = 0

        ! We send in i_am_in_inner_region just for testing.
!       CALL assert (i_am_in_inner_region, 'Err 1 in Bcast_States_Per_LML_Block')

        CALL get_my_group_id(my_group_id)
!       CALL assert(my_group_id == inner_group_id,     &
!                  'Only inner group calls Bcast_States_Per_LML_Block')

        ! Want    i_am_inner_master <=> my_rank == 0:
        CALL get_my_group_pe_id(my_rank)
!       CALL assert(i_am_inner_master .OR. my_rank /= 0,       &
!                  'Err 2a in Bcast_States_Per_L_Block')
!       CALL assert((.not. i_am_inner_master) .OR. my_rank == 0,  &
!                  'Err 2b in Bcast_States_Per_LML_Block')

        CALL MPI_COMM_RANK(mpi_comm_region, my_rank_2, err)
!       CALL assert (my_rank == my_rank_2, 'Err 3 in Bcast_States_Per_LML_Block')

        ! Should already have distributed L_block_last. Just in case:

!       CALL MPI_BCAST(L_block_last, 1, MPI_INTEGER, &
!                      inner_region_master_rank, mpi_comm_region, err)

        ! now we can allocate states_per_L_block. Master already has it rt:

        IF (.NOT. i_am_inner_master) THEN
            ! ACB: changed allocation with zero entries above and below to simplify coupling later on
            ALLOCATE (states_per_LML_block(-1:no_of_LML_blocks + 2), stat=err) !ZM +1   !inast/=L_block_last in m/=0
            !ZM
            states_per_LML_block = 0
            !ALLOCATE (states_per_LML_block(L_block_1st : L_block_last))
            CALL assert(err .EQ. 0, 'allocation error in Bcast_States_Per_LML_Block')
        END IF

!       no_of_L_blocks = L_block_last - L_block_1st + 1

        ! Notice Inner_Region_Master_Rank
        ! bcasts. Only the inner master has the right values and bcasts:

        CALL MPI_BCAST(states_per_LML_block, &
                       no_of_LML_blocks + 4, & ! DDAC : size of States_Per_LML_block has been increased to include zero blocks above and below
!                      no_of_L_blocks, &
                       MPI_INTEGER, &
                       inner_region_master_rank, &
                       mpi_comm_region, err)

    END SUBROUTINE bcast_states_per_LML_block

!-------------------------------------------------------------------------
! DEALLOCATIONS
!---------------------------------------------------------------------------

    SUBROUTINE deallocate_rank0_read_hd_files1(i_am_first_pe, i_am_in_outer_region)

        USE initial_conditions, ONLY: double_ionization

        LOGICAL, INTENT(IN) :: i_am_first_pe, i_am_in_outer_region

        ! Clear any arrays not needed by the first PE
        IF ((i_am_first_pe) .AND. (.NOT. i_am_in_outer_region)) THEN
            IF (molecular_target) THEN
                DEALLOCATE (starg, ltarg, &
                            L_block_nconat, LML_block_l2p, m2p, L_block_cf, &
                            crlv, &
                            eig, wmat, etarg, mnp1)
            ELSE
                DEALLOCATE (starg, ltarg, &
                            L_block_nconat, L_block_l2p, L_block_cf, LML_block_cf, &
                            crlv, &
!                           L_block_nspn, L_block_npty, &
!                           LML_block_nspn, LML_block_npty, &       ! DDAC ADDED THIS
                            LML_block_nconat, LML_block_l2p, m2p, &
                            eig, wmat, etarg, mnp1)
            END IF

            IF (double_ionization) THEN
                DEALLOCATE(ltarg_dication, starg_dication)
            END IF

            IF (dipole_format_id .EQ. RMatrixI_format_id) THEN
                DEALLOCATE (wp_read_store)
                DEALLOCATE (wd_read_store)
                DEALLOCATE (wd_read_store_v)
                DEALLOCATE (LML_wp_read_store)
                DEALLOCATE (LML_wd_read_store)
                DEALLOCATE (LML_wd_read_store_v)
            END IF

            IF (ALLOCATED(crlv_v)) THEN
                DEALLOCATE (crlv_v)
            END IF
        END IF

    END SUBROUTINE deallocate_rank0_read_hd_files1

!---------------------------------------------------------------------------

    SUBROUTINE deallocate_rank0_read_hd_files(i_am_first_pe, i_am_in_outer_region)

        LOGICAL, INTENT(IN) :: i_am_first_pe, i_am_in_outer_region
!GSJA   INTEGER :: err

        ! clear any arrays not needed by the first PE
        IF ((i_am_first_pe) .AND. (.NOT. i_am_in_outer_region)) THEN
            IF (ALLOCATED(dipsto_v)) THEN
                DEALLOCATE (dipsto_v)
            END IF
        END IF

    END SUBROUTINE deallocate_rank0_read_hd_files

!--------------------------------------------------------------------------

    SUBROUTINE deallocate_read_H_parameters(i_am_first_pe, i_am_in_outer_region)

        LOGICAL, INTENT(IN) :: i_am_first_pe, i_am_in_outer_region
        INTEGER             :: err, my_group_id

        CALL get_my_group_id(my_group_id)

        ! 1st Inner region processor only:
        IF ((i_am_first_pe) .AND. (.NOT. i_am_in_outer_region) .AND. .NOT. molecular_target) THEN
            DEALLOCATE (kept, nstk, nchn, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error Read_H_Parameters 1')
        END IF

        ! Outer region processors only:
        IF (my_group_id .EQ. outer_group_id) THEN
            DEALLOCATE (etarg, ltarg, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error Read_H_Parameters 3')
        END IF

    END SUBROUTINE deallocate_read_H_parameters

!--------------------------------------------------------------------------

    SUBROUTINE deallocate_read_H_file(i_am_first_pe, i_am_in_outer_region)

        LOGICAL, INTENT(IN) :: i_am_first_pe, i_am_in_outer_region
        INTEGER             :: err, my_group_id

        CALL get_my_group_id(my_group_id)

        ! 1st Inner region processor only:
        IF ((i_am_first_pe) .AND. (.NOT. i_am_in_outer_region)) THEN
            DEALLOCATE (L_block_post, LML_block_post, L_block_nchan, &
                        LML_block_nchan, states_per_L_block, states_per_LML_block, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error Read_H_File 1')
        END IF

        ! Outer region processors only:
        IF (my_group_id .EQ. outer_group_id) THEN
            DEALLOCATE (L_block_lrgl, L_block_nspn, L_block_npty, L_block_nchan, mnp1, L_block_nconat, L_block_l2p, &
                        LML_block_lrgl, LML_block_nspn, LML_block_npty, LML_block_ml, LML_block_Lblk, &
                        LML_block_nchan, LML_block_nconat, LML_block_l2p, wmat, L_block_cf, L_block_post, &
                        LML_block_post, LML_block_cf, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error Read_H_File 3')
        END IF

    END SUBROUTINE deallocate_read_H_file

!--------------------------------------------------------------------------

    SUBROUTINE deallocate_read_D_file

        INTEGER :: err, my_group_id

        CALL get_my_group_id(my_group_id)

        ! Outer region processors only:
        IF (my_group_id .EQ. outer_group_id) THEN

            DEALLOCATE (crlv, stat=err)

            IF (dipole_format_id .EQ. RMatrixI_format_id) THEN
                DEALLOCATE (wp_read_store, stat=err)
                DEALLOCATE (wd_read_store, stat=err)
                DEALLOCATE (wd_read_store_v, stat=err)
                DEALLOCATE (LML_wp_read_store, stat=err)
                DEALLOCATE (LML_wd_read_store, stat=err)
                DEALLOCATE (LML_wd_read_store_v, stat=err)
            END IF
            IF (ALLOCATED(crlv_v)) DEALLOCATE (crlv_V, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error Read_D_File 3')

        END IF

        !TODO: Deallocate if RMAtrixI format

    END SUBROUTINE deallocate_read_D_file

!--------------------------------------------------------------------------

    SUBROUTINE deallocate_read_spline_files

        INTEGER :: err

        IF (.NOT. molecular_target) THEN
            DEALLOCATE (splwave, bspl_t, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error Read_Spline_Files')
        END IF

    END SUBROUTINE deallocate_read_spline_files

!--------------------------------------------------------------------------

    SUBROUTINE deallocate_readhd(i_am_inner_master, i_am_in_outer_region)

        LOGICAL, INTENT(IN)    :: i_am_inner_master
        LOGICAL, INTENT(IN)    :: i_am_in_outer_region

        CALL deallocate_read_D_file
        CALL deallocate_read_H_file(i_am_inner_master, i_am_in_outer_region)
        CALL deallocate_read_H_parameters(i_am_inner_master, i_am_in_outer_region)

    END SUBROUTINE deallocate_readhd

!---------------------------------------------------------------------------

    SUBROUTINE init_read_in_and_distribute(i_am_inner_master, &
                                           i_am_in_inner_region, &
                                           r_at_region_bndry, &
                                           number_channels, &
                                           Z_minus_N)

        LOGICAL, INTENT(IN)    :: i_am_inner_master
        LOGICAL, INTENT(IN)    :: i_am_in_inner_region
        REAL(wp), INTENT(OUT)  :: r_at_region_bndry
        INTEGER, INTENT(OUT)   :: number_channels
        REAL(wp), INTENT(OUT)  :: Z_minus_N

        IF (i_am_inner_master) THEN
            IF (molecular_target) THEN
                CALL inner_master_reads_molecular_input_files
            ELSE
                CALL inner_master_reads_atomic_input_files
            END IF
        END IF

        ! Broadcast data to other processors
        CALL distribute_ham_data(i_am_inner_master, &
                                 i_am_in_inner_region, &
                                 r_at_region_bndry, &
                                 number_channels, &
                                 Z_minus_N)

    END SUBROUTINE init_read_in_and_distribute

!---------------------------------------------------------------------------

    !> \brief Read the spline coefficients from the psi_inner files
    !> Updated to handle LMLSPi symmetries by D Clarke, G Armstrong
    SUBROUTINE read_psi_files(file_suffix)

        USE file_num, ONLY: file_number

        IMPLICIT NONE

        CHARACTER(LEN=26), INTENT(IN)  :: file_suffix
        INTEGER                        :: i, err
        INTEGER                        :: loc_post
        CHARACTER(LEN=:), ALLOCATABLE  :: filename
        COMPLEX(wp), ALLOCATABLE       :: my_psi_inner(:, :)
        INTEGER, ALLOCATABLE           :: num_blocks_pe(:), block_start_pe(:)
        INTEGER                        :: no_of_inner_region_pes, my_num_LML_blocks, my_starting_block
        INTEGER                        :: my_unit, ii, i_block, my_start, my_fin, size, my_num_minus_1, &
                                          file_size, j, jj
        LOGICAL                        :: file_yes, reform_file_yes
        
        ALLOCATE (wf(LML_block_post(No_of_LML_blocks), numsols), stat=err)
        CALL assert(err .EQ. 0, "Allocation error with wf")

        ! Work out whether we have a file for each LML block (traditional RMT) or whether the newer
        ! several-LML-blocks-per-pe option has been used.
        DO i = 1, no_of_LML_blocks
            filename = 'psi_inner'//TRIM(file_number(i, no_of_LML_blocks))//'.'//file_suffix
            INQUIRE(file=filename, EXIST=file_yes)
            IF (.not. file_yes) THEN
               print *, 'File ', filename, &
                    ' not found: we assume the many-LML-blocks-per-pe formalism has been used'
               exit
            END IF
        END DO    
        IF (file_yes) THEN  ! i.e. no_of_inner_region_pes >= no_of_LML_blocks
           ALLOCATE (my_psi_inner(max_L_block_size, numsols), stat=err)
           CALL assert(err .EQ. 0, "Allocation error with my_psi_inner")
           DO i = 1, no_of_LML_blocks
               file_size = 0 
               loc_post = LML_block_post(i) - LML_block_post(i - 1)
   
               filename = 'psi_inner'//TRIM(file_number(i, no_of_LML_blocks))//'.'//file_suffix
               PRINT *, 'READING ', TRIM(filename)
               INQUIRE(file=filename, SIZE=file_size)
               ! Note that the file size is assumed to be the array size + 8 Bytes (intro or outeo)
               OPEN (UNIT=11, FILE=filename, FORM='UNFORMATTED')
               IF ((file_size / 16) == max_L_block_size * numsols) THEN 
! To return to traditional RMT, comment out the IF section apart from the following read statement. 
                  READ (11) my_psi_inner
               ELSE IF ((file_size / 16) == loc_post * numsols) THEN
                  READ (11) ((my_psi_inner(j,jj), j = 1, loc_post), jj= 1, numsols)
               ELSE
                  PRINT *, 'File ', filename, &
                       ' has file_size =', file_size, ', which does not correspond to max_l_block_size = ',&
                        max_L_block_size, ' or loc_post = ', loc_post, ' with numsols =', numsols
                  PRINT *, 'file_size / 16 =', (file_size / 16)
                  PRINT *, 'The file size is assumed to be the array size + 8 Bytes (intro or outro).'
                  PRINT *, 'If a different relationship is found, adjust "read_psi_files" in readh.f90 .' 
                  CALL assert (.false., 'read_psi_files failure')
               END IF   
                  wf(LML_block_post(i - 1) + 1:LML_block_post(i), :) = my_psi_inner(1:loc_post, :)
               CLOSE (11)
           END DO
        ELSE
            reform_file_yes = .false.
            INQUIRE(file='reform_inner_blocks_parallel_info', EXIST=reform_file_yes)
            IF (.not. reform_file_yes) THEN
               print *, 'File reform_inner_blocks_parallel_info not found:', &
                    ' the many-LML-blocks-per-pe formalism has been used and this file, ', & 
                    ' produced in mpi_layer_lblocks routine setup_Lblock_communicator_size_pn,', &
                    ' should be in this directory.'
               CALL assert(.false., "reform_inner_blocks_parallel_info is needed.")
             END IF
           OPEN (newunit=my_unit, file='reform_inner_blocks_parallel_info', FORM='UNFORMATTED')
           ! This file provides information about the number of LML blocks in the wavefunction array
           ! and the starting block for each array. 
           read(my_unit) no_of_inner_region_pes
           ALLOCATE (block_start_pe(no_of_inner_region_pes), num_blocks_pe(no_of_inner_region_pes), &
                     stat=err)
           CALL assert(err .EQ. 0, "Allocation error with my_psi_inner, ref 2")
           read(my_unit) block_start_pe(1:no_of_inner_region_pes)
           read(my_unit) num_blocks_pe(1:no_of_inner_region_pes)
           CLOSE (my_unit)
           DO i = 1, no_of_inner_region_pes
              my_starting_block = block_start_pe(i)
              my_num_LML_blocks = num_blocks_pe(i)
              my_num_minus_1 = num_blocks_pe(i) - 1
              ALLOCATE (my_psi_inner(max_L_block_size * my_num_LML_blocks, numsols), stat=err)
              CALL assert(err .EQ. 0, "Allocation error with my_psi_inner, ref 3")              
              filename = 'psi_inner'//TRIM(file_number(my_starting_block, no_of_LML_blocks))//'.'//file_suffix
              PRINT *, 'READING ', TRIM(filename)
              size = LML_block_post(my_starting_block+my_num_minus_1) - LML_block_post(my_starting_block-1)
              OPEN (UNIT=11, FILE=filename, FORM='UNFORMATTED')
              READ (11) my_psi_inner(1:size, :)
              CLOSE (11)
              DO ii = 0, my_num_LML_blocks - 1
                 i_block = my_starting_block + ii
                 my_start = LML_block_post(i_block - 1) + 1 - LML_block_post(my_starting_block - 1)
                 my_fin = LML_block_post(i_block) - LML_block_post(my_starting_block - 1)
                 wf(LML_block_post(i_block - 1) + 1:LML_block_post(i_block), :) = &
                      my_psi_inner(my_start:my_fin, :)
              END DO
              DEALLOCATE (my_psi_inner, stat=err)
           END DO
           DEALLOCATE (num_blocks_pe, block_start_pe, stat=err)
           CALL assert(err .EQ. 0, "Deallocation error with my_psi_inner")
        END IF   

    END SUBROUTINE read_psi_files

!---------------------------------------------------------------------------

    !> \brief   Clebsch Gordan Coefficient Table
    !> \authors Andrew Brown, Greg Armstrong, Daniel Clarke
    !> \date    2018
    !>
    !> Produces a table containing the Clebsch Gordan Coefficients coupling each
    !> state.
    SUBROUTINE setup_cg_store

    IMPLICIT NONE

    INTEGER :: ierr
    INTEGER :: ii,jj
    INTEGER :: delta_ML

    ALLOCATE (cg_store (no_of_LML_blocks, no_of_LML_blocks), stat = ierr)

    CALL assert(ierr.EQ.0,"allocation error with cg_store")

    DO ii = 1, no_of_LML_blocks
       DO jj = 1, no_of_LML_blocks
          delta_ML = LML_block_ml(jj) - LML_block_ml(ii)
          IF (coupling_id.eq.LS_coupling_id) then
            cg_store(ii,jj) = cg(LML_block_lrgl(ii),1,LML_block_lrgl(jj),LML_block_ml(ii),delta_ML,LML_block_ml(jj)) &
                                / SQRT (2 * LML_block_lrgl(jj) + 1.0_wp)
          ELSE
            cg_store(ii,jj) = dcg(LML_block_lrgl(ii),2,LML_block_lrgl(jj),LML_block_ml(ii),delta_ML,LML_block_ml(jj)) &
                                / SQRT (LML_block_lrgl(jj) + 1.0_wp)
          END IF
       END DO
    END DO

    END SUBROUTINE setup_cg_store

!---------------------------------------------------------------------------

    SUBROUTINE setup_region_three_channels(count_channels, ntarg_dication, ltarg_dication, starg_dication, lmax, state_id_last)

    INTEGER, INTENT(IN)    :: ntarg_dication
    INTEGER, INTENT(IN)    :: ltarg_dication(ntarg_dication), starg_dication(ntarg_dication)
    INTEGER, INTENT(INOUT) :: state_id_last
    INTEGER                :: lmax, state_id
    INTEGER                :: total_l, total_ml, l_ion, l_two_electrons, la, lb
    INTEGER                :: total_lmax, total_mlmax, big_lmax, little_lmax
    INTEGER                :: total_s, s_ion, two_electron_s
    INTEGER                :: par_ion, par_two_electrons, par_total
    INTEGER                :: itarg
 
    LOGICAL   :: count_channels
    LOGICAL   :: electrons_coupled, eparity_coupled, parity_coupled, spin_coupled, triangle_ok

    little_lmax = lmax
    big_lmax = 2*lmax
    total_lmax = big_lmax + MAXVAL(ltarg)
    total_mlmax = MIN(total_lmax, ML_max)

    state_id = 0 
    IF (count_channels) THEN
       state_id_last = 0
       WRITE(*, *)
       WRITE(*, *) '=========================== REGION III CHANNELS ======================================'
       WRITE(*, *) '    index   ion_L   ion_s     la      lb      L       S   total_L  total_ML  total_S'
    ELSE
       PRINT*, 'state_id_last =', state_id_last 
       ALLOCATE(ion_targ(state_id_last), ion_l(state_id_last), ion_s(state_id_last))
       ALLOCATE(first_l(state_id_last), second_l(state_id_last))
       ALLOCATE(two_electron_l(state_id_last), tot_l(state_id_last))
       ALLOCATE(tot_ml(state_id_last))
    END IF


    DO total_l = 0, total_lmax
       DO total_ml = -total_mlmax, total_mlmax
           par_total = MOD(total_l, 2)  !total (natural) parity
           total_s = 0                  !i.e. singlet, this is S not 2S+1 !!!
           DO itarg = 1, ntarg_dication
              l_ion = ltarg_dication(itarg)
              s_ion = starg_dication(itarg)    !make singlet or triplet
              par_ion = 0             !even parity doubly-charged ion
              DO l_two_electrons = 0, big_lmax
                 DO par_two_electrons = 0, 1       !parity of two outer electrons
                    CALL parity_allowed(par_ion, par_two_electrons, par_total, parity_coupled)
                    CALL satisfies_triangle_rule(l_ion, l_two_electrons, total_l, triangle_ok)

                    IF (parity_coupled .AND. triangle_ok) THEN
                       DO la = 0, little_lmax
                          DO lb = 0, little_lmax
                             CALL parity_allowed(par_ion, MOD(la + lb, 2), par_two_electrons, eparity_coupled)
                             CALL satisfies_triangle_rule(la, lb, l_two_electrons, electrons_coupled)
                             DO two_electron_s = 0,1 ! spin of two outer electrons, NOT THE MULTIPLICITY 2s+1
                                CALL satisfies_triangle_rule(s_ion, total_s, two_electron_s, spin_coupled)
                                IF ( spin_coupled .AND. electrons_coupled .AND. eparity_coupled ) THEN
                                   state_id = state_id + 1

                                   IF (count_channels) THEN
                                      WRITE(*,'(10I8)') state_id, l_ion, s_ion, la, lb, l_two_electrons, &
                                                        two_electron_s, total_l, total_ml, total_s
                                   ELSE
                                      ion_targ(state_id) = itarg
                                      ion_l(state_id) = l_ion
                                      ion_s(state_id) = s_ion
                                      first_l(state_id) = la
                                      second_l(state_id) = lb
                                      two_electron_l(state_id) = l_two_electrons
                                      tot_l(state_id) = total_l
                                      tot_ml(state_id) = total_ml
                                   END IF

                                END IF
                             END DO
                          END DO
                       END DO
                    END IF

                END DO
             END DO
          END DO
       END DO
    END DO

    state_id_last = state_id

!   IF (.NOT. count_channels) CALL deallocate_region_iii_angmom_arrays

    END SUBROUTINE setup_region_three_channels

!---------------------------------------------------------------------------

    SUBROUTINE deallocate_region_iii_angmom_arrays

    INTEGER :: err

    DEALLOCATE(ion_targ, ion_l, ion_s, first_l, second_l, two_electron_l, tot_l, tot_ml, stat=err)
    IF (err /= 0) THEN
         PRINT *, 'deallocation error region III angmom arrays'
     END IF

    END SUBROUTINE deallocate_region_iii_angmom_arrays

END MODULE readhd
