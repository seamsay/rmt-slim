! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles the outer region Arnoldi and Taylor series propagators.
MODULE outer_propagators

    USE precisn,            ONLY: wp, ep
    USE global_data,        ONLY: im, zero
    USE rmt_assert,         ONLY: assert
    USE grid_parameters,    ONLY: x_1st, &
                                  x_last, &
                                  channel_id_1st, &
                                  channel_id_last, &
                                      my_channel_id_1st, my_channel_id_last, my_num_channels, pe_for_channel
    USE initial_conditions, ONLY: propagation_order, &
                                  numsols => no_of_field_confs

    IMPLICIT NONE

    REAL(wp), SAVE, ALLOCATABLE :: taylor_coeff(:)
    REAL(wp), SAVE, ALLOCATABLE :: taylors_series_coeffs(:)
    REAL(wp), SAVE, ALLOCATABLE :: taylors_series_coeffs_2(:)
    REAL(ep), SAVE, ALLOCATABLE :: taylors_series_coeffs_L(:)
    REAL(wp), SAVE              :: inverse_max_order_factorial
    REAL(wp), SAVE              :: max_order_factorial
    REAL(wp), SAVE              :: five_fact_over_max_order_fact

    !! want  1 / 5!  with some extra precision:

    REAL(wp), PARAMETER         :: reciprocal_5_factorial_hi = 32.0_wp/4096.0_wp
    REAL(wp), PARAMETER         :: reciprocal_5_factorial_lo = &
                                      2.133333333333333333333333333_wp/4096.0_wp

    COMPLEX(wp), ALLOCATABLE    :: outer_wavefunction(:, :, :)
    COMPLEX(wp), ALLOCATABLE    :: powers_of_H_array1(:, :, :, :)
    COMPLEX(wp), ALLOCATABLE    :: powers_of_H_array2(:, :, :, :, :)
    COMPLEX(wp), ALLOCATABLE    :: i_to_power(:)
    REAL(wp), ALLOCATABLE       :: minus1_to_power(:)
    REAL(wp), ALLOCATABLE       :: WE_store(:, :, :)
    REAL(wp), ALLOCATABLE       :: WB_store(:, :, :)

    REAL(wp), SAVE              :: t_step, t_step2

    ! The following should be allocatable (and region-dependent):
    COMPLEX(wp), ALLOCATABLE    :: firstderiv_psi_at_inner_bndry(:, :)
    COMPLEX(wp), ALLOCATABLE    :: firstderiv_X_at_inner_bndry(:, :)
    COMPLEX(wp), ALLOCATABLE    :: X(:, :, :, :)
    COMPLEX(wp), ALLOCATABLE    :: h_X(:, :, :)
    COMPLEX(wp), ALLOCATABLE    :: psi_subset_at_boundary(:, :, :)
    COMPLEX(wp), ALLOCATABLE    :: h_psi_subset(:, :, :)
    COMPLEX(wp), ALLOCATABLE    :: r_bndry_lo(:, :, :)
    COMPLEX(wp), ALLOCATABLE    :: r_bndry_hi(:, :, :)
    COMPLEX(wp), ALLOCATABLE    :: dpsi_outer(:, :)
    COMPLEX(wp), ALLOCATABLE    :: new_psi_outer(:, :)
    COMPLEX(wp), ALLOCATABLE    :: h_new_psi_outer(:, :)
    COMPLEX(wp), ALLOCATABLE    :: inner_psi_at_fd_pts(:, :, :)
    COMPLEX(wp), ALLOCATABLE    :: new_psi_b(:, :, :), h_new_psi_b(:, :, :)
    COMPLEX(wp), ALLOCATABLE    :: expec_psi_outer(:, :, :, :, :)

CONTAINS

    SUBROUTINE initialise_outer_propagators(krylov_hdt_desired, del_t, psi_outer_start, &
                                      propagation_order, nfdm, half_fd_order, i_am_pe0_outer, &
                                      i_am_in_first_outer_block)
        USE initial_conditions, only: use_taylor_propagator
        USE outer_hamiltonian,  only: setup_WE_store
        USE local_ham_matrix,   only : RR, master_RR

        LOGICAL, INTENT(IN)       :: krylov_hdt_desired
        REAL(wp), INTENT(IN)      :: del_t
        COMPLEX(wp), INTENT(IN)   :: psi_outer_start(x_1st:x_last, &
                                                     my_channel_id_1st:my_channel_id_last, &
                                                     1:numsols)
        INTEGER, INTENT(IN)       :: propagation_order
        INTEGER, INTENT(IN)       :: nfdm
        INTEGER, INTENT(IN)       :: half_fd_order
        LOGICAL, INTENT(IN)       :: i_am_pe0_outer, i_am_in_first_outer_block
        INTEGER :: max_points_for_H_op

        max_points_for_H_op = 2 * nfdm - half_fd_order

        CALL allocate_saved_arrays
        CALL init_module_outer_propagators(krylov_hdt_desired, del_t)
        CALL init_outer_wavefunction(psi_outer_start)
        IF(.NOT.(use_taylor_propagator)) THEN
           CALL allocate_powers_of_H_array(propagation_order)
           CALL init_powers_of_i(propagation_order)
        END IF
        CALL allocate_arnoldi_prop_arrays(propagation_order, nfdm, half_fd_order, &
                                          i_am_pe0_outer, i_am_in_first_outer_block)
        CALL setup_WE_store(WE_store,RR,x_last - x_1st + 1)
        IF (i_am_in_first_outer_block) THEN
            CALL setup_WE_store(WB_store, master_RR, max_points_for_H_op)
        END IF

    END SUBROUTINE initialise_outer_propagators

!---------------------------------------------------------------------------

    SUBROUTINE allocate_saved_arrays

        INTEGER :: ierror

        ALLOCATE (taylor_coeff(0:propagation_order), &
                  taylors_series_coeffs(0:propagation_order), &
                  taylors_series_coeffs_2(0:propagation_order), &
                  taylors_series_coeffs_L(0:propagation_order), &
                  stat=ierror)

        CALL assert(ierror .EQ. 0, "Allocation error in propagators")

    END SUBROUTINE allocate_saved_arrays

!---------------------------------------------------------------------------

    SUBROUTINE deallocate_outer_propagators

        CALL deallocate_outer_wavefunction
        CALL deallocate_powers_of_H_array
        CALL deallocate_powers_of_i
        CALL deallocate_arnoldi_prop_arrays

    END SUBROUTINE deallocate_outer_propagators

!---------------------------------------------------------------------------

    SUBROUTINE init_module_outer_propagators(krylov_hdt_desired, del_t)

        LOGICAL, INTENT(IN)   :: krylov_hdt_desired
        REAL(wp), INTENT(IN)  :: del_t
        INTEGER               :: i
        REAL(ep)              :: factorial(0:propagation_order)
        REAL(ep)              :: Bfactorial(0:propagation_order)

        ! factorial(i) = i!
        factorial(0) = 1.0_ep
        DO i = 1, propagation_order
            factorial(i) = factorial(i - 1)*REAL(i, ep)
        END DO

        taylors_series_coeffs(0) = 1.0_wp
        DO i = 1, propagation_order
            taylors_series_coeffs(i) = REAL(1.0_ep/factorial(i), wp)
        END DO

        max_order_factorial = REAL(factorial(propagation_order), wp)
        inverse_max_order_factorial = REAL(1.0_ep/factorial(propagation_order), wp)
        five_fact_over_max_order_fact &
            = REAL(120.0_ep/factorial(propagation_order), wp)

        ! Bfactorial(i) = propagation_order! / i!

        Bfactorial(propagation_order) = 1.0_ep
        Bfactorial(propagation_order - 1) = Bfactorial(propagation_order)*REAL(propagation_order, ep)
        DO i = propagation_order - 2, 1, -1
            Bfactorial(i) = Bfactorial(i + 1)*REAL(i + 1, ep)
        END DO
        Bfactorial(0) = Bfactorial(1)

        taylors_series_coeffs_L = Bfactorial

        taylors_series_coeffs_2 = REAL(taylors_series_coeffs_L, wp)
        taylors_series_coeffs_2(2) = 0.5_wp
        taylors_series_coeffs_2(1) = 1.0_wp
        taylors_series_coeffs_2(0) = 1.0_wp

        IF (krylov_hdt_desired) THEN
            ! Krylov subspace spans (H dt), (H dt)^2, ..., (H dt)^n
            t_step = 1.0_wp          !t_step  = 1
            t_step2 = 1.0_wp*Del_t   !t_step2 = dt
        ELSE
            ! Krylov subspace spans H, H^2, ..., H^n
            t_step = 1.0_wp*Del_t    !t_step  = dt
            t_step2 = 1.0_wp         !t_step2 = 1
        END IF

        taylor_coeff(0) = 1.0_wp
        DO i = 1, propagation_order
            taylor_coeff(i) = taylor_coeff(i - 1) / REAL(i, wp)
        END DO

    END SUBROUTINE init_module_outer_propagators

!---------------------------------------------------------------------------

    SUBROUTINE init_outer_wavefunction(psi_outer_start)

        COMPLEX(wp), INTENT(IN)  :: psi_outer_start(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)

        INTEGER :: err

        ALLOCATE (outer_wavefunction(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with Outer_Wavefunction')

        outer_wavefunction = psi_outer_start

    END SUBROUTINE init_outer_wavefunction

!---------------------------------------------------------------------------

    SUBROUTINE update_psi_outer(psi_outer)

        COMPLEX(wp), INTENT(OUT)  :: psi_outer(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)

        psi_outer = outer_wavefunction

    END SUBROUTINE update_psi_outer

!---------------------------------------------------------------------------

    SUBROUTINE update_outer_wavefunction(psi_outer)

        COMPLEX(wp), INTENT(IN)  :: psi_outer(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)

        outer_wavefunction = psi_outer

    END SUBROUTINE update_outer_wavefunction

!---------------------------------------------------------------------------

    SUBROUTINE deallocate_outer_wavefunction

        INTEGER :: err

        DEALLOCATE (outer_wavefunction, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with Outer_Wavefunction')

    END SUBROUTINE deallocate_outer_wavefunction

!---------------------------------------------------------------------------

    SUBROUTINE allocate_powers_of_H_array(propagation_order)

        INTEGER, INTENT(IN) :: propagation_order
        INTEGER :: err

        ALLOCATE (powers_of_H_array1(my_channel_id_1st:my_channel_id_last, 1:numsols, &
                                     0:propagation_order - 1, 0:propagation_order - 1), &
                  powers_of_H_array2(x_1st - 2:x_1st - 1, my_channel_id_1st:my_channel_id_last, 1:numsols, &
                                     0:propagation_order, 0:propagation_order), &
                  stat=err)
        CALL assert(err .EQ. 0, 'allocation error with powers_of_H_array')

    END SUBROUTINE allocate_powers_of_H_array

!---------------------------------------------------------------------------

    SUBROUTINE init_powers_of_i(propagation_order)

        INTEGER, INTENT(IN) :: propagation_order
        INTEGER :: err, m

        ALLOCATE (i_to_power(0:propagation_order), &
                  minus1_to_power(0:propagation_order), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with i_to_power')

        DO m = 0, propagation_order
            i_to_power(m) = im**m
            minus1_to_power(m) = (-1.0_wp)**m
        END DO

    END SUBROUTINE init_powers_of_i

!---------------------------------------------------------------------------

    SUBROUTINE deallocate_powers_of_H_array

        INTEGER :: err

        IF (ALLOCATED(powers_of_H_array1) .AND. ALLOCATED(powers_of_H_array2)) THEN
            DEALLOCATE (powers_of_H_array1, powers_of_H_array2, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error with powers_of_H_array')
        END IF

    END SUBROUTINE deallocate_powers_of_H_array

!---------------------------------------------------------------------------

    SUBROUTINE deallocate_powers_of_i

        INTEGER :: err

        IF (ALLOCATED(i_to_power)) THEN
            DEALLOCATE (i_to_power, minus1_to_power, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error with i_to_power')
        END IF

    END SUBROUTINE deallocate_powers_of_i

!---------------------------------------------------------------------------

    SUBROUTINE reset_powers_of_H_array

        powers_of_H_array1 = zero
        powers_of_H_array2 = zero

    END SUBROUTINE reset_powers_of_H_array

!---------------------------------------------------------------------------

    SUBROUTINE allocate_arnoldi_prop_arrays(propagation_order, no_inner_fd_pts, half_fd_order, &
                                            i_am_pe0_outer, i_am_in_first_outer_block)

        USE initial_conditions, ONLY: dipole_output_desired, &
                                      use_taylor_propagator
        USE lrpots,             ONLY: we_size

        INTEGER, INTENT(IN) :: propagation_order, no_inner_fd_pts, half_fd_order
        LOGICAL, INTENT(IN) :: i_am_pe0_outer, i_am_in_first_outer_block
        INTEGER :: err
        INTEGER :: max_points_for_H_op

        max_points_for_H_op = 2 * no_inner_fd_pts - half_fd_order

        ! Allocations
        ALLOCATE (firstderiv_psi_at_inner_bndry(my_channel_id_1st:my_channel_id_last, 1:numsols), &
                  firstderiv_X_at_inner_bndry(my_channel_id_1st:my_channel_id_last, 1:numsols), &
                  psi_subset_at_boundary(-no_inner_fd_pts:no_inner_fd_pts - half_fd_order - 1, &
                    my_channel_id_1st:my_channel_id_last, 1:numsols), &
                  h_psi_subset(-no_inner_fd_pts:no_inner_fd_pts - half_fd_order - 1, &
                    my_channel_id_1st:my_channel_id_last, 1:numsols), &
                  r_bndry_lo(x_1st - 2:x_1st - 1, my_channel_id_1st:my_channel_id_last, 1:numsols), &
                  r_bndry_hi(x_last + 1:x_last + 2, my_channel_id_1st:my_channel_id_last, 1:numsols), &
                  WE_store(x_1st:x_last, we_size, my_num_channels), &
                  stat=err)
        CALL assert(err .EQ. 0, 'problem allocating outer region arrays in Arnoldi_Propagate')

        IF (.NOT.(use_taylor_propagator)) THEN
            ALLOCATE(X(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols, 0:propagation_order), &
                     h_X(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols), stat=err)
            CALL assert(err .EQ. 0, 'problem allocating outer region arrays X and h_X')
        END IF

        WE_store = 0.0_wp

        IF (dipole_output_desired) THEN
            ALLOCATE (expec_psi_outer(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols, 1:3, 1:2), STAT = err)
            CALL assert(err == 0, 'Memory allocation error in allocate_arnoldi_prop_arrays')
        END IF

        ! Extra allocations for outer master
        IF (i_am_in_first_outer_block) THEN   ! ******
            ALLOCATE (WB_store(max_points_for_H_op, we_size, my_channel_id_1st:my_channel_id_last), &
                      inner_psi_at_fd_pts(1:no_inner_fd_pts, my_channel_id_1st:my_channel_id_last, 1:numsols), &
                      new_psi_b(1:max_points_for_H_op, my_channel_id_1st:my_channel_id_last, 1:numsols), &
                      h_new_psi_b(1:max_points_for_H_op, my_channel_id_1st:my_channel_id_last, 1:numsols), &
                      stat=err)
            CALL assert(err .EQ. 0, 'problem allocating outer region arrays in Arnoldi_Propagate on pe0')
            WB_store = 0.0_wp
        END IF

    END SUBROUTINE allocate_arnoldi_prop_arrays

!---------------------------------------------------------------------------

    SUBROUTINE deallocate_arnoldi_prop_arrays

        INTEGER :: err

        ! Deallocations
        DEALLOCATE (firstderiv_psi_at_inner_bndry,&
                    firstderiv_X_at_inner_bndry, &
                    psi_subset_at_boundary, &
                    h_psi_subset, &
                    r_bndry_lo, &
                    r_bndry_hi, &
                    WE_store, & 
                    stat=err)
        CALL assert(err .EQ. 0, 'problem deallocating outer region arrays in Arnoldi_Propagate')

        IF (ALLOCATED(X)) THEN
            DEALLOCATE(X, stat = err)
            CALL assert(err .EQ. 0, 'problem deallocating outer region array X')
        END IF

        IF (ALLOCATED(h_X)) THEN
            DEALLOCATE(h_X, stat = err)
            CALL assert(err .EQ. 0, 'problem deallocating outer region array h_X')
        END IF

        IF (ALLOCATED(expec_psi_outer)) THEN
            DEALLOCATE(expec_psi_outer, STAT = err)
            CALL assert(err == 0, 'Memory de-allocation error in deallocate_arnoldi_prop_arrays')
        END IF

        ! outer region master arrays for deallocation
        IF (ALLOCATED(WB_store)) THEN
            DEALLOCATE (WB_store, &
                        inner_psi_at_fd_pts, &
                        new_psi_b, &
                        h_new_psi_b, &
                        stat=err )
            CALL assert(err .EQ. 0, 'problem deallocating outer region arrays in Arnoldi_Propagate on pe0')
        END IF


    END SUBROUTINE deallocate_arnoldi_prop_arrays

    !> \brief Propagate the outer-region wavefunction using a Taylor series propagator with maximum order
    !>        given by the parameter propagation_order.
    !>
    !> \param[in]     no_inner_fd_pts    number of grid points extending into the inner region
    !> \param[in]     half_fd_order      Parameter related to the finite-difference order (n), half_fd_order = (n-1)/2 
    !> \param[in]     del_r              grid spacing deltar
    !> \param[in]     z_minus_n          net charge
    !> \param[in]     field_strength     Electric field strength at this timestep
    !> \param[in]     psi_inner_on_grid  Inner-region wavefunction on grid points
    !> \param[in]     r_value_at_r_1st   Value of the radial coordinate r at the first outer-region grid point 
    !> \param[in]     i_am_pe0_outer     determine if the pe is the first assigned to the outer region
    !> \param[in]     i_am_in_first_outer_block     determine if the pe is in the first outer region grid block
    !> \param[out]    hr                 krylov subspace hamiltonian (not used with Taylor propagator
    !> \param[out]    expec_outer        dipole expectation value calculated from the outer-region wavefunction
    !
    SUBROUTINE propagate_outer(no_inner_fd_pts, &
                               half_fd_order, &
                               del_R, &
                               Z_minus_N, &
                               field_strength, &
                               psi_inner_on_grid, &
                               r_value_at_r_1st, &
                               i_am_pe0_outer, i_am_in_first_outer_block, &
                               hr, &
                               expec_outer)
        USE initial_conditions, only: use_taylor_propagator

        INTEGER, INTENT(IN)      :: no_inner_fd_pts, half_fd_order
        REAL(wp), INTENT(IN)     :: del_R
        REAL(wp), INTENT(IN)     :: field_strength(3, numsols)
        REAL(wp), INTENT(IN)     :: Z_minus_N
        COMPLEX(wp), INTENT(IN)  :: psi_inner_on_grid(1:no_inner_fd_pts, my_channel_id_1st:my_channel_id_last, 1:numsols)
        LOGICAL, INTENT(IN)      :: i_am_pe0_outer, i_am_in_first_outer_block
        REAL(wp), INTENT(IN)     :: r_value_at_r_1st
        REAL(wp), INTENT(OUT)    :: hr(0:propagation_order, 0:propagation_order, 1:numsols)
        COMPLEX(wp), INTENT(OUT) :: expec_outer(1:numsols,1:3,1:2)

        IF (use_taylor_propagator) THEN
            CALL taylor_propagate_outer(no_inner_fd_pts, &
                                       half_fd_order, &
                                       del_R, &
                                       Z_minus_N, &
                                       field_strength, &
                                       psi_inner_on_grid, &
                                       r_value_at_r_1st, &
                                       i_am_pe0_outer, i_am_in_first_outer_block, &
                                       expec_outer)
        ELSE
            CALL arnoldi_propagate_outer(no_inner_fd_pts, &
                                       half_fd_order, &
                                       del_R, &
                                       Z_minus_N, &
                                       field_strength, &
                                       psi_inner_on_grid, &
                                       r_value_at_r_1st, &
                                       i_am_pe0_outer, i_am_in_first_outer_block, &
                                       hr,&
                                       expec_outer)
        END IF

        END SUBROUTINE propagate_outer

!---------------------------------------------------------------------------
! Propagate in Krylov subspace.
!---------------------------------------------------------------------------
    SUBROUTINE arnoldi_propagate_outer(no_inner_fd_pts, &
                                       half_fd_order, &
                                       del_R, &
                                       Z_minus_N, &
                                       field_strength, &
                                       psi_inner_on_grid, &
                                       r_value_at_r_1st, &
                                       i_am_pe0_outer, i_am_in_first_outer_block, &
                                       hr, &
                                       expec_outer)

        USE calculation_parameters,   ONLY: gs_refinement_iterations, &
                                            eigen_propagator_desired, &
                                            propagator_switch_limit
        USE eigenstates_in_kryspace,  ONLY: get_eigstuff_for_tridiag_h, &
                                            assemble_mat_from_eigstuff
        USE global_linear_algebra,    ONLY: decrement_V_with_cX, &
                                            decrement_V_with_zX, &
                                            increment_psi_with_zX, &
                                            self_inner_product, &
                                            real_inner_product, &
                                            local_inner_product, &
                                            inner_product
        USE initial_conditions,       ONLY: dipole_output_desired,     &
                                            dipole_velocity_output,    &
                                            dipole_dimensions_desired, &
                                            deltaR,                    &
                                            length_gauge_id,           &
                                            velocity_gauge_id,         &
                                            timings_desired,           &
                                            use_two_inner_grid_pts,    &
                                            no_of_pes_per_sector,  &
                                            debug
        USE kryspace_taylor_sums,     ONLY: cos_of_ht_times, &
                                            sin_of_ht_times, &
                                            no_of_kry_sum_segments
        USE mpi_communications,       ONLY: get_my_group_pe_id, &
                                            get_fresh_remote_bndries, &
                                            i_am_in_0_plus_first_outer_block
        USE mpi_communications,       ONLY: timeindex_val
        USE inner_to_outer_interface, ONLY: recv_psi_at_inner_fd_pts, &
                                            share_psi_at_inner_fd_pts_with_first_outer

        USE outer_to_inner_interface, ONLY: send_fderivs_outer_to_inner
        USE outer_hamiltonian,        ONLY: ham_x_vector_outer
        USE wall_clock,               ONLY: update_outer_time1, &
                                            update_outer_time2, &
                                            update_outer_time3, &
                                            update_outer_time3_inter, &
                                            update_outer_time4, &
                                            update_outer_time5, &
                                            update_outer_time6

        INTEGER, INTENT(IN)      :: no_inner_fd_pts, half_fd_order
        REAL(wp), INTENT(IN)     :: del_R
        REAL(wp), INTENT(IN)     :: field_strength(3, numsols)
        REAL(wp), INTENT(IN)     :: Z_minus_N
        COMPLEX(wp), INTENT(IN)  :: psi_inner_on_grid(1:no_inner_fd_pts, my_channel_id_1st:my_channel_id_last, 1:numsols)
        LOGICAL, INTENT(IN)      :: i_am_pe0_outer, i_am_in_first_outer_block
        REAL(wp), INTENT(IN)     :: r_value_at_r_1st
        REAL(wp), INTENT(OUT)    :: hr(0:propagation_order, 0:propagation_order, 1:numsols)
        COMPLEX(wp), INTENT(OUT) :: expec_outer(1:numsols,1:3,1:2)

        INTEGER     :: grid_length
        INTEGER     :: psi_length, my_psi_length

        ! Wave functions & ham in Krylov subspace.

        REAL(wp)    :: psi_size(1:numsols), V_size(1:numsols)

        REAL(wp)    :: dhr
        COMPLEX(wp) :: dh, h(0:propagation_order, 0:propagation_order, 1:numsols)

        COMPLEX(wp) :: kry_psi(0:propagation_order, 1:numsols)
        COMPLEX(wp) :: cos_kry_psi(0:propagation_order)
        COMPLEX(wp) :: sin_kry_psi(0:propagation_order)

        REAL(wp)    :: sin_of_eigvals(0:propagation_order)
        REAL(wp)    :: cos_of_eigvals(0:propagation_order)

        REAL(wp)    :: re_exp_h(0:propagation_order, 0:propagation_order)
        REAL(wp)    :: im_exp_h(0:propagation_order, 0:propagation_order)

        REAL(wp)    :: eigstates(0:propagation_order, 0:propagation_order, 1:numsols)
        REAL(wp)    :: eigvals(0:propagation_order, 1:numsols)

        INTEGER     :: max_eig_id, info

        INTEGER     :: j, k, m, iteration
        INTEGER     :: S, channel_id, number_channels

        COMPLEX(wp) :: h_inverse(1:propagation_order, 1:numsols)

        COMPLEX(wp) :: wave_factor

        REAL(wp)    :: cos_safe

        INTEGER     :: my_group_pe_id, i, isol

        INTEGER :: Dimension_Counter

        LOGICAL     :: get_expec

        eigvals = 0.0_wp
        eigstates = 0.0_wp

        IF (timings_desired) THEN
            CALL get_my_group_pe_id(my_group_pe_id)
        END IF
            CALL get_my_group_pe_id(my_group_pe_id)

        !-----------------------------------------------------------------------
        ! Arnoldi method for reducing the Hamilt H to matrix h in the Krylov
        ! subspace spanned by Vectors Psi, H*Psi, H**2*Psi, ...
        ! which is orthonormalized to X(0), X(1), X(2), ...
        !
        ! NOTE: if gs_refinement_iterations = 1, then the policy used below
        ! has the effect that the Taylors series should be accurately reproduced,
        ! at the possible expense of the orthonormality of X(0), X(1), X(2), ...
        !-----------------------------------------------------------------------

        number_channels = channel_id_last - channel_id_1st + 1
        grid_length = (x_last - x_1st + 1)
        psi_length = (channel_id_last - channel_id_1st + 1)*grid_length
        my_psi_length = my_num_channels * grid_length
        h = zero

        DO isol = 1, numsols
            psi_size(isol) = SQRT(self_inner_product(del_R, outer_wavefunction(:, :, isol)))
            X(:, :, isol, 0) = outer_wavefunction(:, :, isol) * (1.0_wp / (psi_size(isol) + 1.0E-90_wp))
        END DO
        h_inverse = zero
        firstderiv_psi_at_inner_bndry = zero

        ! reset the array used to transform X values to Psi values
        CALL reset_powers_of_H_array

        ! initialize h_psi_subset with psi inner on grid  values
        h_psi_subset(-no_inner_fd_pts:-1, :, :) = psi_inner_on_grid(1:no_inner_fd_pts, &
                                                                    my_channel_id_1st:my_channel_id_last, :)
        h_psi_subset(0:no_inner_fd_pts - half_fd_order - 1, :, :) = &
            outer_wavefunction(x_1st:x_1st + no_inner_fd_pts - half_fd_order - 1, :, :)
!       psi_subset_at_boundary(-2*propagation_order-2:-1,:)=psi_inner_on_grid(1:2*propagation_order+2,:)
!       psi_subset_at_boundary(0:2*propagation_order-1,:) = outer_wavefunction(x_1st:x_1st+2*propagation_order-1,:)

        ! only need to keep these values for using_H_inner case
        IF ( use_two_inner_grid_pts ) THEN
            psi_subset_at_boundary = h_psi_subset 
        END IF

        DO m = 0, propagation_order - 1

            IF (timings_desired) THEN
                CALL update_outer_time1(m)
            END IF

            ! Get Updated data at boundaries:
            CALL get_fresh_remote_bndries(X(:, :, :, m), r_bndry_lo, r_bndry_hi)

            IF (timings_desired) THEN
                CALL update_outer_time2
            END IF

            IF (i_am_in_first_outer_block) THEN ! ******* first outer_block

                ! Calculate Surface_Terms_For_Outer_Ham using information from the inner region

                SELECT CASE ( use_two_inner_grid_pts )

                CASE (.false.) 

                   CALL get_wave_surface_term &
                        (no_inner_fd_pts, &
                         half_fd_order, &
                         del_R, &
                         Z_minus_N, &
                         h_psi_subset, &
                         r_bndry_lo, &
                         field_strength, &
                         r_value_at_r_1st, &
                         propagation_order, &
                         m)

                CASE (.true.)
                    ! If (m=0) use psi_inner_on_grid
                    ! If (m>0) then need to RECV data from inner region
                    IF (m .EQ. 0) THEN
                        DO channel_id = my_channel_id_1st, my_channel_id_last  ! ******
                            ! set r_bndry_lo
                            r_bndry_lo(x_1st - 1, channel_id, :) = psi_subset_at_boundary(-1, channel_id, :)
                            r_bndry_lo(x_1st - 2, channel_id, :) = psi_subset_at_boundary(-2, channel_id, :)
                        END DO
                    ELSE
                        IF (timings_desired) THEN
                            CALL update_outer_time3
                        END IF

                        IF (no_of_pes_per_sector == 1) THEN 
                           CALL recv_psi_at_inner_fd_pts(no_inner_fd_pts, number_channels, numsols, &
                                inner_psi_at_fd_pts)
                        ELSE IF (i_am_in_0_plus_first_outer_block) THEN
                           CALL share_psi_at_inner_fd_pts_with_first_outer(no_inner_fd_pts, numsols, &
                                inner_psi_at_fd_pts)
                        END IF     

                        IF (timings_desired) THEN
                            CALL update_outer_time3_inter
                        END IF

                        DO channel_id = my_channel_id_1st, my_channel_id_last
                            ! set r_bndry_lo (NB: only works for nfdm=no_inner_fd_pts=2)
                            r_bndry_lo(x_1st - 1, channel_id, :) = inner_psi_at_fd_pts(2, channel_id, :)
                            r_bndry_lo(x_1st - 2, channel_id, :) = inner_psi_at_fd_pts(1, channel_id, :)
                        END DO
                    END IF

                END SELECT

                ! Before calling Ham_x_Vector_outer, need to transform r_bndry_lo
                ! r_bndry_lo has values of Psi and its time-derivatives
                !     at 2 inner region points
                ! Here, we need the value of X at these 2 inner points
                ! So need to transform the values according to the orthonormalization
                ! coefficients
                wave_factor = i_to_power(m)

                CALL transform_vals_psi_to_X(h, &
                                             h_inverse, &
                                             m, &
                                             r_bndry_lo, &
                                             psi_size, &
                                             wave_factor)

            END IF

            IF (timings_desired) THEN
                CALL update_outer_time3
            END IF

            ! Do the H*Psi operation

            get_expec = (m == 0 .AND. dipole_output_desired) 

            IF (get_expec) THEN
                expec_outer = (0.0_wp, 0.0_wp)
            END IF

            CALL ham_x_vector_outer(del_R, &
                                    Z_minus_N, &
                                    field_strength, &
                                    X(:, :, :, m), &
                                    h_X, &
                                    r_bndry_lo, &
                                    r_bndry_hi, &
                                    firstderiv_X_at_inner_bndry, &
                                    WE_store, &
                                    expec_psi_outer,&
                                    get_expec)

            IF (get_expec) THEN
                DO Dimension_Counter=1,3
                    DO isol = 1, numsols
                        IF (dipole_dimensions_desired(Dimension_Counter)) THEN
                            expec_psi_outer(:,:,isol,Dimension_Counter,length_gauge_id) = &
                                psi_size(isol) * expec_psi_outer(:,:,isol,Dimension_Counter,length_gauge_id)
                            expec_outer(isol, Dimension_Counter,length_gauge_id) =                        &
                                local_inner_product(my_psi_length, deltar, outer_wavefunction(:, :, isol),   &
                                expec_psi_outer(:,:,isol,Dimension_Counter,length_gauge_id))

                            IF (dipole_velocity_output) THEN
                                expec_psi_outer(:,:,isol,Dimension_Counter,velocity_gauge_id) = &
                                    psi_size(isol) * expec_psi_outer(:,:,isol,Dimension_Counter,velocity_gauge_id)
                                expec_outer(isol, Dimension_Counter,velocity_gauge_id) =                          &
                                    local_inner_product(my_psi_length, deltar, outer_wavefunction(:, :, isol),       &
                                    expec_psi_outer(:,:,isol,Dimension_Counter,velocity_gauge_id))
                            END IF
                        END IF
                    END DO
                END DO
            END IF 

            h_X = h_X*t_step2

            firstderiv_X_at_inner_bndry = firstderiv_X_at_inner_bndry*t_step2

            !  h_X = H*X(:,:,m) will become X(:,:,m+1).
            !  Must remove parts of h_X in direction X(0), X(1) ... X(m):

            DO isol = 1, numsols
                DO iteration = 1, gs_refinement_iterations
                        
                    DO k = m, MAX(0, m - 1), -1

                        !  we do k = m first, so that we accurately (is this true?)
                        !  calculate  h(m,m) = <X(m) | V> = <X(m) | H | X(m)> = real

                        !  dh should be real, so we constrain it so on 1st Iteration:
                        !  dh should be CONJG(h(m,k)), which we got previously,
                        !  so on 1st Iteration we save some time:

                        IF (iteration == 1) THEN
                            IF (k < m) THEN
                                dhr = CONJG(h(m, k, isol))
                            ELSE
                                dhr = real_inner_product(del_R, X(:, :, isol, k), h_X(:, :, isol))
                                !  dh = <X(k) | V>
                            END IF
                        ELSE
                            dh = inner_product(del_R, X(:, :, isol, k), h_X(:, :, isol)) !  dh = <X(k) | V>
                        END IF

                        IF (iteration == 1) THEN   ! check psi_length or my+psi_length
                            h(k, m, isol) = h(k, m, isol) + dhr
                            CALL decrement_V_with_cX(my_psi_length, dhr, X(:, :, isol, k), h_X(:, :, isol))

                            !  X(:,:,m+1) = X(:,:,m+1) - dhr * X(:,:,k)
                            !  X(:,:,m+1) = X(:,:,m+1) - <X(k) | V> * X(k)
                        ELSE
                            h(k, m, isol) = h(k, m, isol) + dh
                            CALL decrement_V_with_zX(my_psi_length, dh, X(:, :, isol, k), h_X(:, :, isol))
                            !  X(:,:,m+1) = X(:,:,m+1) - dh * X(:,:,k)
                            !  X(:,:,m+1) = X(:,:,m+1) - <X(k) | V> * X(k)
                        END IF

                    END DO !  k loop for GS orthogonalization of X(:,:,:,m+1)

                END DO !  Iteration loop

                V_Size(isol) = SQRT(self_inner_product(del_R, h_X(:, :, isol)))  !  sqrt ( <V | V> )
                h(m + 1, m, isol) = V_Size(isol)

                X(:, :, isol, m + 1) = h_X(:, :, isol) * (1.0_wp / (V_size(isol) + 1.0E-90_wp))

                ! Store for use later in translating inner psi values to X values
                h_inverse(m + 1, isol) = CMPLX(1.0_wp/(V_size(isol) + 1.0E-90_wp), 0.0_wp, wp)
            END DO

            IF (timings_desired) THEN
                CALL update_outer_time4
            END IF

            IF (i_am_in_first_outer_block) THEN
                ! Need to transform spatial deriv d/dr of H*X(:,:,m) into spatial deriv d/dr of H^m f_p
                ! And need to transform value of X(:,:,m) at r=b to value of f at r=b (Vel gauge only)

                CALL transform_vals_X_to_psi(h, &
                                             firstderiv_X_at_inner_bndry, &
                                             psi_size, &
                                             firstderiv_psi_at_inner_bndry, &
                                             m)

                IF (timings_desired) THEN
                    CALL update_outer_time5
                END IF

                ! Send the derivative terms to the inner region for calculating the surface terms
                ! The inner region calculates the surface terms by projecting the derivatives onto the
                ! surface amplitudes.

                CALL send_fderivs_outer_to_inner(firstderiv_psi_at_inner_bndry)

                IF (timings_desired) THEN
                    CALL update_outer_time6
                END IF

                h_psi_subset = h_psi_subset*t_step2

            END IF

        END DO !  m loop

        m = propagation_order

        IF (timings_desired) THEN
            CALL update_outer_time1(m)
        END IF

        ! Get Updated data at boundaries:
        CALL get_fresh_remote_bndries(X(:, :, :, m), r_bndry_lo, r_bndry_hi)

        IF (timings_desired) THEN
            CALL update_outer_time2
        END IF

        IF (i_am_in_first_outer_block) THEN

            SELECT CASE ( use_two_inner_grid_pts ) 

            CASE (.false.)  

                ! Calculate Surface_Terms_For_Outer_Ham using information from the inner region
                ! Take wavefunction values at 2*MaxOrder+2 grid points in inner region
                ! and 2*MaxOrder grid points in outer region and apply the outer Hamiltonian
                ! up to MaxOrder times to get the wavefunction time derivatives needed for
                ! propagating in the outer region:
!               psi_subset_at_boundary = h_psi_subset
                CALL get_wave_surface_term &
                    (no_inner_fd_pts, half_fd_order, &
                     del_R, &
                     Z_minus_N, &
                     h_psi_subset,&
                     r_bndry_lo, &
                     field_strength, &
                     r_value_at_r_1st, &
                     propagation_order, &
                     m)

            CASE (.true.)  
                ! If (m=0) use psi_inner_on_grid
                ! If (m>0) then need to RECV data from inner region
                IF (m .EQ. 0) THEN
                    DO channel_id = my_channel_id_1st, my_channel_id_last
                        ! set r_bndry_lo
                        r_bndry_lo(x_1st - 1, channel_id, :) = psi_subset_at_boundary(-1, channel_id, :)
                        r_bndry_lo(x_1st - 2, channel_id, :) = psi_subset_at_boundary(-2, channel_id, :)
                    END DO
                ELSE

                    IF (timings_desired) THEN
                        CALL update_outer_time3
                    END IF

                    IF (no_of_pes_per_sector == 1) THEN 
                        CALL recv_psi_at_inner_fd_pts(no_inner_fd_pts, number_channels, numsols, &
                             inner_psi_at_fd_pts)
                    ELSE IF (i_am_in_0_plus_first_outer_block) THEN
                        CALL share_psi_at_inner_fd_pts_with_first_outer(no_inner_fd_pts, numsols, &
                                inner_psi_at_fd_pts)
                    END IF     

                    IF (timings_desired) THEN
                        CALL update_outer_time3_inter
                    END IF

                    DO channel_id = my_channel_id_1st, my_channel_id_last
                        ! set r_bndry_lo (NB: only works for nfdm=no_inner_fd_pts=2)
                        r_bndry_lo(x_1st - 1, channel_id, :) = inner_psi_at_fd_pts(2, channel_id, :)
                        r_bndry_lo(x_1st - 2, channel_id, :) = inner_psi_at_fd_pts(1, channel_id, :)
                    END DO
                END IF
            END SELECT

            ! Before calling Ham_x_Vector_outer, need to transform r_bndry_lo
            ! r_bndry_lo has values of Psi and its time-derivatives
            !     at 2 inner region points
            ! Here, we need the value of X at these 2 inner points
            ! So need to transform the values according to the orthonormalization
            ! coefficients
            wave_factor = i_to_power(m)
            CALL transform_vals_psi_to_X(h, &
                                         h_inverse, &
                                         m, &
                                         r_bndry_lo, &
                                         Psi_Size, &
                                         wave_factor)
        END IF

        IF (timings_desired) THEN
            CALL update_outer_time3
        END IF

        ! Do the H*Psi operation

!       IF (field_strength.ne.0.0_wp) THEN

        CALL ham_x_vector_outer(del_R, &
                                Z_minus_N, &
                                field_strength, &
                                X(:, :, :, m), &
                                h_X, &
                                r_bndry_lo, &
                                r_bndry_hi, &
                                firstderiv_X_at_inner_bndry, &
                                WE_store, &
                                expec_psi_outer, &
                                .FALSE.)

        h_X = h_X*t_step2

        cos_safe =0.0_wp

        DO isol = 1, numsols
            h(m, m, isol) = real_inner_product(del_R, X(:, :, isol, m), h_X(:, :, isol))
            h(m - 1, m, isol) = CONJG(h(m, m - 1, isol))
            ! Symmetrize (h)

            DO j = 0, propagation_order
                DO i = j, propagation_order
                    h(j, i, isol) = CONJG(h(i, j, isol))
            ! select the largest value to compare against propagator_switch_limit
                    if (abs(h(j,i,isol)).gt.cos_safe) then
                            cos_safe=abs(h(j,i,isol))
                    endif
                END DO
            END DO
        END DO

        hr = REAL(h, wp)

        !-----------------------------------------------------------------------
        ! Use h as the Ham to propagate Psi.  In the krylov subspace spanned
        ! by X(0), X(1), X(2), ...  we have kry_Psi = (Psi_Size, 0, 0, ... 0)
        !
        ! Let Psi = kry_Psi
        !
        ! Get the next higher derivative of Psi,  d_Psi =-i * h * Psi
        ! Multiply it by Taylors, and then increment Psi with the
        ! result.   Taylor =  Del_t**Order / Order!
        ! Fold the -i into Taylor to get Taylor = (-i)**Order Del_t**Order / Order!
        !
        ! Use Horner's rule so we can sum the small high-order terms 1st:
        !
        !  c_0*Psi + c_1*H*Psi + c_2*H**2*Psi + ... + c_n*H**n*Psi =
        !
        !  c_0*Psi + H*(c_1*Psi + H*(c_2*Psi + ... + H*(c_n-1*Psi + H*(c_n*Psi))))))
        !
        ! c_n = Taylor_Coeff(n)  = ((-i)*Del_t)**n / n!.
        !
        ! here we use:
        !
        !  Psi + H*d_1*(Psi + H*d_2*(Psi + ... + H*d_n-1*(Psi + H*d_n*Psi))))))
        !
        ! d_n = Taylor_Coeff(n) = (-i)*Del_t / n
        !-----------------------------------------------------------------------

        IF ((eigen_propagator_desired) .or.(abs(cos_safe*t_step2).gt.propagator_switch_limit)) THEN

            IF (i_am_pe0_outer) THEN
                print *,' Propagator switched ',abs(cos_safe*t_step2)
            END IF

            DO isol = 1, numsols
                CALL get_eigstuff_for_tridiag_h(hr(:, :, isol), eigstates(:, :, isol), eigvals(:, isol), info)
            END DO

        ELSE

            info = 1 !  Do ELSE part of next IF

        END IF

        IF (info == 0) THEN

            max_eig_id = propagation_order

            DO isol = 1, numsols
                cos_of_eigvals = COS(eigvals(:, isol) * t_step)
                sin_of_eigvals = SIN(eigvals(:, isol) * t_step)

                CALL assemble_mat_from_eigstuff(eigstates(:, :, isol), cos_of_eigvals, max_eig_id, re_exp_h)
                CALL assemble_mat_from_eigstuff(eigstates(:, :, isol), sin_of_eigvals, max_eig_id, im_exp_h)

                kry_psi(:, isol) = psi_size(isol) * (re_exp_h(:, 0) - im*im_exp_h(:, 0))
            END DO

        ELSE

            !  either eigen_propagator_desired = .false., or CALL to
            !  Eigen routine reported failure (INFO > 0).
            !  Either way we do the following:
            !
            ! exp(-iht) * Psi  = Cos(ht) * Psi - i Sin(ht) * Psi
            !
            !  The call only does the series to dt = Del_t/No_of_Kry_Sum_Segments
            !  So, must be repeated No_of_Kry_Sum_Segments times
!           DO i = 1, no_of_kry_sum_segments
!               CALL exp_of_minus_iht_times (hr, kry_psi, exp_kry_psi)
!               kry_psi  =  exp_kry_psi
!           END DO

            kry_psi = zero
            kry_psi(0, :) = psi_size(:)

            DO isol = 1, numsols
                DO i = 1, no_of_kry_sum_segments
                    CALL cos_of_ht_times(hr(:, :, isol), kry_psi(:, isol), cos_kry_psi)
                    CALL sin_of_ht_times(hr(:, :, isol), kry_psi(:, isol), sin_kry_psi, t_step)
                    kry_psi(:, isol) = cos_kry_psi - im*sin_kry_psi
                END DO
            END DO

        END IF

        !-----------------------------------------------------------------------
        ! Let matrix U = ( X(0) X(1) X(2) ... X(m) )  (m+1 column vectors)
        !
        ! We have used   Ham  ==  U h Adjoint(U)
        !          Ham Psi = U h Adjoint(U) Psi = U h kry_Psi
        !       Ham**k Psi = U h**k kry_Psi
        ! Next:  Psi =  U kry_Psi
        !
        ! sum the small high-order terms 1st:
        !-----------------------------------------------------------------------

        !   Keep Psi is ram cache as much as possible.  Don't read
        !   and write it propagation_order+1 times:

        DO isol = 1, numsols
            !$OMP PARALLEL DO PRIVATE(S, m)
            DO S = my_channel_id_1st, my_channel_id_last
                outer_wavefunction(:, S, isol) = kry_psi(propagation_order, isol) * X(:, S, isol, propagation_order)
                DO m = propagation_order - 1, 0, -1   
                    CALL increment_psi_with_zX(grid_length, kry_psi(m, isol), X(:, S, isol, m), outer_wavefunction(:, S, isol))
                END DO
            END DO
        END DO

        ! outer_wavefunction = kry_psi(propagation_order) * X(:,:,propagation_order)
        ! DO m = propagation_order-1, 0, -1
        !   outer_wavefunction = outer_wavefunction + kry_psi(m) * X(:,:,m)
        ! END DO

!       trunc_err1 = SQRT (REAL (kry_psi(propagation_order)*CONJG(kry_psi(propagation_order)), wp))

        IF (timings_desired) THEN
            CALL update_outer_time4
        END IF

    END SUBROUTINE arnoldi_propagate_outer

!---------------------------------------------------------------------------

    !> \brief Propagate the outer-region wavefunction using a Taylor series propagator with maximum order
    !>        given by the parameter propagation_order.
    !>
    !> \param[in]     no_inner_fd_pts    number of grid points extending into the inner region
    !> \param[in]     half_fd_order      Parameter related to the finite-difference order (n), half_fd_order = (n-1)/2 
    !> \param[in]     del_r              grid spacing deltar
    !> \param[in]     z_minus_n          net charge
    !> \param[in]     field_strength     Electric field strength at this timestep
    !> \param[in]     psi_inner_on_grid  Inner-region wavefunction on grid points
    !> \param[in]     r_value_at_r_1st   Value of the radial coordinate r at the first outer-region grid point 
    !> \param[in]     i_am_pe0_outer     determine if the pe is the first assigned to the outer region
    !> \param[out]    expec_outer        dipole expectation value calculated from the outer-region wavefunction


    SUBROUTINE taylor_propagate_outer(no_inner_fd_pts, &
                                      half_fd_order, &
                                      del_r, &
                                      z_minus_n, &
                                      field_strength, &
                                      psi_inner_on_grid, &
                                      r_value_at_r_1st, &
                                      i_am_pe0_outer, i_am_in_first_outer_block, &
                                      expec_outer)

         USE global_linear_algebra,    ONLY: local_inner_product, &
                                             self_inner_product
         USE initial_conditions,       ONLY: dipole_output_desired, &
                                             dipole_velocity_output, &
                                             dipole_dimensions_desired, &
                                             length_gauge_id, &
                                             velocity_gauge_id, &
                                             propagation_order, &
                                             use_two_inner_grid_pts, &
                                             no_of_pes_per_sector
         USE inner_to_outer_interface, ONLY: recv_psi_at_inner_fd_pts, &
                                             share_psi_at_inner_fd_pts_with_first_outer
         USE mpi_communications,       ONLY: get_fresh_remote_bndries, i_am_in_0_plus_first_outer_block 

         USE outer_hamiltonian,        ONLY: ham_x_vector_outer
         USE outer_to_inner_interface, ONLY: send_fderivs_outer_to_inner

         INTEGER, INTENT(IN)      :: no_inner_fd_pts, half_fd_order
         REAL(wp), INTENT(IN)     :: del_r, z_minus_n, r_value_at_r_1st
         REAL(wp), INTENT(IN)     :: field_strength(3, numsols)
         COMPLEX(wp), INTENT(IN)  :: psi_inner_on_grid(no_inner_fd_pts, my_channel_id_1st:my_channel_id_last, numsols)
         LOGICAL, INTENT(IN)      :: i_am_pe0_outer, i_am_in_first_outer_block
         COMPLEX(wp), INTENT(OUT) :: expec_outer(1:numsols,1:3,1:2)

         COMPLEX(wp) :: psi_dot(x_1st:x_last, my_channel_id_1st:my_channel_id_last, numsols)
         COMPLEX(wp) :: h_psi_dot(x_1st:x_last, my_channel_id_1st:my_channel_id_last, numsols)
         COMPLEX(wp) :: first_deriv_of_psi_dot_at_b(my_channel_id_1st:my_channel_id_last, numsols)
         INTEGER     :: channel_id, idip, isol, order
         INTEGER     :: grid_length, number_channels, psi_length, my_psi_length
         LOGICAL     :: get_expec

         grid_length = x_last - x_1st + 1
         number_channels = channel_id_last - channel_id_1st + 1
         psi_length = number_channels*grid_length
         my_psi_length = my_num_channels * grid_length

!        Initially set psi_dot as the zeroth-order time derivative
!        of the outer-region wavefunction
         psi_dot = outer_wavefunction

         h_psi_subset(-no_inner_fd_pts:-1, :, :) = psi_inner_on_grid(1:no_inner_fd_pts, &
                                                                 my_channel_id_1st:my_channel_id_last, :)
         h_psi_subset(0:no_inner_fd_pts - half_fd_order - 1, :, :) = &
                  outer_wavefunction(x_1st:x_1st + no_inner_fd_pts - half_fd_order - 1, :, :)

         IF (use_two_inner_grid_pts) THEN
             psi_subset_at_boundary = h_psi_subset
         END IF

!        Loop over propagator orders. For each order, the (order+1)th term in the series
!        will be calculated, and added to the outer-region wavefunction, so the loop over
!        orders extends from 0 to propagation_order - 1
         DO order = 0, propagation_order - 1

             CALL get_fresh_remote_bndries(psi_dot, r_bndry_lo, r_bndry_hi)

             IF (i_am_in_first_outer_block) THEN

                 SELECT CASE (use_two_inner_grid_pts)

                 CASE (.true.)

                     IF (order .EQ. 0) THEN
                         r_bndry_lo(x_1st - 1, :, :) = psi_subset_at_boundary(-1, :, :)
                         r_bndry_lo(x_1st - 2, :, :) = psi_subset_at_boundary(-2, :, :)
                     ELSE
                        r_bndry_lo(x_1st - 1, :, :) = &
                                       inner_psi_at_fd_pts(2, my_channel_id_1st:my_channel_id_last, :)
                        r_bndry_lo(x_1st - 2, :, :) = &
                                       inner_psi_at_fd_pts(1, my_channel_id_1st:my_channel_id_last, :)
                     END IF

                 CASE (.false.)

                     CALL get_wave_surface_term &
                          (no_inner_fd_pts, &
                           half_fd_order, &
                           del_r, &
                           z_minus_n, &
                           h_psi_subset, &
                           r_bndry_lo, &
                           field_strength, &
                           r_value_at_r_1st, &
                           propagation_order, &
                           order)

                 END SELECT

             END IF

             get_expec = (order == 0 .AND. dipole_output_desired)

             IF (get_expec) expec_outer = zero

             CALL ham_x_vector_outer(del_r, z_minus_n, field_strength, psi_dot, &
                                     h_psi_dot, r_bndry_lo, r_bndry_hi, &
                                     first_deriv_of_psi_dot_at_b, &
                                     WE_store, &
                                     expec_psi_outer, &
                                     get_expec)

             IF (get_expec) THEN   ! ????????????????? ************************ ??????????????
                ! This needs testing with channel parallelization (MP didn't have any Taylor test cases
                 DO idip = 1, 3
                     DO isol = 1, numsols
                         IF (dipole_dimensions_desired(idip)) THEN
                             expec_outer(isol, idip, length_gauge_id) =        &
                                 local_inner_product(psi_length, del_r, outer_wavefunction(:, :, isol), &
                                 expec_psi_outer(:, :, isol, idip, length_gauge_id))

                             IF (dipole_velocity_output) THEN
                                 expec_outer(isol, idip, velocity_gauge_id) =        &
                                     local_inner_product(psi_length, del_r, outer_wavefunction(:, :, isol), &
                                     expec_psi_outer(:, :, isol, idip, velocity_gauge_id))
                             END IF
                         END IF
                     END DO
                 END DO
             END IF

             first_deriv_of_psi_dot_at_b = first_deriv_of_psi_dot_at_b * t_step2

             IF (i_am_in_first_outer_block) THEN
                 CALL send_fderivs_outer_to_inner(first_deriv_of_psi_dot_at_b)
                 IF (use_two_inner_grid_pts) THEN
                    IF (no_of_pes_per_sector == 1) THEN 
                        CALL recv_psi_at_inner_fd_pts(no_inner_fd_pts, number_channels, numsols, &
                             inner_psi_at_fd_pts)
                    ELSE IF (i_am_in_0_plus_first_outer_block) THEN
                        CALL share_psi_at_inner_fd_pts_with_first_outer(no_inner_fd_pts, numsols, &
                                inner_psi_at_fd_pts)
                    END IF     
                 END IF
                 h_psi_subset = h_psi_subset * t_step2
             END IF

!            Calculate the next order term  (-iH*psi(order)*dt)
             psi_dot = -im * h_psi_dot * t_step2

!            multiply by the Taylor coefficient for the next order and increment outer wavefunction
             outer_wavefunction = outer_wavefunction + taylor_coeff(order+1) * psi_dot

         END DO


    END SUBROUTINE taylor_propagate_outer

!---------------------------------------------------------------------------

    SUBROUTINE transform_vals_psi_to_X(kry_h, kry_h_inverse, order, psi_vals, psi_norm, prefactor)
         use mpi_communications, only: get_my_group_pe_id
        COMPLEX(wp), INTENT(IN)      :: kry_h_inverse(1:propagation_order, 1:numsols)
        COMPLEX(wp), INTENT(IN)      :: kry_h(0:propagation_order, 0:propagation_order, 1:numsols)
        INTEGER, INTENT(IN)          :: order
        COMPLEX(wp), INTENT(INOUT)   :: psi_vals(x_1st - 2:x_1st - 1, &
                                                 my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(IN)         :: psi_norm(numsols)
        COMPLEX(wp), INTENT(IN)      :: prefactor

        INTEGER :: row_index, col_index, diagonal_index, isol
        INTEGER :: my_group_pe_id

        ! Set up a matrix of values of H^j X(i) at the 2 inner points
        ! The matrix will be upper left triangular
        ! If we save these values at each order then they can be re-used for calculating
        ! the higher order X values at the inner points
        ! Use relation H^j X(i) = (H^(j+1) X(i-1) - h(i-1,i-1)H^j X(i-1) - h(i-2,i-1)H^j X(i-2)) / h(i,i-1)
        ! Fill the array diagonally
        !     ie for order m, save H^m X(0), H^(m-1) X(1), ..., H X(m-1), X(m)

        ! The first row contains X(0), H X(0), H^2 X(0), ... so is directly related to what is
        ! passed from the inner region: : h_psi_b([1:2],:,m) = f^(m)(r=b-1,b-2)
        !      ie h_psi_b([1:2],:,m) = value of mth time derivative of f at r=b-1 and r=b-2
        ! H^m X(0) = Im^m h_psi_b(:,:,m) / Psi_Size --- WAVE EQN

        call get_my_group_pe_id(my_group_pe_id)
        DO isol = 1, numsols
            row_index = 0
            col_index = order
            powers_of_H_array2(:, :, isol, row_index, col_index) = &
                prefactor * psi_vals(:, :, isol) * (1.0_wp/(psi_norm(isol) + 1.0E-90_wp))

            ! The second row contains X(1), H X(1), H^2 X(1), ...
            ! Use H^(m-1) X(1) = (H^m X(0) - h00 H^(m-1) X(0)) / h10
            IF (order .GT. 0) THEN
                row_index = 1
                col_index = order - 1
                powers_of_H_array2(:, :, isol, row_index, col_index) = &
                    (powers_of_H_array2(:, :, isol, row_index - 1, col_index + 1) - &
                    kry_h(row_index - 1, row_index - 1, isol) * powers_of_H_array2(:, :, isol, row_index - 1, col_index)) * &
                    (kry_h_inverse(row_index, isol))
            END IF

            ! Fill rest of the array - last entry filled is the one required for passing to ham_x_Vector_outer
            IF (order .GT. 1) THEN
                DO diagonal_index = 2, order
                    row_index = diagonal_index
                    col_index = order - diagonal_index
                    powers_of_H_array2(:, :, isol, row_index, col_index) = &
                        (powers_of_H_array2(:, :, isol, row_index - 1, col_index + 1) - &
                        kry_h(row_index - 1, row_index - 1, isol)*powers_of_H_array2(:, :, isol, row_index - 1, col_index) - &
                        kry_h(row_index - 2, row_index - 1, isol)*powers_of_H_array2(:, :, isol, row_index - 2, col_index))* &
                        (kry_h_inverse(row_index, isol))
                END DO
            END IF

            psi_vals(:, :, isol) = powers_of_H_array2(:, :, isol, order, 0)
        END DO

    END SUBROUTINE transform_vals_psi_to_X

!---------------------------------------------------------------------------

    SUBROUTINE transform_vals_X_to_psi(kry_h, x_vals, psi_norm, psi_vals, order)

        COMPLEX(wp), INTENT(IN)    :: kry_h(0:propagation_order, 0:propagation_order, 1:numsols)
        COMPLEX(wp), INTENT(IN)    :: x_vals(my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(IN)       :: psi_norm(1:numsols)
        COMPLEX(wp), INTENT(OUT)   :: psi_vals(my_channel_id_1st:my_channel_id_last, 1:numsols)
        INTEGER, INTENT(IN)        :: order

        INTEGER :: diagonal_index, row_index, col_index, isol

        ! Need to transform spatial deriv of H*X(:,:,m) into spatial deriv of H^m f_p
        ! And need to transform value of X(:,:,m) at r=b to value of f at r=b (Vel gauge only)
        ! Set up a matrix of values of H^j dX(i)/dr on the boundary
        ! The matrix will be upper left triangular
        ! If we save these values at each order then they can be re-used for calculating
        ! the higher order H^n dX/dr values
        ! Use relation H^j X(i) = h(i+1,i)H^(j-1)X(i+1) + h(i,i)H^(j-1)X(i) + h(i-1,i)H^(j-1)X(i-1)
        ! Fill the array diagonally
        !     ie for order m, save dX(m)/dr, H dX(m-1)/dr, H^2 dX(m-2)/dr, ...
        ! The first row contains dX(0)/dr, H dX(0)/dr, H^2 dX(0)/dr, ... so is directly
        ! related to what we need to pass to the inner region:
        ! df/dr(:,:,m) =  (-Im)^m * H^m f = (-Im)^m * H^m dX(0)/dr * Psi_Size --- WAVE EQN
        ! df/dr(:,:,m) =  (-1)^m * H^m f = (-Im)^m * H^m dX(0)/dr * Psi_Size --- DIFFUSION EQN
        ! Only coded now for the wave equation

        powers_of_H_array1(:, :, order, 0) = x_vals

        DO isol = 1, numsols
            DO diagonal_index = 1, order - 1
                row_index = order - diagonal_index
                col_index = diagonal_index
                powers_of_H_array1(:, isol, row_index, col_index) = &
                    kry_h(row_index + 1, row_index, isol)*powers_of_H_array1(:, isol, row_index + 1, col_index - 1) + &
                    kry_h(row_index, row_index, isol)*powers_of_H_array1(:, isol, row_index, col_index - 1) + &
                    kry_h(row_index - 1, row_index, isol)*powers_of_H_array1(:, isol, row_index - 1, col_index - 1)
            END DO
            ! And now do first row
            IF (order .GT. 0) THEN
                row_index = 0
                col_index = order
                powers_of_H_array1(:, isol, row_index, col_index) = &
                    kry_h(row_index + 1, row_index, isol)*powers_of_H_array1(:, isol, row_index + 1, col_index - 1) + &
                    kry_h(row_index, row_index, isol)*powers_of_H_array1(:, isol, row_index, col_index - 1)
            END IF

            psi_vals(:, isol) = minus1_to_power(order) * i_to_power(order) * &
                                psi_norm(isol) * powers_of_H_array1(:, isol, 0, order)
        END DO

    END SUBROUTINE transform_vals_X_to_psi

!---------------------------------------------------------------------------

    SUBROUTINE get_wave_surface_term(no_inner_fd_pts, half_fd_order, &
                                     delR, Z_minus_N, new_psi, h_psi_b_for_outer, &
                                     field_strength, & 
                                     r_value_at_r_1st, &
                                     max_propagation_order, order)

        USE outer_hamiltonian_atrlessthanb, ONLY: ham_x_vec_outer_b
        USE mpi_communications,             ONLY: get_my_group_pe_id, i_am_in_first_outer_block
        
        INTEGER, INTENT(IN)       :: no_inner_fd_pts, half_fd_order
        REAL(wp), INTENT(IN)      :: delR
        REAL(wp), INTENT(IN)      :: Z_minus_N
        INTEGER, INTENT(IN)       :: max_propagation_order, order
        COMPLEX(wp), INTENT(OUT)  :: h_psi_b_for_outer(x_1st - 2:x_1st - 1, &
                                                       my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp),    INTENT(IN)   :: field_strength(3, numsols)
        REAL(wp), INTENT(IN)      :: r_value_at_r_1st
        COMPLEX(wp), INTENT(INOUT)  :: new_psi(-no_inner_fd_pts:no_inner_fd_pts - half_fd_order - 1, &
                                              my_channel_id_1st:my_channel_id_last, 1:numsols)
        
        COMPLEX(wp)               :: h_new_psi(-(2*max_propagation_order + 2):2*max_propagation_order - 1, &
                                                 my_channel_id_1st:my_channel_id_last, 1:numsols)
        INTEGER                   :: channel_id, i
        COMPLEX(wp)               :: minus_i
        COMPLEX(wp)               :: side_grid_points(1:4, my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp)                  :: r_at_first_point
        INTEGER                   :: number_points_for_H_op, start_index
        integer   :: my_group_pe_id, isol 
        !EX!
         call get_my_group_pe_id(my_group_pe_id)
        
        ! Pass in psi that is made up of (2m+2) wavefunction values in the inner region and
        ! 2m wavefunction values in the outer region.  m=max_propagation_order
        ! psi(0) is really psi(x_1st)

        ! Apply the outer Hamiltonian max_propagation_order times to obtain the time derivatives of f
        ! at grid points x_1st-1 and x_1st-2 required for propagation in the outer region

        ! Every time we apply H to a central subset of points and the 2 grid points immediately to the
        ! left and to the right of this subset are passed in to the Ham_x_Vec routines and used in
        ! the calculation of derivatives - in effect used as the surface terms here in the same way
        ! that we will use the actual surface terms in the actual propagation
        ! To apply H max_propagation_order times means we need (2m+2) wavefunction values as the FD rules
        ! need an extra 2 grid points on each iteration.

        h_psi_b_for_outer = zero
        h_new_psi = zero

        minus_i = -im

        IF (order .EQ. 0) THEN ! Order = 0

            ! Set h_psi_b(1) = new_psi(-1) and h_psi_b(2) = new_psi(-2)
!$OMP       PARALLEL DO PRIVATE(channel_id)
            DO channel_id = my_channel_id_1st, my_channel_id_last
                ! set surface terms to be used by outer propagation:
                h_psi_b_for_outer(x_1st - 1, channel_id, :) = new_psi(-1, channel_id, :)
                h_psi_b_for_outer(x_1st - 2, channel_id, :) = new_psi(-2, channel_id, :)
            END DO
!$OMP       END PARALLEL DO

        ELSE  ! order = 1, ..., propagation_order

            number_points_for_H_op = 2*no_inner_fd_pts - half_fd_order - 2*half_fd_order*Order
            r_at_first_point = r_value_at_r_1st + delR*REAL(-no_inner_fd_pts + half_fd_order*Order, wp)
            start_index = half_fd_order* Order + 1

!$OMP       PARALLEL DO PRIVATE(channel_id)
            DO channel_id = my_channel_id_1st, my_channel_id_last
                ! set surface terms to be used locally for calculating H*psi
                side_grid_points(1, channel_id, :) = new_psi(-no_inner_fd_pts + half_fd_order*order - 2, channel_id, :)
                side_grid_points(2, channel_id, :) = new_psi(-no_inner_fd_pts + half_fd_order*order - 1, channel_id, :)
                side_grid_points(3, channel_id, :) = new_psi(no_inner_fd_pts - half_fd_order - half_fd_order*order, channel_id, :)
                side_grid_points(4, channel_id, :) = new_psi(no_inner_fd_pts - half_fd_order - half_fd_order*order + 1,channel_id,:)
            END DO
!$OMP       END PARALLEL DO

            new_psi_b = 0.0_wp
            h_new_psi_b = 0.0_wp

!$OMP       PARALLEL DO PRIVATE(i)
            DO i = 1, number_points_for_H_op
                new_psi_b(i, :, :) = new_psi(i - no_inner_fd_pts + half_fd_order*Order - 1, :, :)
            END DO
!$OMP       END PARALLEL DO

            CALL ham_x_vec_outer_b(delR, &
                                   Z_minus_N, &
                                   field_strength, &
                                   new_psi_b(1:number_points_for_H_op,:, :), &
                                   h_new_psi_b(1:number_points_for_H_op,:, :), &
                                   side_grid_points, &
                                   number_points_for_H_op, &
                                   r_at_first_point,&
                                   wb_store(start_index:start_index + number_points_for_H_op -1, :,:))
            
!$OMP       PARALLEL DO PRIVATE(i)
            DO i = 1, number_points_for_H_op
                h_new_psi(i - no_inner_fd_pts + half_fd_order*Order - 1, :, :) = h_new_psi_b(i, :, :)
            END DO
!$OMP       END PARALLEL DO

            new_psi = minus_i*h_new_psi   ! WAVE EQUATION
            ! Set h_psi_b(1) = new_psi(-1) and h_psi_b(2) = new_psi(-2)
!$OMP       PARALLEL DO PRIVATE(channel_id)
            DO channel_id = my_channel_id_1st, my_channel_id_last
                h_psi_b_for_outer(x_1st - 1, channel_id, :) = new_psi(-1, channel_id, :)
                h_psi_b_for_outer(x_1st - 2, channel_id, :) = new_psi(-2, channel_id, :)
            END DO
!$OMP       END PARALLEL DO

        END IF

    END SUBROUTINE get_wave_surface_term

END MODULE outer_propagators
