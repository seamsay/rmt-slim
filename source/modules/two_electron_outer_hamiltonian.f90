! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Performs operations associated with the two-electron outer region Hamiltonian 
!> (i.e. performing the Hamiltonian-wavefunction multiplication, incrementing with the Laplacian,
!> the centrifugal terms, and the long range potential matrices).


MODULE two_electron_outer_hamiltonian

    USE rmt_assert,             ONLY: assert
    USE precisn,                ONLY: wp
    USE global_data,            ONLY: zero
    USE grid_parameters,        ONLY: x_1st, &
                                      x_last
    USE coupling_rules,         ONLY: field_contraction
    USE initial_conditions,     ONLY: numsols => no_of_field_confs
    USE local_ham_matrix,       ONLY: RR, Rinverse, Rinverse2, &
                                      r_greater, r_less_over_r_greater
    USE potentials,             ONLY: makewemat_reg3, &
                                      makewpmat_reg3
    USE readhd,                 ONLY: two_electron_nchan, &
                                      first_l, &
                                      second_l, &
                                      tot_ml
    USE lrpots,                 ONLY: ksq, &
                                      region_three_we_coupled, &
                                      region_three_wp_coupled

    IMPLICIT NONE

    INTEGER, ALLOCATABLE, SAVE     :: twoe_wp_neighbour_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: twoe_we_neighbour_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: twoe_wp_neighbour_store(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: twoe_we_neighbour_store(:,:)

    PUBLIC setup_region_three_neighbour_counts_and_store

CONTAINS

!---------------------------------------------------------------------

    !> \brief perform hamiltonian times wavefunction multiplication for two electron outer region
    !>
    !> This multiplication is
    !> \f[ \left[ -\frac{1}{2}\frac{d^2}{dr_{N+1}^2} - frac{1}{2}\frac{d^2}{dr_{N+2}^2}
    !>            + \frac{l_a(la + 1)}{2r_{N+1}^2} + \frac{lb(lb + 1)}{2r_{N+2}^2}
    !>            - \frac{Z-N}{r_{N+1}} - \frac{Z-N}{r_{N+2}} + 0.5*k^2
    !>    \right]\Psi_p
    !>    + \sum_{p'} \left[W^E_{pp'} + W^P_{pp'} + W^D_{pp'}\right]\Psi_{p'}
    !>  \f]
    !> where $p,p'$ run over all two-electron channels.
    !>
    !> For each solution (wavefunction) and each channel, apply the centrigual barrier term,
    !> the energy shift and the long range potential matrices.
    !> 
    !> \param[in]     delR               Outer region grid spacing
    !> \param[in]     Z_minus_N          Net charge of the residual ion
    !> \param[in]     field_strength     Electric Field Strength in a.u.
    !> \param[in]     psi_double_outer   Wavefunction on local portion of the two-electron grid. 
    !>                                   dimensions (grid points, grid points, channels, solutions)
    !> \param[out]    h_psi_double_outer Hamiltonian times the wavefunction on local portion of the two-electron grid. 
    !>                                   dimensions (grid points, grid points, channels, solutions)
    !>
    !######################################
    !>
    !> \param[inout]  wf_halo_left       Wavefunction on two grid points inside the one-electron outer region (horizontal axis)
    !> \param[inout]  wf_halo_right      Wavefunction on two grid points beyond the outer edge of the two-electron outer region (horizontal axis)
    !>
    !> \param[inout]  wf_halo_bottom     Wavefunction on two grid points inside the one-electron outer region (vertical axis)
    !> \param[inout]  wf_halo_top        Wavefunction on two grid points beyond the outer edge of the two-electron outer region (vertical axis)
    !>
    !######################################
    !> \param[in]     WE_store_reg3      Saved values for the WE long range potential matrix
    !>                                   dimensions (grid points, grid points, width, channels)
    !>
    SUBROUTINE ham_x_vector_double_outer(delR, z_minus_n, field_strength, psi_double_outer, h_psi_double_outer, &
                                         wf_halo_left, wf_halo_right, &
                                         wf_halo_bottom, wf_halo_top, &
                                         WE_store_reg3)

      USE lrpots, ONLY: region_three_we_width

      REAL(wp), INTENT(IN)       :: delR, Z_minus_N
      REAL(wp), INTENT(IN)       :: field_strength(3, numsols)
      COMPLEX(wp), INTENT(IN)    :: psi_double_outer(x_1st:x_last, x_1st:x_last, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(OUT)   :: h_psi_double_outer(x_1st:x_last, x_1st:x_last, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(INOUT) :: wf_halo_left(x_1st-2:x_1st-1, x_1st:x_last, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(INOUT) :: wf_halo_right(x_last+1:x_last+2, x_1st:x_last, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(INOUT) :: wf_halo_bottom(x_1st:x_last, x_1st-2:x_1st-1, two_electron_nchan, numsols)
      COMPLEX(wp), INTENT(INOUT) :: wf_halo_top(x_1st:x_last, x_last+1:x_last+2, two_electron_nchan, numsols)
      REAL(wp), INTENT(IN)       :: WE_store_reg3(x_1st:x_last, x_1st:x_last, region_three_we_width, two_electron_nchan)

      INTEGER                    :: channel_id, isol
      INTEGER                    :: la, lb
      INTEGER                    :: number_grid_points
      REAL(wp)                   :: z_minus_n_over_r(x_1st:x_last)
      LOGICAL                    :: field_on

      field_on = ANY(field_strength /= 0.0_wp)

      z_minus_n_over_r = Z_minus_N * Rinverse
      number_grid_points = x_last - x_1st + 1

      DO isol = 1, numsols
         DO channel_id = 1, two_electron_nchan

             ! [-0.5*grad_{N+1}^2 -0.5*grad_{N+2}^2 ] psi
             CALL apply_two_electron_laplacian  &
                  (psi_double_outer(:, :, channel_id, isol), &
                   h_psi_double_outer(:, :, channel_id, isol), &
                   wf_halo_left(:, :, channel_id, isol), &
                   wf_halo_right(:, :, channel_id, isol), &
                   wf_halo_bottom(:, :, channel_id, isol), &
                   wf_halo_top(:, :, channel_id, isol), &
                   la, &
                   lb, &
                   number_grid_points, &
                   delR)

             ! - ( (Z-N)/r_(N+1) + (Z-N)/r_(N+2) + 1/2 k^2 ) psi
             CALL apply_two_electron_centrifugal_and_energy_shift &
                  (number_grid_points, &
                  psi_double_outer(:, :, channel_id, isol), &
                  h_psi_double_outer(:, :, channel_id, isol), &
                  z_minus_n_over_r, &
                  ksq(channel_id))

            CALL apply_two_electron_long_range_potentials &
                 (number_grid_points, &
                  channel_id, &
                  field_strength(:, isol), &
                  field_on, &
                  RR, &
                  WE_store_reg3, &
                  psi_double_outer(:, :, :, isol), &
                  h_psi_double_outer(:, :, channel_id, isol))
         END DO
      END DO

    END SUBROUTINE ham_x_vector_double_outer

!---------------------------------------------------------------------

    !> \brief build arrays which decide which channels are coupled
    !> 
    !> Instead of running through every possible channel at run-time and determining whether it is coupled to the current channel,
    !> we calculate ahead of time for each channel, which other channels are its "neighbours" i.e. which have a non-zero coupling
    !> for each of the three potential matrices. The result is two arrays each for WE, WP and WD. There is a neighbour_count of
    !> dimension (number of channels) which for each channel stores the number of neighbours. And there is a neighbour_store of 
    !> dimension (number of channels, number of channels) which stores for channel_i, the list of which channel_js are coupled. 
    !######################
    ! (for later: neigbour_store should be shrinkable in the second dimension I think: see
    ! outer_hamiltonian to see how it's done for the one-electron outer region
    ! #####################
    SUBROUTINE setup_region_three_neighbour_counts_and_store

        USE lrpots, ONLY: region_three_we_width, region_three_wp_width

        INTEGER     :: channel_i, channel_j
        INTEGER     :: coupling_index
        REAL(wp)    :: nonzero = 1.0e-14_wp
        REAL(wp)    :: we_ham3
        COMPLEX(wp) :: wp_ham3(3)

        CALL allocate_region_three_neighbour_stores
        ! WE
        DO channel_i = 1, two_electron_nchan

            coupling_index = 0
            DO channel_j = 1, two_electron_nchan

                IF (ABS(channel_i - channel_j) <= region_three_we_width ) THEN
                    CALL makewemat_reg3(r_greater(x_1st, x_1st), r_less_over_r_greater(x_1st, x_1st), &
                                        channel_i, channel_j, we_ham3)
                ELSE
                    we_ham3 = 0.0_wp
                END IF

                IF  (ABS(WE_ham3) > nonzero) THEN
                    IF (region_three_we_coupled(channel_i, channel_j)) THEN
                        coupling_index = coupling_index + 1
                        twoe_we_neighbour_count(channel_i) = coupling_index
                        twoe_we_neighbour_store(channel_i,coupling_index) = channel_j
                    END IF
                END IF
            END DO
        END DO


        ! WP
        DO channel_i = 1, two_electron_nchan

            coupling_index = 0
            DO channel_j = 1, two_electron_nchan

                IF (ABS(channel_i - channel_j) <= region_three_wp_width) THEN
                    CALL makewpmat_reg3(channel_i, channel_j, wp_ham3)
                ELSE
                    wp_ham3 = zero
                END IF

                IF ( any(ABS(wp_ham3) > nonzero) ) THEN
                    IF (region_three_wp_coupled(channel_i, channel_j)) THEN
                        coupling_index = coupling_index + 1
                        twoe_wp_neighbour_count(channel_i) = coupling_index
                        twoe_wp_neighbour_store(channel_i,coupling_index) = channel_j
                    END IF
                END IF
            END DO
        END DO

        region_three_we_width = MAXVAL(twoe_we_neighbour_count)
        region_three_wp_width = MAXVAL(twoe_wp_neighbour_count)

    END SUBROUTINE setup_region_three_neighbour_counts_and_store

!---------------------------------------------------------------------

    !> \brief apply the two-electron Laplacian for each two-electron channel and each solution
    !>
    !> \param[in]     psi                 two-electron wavefunction for a single channel and solution on local 2D grid
    !>                                    dimension(number_grid_points, number_grid_points)
    !> \param[inout]  H_psi               Hamiltonian times psi on local 2D grid
    !>                                    dimension(number_grid_points, number_grid_points)
    !> \param[in]     la                  orbital angular momentum of electron N+1
    !> \param[in]     lb                  orbital angular momentum of electron N+2
    !> \param[in]     wf_halo_left        Two-electron wavefunction values at two points in the one-electron outer region
    !> \param[in]     wf_halo_right       Two-electron wavefunction values at two points outside the two-electron outer region
    !> \param[in]     wf_halo_bottom      Two-electron wavefunction values at two points in the one-electron outer region
    !> \param[in]     wf_halo_top         Two-electron wavefunction values at two points outside the two-electron outer region
    !> \param[in]     number_grid_points  number of radial grid points spanned in each dimension on local 2D grid
    !> \param[in]     delR                radial grid spacing

    SUBROUTINE apply_two_electron_laplacian(psi, H_psi, &
               wf_halo_left, wf_halo_right, &
               wf_halo_bottom, wf_halo_top, &
               la, lb, &
               number_grid_points, delR)

        USE local_ham_matrix, ONLY: incr_with_laplacian

        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, number_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_grid_points, number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: wf_halo_left(x_1st - 2:x_1st - 1, number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: wf_halo_right(x_last + 1:x_last + 2, number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: wf_halo_bottom(number_grid_points, x_1st - 2:x_1st - 1)
        COMPLEX(wp), INTENT(IN)    :: wf_halo_top(number_grid_points, x_last + 1:x_last + 2)
        INTEGER, INTENT(IN)        :: la, lb
        INTEGER, INTENT(IN)        :: number_grid_points
        REAL(wp), INTENT(IN)       :: delR

        INTEGER     :: j
        REAL(wp)    :: prefactor, one_over_delr_sq

        prefactor = -0.5_wp
        one_over_delr_sq = 1.0_wp/(delR*delR)

        H_psi = zero

        ! apply Laplacian for electron N+1, using la for the centripetal term, and wavefunctions from the left and right boundaries
        DO j = 1, number_grid_points
            CALL incr_with_laplacian(psi(:, j), H_psi(:, j), la, one_over_delr_sq, prefactor, Rinverse2, &
                                     wf_halo_left(:, j), wf_halo_right(:, j))
        END DO

        ! apply Laplacian for electron N+2, using lb for the centripetal term, and wavefunctions from the bottom and top boundaries
        DO j = 1, number_grid_points
            CALL incr_with_laplacian(psi(j, :), H_psi(j, :), lb, one_over_delr_sq, prefactor, Rinverse2, &
                                     wf_halo_bottom(j, :), wf_halo_top(j, :))
        END DO

    END SUBROUTINE apply_two_electron_laplacian

!---------------------------------------------------------------------

    !> \brief apply the centrifugal barrier and energy shift terms of the two-e hamiltonian for a single channel
    !> 
    !> \param[in]    number_grid_points
    !> \param[in]    psi                     wavefunction for a single channel on local 2D grid: dimension (grid points, grid points)
    !> \param[inout] H_psi                   hamiltonian times wavefunction on local 2D grid
    !>                                       dimension (grid points, grid_points)
    !> \param[in]    Z_minus_N_over_R
    !> \param[in]    k2                      k squared (energy) value for this channel
                                               
    SUBROUTINE apply_two_electron_centrifugal_and_energy_shift (number_grid_points, psi, H_psi, Z_minus_N_over_R, k2)

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, number_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_grid_points, number_grid_points)
        REAL(wp), INTENT(IN)       :: Z_minus_N_over_R(number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: k2

        INTEGER                    :: i, j

        DO i = 1, number_grid_points
            DO j = 1, number_grid_points
                H_psi(i, j) = H_psi(i, j) - &
                              ( Z_minus_N_over_R(i) + Z_minus_N_over_R(j) + 0.5_wp * k2 ) * psi(i, j)
            END DO
        END DO

    END SUBROUTINE apply_two_electron_centrifugal_and_energy_shift

!---------------------------------------------------------------------

    !> \brief apply the three long range potential matrices (WE, WP and WD) to the wavefunction for a single channel
    !>
    !> \param[in]    number_grid_points
    !> \param[in]    channel_id          ID for the current channel (needed to determine couplings)
    !> \param[in]    field_strength      3D electric field strength in a.u. (needed for laser coupling terms)
    !> \param[in]    field_on            logical: if false then laser coupling terms can be ignored
    !> \param[in]    r_store             radial coordinates of the local grid points dimension(grid points)
    !> \param[in]    WE_store_reg3       stored values of the WE potential matrix 
    !>                                   dimension (grid points, grid points, channels, channels)
    !> \param[in]    psi                 Wavefunction (note: for all channels), dimension (grid points, grid points, channels)
    !> \param[inout] H_psi               Hamiltonian times wavefunction, dimension (grid points, grid points)
    !>
    SUBROUTINE apply_two_electron_long_range_potentials(number_grid_points, &
                                                           channel_id, &
                                                           field_strength, field_on, &
                                                           r_store, WE_store_reg3, psi, H_psi)

        USE initial_conditions, ONLY: use_two_electron_we_ham, use_two_electron_wp_ham
        USE lrpots,             ONLY: region_three_we_width

        INTEGER, INTENT(IN)        :: number_grid_points
        INTEGER, INTENT(IN)        :: channel_id
        REAL(wp), INTENT(IN)       :: field_strength(3)
        REAL(wp), INTENT(IN)       :: r_store(number_grid_points)
        REAL(wp), INTENT(IN)       :: WE_store_reg3(number_grid_points, number_grid_points, &
                                                    region_three_we_width, two_electron_nchan)
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, number_grid_points, 1:two_electron_nchan)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_grid_points, number_grid_points)
        LOGICAL, INTENT(IN)        :: field_on

        IF (use_two_electron_we_ham) THEN
            CALL incr_w_we_ham_x_vec_reg3(psi, H_psi, channel_id, number_grid_points, WE_store_reg3)
        END IF

        IF (field_on .AND. use_two_electron_wp_ham) THEN
           CALL incr_w_wp_ham_x_vec_len_reg3(psi, H_psi, channel_id, field_strength, number_grid_points, r_store)
        END IF

    END SUBROUTINE apply_two_electron_long_range_potentials

!---------------------------------------------------------------------

    !> \brief apply the WE long range potential matrix (coupling between outer electrons and residual doubly-charged ion,
    !>                                                  and between the two outer electrons themselves)
    !>
    !> \param[in]    psi                Wavefunction on the local 2D grid (note, full wavefunction for all channels)  
    !>                                  dimension (grid points, grid points, channels)
    !> \param[inout] H_psi              Hamiltonian times the wavefunction on the local 2D grid for a specific channel  
    !>                                  dimension (grid_points, grid_points)
    !> \param[in]    channel_i          ID of the current channel
    !> \param[in]    number_grid_points 
    !> \param[in]    WE_store_reg3      Stored values for the WE coupling matrix (grid points, grid points, width, channels)
    !> 
    SUBROUTINE incr_w_we_ham_x_vec_reg3(psi, H_psi, channel_i, number_grid_points, WE_store_reg3)

        USE lrpots, ONLY: region_three_we_width

        INTEGER, INTENT(IN)        :: channel_i
        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, number_grid_points, two_electron_nchan)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_grid_points, number_grid_points)
        REAL(wp), INTENT(IN)       :: WE_store_reg3(number_grid_points, number_grid_points, &
                                                    region_three_we_width, two_electron_nchan)

        INTEGER                    :: i, ii, j
        INTEGER                    :: channel_j

        DO ii = 1, twoe_we_neighbour_count(channel_i)
            channel_j = twoe_we_neighbour_store(channel_i, ii)
            DO i = 1, number_grid_points
                DO j = 1, number_grid_points
                    H_psi(i, j) = H_psi(i, j) + WE_store_reg3(i, j, ii, channel_i)*psi(i, j, channel_j)
                END DO
            END DO
        END DO       

    END SUBROUTINE incr_w_we_ham_x_vec_reg3

!---------------------------------------------------------------------

    !> \brief Apply the two-electron WP potential
    !>
    !> Multiplies the vector \c Psi (corresponding to channel \c i) by the \f$ W_P \f$ potential
    !> in length gauge and adds the result to \c H_Psi (corresponding to channel \c j). Channels that
    !> are coupled by \f$ W_P \f$ are listed in the `twoe_wp_neighbour_store`.
    !> 
    !> \param[in]    psi                Full (all channels) wavefunction on the local 2D grid
    !> \param[in]    H_psi              Hamiltonian times the wavefunction for the current channel on the local 2D grid
    !> \param[in]    channel_i          ID for the current channel
    !> \param[in]    field_strength     3D Electric field strength in a.u.
    !> \param[in]    number_grid_points 
    !> \param[in]    r_store            radial coordinates for the local grid points dimension (grid points)
    !>
    SUBROUTINE incr_w_wp_ham_x_vec_len_reg3(psi, H_psi, channel_i, field_strength, number_grid_points, r_store)

        USE coupling_rules, ONLY: field_contraction

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(INOUT) :: H_psi(1:number_grid_points, 1:number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points, 1:number_grid_points, two_electron_nchan)
        INTEGER, INTENT(IN)        :: channel_i
        REAL(wp), INTENT(IN)       :: field_strength(3)
        REAL(wp), INTENT(IN)       :: r_store(number_grid_points)

        INTEGER     :: i, j
        INTEGER     :: channel_j, ii
        INTEGER     :: total_Mi, total_Mj
        COMPLEX(wp) :: wp_ham3(3), wp_ham3_coeff

        total_Mi = tot_ml(channel_i)
        DO ii = 1, twoe_wp_neighbour_count(channel_i)
            channel_j = twoe_wp_neighbour_store(channel_i,ii)
            total_Mj = tot_ml(channel_j)
            CALL makewpmat_reg3(channel_i, channel_j, wp_ham3)

            wp_ham3_coeff = field_contraction(field_strength, wp_ham3, total_Mj, total_Mi)

            IF (wp_ham3_coeff /= zero) THEN
                DO i = 1, number_grid_points
                   DO j = 1, number_grid_points
                       H_psi(i, j) = H_psi(i, j) + wp_ham3_coeff*(r_store(i) + r_store(j))*psi(i, j, channel_j)
                   END DO
                END DO
            END IF

        END DO

    END SUBROUTINE incr_w_wp_ham_x_vec_len_reg3

!---------------------------------------------------------------------

    !> \brief Setup and store the WE array
    !>
    !> The array storing the WE potential coefficients  (coupling the electron to the residual
    !> ion) is set up outside of the time loop to avoid unnecessary steps in the
    !> time propagation.
    !> 
    !> \param[inout] WE_store              Stored array of coefficients (grid points, grid points, width, channels)
    !> \param[in]    r_greater             radial coordinates of outermost electron (grid points, grid points)
    !> \param[in]    r_less_over_r_greater Values of MIN(r_{N+1}, r_{N+2}}/MAX(r_{N+1}, r_{N+2}) needed in multipole expansion
    !>                                     dimension (grid ponts, grid points)
    !> \param[in]    number_grid_points    
    !>
    SUBROUTINE store_region_three_we (WE_store, r_greater, r_less_over_r_greater, number_grid_points)

        USE lrpots,     ONLY: region_three_we_width
        USE potentials, ONLY: makewemat_reg3

        INTEGER,  INTENT(IN)    :: number_grid_points
        REAL(wp), INTENT(INOUT) :: WE_store(number_grid_points, number_grid_points, region_three_we_width, two_electron_nchan)
        REAL(wp), INTENT(IN)    :: r_greater(number_grid_points, number_grid_points)
        REAL(wp), INTENT(IN)    :: r_less_over_r_greater(number_grid_points, number_grid_points)
        INTEGER                 :: channel_i, channel_j
        INTEGER                 :: ii, i, j
        REAL(wp)                :: WE_ham

        DO channel_i = 1, two_electron_nchan

            DO ii = 1, twoe_we_neighbour_count(channel_i)
                channel_j = twoe_we_neighbour_store(channel_i,ii)

                DO i = 1, number_grid_points
                    DO j = 1, number_grid_points
                        CALL makewemat_reg3(r_greater(i, j), r_less_over_r_greater(i, j), channel_i, channel_j, WE_ham)
                        WE_store(i, j, ii, channel_i) = WE_ham
                    END DO
                END DO
            END DO
        END DO

    END SUBROUTINE store_region_three_we

!---------------------------------------------------------------------

    !> \brief Allocate the arrays for the coupling information
    !>
    !> For each two-electron channel i, store the channels j to which i couples via
    !> the \f$ W_D, W_E \f$ and \f$ W_P\f$ potentials.
    !>
    SUBROUTINE allocate_region_three_neighbour_stores

        USE lrpots, ONLY: region_three_we_width, region_three_wp_width
        USE readhd, ONLY: two_electron_nchan

        IMPLICIT NONE
        INTEGER :: ierr

        allocate(twoe_wp_neighbour_store(two_electron_nchan, 2*region_three_wp_width + 1),&
                 twoe_we_neighbour_store(two_electron_nchan, 2*region_three_we_width + 1),&
                 twoe_wp_neighbour_count(two_electron_nchan),&
                 twoe_we_neighbour_count(two_electron_nchan),&
                 stat=ierr)

        CALL assert((ierr == 0), 'allocation error in two_electron_outer_hamiltonian stores')

        ! Initialize
        twoe_wp_neighbour_count = 0  ;  twoe_wp_neighbour_store = (0,0)
        twoe_we_neighbour_count = 0  ;  twoe_we_neighbour_store = (0,0)

    END SUBROUTINE allocate_region_three_neighbour_stores

!---------------------------------------------------------------------

END MODULE two_electron_outer_hamiltonian
