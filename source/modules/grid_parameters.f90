! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Sets up outer region grid and communications associated therewith.

MODULE grid_parameters

    USE precisn, ONLY: wp
    
    IMPLICIT NONE

    ! Each outer region processor has psi_outer on grid points X_1st to X_Last
    ! The 1st PE in the outer region has a smaller value of X_Last than the others
    ! because this PE has additional work to do to get H^n values at the two grid
    ! points inside the inner region
    !
    ! HUGO: All integrations should use a 5-point method similar to the determination
    !       of derivatives. The total number of integration points should therefore be
    !       a multiple of 4 plus 1. The easiest way to implement this is by a grid length
    !       of 4n+1 for the Outer Master node (the 1st PE in the outer region), and a grid
    !       length of 4m for every other. This allows us to define the weights for the
    !       self_local_inner, local_inner and real_local_inner in a straightforward manner.
    
    INTEGER, PARAMETER  :: x_1st = 1
    INTEGER, SAVE       :: x_last

    ! Each outer region processor handles a number of grid points given by Block_Length:
    INTEGER             :: block_length_master != x_last_master - x_1st + 1
    INTEGER             :: block_length_others != x_last_others - x_1st + 1

    ! R_1st to R_Last is total number of grid points in the outer region
    ! The outer region is made up of no_of_blocks, each with Block_Length grid points
    INTEGER, PARAMETER  :: r_1st = 1
    INTEGER             :: r_last  !=  block_length_master  +                      &
!                                      ((no_of_blocks-1) * block_length_others) +  &
!                                      r_1st - 1

    INTEGER, SAVE       :: no_of_states_in_angular_basis
    ! DO NOT CHANGE
    INTEGER, PARAMETER  :: channel_id_1st = 1 ! MUST = 1
    INTEGER, SAVE       :: channel_id_last
    INTEGER, ALLOCATABLE, SAVE  :: pe_for_channel(:) ! pe within mpi_block_comm for each channel
    INTEGER, SAVE       :: my_channel_id_1st, my_channel_id_last   ! distinct if no_of_pes_per_sector > 1
    INTEGER, SAVE       :: my_num_channels  ! my_channel_id_last - my_channel_id_1st + 1
    
    !  Parameters for separating grid into ionization regions:
!   REAL(wp),PARAMETER  :: single_ioniz_bndry_in_au = 50.0_wp
!   INTEGER,SAVE        :: single_ioniz_bndry_in_grid_pts

CONTAINS

    SUBROUTINE init_channel_id_last(number_channels)
        USE initial_conditions, ONLY : debug
        USE rmt_assert,         ONLY : assert
        
        INTEGER, INTENT(IN) :: number_channels
        INTEGER             :: ierr

        channel_id_last = number_channels + channel_id_1st - 1
        allocate (pe_for_channel(channel_id_1st:channel_id_last), stat=ierr) 
        call assert (ierr == 0, 'allocation failure, pe_for_channel, module grid_parameters')   
        
        IF (debug) PRINT *, 'Channel_ID_Last = ', channel_id_last

    END SUBROUTINE init_channel_id_last
    
!-----------------------------------------------------------------------

    SUBROUTINE init_grid_parameters_module(i_am_outer_master, &
                                           half_fd_order, &
                                           propagation_order, &
                                           i_am_in_outer_region)

        USE calculation_parameters, ONLY: init_nfdm

        LOGICAL, INTENT(IN) :: i_am_outer_master  ! This is a dummy, for no_of_pes_per_sector > 1
        ! it is actually i_am_in_first_outer_block
        LOGICAL, INTENT(IN) :: i_am_in_outer_region
        INTEGER, INTENT(IN) :: half_fd_order
        INTEGER, INTENT(IN) :: propagation_order

        CALL init_grid_parameters(i_am_outer_master)

        CALL check_propagation_order(half_fd_order, propagation_order, &
                                     i_am_in_outer_region)

        CALL init_nfdm

    END SUBROUTINE init_grid_parameters_module

!-----------------------------------------------------------------------

    SUBROUTINE init_grid_parameters(i_am_outer_master)

        USE initial_conditions, ONLY: x_last_master, x_last_others

        LOGICAL, INTENT(IN) :: i_am_outer_master  ! see above: actually i_am_in_first_outer_block

        IF (i_am_outer_master) THEN
            x_last = x_last_master
        ELSE
            x_last = x_last_others
        END IF

    END SUBROUTINE init_grid_parameters

!-----------------------------------------------------------------------

    SUBROUTINE check_propagation_order(half_fd_order, propagation_order, &
                                       i_am_in_outer_region)

        USE rmt_assert, ONLY: assert

        INTEGER, INTENT(IN) :: half_fd_order
        INTEGER, INTENT(IN) :: propagation_order
        LOGICAL, INTENT(IN) :: i_am_in_outer_region

        IF (i_am_in_outer_region) THEN
            CALL assert(MOD(x_last, 2*half_fd_order) .EQ. 0, 'Both X_Last parameters should be divisible by 4')
        END IF

        CALL assert(half_fd_order .EQ. 2, 'only five-point methods implemented')

        IF (x_last .LT. half_fd_order*propagation_order) THEN
            PRINT *, ' X_Last=', x_last
            PRINT *, ' half_fd_order=', half_fd_order
            PRINT *, ' Propagation_Order=', propagation_order
            CALL assert(.false., 'Error: X_Last needs to be .GE. half_fd_order*Propagation_Order')
        END IF

    END SUBROUTINE check_propagation_order

!-----------------------------------------------------------------------

    SUBROUTINE derive_grid_parameters

        USE communications_parameters, ONLY: no_of_blocks
        USE initial_conditions,        ONLY: x_last_master, x_last_others

        block_length_master = x_last_master - x_1st + 1
        block_length_others = x_last_others - x_1st + 1

        ! r_1st to r_Last is total number of grid points in the outer region
        ! The outer region is made up of no_of_blocks, each with Block_Length grid points
!       INTEGER, PARAMETER     :: r_1st   = 1
        r_last = block_length_master + &
                 ((no_of_blocks - 1)*block_length_others) + &
                 r_1st - 1

    END SUBROUTINE derive_grid_parameters

END MODULE grid_parameters


!> @page outer_para Outer Region Parallelisation
!!
!! @brief Outer Region Parallelisation Strategy
!!
!!
!!### Outer region parallelisation
!!
!!The outer region parallelisation is somewhat easier to envisage than the
!!inner region, as it is actually a division of physical space. Two layers
!!of parallelism are employed in the outer region, one using MPI and one
!!using OpenMP.
!!
!!#### Layer 1
!!
!!The major division in the outer region is a division of the physical
!!space. Thus, an outer region of 100 a.u. might be divided into smaller
!!sectors of 25 a.u with each sector handled by an MPI task. Because the
!!outer region uses an explicit grid based representation of the
!!wavefunction, this corresponds to each MPI task handling a set number of
!!grid points.
!!
!! @image html outer_grid.png
!! @image latex outer_grid.png
!!
!!In practice, the outer region master (first MPI-task in the outer
!!region, highlighted red above) is allocated a smaller number of grid
!!points than the rest of the outer region, to allow additional resource
!!for the extra communication responsibilities with the inner region.
!!
!!Performance is enhanced by reducing the number of grid points per sector,
!!(`x_last_master` and `x_last_others`) and increasing the number of outer-region
!!MPI tasks (`no_of_pes_to_use_outer`) to maintain the size of the outer region.
!!Note however that the finite-difference rule implementation requires a minimum
!!of 16 grid points per sector, so there is a fundamental limit to this strategy.
!!
!!#### Layer 2
!!
!! @image html outer_grid_channels.png
!! @image latex outer_grid_channels.png 
!!
!!An additional layer of MPI parallelisation can be used in each outer region
!!sector. In analogy with the inner region, each grid sector is referred to as a
!!'block', and each block covers some region of space for all electron emission
!!channels. Communication between each block is handled using the
!!`mpi_comm_inter_block` communicator. Each block may then be split amongst
!!multiple MPI tasks, in which case communication within each block is handled
!!using communicator `mpi_comm_block`.
!!
!!The number of outer region cores is set by the input parameter
!!`no_of_pes_to_use_outer`. The number of PEs to use in each sector (block) is set
!!by `no_of_pes_per_sector`. Thus computing the size of the outer region can
!!be rather complicated (see @ref inputs "Inputs")
!!
!!#### Layer 3
!!
!!As in the inner region, additional performance can be extracted with the
!!use of OpenMP parallelism of each MPI task. Thus a number of shared
!!memory threads can be allocated per outer region task (this is
!!controlled separately from the number of OpenMP threads in the inner
!!region). In the outer region, most of the calculation takes place for
!!each electron-emission channel independently of the other emission
!!channels, so all do loop structures which loop over the variable
!!`channel_ID` are parallelised with the `!$OMP PARALLEL DO` structure.
!!
!!In some sense, Layer 3 has been superseded by layer 2, as explicit
!!parallelisation over the channels is more flexible: it is not limited for
!!instance by the number of cores in the NUMA region. However Layer 3 remains as
!!a legacy as it may still prove useful.
