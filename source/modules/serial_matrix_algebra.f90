! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Routines for Krylov subspace Hamiltonian algebra.
MODULE serial_matrix_algebra

    USE precisn,     ONLY: wp
    USE rmt_assert,  ONLY: assert
    USE global_data, ONLY: im, one, zero, rtwo, rone
    USE MPI

    IMPLICIT NONE

    ! scaling part of pade involves 2**(-ns)
    !   Use a lookup table for values of
    !   ns between 0 and - max_pade_scale_power
    INTEGER, PARAMETER   :: max_pade_scale_power = 50
    INTEGER, PARAMETER   :: ideg = 6
    INTEGER, PARAMETER   :: mmax = 50
    INTEGER              :: liwsp, lfree2, lwork_diag

    REAL(wp), ALLOCATABLE, SAVE  :: two_to_power(:)
    COMPLEX(wp), ALLOCATABLE     :: wsp_pade(:), work_diag(:)
    INTEGER, ALLOCATABLE         :: iwsp_pade(:)

    PUBLIC augment_reduced_hamiltonian
    PUBLIC exponentiate_reduced_ham_diag
    PUBLIC exponentiate_reduced_ham_pade
    PUBLIC init_serial_matrix_algebra
    PUBLIC dealloc_serial_matrix_algebra

CONTAINS

    SUBROUTINE init_serial_matrix_algebra(propagation_order)

        INTEGER, INTENT(IN) :: propagation_order
        INTEGER :: err, m

        liwsp = mmax + 2
        lfree2 = 4*(2*propagation_order + 2)*(2*propagation_order + 2) + ideg + 1
        lwork_diag = 2*(2*propagation_order + 2)*(2*propagation_order + 2)

        ALLOCATE (two_to_power(-max_pade_scale_power:0), &
                  iwsp_pade(liwsp), wsp_pade(lfree2), work_diag(lwork_diag), stat=err)
        CALL assert(err .EQ. 0, 'ALLOCATION error Init_serial_matrix_algebra')

        DO m = -max_pade_scale_power, 0
            two_to_power(m) = REAL(2.0_wp**m, wp)
        END DO

    END SUBROUTINE init_serial_matrix_algebra

!---------------------------------------------------------------------------

    SUBROUTINE dealloc_serial_matrix_algebra

        INTEGER :: err

        DEALLOCATE (two_to_power, wsp_pade, iwsp_pade, work_diag, stat=err)
        CALL assert(err .EQ. 0, 'problem deallocating serial_matrix_algebra')

    END SUBROUTINE dealloc_serial_matrix_algebra

!---------------------------------------------------------------------------

    SUBROUTINE augment_reduced_hamiltonian(k1, m, mx, matin_d, matin_offd, matout)

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: k1, m, mx
        INTEGER                     :: i, j
        COMPLEX(wp), INTENT(IN)     :: matin_d(mx)       ! diagonal elements
        REAL(wp), INTENT(IN)        :: matin_offd(mx-1)  ! off-diagonal elements (i) == (i,i+1) = (i+1,i)
        COMPLEX(wp), INTENT(INOUT)  :: matout(mx, mx)

        matout(1,1) = -im * matin_d(1)
        if (m > 1) then
           matout(2,1) = -im * matin_offd(1)
           DO i = 2, m - 1
               matout(i-1,i) = -im * matin_offd(i-1)
               matout(i,i) = -im * matin_d(i)
               matout (i+1,i) = -im * matin_offd(i)
           END DO
           matout(m-1,m) = -im * matin_offd(m-1)
           matout(m,m) = -im * matin_d(m)
        end if

        IF (k1 == 1) THEN
            matout(1, m + 1) = one
            matout(1, m + 2) = zero
        END IF

        IF (k1 .GT. 1) THEN
            matout(1, m + 1) = one
            DO i = 1, k1 - 1
                matout(m + i, m + i + 1) = one
            END DO
        END IF

        matout(m + 1, m) = zero
        matout(mx - 1, mx) = zero

    END SUBROUTINE augment_reduced_hamiltonian

!---------------------------------------------------------------------------

    SUBROUTINE exponentiate_reduced_ham_diag(istep, mx, t, H, tayvec, i_am_inner_master)

        USE initial_conditions, ONLY: debug
        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: mx, istep
        COMPLEX(wp), INTENT(IN)     :: H(mx, mx), t
        COMPLEX(wp), INTENT(OUT)    :: tayvec(mx)
        LOGICAL, INTENT(IN)         :: i_am_inner_master
        INTEGER                     :: i, lwork, info
        INTEGER                     :: ipiv(mx)
        REAL(wp)                    :: rwork(2*mx)
        COMPLEX(wp)                 :: vl(mx, mx), vr(mx, mx), eigs(mx)
        COMPLEX(wp)                 :: expeigs(mx, mx), taymat(mx, mx)
        COMPLEX(wp)                 :: numer(mx, mx), denom(mx, mx), ident(mx, mx), &
                                       invvr(mx, mx)
        COMPLEX(wp), PARAMETER      :: zero = (0.0_wp, 0.0_wp), one = (1.0_wp, 0.0_wp)
        COMPLEX(wp)                 :: alpha, beta2
        CHARACTER(LEN=1)            :: jobvl, jobvr

        jobvl = 'n'
        jobvr = 'v'
        lwork = 2*mx*mx

!       PRINT *,H

!       ALLOCATE(work(MAX(1,lwork)),stat=err)
!       IF (err/=0) THEN
!           PRINT*, 'allocation error in work taylor'
!           CALL helium_stop
!       END IF

        eigs = zero
        vl = zero
        vr = zero
        ident(:, :) = zero

        DO i = 1, mx
            ident(i, i) = one
        END DO

        !---------------------------------------------------------------------------
        ! DIAGONALIZATION OF redH (ARNOLDI HAMILTONIAN)

        CALL ZGEEV(jobvl, jobvr, mx, H, mx, eigs, vl, mx, vr, mx, work_diag, lwork_diag, rwork, info)
        IF (info /= 0) THEN
            PRINT *, 'info for zgeev =', info
            STOP
        END IF

        IF (istep == 0 .AND. i_am_inner_master .AND. debug) THEN
            PRINT *, 'eigs of reduced ham for step', istep
            DO i = 1, mx
                PRINT *, i, eigs(i)
            END DO
        END IF

!       DEALLOCATE(work, stat=err)
!       IF (err/=0) THEN
!           PRINT* ,'deallocation problem with work in Exponentiate_Reduced_Ham_Diag'
!           CALL helium_stop
!       END IF

        !---------------------------------------------------------------------------
        ! GET INVERSE OF EIGENVECTOR VR

        invvr = ident
        denom = vr

        CALL ZGESV(mx, mx, denom, mx, ipiv, invvr, mx, info)

        !---------------------------------------------------------------------------
        ! GET EXPONENTIAL OF DIAGONAL ELEMENTS FOR ARNOLDI PROPAGATION

        expeigs = zero

        DO i = 1, mx
            expeigs(i, i) = exp(eigs(i)*t)
        END DO

        !---------------------------------------------------------------------------
        ! ARNOLDI PROPAGATION

        alpha = one
        beta2 = zero

        CALL ZGEMM('n', 'n', mx, mx, mx, alpha, expeigs, mx, invvr, mx, beta2, numer, mx)

        CALL ZGEMM('n', 'n', mx, mx, mx, alpha, vr, mx, numer, mx, beta2, taymat, mx)

        tayvec = zero
        tayvec = taymat(:, 1)

    END SUBROUTINE exponentiate_reduced_ham_diag

!---------------------------------------------------------------------------
! EXPONENTIATION OF redH USING PADE ROUTINE
!---------------------------------------------------------------------------

    SUBROUTINE exponentiate_reduced_ham_pade(m, mx, t, H, tayvec)

!       USE distribute_hd_blocks2, ONLY: numrows

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: m, mx
        COMPLEX(wp), INTENT(IN)     :: H(mx, mx), t
        COMPLEX(wp), INTENT(OUT)    :: tayvec(mx)
!       COMPLEX(wp), ALLOCATABLE    :: wsp(:)
!       INTEGER,     ALLOCATABLE    :: iwsp(:)
        INTEGER, PARAMETER          :: ideg = 6
        INTEGER                     :: iflag, & !lwsp
                                       iexph, iphihs, iphihf, ns

!       ideg = 6               ! degree of pade expansion
!       mmax = 50              ! maximum order for krylov expansion
!       liwsp = mmax+2
!       lwsp = numrows*(liwsp)+5*(liwsp)*2+ideg+1
!       lfree2 = 4*mx*mx+ideg+1

!       ALLOCATE(iwsp(liwsp),wsp(lwsp),stat = err)   ! This was a bug. I think that I have fixed it.

        ! Moved earlier, so that memory not continuously allocated and deallocated

!       ALLOCATE(iwsp(liwsp),wsp(lfree2),stat = err)
!       IF (err/=0) THEN
!         PRINT*, 'allocation error in Exponentiate_Reduced_Ham_Pade'
!         CALL helium_stop
!       END IF

!       wsp = zero

        CALL ZGPADM(ideg, mx, t, H, mx, wsp_pade, lfree2, iwsp_pade, liwsp, iexph, ns, iflag)
        CALL assert(iflag .GE. 0, 'error in pade routine')

        iphihs = iexph + ((mx - 1)*(mx - 1)) - 1
        iphihf = (iexph + ((mx - 1)*(mx - 1)) - 1) + (m - 1)

        tayvec = zero
        tayvec(1:iphihf - iphihs + 1) = wsp_pade(iphihs:iphihf)

!       DEALLOCATE(iwsp,wsp,stat = err)
!       IF (err/=0) THEN
!           PRINT*, 'deallocation error in exp_itredh_pade'
!           CALL helium_stop
!       END IF

    END SUBROUTINE exponentiate_reduced_ham_pade

!---------------------------------------------------------------------------
! PADE SUBROUTINE TAKEN FROM EXPOKIT SOFTWARE WRITTEN BY SIDJE
!---------------------------------------------------------------------------

    SUBROUTINE ZGPADM(ideg, m, t, H, ldh, wsp, lwsp, ipiv, lipiv, iexph, ns, iflag)

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: ideg, lipiv
        INTEGER                     :: m, ldh, lwsp, iexph, ns, iflag, ipiv(lipiv)
        COMPLEX(wp)                 :: t
        COMPLEX(wp)                 :: H(ldh, m), wsp(lwsp)

        !-----Purpose----------
        !
        !     Computes exp(t*H), the matrix exponential of a general complex
        !     matrix in full, using the irreducible rational Pade approximation
        !     to the exponential exp(z) = r(z) = (+/-)( I + 2*(q(z)/p(z)) ),
        !     combined with scaling-and-squaring.
        !
        !-----Arguments--------
        !
        !     ideg      : (input) the degre of the diagonal Pade to be used.
        !                 a value of 6 is generally satisfactory.
        !
        !     m         : (input) order of H.
        !
        !     H(ldh,m)  : (input) argument matrix.
        !
        !     t         : (input) time-scale (can be < 0).
        !
        !     wsp(lwsp) : (workspace/output) lwsp .ge. 4*m*m+ideg+1.
        !
        !     ipiv(m)   : (workspace)
        !
        !     iexph     : (output) number such that wsp(iexph) points to exp(tH)
        !                 i.e., exp(tH) is located at wsp(iexph ... iexph+m*m-1)
        !                       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        !                 NOTE: if the routine was called with wsp(iptr),
        !                       then exp(tH) will start at wsp(iptr+iexph-1).
        !
        !     ns        : (output) number of scaling-squaring used.
        !
        !     iflag     : (output) exit flag.
        !                       0 - no problem
        !                      <0 - problem

        INTEGER     :: i, j, k, icoef, mm, ih2, iodd, iused, ifree, iq, ip, iput, iget
        REAL(wp)    :: hnorm
        COMPLEX(wp) :: cp, cq, scale, scale2

        ! check restrictions on input parameters ...
        mm = m*m
        iflag = 0
        IF (ldh .LT. m) iflag = -1
        IF (lwsp .LT. 4*mm + ideg + 1) iflag = -2
        IF (lipiv .LT. m) iflag = -3
        IF (iflag .NE. 0) STOP 'bad sizes (in input of ZGPADM)'

        ! initialise pointers ...

        icoef = 1
        ih2 = icoef + (ideg + 1)
        ip = ih2 + mm
        iq = ip + mm
        ifree = iq + mm

        ! scaling: seek ns such that ||t*H/2^ns|| < 1/2;
        ! and set scale = t/2^ns ...

        DO i = 1, m
            wsp(i) = zero
        END DO

        DO j = 1, m
            DO i = 1, m
                wsp(i) = wsp(i) + ABS(H(i, j))
            END DO
        END DO

        hnorm = 0.0_wp
        DO i = 1, m
            hnorm = MAX(hnorm, DBLE(wsp(i)))
        END DO

        !  hnorm = CDABS( t*hnorm )     !should probably be abs
        hnorm = ABS(REAL(t, wp)*hnorm)     !should probably be abs
        IF (hnorm .EQ. 0.0_wp) STOP 'Error - null H in input of ZGPADM. Try a smaller delta_t'

        ! Could be expensive to compute 2**ns every time
        ! ns = MAX( 0,INT(LOG(hnorm)/LOG(2.0_wp))+2 )
        ! scale =   t/DBLE(2**ns)

        ! Try using look up table instead - is this really quicker?
        ns = MAX(0, INT(LOG(hnorm)/LOG(2.0_wp)) + 2)
        ns = MIN(ns, max_pade_scale_power)
        scale = t*two_to_power(-ns)

        scale2 = scale*scale

        ! compute Pade coefficients ...

        i = ideg + 1
        j = 2*ideg + 1
        wsp(icoef) = one
        DO k = 1, ideg
            wsp(icoef + k) = (wsp(icoef + k - 1)*DBLE(i - k))/DBLE(k*(j - k))
        END DO

        ! H2 = scale2*H*H ...

        CALL ZGEMM('n', 'n', m, m, m, scale2, H, ldh, H, ldh, zero, wsp(ih2), m)

        ! initialise p (numerator) and q (denominator) ...

        cp = wsp(icoef + ideg - 1)
        cq = wsp(icoef + ideg)
        DO j = 1, m
            DO i = 1, m
                wsp(ip + (j - 1)*m + i - 1) = zero
                wsp(iq + (j - 1)*m + i - 1) = zero
            END DO
            wsp(ip + (j - 1)*(m + 1)) = cp
            wsp(iq + (j - 1)*(m + 1)) = cq
        END DO

        ! Apply Horner rule ...

        iodd = 1
        k = ideg - 1
 100    CONTINUE
        iused = iodd*iq + (1 - iodd)*ip
        CALL ZGEMM('n', 'n', m, m, m, one, wsp(iused), m, &
                   wsp(ih2), m, zero, wsp(ifree), m)
        DO j = 1, m
            wsp(ifree + (j - 1)*(m + 1)) = wsp(ifree + (j - 1)*(m + 1)) + wsp(icoef + k - 1)
        END DO
        ip = (1 - iodd)*ifree + iodd*ip
        iq = iodd*ifree + (1 - iodd)*iq
        ifree = iused
        iodd = 1 - iodd
        k = k - 1
        IF (k .GT. 0) GOTO 100

        ! Obtain (+/-)(I + 2*(p\q)) ...

        IF (iodd .NE. 0) THEN
            CALL ZGEMM('n', 'n', m, m, m, scale, wsp(iq), m, &
                       H, ldh, zero, wsp(ifree), m)
            iq = ifree
        ELSE
            CALL ZGEMM('n', 'n', m, m, m, scale, wsp(ip), m, &
                       H, ldh, zero, wsp(ifree), m)
            ip = ifree
        END IF

        CALL ZAXPY(mm, -one, wsp(ip), 1, wsp(iq), 1)
        CALL ZGESV(m, m, wsp(iq), m, ipiv, wsp(ip), m, iflag)
        IF (iflag .NE. 0) STOP 'Problem in ZGESV (within ZGPADM)'

        CALL ZDSCAL(mm, rtwo, wsp(ip), 1)
        DO j = 1, m
            wsp(ip + (j - 1)*(m + 1)) = wsp(ip + (j - 1)*(m + 1)) + one
        END DO

        iput = ip
        IF (ns .EQ. 0 .AND. iodd .NE. 0) THEN
            CALL ZDSCAL(mm, -rone, wsp(ip), 1)
            GOTO 200
        END IF

        ! squaring : exp(t*H) = (exp(t*H))^(2^ns) ...

        iodd = 1
        DO k = 1, ns
            iget = iodd*ip + (1 - iodd)*iq
            iput = (1 - iodd)*ip + iodd*iq
            CALL ZGEMM('n', 'n', m, m, m, one, wsp(iget), m, wsp(iget), m, &
                       zero, wsp(iput), m)
            iodd = 1 - iodd
        END DO
 200    CONTINUE
        iexph = iput

    END SUBROUTINE ZGPADM

END MODULE serial_matrix_algebra
