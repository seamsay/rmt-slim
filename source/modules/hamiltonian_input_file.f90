! Copyright 2023
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief A piecemeal reimplementation of the code in `readhd`.
MODULE hamiltonian_input_file
    ! NOTE: We define the H file in terms of explicit kinds to decouple RMT's working precision from the types used in the H file format.
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64

    USE initial_conditions, ONLY: jK_coupling_id, LS_coupling_id
    USE precisn, ONLY: wp
    USE rmt_assert, ONLY: assert
    USE wall_clock, ONLY: time => hel_time

    IMPLICIT NONE

    !> \brief A derived type defining the format of H files.
    !>
    !> The structure of this type maps almost 1:1 with the structure of the H file itself. The structure is defined in terms of
    !> specified-width numeric types (i.e. `int32` and `real64`) because it is an external filetype and setting `wp` to a different
    !> width shouldn't affect the way the file is read.
    !>
    !> The names of the fields match what is currently being used in the code.
    !>
    !> # H File Format
    !>
    !> The files consist of a series of [_Unformatted I/O Records_][scipy-FortranFile]. The format of these records is compiler-and
    !> machine-dependent, although most compilers will use a `<record bytes><record data><record bytes>` format and most machines
    !> will be little-endian and use IEEE 754 floating point numbers.
    !>
    !> The first record contains 7 scalars acting as a header, 5 32-bit integers followed by 2 64-bit real numbers.
    !>
    !> The next 3 records contain 1D arrays, the first is a 64-bit real array and the next two are 32-bit integer arrays. The length
    !> of these arrays is given by the 5th integer from the header.
    !>
    !> The next record contains a \f$3 \times n\f$ matrix, with \f$n\f$ specified by the 3rd integer in the header.
    !>
    !> The remaining records define the L blocks, which are documented in `l_block`.
    !>
    !> [scipy-FortranFile]: https://docs.scipy.org/doc/scipy-1.9.3/reference/generated/scipy.io.FortranFile.html
    ! Semantically `lrang2`, `lamax`, and `ntarg` are length type parameters.
    TYPE H_file
        ! TODO: Document what each field is.
        ! TODO: More appropriate names?
        INTEGER(INT32):: nelc
        INTEGER(INT32):: nz
        INTEGER(INT32):: lrang2
        INTEGER(INT32):: lamax
        INTEGER(INT32):: ntarg
        REAL(REAL64):: rmatr
        REAL(REAL64):: bbloch

        REAL(REAL64), DIMENSION(:), ALLOCATABLE:: etarg  ! DIMENSION(ntarg)
        INTEGER(INT32), DIMENSION(:), ALLOCATABLE:: ltarg  ! DIMENSION(ntarg)
        INTEGER(INT32), DIMENSION(:), ALLOCATABLE:: starg  ! DIMENSION(ntarg)

        REAL(REAL64), DIMENSION(:, :), ALLOCATABLE:: cfbut  ! DIMENSION(3, lrang2)

        TYPE(L_block), DIMENSION(:), ALLOCATABLE:: L_blocks
    END TYPE H_file

    !> \brief A derived type defining the format of L blocks in the H files.
    !>
    !> The structure of this type maps almost 1:1 with the structure of the H file itself. The structure is defined in terms of
    !> specified-width numeric types (i.e. `int32` and `real64`) because it is an external filetype and setting `wp` to a different
    !> width shouldn't affect the way the file is read.
    !>
    !> The names of the fields match what is currently being used in the code.
    !>
    !> # L BLock Format
    !>
    !> Each L block consists of 6 records.
    !>
    !> The first record contains 6 scalars acting as a subheader, all are 32-bit integers. The 6th integer from the subheader
    !> indicates whether this is the last L block in the file, if it is greater than zero then there are still L blocks left to
    !> read. Empirically this integer seems to be either `1` or `2`, but it is not known if these have any special meaning.
    !>
    !> The second record is a 1D array of 32-bit integers, the size of this array is given by the 5th integer of the**file**
    !> header.
    !>
    !> The third record is a 1D array of 32-bit integers, the size of this array is given by the 4th integer of**this L block's**
    !> subheader.
    !>
    !> The fourth record is a 3D array of 64-bit real numbers, the first and second dimensions of this array are given by the 4th
    !> integer of**this L block's**subheader and the third dimension is given by the 4th integer of the**file's**header.
    !>
    !> The fifth record is a 1D array of 64-bit real numbers, the size of this array is given by the 5th integer of**this L
    !> block's**subheader.
    !>
    !> The sixth record is a 2D array of 64-bit real numbers, the first dimension is given by the 4th integer of**this L block's**
    !> subheader and the second dimension by**this L block's**subheader.
    !>
    !> If the 6th integer of this L block's subheader is greater than zero another L block needs to be read. The 6th integer is not
    !> stored in this derived type.
    ! Semantically `nchan` and `nstat` are length type parameters.
    ! Semantically this type is also parameterised by `ntarg` and `lamax`.
    TYPE L_block
        ! TODO: Document what each field is.
        ! TODO: More appropriate names?
        ! TODO: What constraints exist on these values? E.g. if lrgl is zero then npty must also be zero.
        INTEGER(INT32):: lrgl  ! TODO: There are restrictions on this value based on the coupling, use a derived type that checks this (we can then define methods on this making stuff like calculating `lplusp` less error prone.). In fact, this is essentially a rational number with a denominator of either 1 or 2 depending on the coupling. Maybe this should happen in a step after file parsing though, this type should be able to read valid files without extra information.
        INTEGER(INT32):: nspn
        INTEGER(INT32):: npty
        INTEGER(INT32):: nchan
        INTEGER(INT32):: nstat

        INTEGER(INT32), DIMENSION(:), ALLOCATABLE:: nconat  ! DIMENSION(ntarg)
        INTEGER(INT32), DIMENSION(:), ALLOCATABLE:: l2p  ! DIMENSION(nchan)
        REAL(REAL64), DIMENSION(:, :, :), ALLOCATABLE:: cf  ! DIMENSION(nchan, nchan, lamax)
        REAL(REAL64), DIMENSION(:), ALLOCATABLE:: eig  ! DIMENSION(nstat)
        REAL(REAL64), DIMENSION(:, :), ALLOCATABLE:: wmat  ! DIMENSION(nchan, nstat)
    END TYPE

    ! IMPORTANT: These are only here to facilitate the fact that parameter reading is separate from data reading, once that has been refactored then these variables can go.
    TYPE(H_file), SAVE, ALLOCATABLE:: read_H_cache_H_file

    INTERFACE log_value
        MODULE PROCEDURE log_value_integer_r1, log_value_integer_r2
    END INTERFACE

    PRIVATE

    PUBLIC:: log_H_parameters, log_atomic_structure, log_debug_info, log_total_channels, log_wmat
    PUBLIC:: read_H_parameters2, read_H_file2
    PUBLIC:: H_file, L_block, parse_H_file, deallocate_H_file
CONTAINS
    !> \brief Parse an H file into the `h_file` derived type.
    !>
    !> \param[in] path The path to the H file.
    !>
    !> See the `h_file` documentation for a description of the returned type.
    !>
    !> \code
    !> TYPE(H_file):: parsed
    !> parsed = parse_H_file("tests/atomic_tests/small_tests/Ne_4cores/inputs/H")
    !>
    !> CALL assert(SIZE(parsed%etarg) == parsed%ntarg)
    !> CALL assert(ALL(SHAPE(parsed%l_blocks(1)%wmat) == [parsed%l_blocks(1)%nchan, parsed%l_blocks(1)%nstat]))
    !> \endcode
    FUNCTION parse_H_file(path) RESULT(H)
        TYPE(H_file):: H
        CHARACTER(len=*), INTENT(IN):: path

        INTEGER:: io, nL, last_L_block_integer
        LOGICAL:: last_L_block
        TYPE(L_block), DIMENSION(:), ALLOCATABLE:: Ls, Ls_temp

        OPEN (newunit = io, file = path, status="old", action="read", form="unformatted")

        READ (io) H%nelc, H%nz, H%lrang2, H%lamax, H%ntarg, H%rmatr, H%bbloch

        ALLOCATE (H%etarg(H%ntarg), H%ltarg(H%ntarg), H%starg(H%ntarg), H%cfbut(3, H%lrang2))
        READ (io) H%etarg
        READ (io) H%ltarg
        READ (io) H%starg
        READ (io) H%cfbut

        nL = 0
        ALLOCATE (Ls(500))
        last_L_block = .FALSE.

        DO WHILE (.NOT. last_L_block)
            nL = nl+1
            IF (nL > SIZE(Ls)) THEN
                ALLOCATE (Ls_temp(2*SIZE(Ls)))
                Ls_temp(:SIZE(Ls)) = Ls
                CALL MOVE_ALLOC(Ls_temp, Ls)
            END IF

            ASSOCIATE (L => Ls(nL))
                READ (io) L%lrgl, L%nspn, L%npty, L%nchan, L%nstat, last_L_block_integer

                last_L_block = last_L_block_integer < 1

                ALLOCATE ( &
                    L%nconat(H%ntarg), &
                    L%l2p(L%nchan), &
                    L%cf(L%nchan, L%nchan, H%lamax), &
                    L%eig(L%nstat), &
                    L%wmat(L%nchan, L%nstat) &
                    )

                READ (io) L%nconat
                READ (io) L%l2p
                READ (io) L%cf
                READ (io) L%eig
                READ (io) L%wmat
            END ASSOCIATE
        END DO

        H%L_blocks = Ls(:nL)

        CLOSE (io)
    END FUNCTION

    SUBROUTINE deallocate_H_file(H)
        TYPE(H_file), INTENT(INOUT):: H

        INTEGER:: i

        DO i = 1, SIZE(H%L_blocks)
            ASSOCIATE (L => H%L_blocks(i))
                DEALLOCATE (L%nconat, L%l2p, L%cf, L%eig, L%wmat)
            END ASSOCIATE
        END DO
        DEALLOCATE (H%L_blocks, H%etarg, H%ltarg, H%starg, H%cfbut)
    END SUBROUTINE

    PURE SUBROUTINE ML_stride_maxm(lrgl, npty, set_ML_max, ML_max, xy_plane_desired, coupling_id, stride, maxm, offset)
        INTEGER, INTENT(IN):: lrgl, npty, coupling_id
        LOGICAL, INTENT(IN):: xy_plane_desired, set_ML_max
        INTEGER, INTENT(IN):: ML_max
        INTEGER, INTENT(OUT):: stride
        INTEGER, INTENT(OUT):: offset
        INTEGER, INTENT(INOUT):: maxm

        INTEGER:: jK_multiplier, actual_lrgl

        ! In jK coupling angular momentum values are stored as double their actual value.
        jK_multiplier = MERGE(2, 1, coupling_id == jK_coupling_id)

        actual_lrgl = MERGE(lrgl/2, lrgl, coupling_id == jK_coupling_id)
        ! With xy-plane polarisation only certain values of ML are accessible.
        ! These are the odd values if `npty` is 1 or the even values if it is 0.
        ! Therefore (with xy-plane polarisation enabled) if `lrgl` is odd (even) and `npty` is 0 (1) then
        ! we need to start counting ML values from one less than `maxm` (and go up to one less) and skip every other
        ! value.
        stride = jK_multiplier*MERGE(2, 1, xy_plane_desired)
        maxm = MERGE(MIN(lrgl, ML_max), lrgl, set_ML_max)

        IF (xy_plane_desired) THEN
            IF ((coupling_id == LS_coupling_id) .OR. (mod(maxm, 2) == 0)) THEN
                offset = jK_multiplier*MOD(actual_lrgl + npty, 2)
            ELSE
                offset = jK_multiplier*MOD(actual_lrgl, 2)
            END IF
        ELSE
            offset = 0
        END IF

    END SUBROUTINE

    PURE FUNCTION n_ML_blocks_in_L_block(lrgl, npty, set_ML_max, ML_max, xy_plane_desired, coupling_id) RESULT(n)
        INTEGER, INTENT(IN):: lrgl, npty, ML_max, coupling_id
        LOGICAL, INTENT(IN):: set_ML_max, xy_plane_desired
        INTEGER:: n, offset, m 

        INTEGER:: stride, maxm

        CALL ML_stride_maxm(lrgl, npty, set_ML_max, ML_max, xy_plane_desired, coupling_id, stride, maxm, offset)

        n = 0
        DO m = -maxm+offset,maxm-offset,stride
            n = n + 1
        END DO
    END FUNCTION

    PURE FUNCTION include_symmetry(lrgl, npty, lplusp, ML_max, coupling_id)
        USE initial_conditions, ONLY: jK_coupling_id, LS_coupling_id
        INTEGER, INTENT(IN):: lrgl, npty, lplusp, ML_max, coupling_id
        INTEGER :: actual_lrgl
        LOGICAL:: include_symmetry

        IF (coupling_id.eq.LS_coupling_id) THEN
            actual_lrgl = lrgl
        ELSEIF (coupling_id.eq.jK_coupling_id) THEN
            actual_lrgl = lrgl/2
        END IF

        include_symmetry = ML_max /= 0 .OR. lplusp == MOD(actual_lrgl+npty, 2)
    END FUNCTION

    !> \brief Read parameters that the code needs from the H file.
    !>
    !> \param[in] path The path to the H file.
    !>
    !> This function parses and caches the H file contents, then retrieves the parameters needed by the rest of the code in exactly
    !> the same way that `readhd:: read_h_parameters()` does.
    !>
    !> **IMPORTANT:** Each call to `hamiltonian_input_file:: read_h_parameters2()` must be followed by _exactly_ one call to
    !> `hamiltonian_input_file:: read_h_file2()` before any more calls to `hamiltonian_input_file:: read_h_parameters2()` are made.
    SUBROUTINE read_H_parameters2(path, &
                                  reduced_L_max, &
                                  coupling_id, &
                                  lplusp, &
                                  set_ML_max, &  ! TODO: Unneeded? Seems that ML_max is-1 if not set.
                                  xy_plane_desired, &
                                  nelc, &
                                  nz, &
                                  lrang2, &
                                  lamax, &
                                  ntarg, &
                                  rmatr, &
                                  bbloch, &
                                  etarg, &
                                  ltarg, &
                                  starg, &
                                  nchmx, &
                                  nstmx, &
                                  lmaxp1, &
                                  no_of_L_blocks, &
                                  no_of_LML_blocks, &
                                  ML_max, &
                                  kept, &
                                  nstk, &
                                  nchn, &
                                  L_max, &
                                  last_lrgl, &
                                  inast)
        CHARACTER(len=*), INTENT(IN):: path
        INTEGER, INTENT(IN):: reduced_L_max, coupling_id, lplusp
        LOGICAL, INTENT(IN):: set_ML_max, xy_plane_desired
        INTEGER, INTENT(OUT):: nelc, nz, lrang2, lamax, ntarg
        REAL(wp), INTENT(OUT):: rmatr, bbloch
        REAL(wp), DIMENSION(:), ALLOCATABLE, INTENT(OUT):: etarg
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(OUT):: ltarg, starg
        INTEGER, INTENT(INOUT):: nchmx, no_of_L_blocks, no_of_LML_blocks
        INTEGER, INTENT(OUT):: nstmx, lmaxp1
        INTEGER, INTENT(INOUT):: ML_max
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(OUT):: kept, nstk, nchn
        INTEGER, INTENT(INOUT):: L_max
        INTEGER, INTENT(OUT):: last_lrgl, inast

        INTEGER:: nL, L_i

        IF (ALLOCATED(read_H_cache_H_file)) THEN
            ERROR STOP "`read_H_parameters2` has been called twice in a row without a call to `read_H_file2`."
        END IF

        ALLOCATE (read_H_cache_H_file)
        read_H_cache_H_file = parse_H_file(path)

        nL = SIZE(read_H_cache_H_file%L_blocks)

        ALLOCATE (kept(nL), nstk(nL), nchn(nL))
        kept = -1
        nstk = -1
        nchn = -1

        nelc = INT(read_H_cache_H_file%nelc)  ! TODO: Used to calculate `Z_minus_N`, but otherwise unused.
        nz = INT(read_H_cache_H_file%nz)  ! TODO: Used to calculate `Z_minus_N`, but otherwise unused.
        lrang2 = INT(read_H_cache_H_file%lrang2)  ! TODO: Unused outside of reading H file.
        lamax = INT(read_H_cache_H_file%lamax)
        ntarg = INT(read_H_cache_H_file%ntarg)
        rmatr = REAL(read_H_cache_H_file%rmatr, wp)  ! TODO: Renamed to `r_at_region_bndry`, both names used.
        bbloch = REAL(read_H_cache_H_file%bbloch, wp)  ! TODO: Unused.

        etarg = REAL(read_H_cache_H_file%etarg, wp)
        starg = INT(read_H_cache_H_file%starg)
        ltarg = INT(read_H_cache_H_file%ltarg)  ! TODO: Unused.

        ! TODO: Can these be replaced with `MAXIMUM(read_H_cache_H_file%L_blocks%nchn)`, `SUM(kept)`, and a sum
        !       over a temporary array respectively?
        nchmx = 0
        no_of_L_blocks = 0
        no_of_LML_blocks = 0
        L_max = 0
        DO L_i = 1, nL
            ASSOCIATE (L => read_H_cache_H_file%L_blocks(L_i))
                ! NOTE: This assumes the L blocks are ordered by their L value.
                IF (reduced_L_max /= -1 .AND. L%lrgl > reduced_L_max) EXIT

                ! TODO: Should this really be increased if the symmetry isn't included? Test this by comparing the allocated size
                ! to the maximum of the respective array. If not then we can just use `MAXVAL` on it after the array.
                nchmx = MAX(nchmx, INT(L%nchan))
                L_max = MAX(L_max, INT(L%lrgl))

                ! TODO: Should this be set if the symmetry isn't include?
                nstk(L_i) = INT(L%nstat)


                IF (include_symmetry(L%lrgl, L%npty, lplusp, ML_max, coupling_id)) THEN
                    kept(L_i) = 1
                    no_of_L_blocks = no_of_L_blocks+1
                    nchn(no_of_L_blocks) = INT(L%nchan)
                    no_of_LML_blocks = no_of_LML_blocks + &
                                       n_ML_blocks_in_L_block(L%lrgl, &
                                                              L%npty, &
                                                              set_ML_max, &
                                                              ML_max, &
                                                              xy_plane_desired, &
                                                              coupling_id)
                ELSE
                    kept(L_i) = 0
                END IF
            END ASSOCIATE
        END DO

        IF (.NOT. set_ML_max) ML_max = L_max
        inast = nL
        last_lrgl = INT(read_H_cache_H_file%L_blocks(nL)%lrgl)
        lmaxp1 = last_lrgl+1  ! TODO: Unused.

        ! TODO: `nstk` and `kept` are already of size `nL`.
        nstk = nstk(:nL)
        kept = kept(:nL)
        nchn = nchn(:no_of_L_blocks)

        nstmx = MAXVAL(nstk)
    END SUBROUTINE read_H_parameters2

    SUBROUTINE log_H_parameters(nchmx, lplusp, L_max, ML_max, no_of_L_blocks, no_of_LML_blocks, coupling_id)
        INTEGER, INTENT(IN):: nchmx, lplusp, L_max, ML_max, no_of_L_blocks, no_of_LML_blocks, coupling_id

        INTEGER:: nL, L_i
        LOGICAL:: symmetry_included

        IF (.NOT. ALLOCATED(read_H_cache_H_file)) THEN
            ERROR STOP "`log_H_parameters` can only be called after a call to `read_H_parameters2` and before the corresponding&
                        & call to `read_H_file2`."
        END IF

        nL = SIZE(read_H_cache_H_file%L_blocks)

        WRITE (*, "(A)") " States Included"
        WRITE (*, "(6A9)") "L", "S", "PI", "#chans", "#states", "include?"
        WRITE (*, "(A, A)") REPEAT(" ", 5), REPEAT("=", 6*9 - 5)

        DO L_i = 1, nL
            ASSOCIATE (L => read_H_cache_H_file%L_blocks(L_i))
                    symmetry_included = &
                     include_symmetry(L%lrgl, L%npty, lplusp, ML_max, coupling_id)
                WRITE (*, "(5I9, L9)") L%lrgl, L%nspn, L%npty, L%nchan, L%nstat, symmetry_included
            END ASSOCIATE
        END DO

        WRITE (*, *)

        WRITE (*, *) "Number of electrons : ", read_H_cache_H_file%nelc
        WRITE (*, *) "Atomic number       : ", read_H_cache_H_file%nz
        WRITE (*, *) "R-matrix boundary   : ", read_H_cache_H_file%rmatr
        WRITE (*, *) ""

        WRITE (*, *) 'Max # of channels   : ', nchmx
        WRITE (*, *) 'L_max               : ', L_max
        WRITE (*, *) 'No_Of_L_Blocks      : ', no_of_L_blocks
        WRITE (*, *) 'No_Of_LML_Blocks    : ', no_of_LML_blocks
    END SUBROUTINE

    PURE FUNCTION non_contributing_eigenstates(wmat, threshold) RESULT(non_contributing)
        REAL(wp), DIMENSION(:, :), INTENT(IN):: wmat
        REAL(wp), INTENT(IN):: threshold
        INTEGER:: non_contributing

        INTEGER:: channel, state, max_contributing

        max_contributing = 0
        DO channel = LBOUND(wmat, 1), UBOUND(wmat, 1)
            sloop: DO state = UBOUND(wmat, 2), LBOUND(wmat, 2), -1
                IF (ABS(wmat(channel, state)) > threshold) THEN
                    max_contributing = MAX(max_contributing, state)
                    EXIT sloop
                END IF
            END DO sloop
        END DO

        non_contributing = UBOUND(wmat, 2) - max_contributing
    END FUNCTION

    !> \brief Read data that the code needs from the H file.
    !>
    !> This function retrieves the remaining data from the parsed H file in exactly the same way as `readhd:: read_h_file()`, then
    !> deletes the cache that was created by `hamiltonian_input_file:: read_h_parameters2()`.
    !>
    !> **IMPORTANT:** Each call to `hamiltonian_input_file:: read_h_file2()` must have been preceded by _exactly_ one call to
    !> `hamiltonian_input_file:: read_h_parameters2()`.
    SUBROUTINE read_H_file2(adjust_gs_energy, &
                            gs_finast, &
                            gs_energy_desired, &
                            remove_eigvals_threshold, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            nchmx, &
                            nstmx, &
                            lplusp, &
                            set_ML_max, &
                            ML_max, &
                            reduced_L_max,&
                            reduced_lamax, &
                            coupling_id, &
                            xy_plane_desired, &
                            neigsrem_1st_sym, &
                            neigsrem_higher_syms, &
                            surf_amp_threshold_value, &
                            lamax, &
                            L_block_tot_nchan, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            L_block_nchan, &
                            mnp1, &
                            neigsrem, &
                            L_block_post, &
                            states_per_L_block, &
                            max_L_block_size, &
                            L_block_nconat, &
                            L_block_l2p, &
                            lrgt, &
                            eig, &
                            wmat, &
                            L_block_cf, &
                            LML_block_lrgl, &
                            LML_block_nspn, &
                            LML_block_npty, &
                            LML_block_nchan, &
                            LML_block_tot_nchan, &
                            LML_block_ml, &
                            LML_block_Lblk, &
                            LML_block_post, &
                            states_per_LML_block, &
                            LML_block_nconat, &
                            LML_block_l2p, &
                            m2p, &
                            lstartm1, &
                            LML_block_cf)
        LOGICAL, INTENT(IN):: adjust_gs_energy
        INTEGER, INTENT(IN):: gs_finast
        REAL(wp), INTENT(IN):: gs_energy_desired
        LOGICAL, INTENT(IN):: remove_eigvals_threshold
        INTEGER, INTENT(IN):: no_of_L_blocks, no_of_LML_blocks, nchmx, nstmx, lplusp
        LOGICAL, INTENT(IN):: set_ML_max
        INTEGER, INTENT(IN):: ML_max, reduced_L_max, reduced_lamax, coupling_id
        LOGICAL, INTENT(IN):: xy_plane_desired
        INTEGER, INTENT(IN):: neigsrem_1st_sym, neigsrem_higher_syms
        REAL(wp), INTENT(IN):: surf_amp_threshold_value
        INTEGER, INTENT(INOUT):: lamax
        INTEGER, INTENT(OUT):: L_block_tot_nchan, LML_block_tot_nchan
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT):: L_block_lrgl, L_block_nspn, L_block_npty, mnp1, L_block_nchan
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT):: L_block_post, neigsrem, states_per_L_block
        INTEGER, INTENT(OUT):: max_L_block_size
        INTEGER, DIMENSION(:, :), ALLOCATABLE, INTENT(INOUT):: L_block_nconat
        INTEGER, DIMENSION(:, :), ALLOCATABLE, INTENT(OUT):: L_block_l2p, lrgt
        REAL(wp), DIMENSION(:, :), ALLOCATABLE, INTENT(INOUT):: eig
        REAL(wp), DIMENSION(:, :, :), ALLOCATABLE, INTENT(INOUT):: wmat
        REAL(wp), DIMENSION(:, :, :, :), ALLOCATABLE, INTENT(OUT):: L_block_cf
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(OUT):: LML_block_lrgl, LML_block_nspn, LML_block_npty
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT):: LML_block_nchan
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(OUT):: LML_block_ml, LML_block_Lblk, LML_block_post, states_per_LML_block
        INTEGER, DIMENSION(:, :), ALLOCATABLE, INTENT(OUT):: LML_block_nconat, LML_block_l2p, m2p
        INTEGER, DIMENSION(:, :), ALLOCATABLE, INTENT(INOUT):: lstartm1
        REAL(wp), DIMENSION(:, :, :, :), ALLOCATABLE, INTENT(OUT):: LML_block_cf

        INTEGER:: L_i, i, j, stride, maxm, no_ML_values, shift_LML_blocks
        INTEGER:: term_counter, offset

        IF (.NOT. ALLOCATED(read_H_cache_H_file)) THEN
            ERROR STOP "`read_H_file2` called before `read_H_parameters2`."
        END IF

        ALLOCATE (L_block_lrgl(no_of_L_blocks), &
                  L_block_nspn(no_of_L_blocks), &
                  L_block_npty(no_of_L_blocks), &
                  L_block_nchan(no_of_L_blocks), &
                  mnp1(no_of_L_blocks), &
                  neigsrem(no_of_L_blocks), &
                  states_per_L_block(-1:no_of_L_blocks+2), &
                  L_block_post(0:no_of_L_blocks), &
                  L_block_nconat(read_H_cache_H_file%ntarg, no_of_L_blocks), &
                  L_block_l2p(nchmx, no_of_L_blocks), &
                  lrgt(nchmx, no_of_L_blocks), &
                  eig(nstmx, no_of_L_blocks), &
                  wmat(nchmx, nstmx, no_of_L_blocks), &
                  L_block_cf(nchmx, nchmx, lamax, no_of_L_blocks), &
                  LML_block_lrgl(no_of_LML_blocks), &
                  LML_block_nspn(no_of_LML_blocks), &
                  LML_block_npty(no_of_LML_blocks), &
                  LML_block_nchan(no_of_LML_blocks), &
                  LML_block_ml(no_of_LML_blocks), &
                  LML_block_Lblk(no_of_LML_blocks), &
                  LML_block_post(0:no_of_LML_blocks), &
                  states_per_LML_block(-1:no_of_LML_blocks+2), &
                  LML_block_nconat(read_H_cache_H_file%ntarg, no_of_LML_blocks), &
                  LML_block_l2p(nchmx, no_of_LML_blocks), &
                  m2p(nchmx, no_of_LML_blocks), &
                  lstartm1(no_of_LML_blocks, 2), &
                  LML_block_cf(nchmx, nchmx, lamax, no_of_LML_blocks))

        L_block_lrgl = 0  ! TODO: Unused outside of reading H file?
        L_block_nspn = 0  ! TODO: Unused?
        L_block_npty = 0  ! TODO: Unused?
        L_block_nchan = 0
        mnp1 = 0  ! TODO: Unused outside of reading H and D files?
        neigsrem = 0  ! TODO: Unused?
        states_per_L_block = 0  ! TODO: Unused outside reading and broadcasting H and D files.
        L_block_post(0) = 0
        L_block_post(1:) = -1
        L_block_nconat = 0  ! TODO: Unused outside of reading and broadcasting H and D files.
        L_block_l2p = 0
        lrgt = 0  ! TODO: Unused.
        eig = 0.0_WP  ! TODO: Unused outside of broadcasting H file.
        wmat = 0.0_WP  ! TODO: Unused outside of broadcasting H file.
        L_block_cf = 0.0_WP  ! TODO: Unused outside of reading H file.
        LML_block_lrgl = 0
        LML_block_nspn = 0
        LML_block_npty = 0
        LML_block_nchan = 0
        LML_block_ml = 0
        LML_block_Lblk = -1
        LML_block_post(0) = 0
        LML_block_post(1:) = -1
        states_per_LML_block = 0
        LML_block_nconat = 0
        LML_block_l2p = 0
        m2p = 0
        LML_block_cf = 0.0_WP

        i = 0
        DO L_i = 1, SIZE(read_H_cache_H_file%L_blocks)
            ASSOCIATE (L => read_H_cache_H_file%L_blocks(L_i))
                IF (reduced_L_max /= -1 .AND. L%lrgl > reduced_L_max) EXIT
                IF (.NOT. include_symmetry(L%lrgl, L%npty, lplusp, ML_max, coupling_id)) CYCLE

                i = i+1

                L_block_lrgl(i) = INT(L%lrgl)
                L_block_nspn(i) = INT(L%nspn)
                L_block_npty(i) = INT(L%npty)
                L_block_nchan(i) = INT(L%nchan)
                mnp1(i) = INT(L%nstat)
                L_block_nconat(:, i) = INT(L%nconat)
                L_block_l2p(:L%nchan, i) = INT(L%l2p)
                L_block_cf(:L%nchan, :L%nchan, :lamax, i) = REAL(L%cf, wp)
                eig(:L%nstat, i) = REAL(L%eig, wp)
                wmat(:L%nchan, :L%nstat, i) = REAL(L%wmat, wp)

                IF (remove_eigvals_threshold) THEN
                    neigsrem(i) = non_contributing_eigenstates(REAL(L%wmat, wp), surf_amp_threshold_value)
                ELSE IF (i == 1) THEN
                    neigsrem(i) = neigsrem_1st_sym
                ELSE
                    neigsrem(i) = neigsrem_higher_syms
                END IF

                states_per_L_block(i) = mnp1(i) - neigsrem(i)
                L_block_post(i) = L_block_post(i-1) + states_per_L_block(i)
            END ASSOCIATE
        END DO

        L_block_tot_nchan = SUM(L_block_nchan)  ! TODO: Unused outside of reading D files.

        max_L_block_size = MAXVAL(states_per_L_block)

        shift_LML_blocks = 0
        term_counter = 0
        DO i = 1, no_of_L_blocks
            CALL ML_stride_maxm(L_block_lrgl(i), &
                                L_block_npty(i), &
                                set_ML_max, &
                                ML_max, &
                                xy_plane_desired, &
                                coupling_id, &
                                stride, &
                                maxm, &
                                offset)
            no_ML_values = n_ML_blocks_in_L_block(L_block_lrgl(i), &
                                                  L_block_npty(i), &
                                                  set_ML_max, &
                                                  ML_max, &
                                                  xy_plane_desired, &
                                                  coupling_id)

            DO j = 1, no_ML_values
                term_counter = term_counter+1
                ! gs_finast refers to the index of the lml_block, but the energies are set at the l_block level. So, if
                ! the current lml block is the GS symmetry, then set the corresponding energy for the entire l block
                ! or, to put it another way, set the energy for all of the m values (j) for this l (i).
                IF (term_counter == gs_finast) THEN
                    call print_symmetry(L_block_lrgl(i), L_block_nspn(i), L_block_npty(i), -maxm+stride*(j-1), coupling_id)
                    IF (adjust_gs_energy) THEN
                        eig(1, i) = gs_energy_desired+read_H_cache_H_file%etarg(1)
                    END IF
                END IF

                states_per_LML_block(shift_LML_blocks+j) = states_per_L_block(i)
                LML_block_Lblk(shift_LML_blocks+j) = i
                LML_block_lrgl(shift_LML_blocks+j) = L_block_lrgl(i)
                LML_block_nspn(shift_LML_blocks+j) = L_block_nspn(i)
                LML_block_npty(shift_LML_blocks+j) = L_block_npty(i)
                ! TODO: Potential adjustment for half-integer j needed here, see fa81ef1f53a6a08fae96a65ac67adef34240c126.
                LML_block_ml(shift_LML_blocks+j) = -maxm + offset + stride*(j-1)
                LML_block_nchan(shift_LML_blocks+j) = L_block_nchan(i)

                LML_block_l2p(:L_block_nchan(i), shift_LML_blocks+j) = L_block_l2p(:L_block_nchan(i), i)
                LML_block_cf(:L_block_nchan(i), :L_block_nchan(i), :lamax, shift_LML_blocks+j) = &
                    L_block_cf(:L_block_nchan(i), :L_block_nchan(i), :lamax, i)
                LML_block_post(shift_LML_blocks+j) = &
                    LML_block_post(shift_LML_blocks+j - 1) + states_per_LML_block(shift_LML_blocks+j)

                LML_block_nconat(:, shift_LML_blocks+j) = L_block_nconat(:, i)
            END DO

            shift_LML_blocks = shift_LML_blocks+no_ML_values
        END DO

        states_per_LML_block(no_of_LML_blocks+1:) = 0
        LML_block_tot_nchan = SUM(LML_block_nchan)

        lstartm1 = 0  ! TODO: Unused.
        DO i = 2, no_of_LML_blocks
            lstartm1(i, LML_block_npty(i) + 1) = lstartm1(i-1, LML_block_npty(i-1) + 1) + LML_block_nchan(i-1)
        END DO

        CALL assert(reduced_lamax >= -1, 'Reset reduced_lamax to a positive value')
        IF (reduced_lamax /= -1 .AND. reduced_lamax <= lamax) THEN
            lamax = reduced_lamax
        END IF

        CALL check_coupling_id(L_block_lrgl, coupling_id)

        CALL deallocate_H_file(read_H_cache_H_file)
        DEALLOCATE (read_H_cache_H_file)
    END SUBROUTINE

    !> Given quantum numbers, pretty print the symmetry/term symbol
    SUBROUTINE  print_symmetry(lrgl, nspn, npty, ml, coupling_id)
        use initial_conditions, only: jK_coupling_id
        implicit none
        integer, intent(in) :: lrgl
        integer, intent(in) :: nspn
        integer, intent(in) :: npty
        integer, intent(in) :: ml
        integer, intent(in) :: coupling_id
        character :: L_letters(4) = ['S', 'P', 'D', 'F']

        IF (coupling_id == jK_coupling_id) THEN
            IF (modulo(lrgl, 2) == 0) THEN
                WRITE(*,"(30X,A1)") merge("e", "o", npty==0)
                WRITE(*,"('Ground state symmetry is (J=',I1,')')")  lrgl
                WRITE(*,"(29X,SPI2)") ml
            ELSE
                WRITE(*,"(32X,A1)") merge("e", "o", npty==0)
                WRITE(*,"('Ground state symmetry is (J=',I1,'/2)')")  lrgl
                WRITE(*,"(31X,SPI2,A2)") ml,"/2"
            END IF
        ELSE
            write(*,"(25X,I1,1X,A1)") nspn, merge("e", "o", npty==0)
            WRITE(*, "('Ground state symmetry is  ', A1)") L_letters(lrgl+1)
            write(*,"(26X,SPI2)") ml
        END IF
        WRITE(*,*)

    END SUBROUTINE print_symmetry

    SUBROUTINE check_coupling_id(L_block_lrgl, coupling_id)
        INTEGER, DIMENSION(:), INTENT(IN):: L_block_lrgl
        INTEGER, INTENT(IN):: coupling_id

        LOGICAL:: some_L_values_are_odd, all_L_values_are_even_or_odd

        some_L_values_are_odd = ANY(MOD(L_block_lrgl(:), 2) == 1)
        all_L_values_are_even_or_odd = ALL(MOD(L_block_lrgl(:), 2) == 1) .OR. ALL(MOD(L_block_lrgl(:), 2) == 0)

        SELECT CASE (coupling_id)
        CASE (LS_coupling_id)
            CALL assert(some_L_values_are_odd, 'coupling id should be set for jK coupling')
        CASE (jK_coupling_id)
            CALL assert(all_L_values_are_even_or_odd, 'coupling id should be set for LS coupling')
        END SELECT
    END SUBROUTINE

    SUBROUTINE log_atomic_structure(etarg, lrgls, nspns, nptys, nchans, nstates, neigsrem, posts, nconats, eigenvalues)
        REAL(wp), INTENT(IN):: etarg
        INTEGER, DIMENSION(:), INTENT(IN):: lrgls, nspns, nptys, nchans, nstates, neigsrem, posts
        INTEGER, DIMENSION(:, :), INTENT(IN):: nconats
        REAL(wp), DIMENSION(:, :), INTENT(IN):: eigenvalues

        INTEGER:: io, i, j

        CALL assert(SIZE(nspns) == SIZE(lrgls), "`nspns` has wrong size")
        CALL assert(SIZE(nptys) == SIZE(lrgls), "`nptys` has wrong size")
        CALL assert(SIZE(nchans) == SIZE(lrgls), "`nchans` has wrong size")
        CALL assert(SIZE(nstates) == SIZE(lrgls), "`nstates` has wrong size")
        CALL assert(SIZE(neigsrem) == SIZE(lrgls), "`neigsrem` has wrong size")
        CALL assert(SIZE(posts) >= SIZE(lrgls), "`posts` has wrong size")
        CALL assert(SIZE(nconats, 2) == SIZE(lrgls), "`nconats` has wrong size")

        OPEN (newunit = io, file="atomic_structure", status="replace")

        DO i = 1, SIZE(lrgls)
            WRITE (io, "(5A8)") "L", "S", "PI", "#chans", "#states"
            WRITE (io, "(5I8)") lrgls(i), nspns(i), nptys(i), nchans(i), nstates(i)
            WRITE (io, *) "# no of connected chans per target state"
            WRITE (io, *) (nconats(j, i), j = 1, SIZE(nconats, 1))
            WRITE (io, *) "neigrem for sym", i, "=", neigsrem(i)
            WRITE (io, *) "POSTI", posts(i), "EIGENVALUES:"
            WRITE (io, *) (eigenvalues(j, i) - etarg, j = 1, nstates(i) - neigsrem(i))
            WRITE (io, *) "EXCLUDED"
            WRITE (io, *) (eigenvalues(j, i) - etarg, j = nstates(i) - neigsrem(i) + 1, nstates(i))
            WRITE (io, *) 'Symmetry ', i, ' done'
            WRITE (io, *) "============================================================"
        END DO

        CLOSE (io)
    END SUBROUTINE

    SUBROUTINE log_wmat(path, nchans, nstates, wmat)
        CHARACTER(len=*), INTENT(IN):: path
        INTEGER, DIMENSION(:), INTENT(IN):: nchans, nstates
        REAL(wp), DIMENSION(:, :, :), INTENT(IN):: wmat

        INTEGER:: io, i, channel, state

        OPEN (newunit = io, file = path, form="formatted", status="replace")

        DO i = LBOUND(wmat, 3), UBOUND(wmat, 3)
            DO channel = 1, nchans(i)
                DO state = 1, nstates(i)
                    WRITE (io, *) i, channel, state, wmat(channel, state, i)
                END DO
            END DO
        END DO

        CLOSE (io)
    END SUBROUTINE

    SUBROUTINE log_total_channels(L_block_tot_nchan, LML_block_tot_nchan)
        INTEGER, INTENT(IN):: L_block_tot_nchan, LML_block_tot_nchan

        PRINT *, '# channels (LSPi   blocks) : ', L_block_tot_nchan
        PRINT *, '# channels (LMLSPi blocks) : ', LML_block_tot_nchan
    END SUBROUTINE

    SUBROUTINE log_value_integer_r1(name, array)
        CHARACTER(len=*), INTENT(IN):: name
        INTEGER, DIMENSION(:), INTENT(IN):: array

        INTEGER:: i

        PRINT *, name
        DO i = LBOUND(array, 1), UBOUND(array, 1)
            PRINT *, i, array(i)
        END DO
    END SUBROUTINE

    SUBROUTINE log_value_integer_r2(name, array)
        CHARACTER(len=*), INTENT(IN):: name
        INTEGER, DIMENSION(:, :), INTENT(IN):: array

        CALL log_value(name, RESHAPE(array, [SIZE(array)]))
    END SUBROUTINE

    SUBROUTINE log_debug_info(states_per_L_block, &
                              L_block_lrgl, &
                              L_block_npty, &
                              states_per_LML_block, &
                              LML_block_lrgl, &
                              LML_block_nspn, &
                              LML_block_npty, &
                              LML_block_ml, &
                              LML_block_nchan, &
                              LML_block_post, &
                              lstartm1)
        INTEGER, DIMENSION(:), INTENT(IN):: states_per_L_block, L_block_lrgl, L_block_npty, states_per_LML_block, LML_block_lrgl
        INTEGER, DIMENSION(:), INTENT(IN):: LML_block_nspn, LML_block_npty, LML_block_ml, LML_block_nchan, LML_block_post
        INTEGER, DIMENSION(:, :), INTENT(IN):: lstartm1

        CALL log_value("states_per_L_block", states_per_L_block)
        CALL log_value("L_block_lrgl", L_block_lrgl)
        CALL log_value("L_block_npty", L_block_npty)
        CALL log_value("states_per_LML_block", states_per_LML_block)
        CALL log_value("LML_block_lrgl", LML_block_lrgl)
        CALL log_value("LML_block_nspn", LML_block_nspn)
        CALL log_value("LML_block_npty", LML_block_npty)
        CALL log_value("LML_block_ml", LML_block_ml)
        CALL log_value("LML_block_nchan", LML_block_nchan)
        CALL log_value("LML_block_post", LML_block_post)
        CALL log_value("lstartm1", lstartm1)
    END SUBROUTINE
END MODULE hamiltonian_input_file
