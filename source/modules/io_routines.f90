! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief General routines for writing data arrays to file. Used only at checkpoints
!> and the end of a calculation to output wavefunction data, population data.
MODULE io_routines

    USE precisn,            ONLY: wp, decimal_precision_wp
    USE grid_parameters,    ONLY: x_1st, &
                                  x_last, &
                                  channel_id_1st, &
                                  channel_id_last, &
                                  my_channel_id_1st, &
                                  my_channel_id_last
    USE initial_conditions, ONLY: disk_path, &
                                  checkpts_per_run, &
                                  no_of_pes_to_use_outer, &
                                  no_of_pes_per_sector, &
                                  numsols => no_of_field_confs
    USE io_parameters,      ONLY: version
    USE mpi_communications, ONLY: all_processor_barrier_region, all_processor_barrier_inter_block
    USE readhd,             ONLY: max_L_block_size, &
                                  no_of_LML_blocks
    USE file_num,           ONLY: file_number

    IMPLICIT NONE

CONTAINS

    !> \brief   Create directory chain
    !> \authors J Benda
    !> \date    2018
    !>
    !> Creates the requested directory, including all needed parent directories
    !> that may have not existed before. This subroutine needs different system
    !> call in Unix and in Windows. The distinction is done by the presence of the
    !> environment variable 'SystemRoot', which is always defined in Windows
    !> (typically as 'C:\Windows').
    !>

    SUBROUTINE create_directory(path)

        CHARACTER(LEN=*), INTENT(IN) :: path
        CHARACTER(LEN=LEN(path))     :: fixedpath
        INTEGER :: i, ierr
        CHARACTER(LEN=1) :: fwd, bwd

        fwd = '/'
        bwd = CHAR(92) ! backslash

        ! Windows always defines the environment variable 'SystemRoot'
        CALL GET_ENVIRONMENT_VARIABLE('SystemRoot', STATUS=i)

        IF (i == 0) THEN
            
            ! For windows path, replace all forward slashes with backslashes
            FORALL (i=1:LEN(path)) fixedpath(i:i) = MERGE(bwd, path(i:i), path(i:i) == fwd)

            CALL EXECUTE_COMMAND_LINE("mkdir "//fixedpath//" 2>NUL")
        ELSE
            ! Unix-like
            CALL EXECUTE_COMMAND_LINE("mkdir -p "//path//" 2>/dev/null", CMDSTAT = ierr)
        END IF

    END SUBROUTINE create_directory

    ! Output the population data for each channel in unformatted binary data
    ! dump. The form of the file is
    ! <header info>
    ! <time data for checkpoint 1>
    ! <pop data for checkpoint 1>
    ! <time data for checkpoint 2>
    ! <pop data for checkpoint 2>
    ! ...
    ! The header information is overwritten on each checkpoint, and the time and
    ! pop data is appended to the file on each checkpoint

    SUBROUTINE binary_write_arrays(time_array,&
                                   popL_array,&
                                   array_name_6, &
                                   nrec,&
                                   nchans)

        CHARACTER(LEN=6), INTENT(IN) :: array_name_6
        INTEGER, INTENT(IN)          :: nrec
        INTEGER, INTENT(IN)          :: nchans
        REAL(wp), INTENT(IN)         :: time_array(nrec)
        REAL(wp), INTENT(IN)         :: popL_array(nrec, nchans, numsols)
        CHARACTER(LEN=128)           :: my_name
        CHARACTER(LEN=5)             :: path

        path = 'data/'

        CALL create_directory(disk_path//path)

        my_name = disk_path//path//array_name_6//'.'//version

        CALL write_data_info(nchans, nrec, my_name, numsols)

        OPEN (2, FILE=my_name, FORM='UNFORMATTED', ACCESS='STREAM', POSITION='APPEND')

        WRITE(2) popL_array

        CLOSE(2)
    
    END SUBROUTINE binary_write_arrays

    ! Overwrite the header information in the popchn output file with info about
    ! the number of channels, the number of records (data points) and the number
    ! of checkpts_per_run

    SUBROUTINE write_data_info(nchans, nrec, my_name, numsols)
        use calculation_parameters, only : delta_t
        use initial_conditions,     only : Timesteps_Per_Output
        INTEGER, INTENT(IN)          :: nchans, nrec, numsols
        CHARACTER(LEN=128),INTENT(IN):: my_name
        
        OPEN (2, FILE=my_name, FORM='UNFORMATTED', ACCESS='STREAM')

        WRITE(2) nchans, nrec, checkpts_per_run, numsols, Timesteps_Per_Output, delta_t

        CLOSE(2)
    
    END SUBROUTINE write_data_info

!----------------------------------------------------------------------
    SUBROUTINE get_array_name(channel,name6)

        INTEGER, INTENT(IN)           :: channel
        INTEGER                       :: width        
        CHARACTER(LEN=6), INTENT(OUT) :: name6
        CHARACTER(LEN=6)              :: channel_image,index_format               
        CHARACTER(LEN=4)              :: prefix               

        width = 1 + INT(LOG10(REAL(channel))) 
        width = MAX(width,2)   
        WRITE (index_format, '("(I",I1,".",I1,")")') width, width
        WRITE (channel_image, index_format) channel

        SELECT CASE (width)
        CASE (2) 
            prefix='popL'
        CASE (3)
            prefix='ppL'
        CASE (4)
            prefix='pL'
        CASE (5)
            prefix='p'
        END SELECT
        name6 = TRIM(prefix)//channel_image 

    END SUBROUTINE get_array_name


!-----------------------------------------------------------------------
! Write array to file path//array_name_6.version
! ALWAYS starts writing the array at The_Array'First
!-----------------------------------------------------------------------
    SUBROUTINE write_real_arrays(data_array_1, data_array_2, &
                                 array_name_6, &
                                 this_is_the_initial_stage, &
                                 data_1st, data_last, &
                                 index_of_first_data_pt, index_of_last_data_pt)

        CHARACTER(LEN=6), INTENT(IN) :: array_name_6
        LOGICAL, INTENT(IN)          :: this_is_the_initial_stage
        INTEGER, INTENT(IN)          :: data_1st, data_last
        INTEGER, INTENT(IN)          :: index_of_first_data_pt
        INTEGER, INTENT(IN)          :: index_of_last_data_pt
        REAL(wp), INTENT(IN)         :: data_array_1(data_1st:data_last)
        REAL(wp), INTENT(IN)         :: data_array_2(data_1st:data_last, numsols)
        CHARACTER(LEN=128)           :: my_name
        CHARACTER(LEN=5)             :: path
        INTEGER                      :: i,j
        CHARACTER(LEN=100)           :: writefmt
        CHARACTER(LEN=20)            :: ddp, eep, num
        INTEGER                      :: gap_size_pop = 2*decimal_precision_wp - 11

        WRITE (num, '(I0)') 1 + numsols
        WRITE (ddp, '(I0)') decimal_precision_wp + 8
        WRITE (eep, '(I0)') decimal_precision_wp - 1

        writefmt = "(" // TRIM(num) // "E"// TRIM(ddp) //"."// TRIM(eep) // "E3)"

        !! In all that follows, it is assumed that the Starting_Index of
        !! the array is Data_1st == The_Array'First.
        !! That's where printing begins.

        path = 'data/'

        CALL create_directory(disk_path//path)
        my_name = disk_path//path//array_name_6//'.'//version

        IF (this_is_the_initial_stage) THEN
            OPEN (2, FILE=my_name)
            ! add headers to file columns
            WRITE(2,'(7A)', advance='NO') '   Time'
            DO i = 1,numsols
                DO j=1,gap_size_pop
                    write(2,'(A)', advance='NO') ' '
                END DO
                write(2,'(I4.4)', advance='NO') i
            END DO
            write(2,'(A)', advance='YES') ' '
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ELSE
            OPEN (2, FILE=my_name, STATUS='OLD', POSITION='APPEND')
        END IF

        DO j = index_of_first_data_pt, index_of_last_data_pt
            WRITE (2, writefmt) data_array_1(j), data_array_2(j, :)
        END DO
        CLOSE (2)

    END SUBROUTINE write_real_arrays

!-----------------------------------------------------------------------
! Write my Psi column by column.  Binary format.
!-----------------------------------------------------------------------

    ! code adapted for mpi-parallelised channel distribution within a block when no_of_pes_per_sector > 1.
    ! Rather than gather everything into a high-memory large array on the mpi_block_comm leader,
    ! the leader collects info in turn and writes in turn. The unformatted writes are split into several
    ! unformatted lines, one for each task in the communicator.
    ! The routine below deals with and individual write.
    
    SUBROUTINE write_ground_wavefunc_outer(my_psi_outer, my_number_channels, my_block_id, i_count)

        COMPLEX(wp), INTENT(IN) :: my_psi_outer(x_1st:x_last, my_number_channels)
        INTEGER, INTENT(IN)     :: my_number_channels ! for the set corresponding to i_count
        INTEGER, INTENT(IN)     :: my_block_id
        INTEGER, INTENT(IN)     :: i_count
        ! corresponds to my_block_group_pe_id + 1 for the non-reading tasks 

        CHARACTER(LEN=128), SAVE      :: my_name
        CHARACTER(LEN=7), SAVE        :: path

        path = 'ground/'

        ! Every outer process attempts to create the output directory. At most one will
        ! succeed, and others need to wait for it before writing files.
        IF (i_count == 1) THEN
           CALL create_directory(disk_path//path)
           CALL all_processor_barrier_inter_block   ! barrier for first pes in each mpi_comm_block

           my_name = disk_path//path//'psi_outer'//TRIM(file_number(my_block_id,no_of_pes_to_use_outer))//'.'//version

           ! My_Psi_Outer
           OPEN (2, FILE=my_name, FORM='Unformatted')
        END IF
        WRITE (2) my_psi_outer
        IF (i_count == no_of_pes_per_sector) CLOSE (2)

    END SUBROUTINE write_ground_wavefunc_outer

!-----------------------------------------------------------------------

    SUBROUTINE write_ground_wavefunc_inner(my_psi_inner, my_block_id, i_am_block_master)
        USE distribute_hd_blocks2, ONLY: numrows_blocks

        COMPLEX(wp), INTENT(IN)   :: my_psi_inner(1:numrows_blocks)
        INTEGER, INTENT(IN)       :: my_block_id
        LOGICAL, INTENT(IN)       :: i_am_block_master

        CHARACTER(LEN=128)        :: my_name
        CHARACTER(LEN=7)          :: path

        path = 'ground/'

        ! Every inner process attempts to create the output directory. At most one will
        ! succeed, and others need to wait for it before writing files.
        CALL create_directory(disk_path//path)
        CALL all_processor_barrier_region

        ! only master cores of each block and those handling order 0 need to write:
        IF (i_am_block_master) THEN

            ! my_psi_inner
            my_name = disk_path//path//'psi_inner'//TRIM(file_number(my_block_id,no_of_LML_blocks))//'.'//version
            OPEN (2, FILE=my_name, FORM='Unformatted')
            WRITE (2) my_psi_inner
            CLOSE (2)

        END IF

    END SUBROUTINE write_ground_wavefunc_inner

!-----------------------------------------------------------------------
! Read my Psi column by column.  Binary format.
!-----------------------------------------------------------------------

    ! code adapted for mpi-parallelised channel distribution within a block when no_of_pes_per_sector > 1.
    ! Rather than read everything into a high-memory large array on the mpi_block_comm leader,
    ! the leader reads info in turn and sends to each non-reading task. The unformatted reads are split into
    ! several unformatted lines, one for each task in the communicator.
    ! The routine below deals with and individual read.
! NOTE: this routine in serial or parallel form is currently not used in the main code.
    SUBROUTINE read_ground_wavefunc_outer(my_psi_outer, my_number_channels, my_block_id, i_count)
        COMPLEX(wp), INTENT(OUT) :: my_psi_outer(x_1st:x_last, my_number_channels)
        INTEGER, INTENT(IN)     :: my_number_channels ! for the set corresponding to i_count
        INTEGER, INTENT(IN)     :: my_block_id
        INTEGER, INTENT(IN)     :: i_count   ! my_block_group_pe_id + 1 for the non-reading tasks

        CHARACTER(LEN=128), SAVE       :: my_name
        CHARACTER(LEN=7), SAVE         :: path

        path = 'ground/'

        my_name = disk_path//path//'psi_outer'//TRIM(file_number(my_block_id,no_of_pes_to_use_outer))//'.'//version

        IF (i_count == 1) OPEN (2, FILE=my_name, FORM='Unformatted')
        READ (2) my_psi_outer
        IF (i_count == no_of_pes_per_sector) CLOSE (2)

    END SUBROUTINE read_ground_wavefunc_outer

!-----------------------------------------------------------------------

    SUBROUTINE read_ground_wavefunc_inner(my_psi_inner, my_block_id, i_am_block_master)
      USE distribute_hd_blocks2, ONLY: numrows_blocks, numrows
      USE mpi_layer_lblocks, ONLY    : my_num_LML_blocks

        COMPLEX(wp), INTENT(OUT)   :: my_psi_inner(1:numrows_blocks)
        INTEGER, INTENT(IN)        :: my_block_id
        LOGICAL, INTENT(IN)        :: i_am_block_master

        COMPLEX(wp)                :: temp_array(max_L_block_size)
!        INTEGER, SAVE              :: first = 2
        ! first = 2 allows read in from checkpointed run with  lb_size > 1        
        INTEGER, SAVE              :: first = 3       
        integer                    :: i, sum, num, numb 
        CHARACTER(LEN=128)         :: my_name
        CHARACTER(LEN=7)           :: path

        path = 'ground/'

        my_psi_inner = (0.0_wp, 0.0_wp)

        ! only master cores of each block and those handling order 0 need to read:
        IF (i_am_block_master) THEN

            ! my_psi_inner
            my_name = disk_path//path//'psi_inner'//TRIM(file_number(my_block_id,no_of_LML_blocks))//'.'//version
            IF (first == 2) then 
               sum = 0
               DO i = 1, my_num_LML_blocks
                  num = numrows(i)
                  OPEN (2, FILE=my_name, FORM='Unformatted')
                  temp_array = (0.0_wp, 0.0_wp)
                  READ (2) temp_array
                  CLOSE (2)
                  my_psi_inner(sum+1:sum+num) = temp_array(1:num)
                  sum = sum + num
                  my_name = &
             disk_path//path//'psi_inner'//TRIM(file_number(my_block_id+i,no_of_LML_blocks))//'.'//version
               END DO   
               first = 3
            ELSE
               OPEN (2, FILE=my_name, FORM='Unformatted')
               READ (2) my_psi_inner
               CLOSE (2)
            END IF
        END IF

    END SUBROUTINE read_ground_wavefunc_inner

!-----------------------------------------------------------------------

    SUBROUTINE write_my_wavefunc_inner_checkpt(numrows_blocks, my_psi_inner, my_block_id, &
                                               i_am_block_master, checkpt)

        INTEGER, INTENT(IN)       :: numrows_blocks  
        COMPLEX(wp), INTENT(IN)   :: my_psi_inner(1:numrows_blocks, 1:numsols)
        INTEGER, INTENT(IN)       :: my_block_id, checkpt
        LOGICAL, INTENT(IN)       :: i_am_block_master

        CHARACTER(LEN=128)        :: my_name, checkpt_image, checkpt_format
        CHARACTER(:), ALLOCATABLE :: path

        INTEGER :: width

        ! pad the checkpoint index by zeros to make names of all checkpoint directories equally long
        width = 1 + INT(LOG10(REAL(MAX(checkpt, checkpts_per_run))))
        WRITE (checkpt_format, '("(I",I1,".",I1,")")') width, width
        WRITE (checkpt_image, checkpt_format) checkpt
        path = 'state/'//TRIM(checkpt_image)//'/'

        ! Every inner process attempts to create the output directory. At most one will
        ! succeed, and others need to wait for it before writing files.
        CALL create_directory(path)
        CALL all_processor_barrier_region

        ! only master cores of each block need to write:
        IF (i_am_block_master) THEN

            ! my_psi_inner
            my_name = disk_path//path//'psi_inner'//TRIM(file_number(my_block_id,no_of_LML_blocks))//'.'//version
            OPEN (2, FILE=my_name, FORM='Unformatted')
            WRITE (2) my_psi_inner
            CLOSE (2)

        END IF

    END SUBROUTINE write_my_wavefunc_inner_checkpt

!-----------------------------------------------------------------------
! Write my Psi column by column.  Binary format.  Give it name my_name
! where my_name = path // 'psi' // image(My_Task_ID) // '.' // image(version),
! where my_task_id and version are integers.
!-----------------------------------------------------------------------

    SUBROUTINE write_my_wavefunc_outer(my_psi_outer, my_block_id, my_discarded_pop_outer, &
                                       my_discarded_pop_outer_si)

!        COMPLEX(wp), INTENT(IN) :: my_psi_outer(x_1st:x_last, channel_id_1st:channel_id_last, 1:numsols)
!        REAL(wp), INTENT(IN)    :: my_discarded_pop_outer(channel_id_1st:channel_id_last, 1:numsols)
!        REAL(wp), INTENT(IN)    :: my_discarded_pop_outer_si(channel_id_1st:channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(IN) :: my_psi_outer(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(IN)    :: my_discarded_pop_outer(my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(IN)    :: my_discarded_pop_outer_si(my_channel_id_1st:my_channel_id_last, 1:numsols)
        INTEGER, INTENT(IN)     :: my_block_id

        CHARACTER(LEN=128)      :: my_name
        CHARACTER(LEN=6)        :: path

        path = 'state/'

        ! Every outer process attempts to create the output directory. At most one will
        ! succeed, and others need to wait for it before writing files.
        CALL create_directory(disk_path//path)
        CALL all_processor_barrier_region

        my_name = &
             disk_path//path//'psi_outer'//TRIM(file_number(my_block_id,no_of_pes_to_use_outer))//'.'//version

        OPEN (2, FILE=my_name, FORM='Unformatted')
        WRITE (2) my_psi_outer
        WRITE (2) my_discarded_pop_outer
        WRITE (2) my_discarded_pop_outer_si
        CLOSE (2)

    END SUBROUTINE write_my_wavefunc_outer

!-----------------------------------------------------------------------

       ! code for mpi-parallelised channel distribution within a block.
       ! Rather than gather everything into a high-memory large array on the mpi_block_comm leader,
       ! the leader collects info in turn and writes in turn. The unformatted writes are in order of
       ! wavefunction, discarded_pop and discarded_pop:si, but split into several unformatted lines,
       ! one for each task in the communicator.
       ! The routine below deals with and individual write.


    SUBROUTINE write_my_wavefunc_outer_parallel(my_psi_outer, my_block_id, my_discarded_pop_outer, &
         my_discarded_pop_outer_si, loop, i_count, isol, my_number_channels)

        COMPLEX(wp), INTENT(INOUT) :: my_psi_outer(x_1st:x_last, my_number_channels)
      !  a particular isol = 1 to numsols is written out
        REAL(wp), INTENT(INOUT)    :: my_discarded_pop_outer(my_number_channels)
        REAL(wp), INTENT(INOUT)    :: my_discarded_pop_outer_si(my_number_channels)
        INTEGER, INTENT(IN)        :: my_block_id
        INTEGER, INTENT(IN)        :: i_count    ! my_block_group_pe_id + 1
        INTEGER, INTENT(IN)        :: loop  ! 1 for my_psi_outer, 2 for dis pop, 3 for disc pop su
        INTEGER, INTENT(IN)        :: isol  ! needed for first and last to open/close the file
        INTEGER, INTENT(IN)        :: my_number_channels

        CHARACTER(LEN=128), save      :: my_name
        CHARACTER(LEN=6), save        :: path

        path = 'state/'

        ! Every outer process attempts to create the output directory. At most one will
        ! succeed, and others need to wait for it before writing files.
        IF (loop == 1) THEN 
           IF (i_count == 1 .and. isol == 1) THEN
              CALL create_directory(disk_path//path)
              CALL all_processor_barrier_inter_block   ! barrier for first pes in each mpi_comm_block

              my_name = &
       disk_path//path//'psi_outer'//TRIM(file_number(my_block_id,no_of_pes_to_use_outer))//'.'//version
              OPEN (2, FILE=my_name, FORM='Unformatted')
           END IF

           WRITE (2) my_psi_outer
        END IF
        IF (loop == 2) THEN
           WRITE (2) my_discarded_pop_outer
        END IF
        IF (loop == 3) THEN
           WRITE (2) my_discarded_pop_outer_si
           IF (i_count == no_of_pes_per_sector .and. isol == numsols)  CLOSE (2)
        END IF

    END SUBROUTINE write_my_wavefunc_outer_parallel

!-----------------------------------------------------------------------

    SUBROUTINE write_my_wavefunc_outer_checkpt(my_psi_outer, &
                                               my_block_id, &
                                               my_discarded_pop_outer, &
                                               my_discarded_pop_outer_si, &
                                               checkpt)

!        COMPLEX(wp), INTENT(IN)   :: my_psi_outer(x_1st:x_last, channel_id_1st:channel_id_last, 1:numsols)
!        REAL(wp), INTENT(IN)      :: my_discarded_pop_outer(channel_id_1st:channel_id_last, 1:numsols)
!        REAL(wp), INTENT(IN)      :: my_discarded_pop_outer_si(channel_id_1st:channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(IN)   :: my_psi_outer(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(IN)      :: my_discarded_pop_outer(my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(IN)      :: my_discarded_pop_outer_si(my_channel_id_1st:my_channel_id_last, 1:numsols)
        INTEGER, INTENT(IN)       :: my_block_id, checkpt

        CHARACTER(LEN=128)        :: my_name, CheckPt_Image, CheckPt_Format
        CHARACTER(:), ALLOCATABLE :: path

        INTEGER :: width

        ! pad the checkpoint index by zeros to make names of all checkpoint directories equally long
        width = 1 + INT(LOG10(REAL(MAX(checkpt, checkpts_per_run))))
        WRITE (checkpt_format, '("(I",I1,".",I1,")")') width, width
        WRITE (checkpt_image, checkpt_format) checkpt
        path = 'state/'//TRIM(checkpt_image)//'/'

        ! Every outer process attempts to create the output directory. At most one will
        ! succeed, and others need to wait for it before writing files.
        CALL create_directory(path)
        CALL all_processor_barrier_region

        my_name = disk_path//path//'psi_outer'//TRIM(file_number(my_block_id,no_of_pes_to_use_outer))//'.'//version

        OPEN (2, FILE=my_name, FORM='Unformatted')
        WRITE (2) my_psi_outer
        WRITE (2) my_discarded_pop_outer
        WRITE (2) my_discarded_pop_outer_si
        CLOSE (2)

    END SUBROUTINE write_my_wavefunc_outer_checkpt

!-----------------------------------------------------------------------

       ! code for mpi-parallelised channel distribution within a block.
       ! Rather than gather everything into a high-memory large array on the mpi_block_comm leader,
       ! the leader collects info in turn and writes in turn. The unformatted writes are in order of
       ! wavefunction, discarded_pop and discarded_pop:si, but split into several unformatted lines,
       ! one for each task in the communicator.
       ! The routine below deals with and individual write.

    SUBROUTINE write_my_wavefunc_outer_checkpt_parallel(my_psi_outer, &
                                               my_block_id, &
                                               my_discarded_pop_outer, &
                                               my_discarded_pop_outer_si, &
                                               checkpt, loop, i_count, isol, my_number_channels)

        COMPLEX(wp), INTENT(INOUT) :: my_psi_outer(x_1st:x_last, my_number_channels)
      !  a particular isol = 1 to numsols is written out
        REAL(wp), INTENT(INOUT)    :: my_discarded_pop_outer(my_number_channels)
        REAL(wp), INTENT(INOUT)    :: my_discarded_pop_outer_si(my_number_channels)
        INTEGER, INTENT(IN)        :: my_block_id, checkpt
        INTEGER, INTENT(IN)        :: i_count    ! my_block_group_pe_id + 1
        INTEGER, INTENT(IN)        :: loop  ! 1 for my_psi_outer, 2 for dis pop, 3 for disc pop su
        INTEGER, INTENT(IN)        :: isol  ! needed for first and last to open/close the file
        INTEGER, INTENT(IN)        :: my_number_channels

        CHARACTER(LEN=128), save        :: my_name, CheckPt_Image, CheckPt_Format
        CHARACTER(:), ALLOCATABLE, save :: path

        INTEGER, save :: width

        IF (loop == 1) THEN 
           IF (i_count == 1 .and. isol == 1) THEN
        ! pad the checkpoint index by zeros to make names of all checkpoint directories equally long
              width = 1 + INT(LOG10(REAL(MAX(checkpt, checkpts_per_run))))
              WRITE (checkpt_format, '("(I",I1,".",I1,")")') width, width
              WRITE (checkpt_image, checkpt_format) checkpt
              path = 'state/'//TRIM(checkpt_image)//'/'

        ! Every outer process attempts to create the output directory. At most one will
        ! succeed, and others need to wait for it before writing files.
              CALL create_directory(path)
              CALL all_processor_barrier_inter_block   ! barrier for first pes in each mpi_comm_block

               my_name = &
         disk_path//path//'psi_outer'//TRIM(file_number(my_block_id,no_of_pes_to_use_outer))//'.'//version

               OPEN (3, FILE=my_name, FORM='Unformatted')
           END IF

           WRITE (3) my_psi_outer
        END IF
        IF (loop == 2) THEN
           WRITE (3) my_discarded_pop_outer
        END IF
        IF (loop == 3) THEN
           WRITE (3) my_discarded_pop_outer_si
           IF (i_count == no_of_pes_per_sector .and. isol == numsols)  CLOSE (3)
        END IF 

    END SUBROUTINE write_my_wavefunc_outer_checkpt_parallel

!-----------------------------------------------------------------------

    SUBROUTINE write_my_wavefunc_inner(numrows_blocks, my_psi_inner, my_block_id, &
                                       i_am_block_master)

        INTEGER, INTENT(IN)       :: numrows_blocks  
        COMPLEX(wp), INTENT(IN)   :: my_psi_inner(1:numrows_blocks, 1:numsols)
        INTEGER, INTENT(IN)       :: my_block_id
        LOGICAL, INTENT(IN)       :: i_am_block_master

        CHARACTER(LEN=128)        :: my_name
        CHARACTER(LEN=6)          :: path

        path = 'state/'

        ! Every inner process attempts to create the output directory. At most one will
        ! succeed, and others need to wait for it before writing files.
        CALL create_directory(disk_path//path)
        CALL all_processor_barrier_region

        ! only master cores of each block and those handling order 0 need to write:
        IF (i_am_block_master) THEN

            ! my_psi_inner
            my_name = disk_path//path//'psi_inner'//TRIM(file_number(my_block_id,no_of_LML_blocks))//'.'//version
            OPEN (2, FILE=my_name, FORM='Unformatted')
            WRITE (2) my_psi_inner
            CLOSE (2)

        END IF

    END SUBROUTINE write_my_wavefunc_inner

!-----------------------------------------------------------------------
! Read in My_Psi column by column.  Binary format.  Give it name my_name
! where my_name = path // 'psi' // Image(My_Task_ID) // '.' // Image(version),
! where My_Task_ID and version are integers.
!-----------------------------------------------------------------------

    SUBROUTINE read_my_wavefunc_outer(my_psi_outer, my_block_id, my_discarded_pop_outer, &
                                      my_discarded_pop_outer_si)

        COMPLEX(wp), INTENT(OUT) :: my_psi_outer(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(OUT)    :: my_discarded_pop_outer(my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(OUT)    :: my_discarded_pop_outer_si(my_channel_id_1st:my_channel_id_last, 1:numsols)
        INTEGER, INTENT(IN)      :: my_block_id

        CHARACTER(LEN=128) :: my_name
        CHARACTER(LEN=6)   :: path
        INTEGER            :: stat

        my_psi_outer = (0.0_wp, 0.0_wp)

        path = 'state/'

        my_name = &
             disk_path//path//'psi_outer'//TRIM(file_number(my_block_id,no_of_pes_to_use_outer))//'.'//version

        OPEN (2, FILE=my_name, FORM='Unformatted')
        READ (2) my_psi_outer

        READ (UNIT=2, IOSTAT=stat) my_discarded_pop_outer
        IF (stat /= 0) my_discarded_pop_outer = 0

        READ (UNIT=2, IOSTAT=stat) my_discarded_pop_outer_si
        IF (stat /= 0) my_discarded_pop_outer_si = 0
        CLOSE (2)

    END SUBROUTINE read_my_wavefunc_outer

!-----------------------------------------------------------------------

    ! code for mpi-parallelised channel distribution within a block.
    ! Rather than read everything into a high-memory large array on the mpi_block_comm leader, then
    ! distribute, the leader reads info and sends to the other tasks in turn. The unformatted reads
    ! are in order of wavefunction, discarded_pop and discarded_pop:si, but split into several
    ! unformatted lines, one for each task in the communicator.
    ! The routine below deals with and individual read
    
    SUBROUTINE read_my_wavefunc_outer_parallel(my_psi_outer, my_block_id, my_discarded_pop_outer, &
                                  my_discarded_pop_outer_si, loop, i_count, isol, my_number_channels)
      ! loop, i, isol are passed, needed to tell the routine whether to open/close the file and what to read.
        use mpi_communications, only: get_my_pe_id      
        COMPLEX(wp), INTENT(INOUT) :: my_psi_outer(x_1st:x_last, my_number_channels)
        !  a particular isol = 1 to numsols is read in
        REAL(wp), INTENT(INOUT)    :: my_discarded_pop_outer(my_number_channels)
        REAL(wp), INTENT(INOUT)    :: my_discarded_pop_outer_si(my_number_channels)
        INTEGER, INTENT(IN)        :: my_block_id
        INTEGER, INTENT(IN)        :: i_count    ! my_block_group_pe_id + 1
        INTEGER, INTENT(IN)        :: loop  ! 1 for my_psi_outer, 2 for dis pop, 3 for disc pop su
        INTEGER, INTENT(IN)        :: isol  ! needed for first and last to open/close the file
        INTEGER, INTENT(IN)        :: my_number_channels
        integer                    :: my_pe_id, i_write
        
        CHARACTER(LEN=128), SAVE :: my_name
        CHARACTER(LEN=6), SAVE   :: path
        INTEGER            :: stat


        path = 'state/'

        my_name = disk_path//path//'psi_outer'//TRIM(file_number(my_block_id,no_of_pes_to_use_outer))//'.'//version

        IF (loop == 1) THEN
           IF (i_count == 1 .and. isol == 1) OPEN (2, FILE=my_name, FORM='Unformatted')
           my_psi_outer = (0.0_wp, 0.0_wp)
           READ (2) my_psi_outer
        END IF
        IF (loop == 2) THEN
           READ (UNIT=2, IOSTAT=stat) my_discarded_pop_outer
           IF (stat /= 0) my_discarded_pop_outer = 0
        END IF
        IF (loop == 3) THEN
           READ (UNIT=2, IOSTAT=stat) my_discarded_pop_outer_si
           IF (stat /= 0) my_discarded_pop_outer_si = 0
           IF (i_count == no_of_pes_per_sector .and. isol == numsols)  CLOSE (2)
        END IF
    END SUBROUTINE read_my_wavefunc_outer_parallel

!-----------------------------------------------------------------------

    SUBROUTINE read_my_wavefunc_inner(my_psi_inner, my_block_id, i_am_block_master)
      USE distribute_hd_blocks2, ONLY: numrows_blocks, numrows
      USE mpi_layer_lblocks,     ONLY: my_num_LML_blocks

        COMPLEX(wp), INTENT(OUT)   :: my_psi_inner(numrows_blocks, 1:numsols)
        COMPLEX(wp)                :: temp_array(max_L_block_size)
        INTEGER, INTENT(IN)        :: my_block_id
        LOGICAL, INTENT(IN)        :: i_am_block_master

        CHARACTER(LEN=128) :: my_name
        CHARACTER(LEN=6)   :: path

!        INTEGER, SAVE              :: first = 2       
        ! first = 2 allows read in from checkpointed run with  lb_size > 1        
        integer, SAVE              :: first = 3       
        INTEGER                    :: i, sum, num, numb 

        my_psi_inner = (0.0_wp, 0.0_wp)

        path = 'state/'

        ! Only master cores of each block and those handling order 0 need to read:
        IF (i_am_block_master) THEN

            ! my_psi_inner
            my_name = disk_path//path//'psi_inner'//TRIM(file_number(my_block_id,no_of_LML_blocks))//'.'//version
            IF (first == 2) then
               sum = 0
               DO i = 1, my_num_LML_blocks
                  num = numrows(i)
                  OPEN (2, FILE=my_name, FORM='Unformatted')
                  temp_array = (0.0_wp, 0.0_wp)
                  READ (2) temp_array
                  CLOSE (2)
                  my_psi_inner(sum+1:sum+num,1) = temp_array(1:num)
                  sum = sum + num
                  my_name = &
            disk_path//path//'psi_inner'//TRIM(file_number(my_block_id+i,no_of_LML_blocks))//'.'//version
               END DO 
               first = 3
            ELSE
               OPEN (2, FILE=my_name, FORM='Unformatted')
               READ (2) my_psi_inner
               CLOSE (2)

            END IF
        END IF

    END SUBROUTINE read_my_wavefunc_inner

!-----------------------------------------------------------------------
! READ IN STAGE OF COMPUTATION FROM StatusFile
! Start_of_Run_TimeIndex OUT parameter, version is an IN
! parameter.  The name of the status file that it read is fixed,
! except for a tag at the end that equals the version number.
! This stuff is for breaking the integration into several stages
! So you can run the program shorter batch queues on the Cray.
!-----------------------------------------------------------------------

    SUBROUTINE get_stage(timeindex_first, stage_first, start_of_run_timeindex, stage)

        INTEGER, INTENT(IN)  :: timeindex_first, stage_first
        INTEGER, INTENT(OUT) :: start_of_run_timeindex, stage

        INTEGER              :: previous_timeindex
        INTEGER              :: previous_stage
        CHARACTER(LEN=128)   :: statusfilename
        CHARACTER(LEN=6)     :: statusfileroot

        LOGICAL, PARAMETER   :: outputdesired = .true.

        ! Status of the computation is stored in file: hstat.xyz
        CALL initialize_status_file_root(statusfileroot)
        statusfilename = disk_path//statusfileroot//version

        OPEN (UNIT=2, FILE=statusfilename, STATUS='OLD')
        READ (2, *) previous_stage
        READ (2, *) previous_timeindex
        CLOSE (UNIT=2)

        IF (previous_stage <= (stage_first - 1)) THEN
            previous_stage = stage_first - 1
        END IF
        stage = previous_stage + 1

        IF (previous_timeindex <= (timeindex_first)) THEN
            start_of_run_timeindex = timeindex_first
        ELSE
            start_of_run_timeindex = previous_timeindex 
        END IF

        IF (outputdesired) THEN
            PRINT *, 'Stage info.'
            PRINT *, '==========='
            PRINT *, 'This is stage:                    ', stage
            PRINT *, 'The run_timeindex will start from:', start_of_run_timeindex
            PRINT *, ""
        END IF

    END SUBROUTINE get_stage

!-----------------------------------------------------------------------
! Store Status file root name.
!-----------------------------------------------------------------------

    SUBROUTINE initialize_status_file_root(statusfileroot)

        CHARACTER(LEN=6), INTENT(OUT) :: statusfileroot

        statusfileroot = 'hstat.'

    END SUBROUTINE initialize_status_file_root

!-----------------------------------------------------------------------
! CHECK StatusFile exists
!-----------------------------------------------------------------------

    SUBROUTINE check_status_file_exists(status_file_exists)

        USE calculation_parameters, ONLY: make_status_file_if_missing

        CHARACTER(LEN=128)    :: statusfilename
        CHARACTER(LEN=6)      :: statusfileroot
        LOGICAL, INTENT(OUT)  :: status_file_exists

        CALL initialize_status_file_root(statusfileroot)

        statusfilename = disk_path//statusfileroot//version

        INQUIRE (FILE=statusfilename, EXIST=status_file_exists)

        IF ((.NOT. status_file_exists) .AND. make_status_file_if_missing) THEN
            PRINT *, 'CREATING STATUS FILE: ', statusfilename
            CALL create_status_file(statusfilename)
        END IF
        INQUIRE (FILE=statusfilename, EXIST=status_file_exists)

    END SUBROUTINE check_status_file_exists

!-----------------------------------------------------------------------
! If Status File Does Not Exist then create it
!-----------------------------------------------------------------------

    SUBROUTINE create_status_file(statusfilename)

        CHARACTER(LEN=128), INTENT(IN)  :: statusfilename
        INTEGER :: j

        OPEN (2, FILE=statusfilename)
        DO j = 1, 2
            WRITE (2, *) - 1
        END DO
        CLOSE (2)

    END SUBROUTINE create_status_file

!-----------------------------------------------------------------------
! WRITE STAGE OF COMPUTATION TO StatusFile
!-----------------------------------------------------------------------

    SUBROUTINE update_status(previous_stage, &
                             previous_timeindex, &
                             timesteps_per_output, &
                             num_pes_to_use, &
                             num_pes_to_use_inner, &
                             num_pes_to_use_outer, &
                             Z_minus_N, &
                             lplusp, &
                             set_ML_max, &
                             ML_max, &
                             r_at_region_boundary, &
                             deltat, &
                             delr, &
                             offset_in_stepsizes, &
                             first_grid_point_on_pe, &
                             last_grid_point_on_pe, &
                             first_channel_id, &
                             last_channel_id, &
                             dim_of_inner_ham_block_on_pe, &
                             taylors_order, &
                             absorption_desired, &
                             absorption_interval, &
                             sigma_factor, &
                             start_factor, &
                             use_2colour_field, &
                             ellipticity, &
                             intensity, &
                             frequency, &
                             periods_of_ramp_on, &
                             periods_of_pulse, &
                             ellipticity_2, &
                             intensity_2, &
                             frequency_2, &
                             periods_of_ramp_on_2, &
                             periods_of_pulse_2, &
                             delay_XUV_in_IR_periods,&
                             use_pulse_train,&
                             APT_bursts,&
                             APT_ramp_bursts,&
                             APT_envelope_power,&
                             angle_between_pulses_in_deg)

        INTEGER, INTENT(IN)    :: previous_stage, previous_timeindex, timesteps_per_output
        INTEGER, INTENT(IN)    :: num_pes_to_use, num_pes_to_use_inner, num_pes_to_use_outer
        REAL(wp), INTENT(IN)   :: Z_minus_N
        INTEGER, INTENT(IN)    :: lplusp
        INTEGER, INTENT(IN)    :: ML_max
        LOGICAL, INTENT(IN)    :: set_ML_max
        REAL(wp), INTENT(IN)   :: ellipticity, ellipticity_2
        REAL(wp), INTENT(IN)   :: r_at_region_boundary, deltat, delr, offset_in_stepsizes
        INTEGER, INTENT(IN)    :: first_grid_point_on_pe, last_grid_point_on_pe
        INTEGER, INTENT(IN)    :: first_channel_id, last_channel_id
        INTEGER, INTENT(IN)    :: dim_of_inner_ham_block_on_pe
        INTEGER, INTENT(IN)    :: taylors_order, absorption_interval
        REAL(wp), INTENT(IN)   :: sigma_factor, start_factor
        LOGICAL, INTENT(IN)    :: use_2colour_field, absorption_desired
        REAL(wp), INTENT(IN)   :: intensity, frequency
        REAL(wp), INTENT(IN)   :: periods_of_ramp_on, periods_of_pulse
        REAL(wp), INTENT(IN)   :: intensity_2, frequency_2
        REAL(wp), INTENT(IN)   :: periods_of_ramp_on_2, periods_of_pulse_2
        Real(wp), INTENT(IN)   :: delay_XUV_in_IR_periods
        LOGICAL, INTENT(IN)    :: use_pulse_train
        REAL(wp), INTENT(IN)   :: APT_bursts, APT_ramp_bursts, APT_envelope_power
        REAL(wp), INTENT(IN)   :: angle_between_pulses_in_deg

        CHARACTER(LEN=128)     :: statusfilename
        CHARACTER(LEN=6)       :: statusfileroot
        REAL(wp)               :: field_period

        ! Status of the computation is stored in file: hstat.xyz
        CALL initialize_status_file_root(statusfileroot)

        statusfilename = disk_path//statusfileroot//version
        field_period = 3.141592653589793_wp*2.0_wp/frequency

        OPEN (UNIT=2, FILE=statusfilename)

        WRITE (2, *) previous_stage, &
            '  Previous Stage number'
        WRITE (2, *) previous_timeindex, &
            '  Most recently used value of TimeIndex'
        WRITE (2, *) ' '
        WRITE (2, *) 'Version                      =    ', version
        WRITE (2, *) 'Time (units of field period) = ', &
            previous_timeindex*deltat/field_period
        WRITE (2, *) 'Timesteps_Per_Output         = ', timesteps_per_output
        WRITE (2, *) ' '
        WRITE (2, *) 'Number of PEs                = ', num_pes_to_use
        WRITE (2, *) 'Number of PEs Inner Region   = ', num_pes_to_use_inner
        WRITE (2, *) 'Number of PEs Outer Region   = ', num_pes_to_use_outer
        WRITE (2, *) ' '
        WRITE (2, *) 'Z-N (charge of nucleus - number of electrons) = ', Z_minus_N
        WRITE (2, *) 'lplusp                        = ', lplusp
        WRITE (2, *) 'Set_ML_Max                    = ', set_ML_max
        WRITE (2, *) 'ML_Max                        = ', ML_max
        WRITE (2, *) 'r_at_region_boundary          = ', r_at_region_boundary
        WRITE (2, *) ' '
        WRITE (2, *) 'DeltaT (au)                  = ', deltat
        WRITE (2, *) 'Order Taylor/Arnoldi         = ', taylors_order
        WRITE (2, *) ' '
        WRITE (2, *) 'max_L_block_size (dimension of inner Ham block) = ', dim_of_inner_ham_block_on_pe
        WRITE (2, *) ' '
        WRITE (2, *) 'Delta_R                      = ', delr
        WRITE (2, *) 'offset_in_stepsizes          = ', offset_in_stepsizes
        WRITE (2, *) 'X_1st, X_Last                = ', first_grid_point_on_pe, last_grid_point_on_pe
        WRITE (2, *) 'Channel_ID_1st               = ', first_channel_id
        WRITE (2, *) 'Channel_ID_Last              = ', last_channel_id
        WRITE (2, *) ' '
        IF (absorption_desired) THEN
            WRITE (2, *) 'ABSORB_DESIRED               = ', absorption_desired
            WRITE (2, *) '  Absorption_Interval        = ', absorption_interval
            WRITE (2, *) '  Sigma_Factor               = ', sigma_factor
            WRITE (2, *) '  Start_Factor               = ', start_factor
        END IF
        WRITE (2, *) ' '
        WRITE (2, *) 'Number of field configs      = ', numsols
        WRITE (2, *)
        WRITE (2, *) 'Use_2Colour_Field            = ', use_2colour_field
        WRITE (2, *) ' '
        WRITE (2, *) 'FIELD 1: '
        WRITE (2, *) '  Ellipticity                  = ', ellipticity
        WRITE (2, *) '  Intensity (10^14 W/cm2)      = ', intensity
        WRITE (2, *) '  Frequency (au)               = ', frequency
        WRITE (2, *) '  Periods_Of_Ramp_On           = ', periods_of_ramp_on
        WRITE (2, *) '  Periods_Of_Pulse             = ', periods_of_pulse
        WRITE (2, *) ' '
        IF (use_2colour_field) THEN
            IF (use_pulse_train) THEN
                WRITE (2, *) 'FIELD 2, PULSE TRAIN:'
                WRITE (2, *) '  Intensity_2 (10^14 W/cm2)    = ', intensity_2
                WRITE (2, *) '  Frequency_2 (au)             = ', frequency_2
                WRITE (2, *) '  APT_bursts:                  = ', APT_bursts
                WRITE (2, *) '  APT_ramp_bursts:             = ', APT_ramp_bursts
                WRITE (2, *) '  APT_envelope_power:          = ', APT_envelope_power
                WRITE (2, *) ' '
            ELSE
                WRITE (2, *) 'FIELD 2: '
                WRITE (2, *) '  Ellipticity_2                = ', ellipticity_2
                WRITE (2, *) '  Intensity_2 (10^14 W/cm2)    = ', intensity_2
                WRITE (2, *) '  Frequency_2 (au)             = ', frequency_2
                WRITE (2, *) '  Periods_Of_Ramp_On_2         = ', periods_of_ramp_on_2
                WRITE (2, *) '  Periods_Of_Pulse_2           = ', periods_of_pulse_2
                WRITE (2, *) ' '
            END IF
            WRITE (2, *) 'Delay of XUV w.r.t. IR      = ', delay_XUV_in_IR_periods
            WRITE (2, *) 'Angle between pulses in deg = ', angle_between_pulses_in_deg
        END IF
        WRITE (2, *) ' '
        WRITE (2, *) 'To initialize this file make top 2 lines:'
        WRITE (2, *) '-1 '
        WRITE (2, *) '-1 '

        CLOSE (UNIT=2)

    END SUBROUTINE update_status

!-----------------------------------------------------------------------

    SUBROUTINE write_inner_wavefunction_files(psi_inner, &
                                              timeindex, &
                                              timesteps_per_output, &
                                              my_num_LML_blocks, my_post, &
                                              my_block_id)

        COMPLEX(wp), ALLOCATABLE, INTENT(IN)  :: psi_inner(:, :)
        INTEGER, INTENT(IN)      :: timeindex
        INTEGER, INTENT(IN)      :: timesteps_per_output
        INTEGER, INTENT(IN)      :: my_block_id
        INTEGER, INTENT(in)      :: my_num_LML_blocks, my_post(my_num_LML_blocks)
        INTEGER                  :: it, it1, it2, it3, it4
        INTEGER                  :: ml_index, i_block
        CHARACTER(LEN=15)        :: name


        it = ((timeindex + 1) - 1)/timesteps_per_output
        it4 = it/1000
        it3 = (it - 1000*it4)/100
        it2 = (it - 1000*it4 - 100*it3)/10
        it1 = (it - 1000*it4 - 100*it3 - 10*it2)
        name = disk_path//'wave.'//TRIM(file_number(my_block_id,no_of_LML_blocks))//'.'//ACHAR(it4 + 48)//ACHAR(it3 + 48) &
               //ACHAR(it2 + 48)//ACHAR(it1 + 48)


        OPEN (UNIT=47, FILE=name)
        WRITE (47, *) my_post(1:my_num_LML_blocks)
        DO i_block = 1, my_num_LML_blocks
           DO ml_index = 1, my_post(i_block)
               WRITE (47, '(I9,*(E25.15E3))') ml_index, psi_inner(ml_index, 1:numsols)
           END DO
        END DO
        CLOSE (47)

    END SUBROUTINE write_inner_wavefunction_files

!-----------------------------------------------------------------------

END MODULE io_routines


!> @page comp_output2 Output Directories
!> 
!! @brief Describes contents of the 'ground', 'state' and 'data' directories.
!! @tableofcontents
!!
!!@section chan_pops Channel populations
!!The `data` directory contains files describing the population in
!!
!!* The ground state
!!* Each of the electron emission channels after every `Timesteps_Per_Output` iterations
!!
!! The channel numbering follows the standard R-matrix protocol, in ascending order
!! sorted by
!! 
!! 1. The total angular momentum of the final state
!! 2. The parity of the final state (even then odd) 
!! 3. The magnetic sublevel (`M_L`) of the final state.
!! 4. The energy of residual ion state to which the emitted electron is coupled
!! 5. The angular momentum of the emitted electron
!! 
!! E.G. consider a calculation for argon comprising the \f$^2P^o\f$ and \f$^2S^e\f$
!! ionisation thresholds, with \f$L_{\mathrm{max}}=3\f$ and an electric field, linearly
!! polarized along the z-axis. The possible final states are
!! \f$^1S^e\f$, \f$^1P^o\f$, \f$^1D^e\f$ and \f$^1F^o\f$ and the 11 channels are ordered as:
!! 
!! State     | Residual ion      | Continuum electron | Channel ID
!!  ---               | ---               | ---                | ---
!! \f$^1S^e\f$       | \f$\phantom{1}\f$ | \f$\phantom{1}\f$  | \f$\phantom{1}\f$
!!  \f$\phantom{1}\f$ | \f$^2P^o\f$   +   | \f$\epsilon p\f$   | 1
!!  \f$\phantom{1}\f$ | \f$^2S^e\f$   +   | \f$\epsilon s\f$   | 2
!!  \f$^1P^o\f$       | \f$\phantom{1}\f$ | \f$\phantom{1}\f$  | \f$\phantom{1}\f$
!!  \f$\phantom{1}\f$ | \f$^2P^o\f$   +   | \f$\epsilon s\f$   | 3
!!  \f$\phantom{1}\f$ | \f$^2P^o\f$   +   | \f$\epsilon d\f$   | 4
!!  \f$\phantom{1}\f$ | \f$^2S^e\f$   +   | \f$\epsilon p\f$   | 5
!!  \f$^1D^e\f$       | \f$\phantom{1}\f$ | \f$\phantom{1}\f$  | \f$\phantom{1}\f$
!!  \f$\phantom{1}\f$ | \f$^2P^o\f$   +   | \f$\epsilon p\f$   | 6
!!  \f$\phantom{1}\f$ | \f$^2P^o\f$   +   | \f$\epsilon f\f$   | 7
!!  \f$\phantom{1}\f$ | \f$^2S^e\f$   +   | \f$\epsilon d\f$   | 8
!!  \f$^1F^o\f$       | \f$\phantom{1}\f$ | \f$\phantom{1}\f$  | \f$\phantom{1}\f$
!!  \f$\phantom{1}\f$ | \f$^2P^o\f$   +   | \f$\epsilon d\f$   | 9
!!  \f$\phantom{1}\f$ | \f$^2P^o\f$   +   | \f$\epsilon g\f$   | 10
!!  \f$\phantom{1}\f$ | \f$^2S^e\f$   +   | \f$\epsilon f\f$   | 11
!! 
!! Note that, if the input parameter `binary_data_files = .true.`
!! then the channel population files are not written in plain text, but as 
!! a single, binary file using stream-io. This can yield substantial run-time
!!  savings when many outer region channels are included in the calculation.
!!  A python utility for reconstructing the population files in post processing 
!! is provided: @ref data_recon. 
!! 
!! For information on reconstructing the wavefunction see @ref
!! reform_wavefunction "/source/programs/reform.f90"

