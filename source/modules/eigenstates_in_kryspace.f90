! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles the treatment of the Krylov space Hamiltonian for the 
!> Arnoldi propagator method.

MODULE eigenstates_in_kryspace

    USE initial_conditions, ONLY: propagation_order
    USE precisn,            ONLY: wp

    IMPLICIT NONE

CONTAINS

    SUBROUTINE get_eigstuff_for_tridiag_h(h, eigstates, eigvals, info)

        USE ql_eigendecomposition, ONLY: ql_eigvals_and_vecs

        REAL(wp), INTENT(IN) :: h(0:propagation_order, 0:propagation_order)
        REAL(wp) :: eigstates(0:propagation_order, 0:propagation_order)
        REAL(wp) :: eigvals(0:propagation_order)
        INTEGER  :: n, i, info
        REAL(wp) :: D(0:propagation_order)
        REAL(wp) :: E(0:propagation_order)

        eigvals = 0.0_wp

        n = propagation_order + 1

        !  D = diag part of h
        !  E = subdiag part of h

        DO i = 0, propagation_order
            D(i) = h(i, i)
        END DO

        E(0) = 0.0_wp        ! This arbitrary in QL_eigvals_and_vecs
        DO i = 1, propagation_order
            E(i) = h(i, i - 1)
        END DO

        CALL ql_eigvals_and_vecs(n, D, E, eigstates, info)

        eigvals = D

    END SUBROUTINE get_eigstuff_for_tridiag_h

!---------------------------------------------------------------------

    SUBROUTINE assemble_mat_from_eigstuff(eigstates, eigvals, &
                                          max_eig_id, result_mat)

        REAL(wp), INTENT(IN)  :: eigstates(0:propagation_order, 0:propagation_order)
        REAL(wp), INTENT(IN)  :: eigvals(0:propagation_order)
        REAL(wp), INTENT(OUT) :: result_mat(0:propagation_order, 0:propagation_order)
        INTEGER, INTENT(IN)   :: max_eig_id
        INTEGER               :: eig_id, Row_ID, j
        REAL(wp)              :: mat(0:propagation_order, 0:propagation_order), sum

        mat = 0.0_wp
        result_mat = 0.0_wp

        DO eig_id = 0, max_eig_id
            mat(:, eig_id) = eigvals(eig_id)*eigstates(:, eig_id)
        END DO

        DO eig_id = 0, propagation_order
            DO row_id = 0, propagation_order

                sum = 0.0_wp
                DO j = 0, max_eig_id
                    sum = sum + mat(row_id, j)*eigstates(eig_id, j)
                END DO
                result_mat(row_id, eig_id) = sum

            END DO
        END DO

    END SUBROUTINE assemble_mat_from_eigstuff

!---------------------------------------------------------------------

    SUBROUTINE test_eigdecomposition(initial_mat, eigstates, &
                                     eigvals, max_eig_id, error)

        REAL(wp), INTENT(IN)  :: eigstates(0:propagation_order, 0:propagation_order)
        REAL(wp), INTENT(IN)  :: initial_mat(0:propagation_order, 0:propagation_order)
        REAL(wp), INTENT(IN)  :: eigvals(0:propagation_order)
        INTEGER, INTENT(IN)   :: max_eig_id
        REAL(wp)              :: result_mat(0:propagation_order, 0:propagation_order)
        REAL(wp), INTENT(OUT) :: error

        CALL assemble_mat_from_eigstuff(eigstates, eigvals, &
                                        max_eig_id, result_mat)

        error = MAXVAL(ABS(initial_mat - result_mat))/eigvals(max_eig_id)

    END SUBROUTINE test_eigdecomposition

!---------------------------------------------------------------------

    SUBROUTINE get_kry_norm(V, norm)

        USE global_data, ONLY: zero

        INTEGER     :: j
        COMPLEX(wp) :: V(0:propagation_order), sum
        REAL(wp)    :: norm

        sum = zero
        DO j = 0, propagation_order
            sum = sum + V(j)*CONJG(V(j))
        END DO

        norm = SQRT(REAL(sum, wp))

    END SUBROUTINE get_kry_norm

!-----------------------------------------------------------------------

    SUBROUTINE get_index_of_nearest_eig(desired_eigval, desired_index, eigenvals)

        REAL(wp), INTENT(IN) :: desired_eigval, eigenvals(0:propagation_order)
        INTEGER, INTENT(OUT) :: desired_index
        REAL(wp)             :: min_diff, diff
        INTEGER              :: i

        min_diff = desired_eigval - eigenvals(0)
        desired_index = 0

        DO i = 1, propagation_order

            diff = desired_eigval - eigenvals(i)
            IF (ABS(diff) < ABS(min_diff)) THEN
                min_diff = diff
                desired_index = i
            END IF

        END DO

    END SUBROUTINE get_index_of_nearest_eig

END MODULE eigenstates_in_kryspace
