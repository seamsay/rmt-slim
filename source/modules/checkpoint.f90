! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles periodic write-outs of wavefunction files, calculation parameters and
!> data arrays for calculation restart.

MODULE checkpoint

    USE precisn,            ONLY: wp
    USE mpi_communications, ONLY: i_am_inner_master, &
                                  my_block_id
    use distribute_hd_blocks2, only: numrows_blocks

    IMPLICIT NONE

CONTAINS

    SUBROUTINE checkpoint_writes(stage, &
                                 stage_first, &
                                 pop_index, &
                                 previous_timeindex, &
                                 Z_minus_N, &
                                 r_at_region_bndry, &
                                 max_L_block_size)

        USE wall_clock,         ONLY: hel_time

        INTEGER, INTENT(IN)  :: stage
        INTEGER, INTENT(IN)  :: stage_first
        INTEGER, INTENT(IN)  :: pop_index
        INTEGER, INTENT(IN)  :: previous_timeindex
        INTEGER, INTENT(IN)  :: max_L_block_size
        REAL(wp), INTENT(IN) :: Z_minus_N
        REAL(wp), INTENT(IN) :: r_at_region_bndry

        REAL(wp)             :: checkpoint_start_time
        REAL(wp)             :: checkpoint_end_time

        checkpoint_start_time = hel_time()


        CALL checkpoint_data_files(stage, &
                                   stage_first, &
                                   pop_index, &
                                   my_block_id)

        CALL write_status_file(stage, &
                               previous_timeindex, &
                               Z_minus_N, &
                               r_at_region_bndry, &
                               max_L_block_size)

        checkpoint_end_time = hel_time()

        IF (i_am_inner_master) THEN
            PRINT*
            PRINT *, 'Checkpointing: Time taken (secs): ', checkpoint_end_time - checkpoint_start_time
            PRINT*
        END IF

    END SUBROUTINE checkpoint_writes

!---------------------------------------------------------------------------

    SUBROUTINE write_status_file(previous_stage, &
                                 previous_timeindex, &
                                 Z_minus_N, &
                                 r_at_region_bndry, &
                                 max_L_block_size)

        USE calculation_parameters, ONLY: delta_t
        USE grid_parameters,        ONLY: x_1st, &
                                          x_last, &
                                          channel_id_1st, &
                                          channel_id_last
        USE initial_conditions,     ONLY: use_2colour_field, &
                                          lplusp, &
                                          set_ML_max, &
                                          ML_max, &
                                          ellipticity, &
                                          ellipticity_xuv, &
                                          frequency, &
                                          frequency_xuv, &
                                          periods_of_ramp_on, &
                                          periods_of_ramp_on_xuv, &
                                          periods_of_pulse, &
                                          periods_of_pulse_xuv, &
                                          intensity, &
                                          intensity_xuv, &
                                          no_of_pes_to_use, &
                                          no_of_pes_to_use_inner, &
                                          no_of_pes_to_use_outer, &
                                          propagation_order, &
                                          deltar, &
                                          offset_in_stepsizes, &
                                          delay_xuv_in_ir_periods, &
                                          timesteps_per_output, &
                                          absorb_desired, &
                                          absorption_interval, &
                                          sigma_factor, &
                                          start_factor,&
                                          use_pulse_train,&
                                          APT_bursts,&
                                          APT_ramp_bursts,&
                                          APT_envelope_power,&
                                          angle_between_pulses_in_deg

        USE io_routines,            ONLY: update_status

        INTEGER, INTENT(IN)  :: previous_stage
        INTEGER, INTENT(IN)  :: previous_timeindex
        REAL(wp), INTENT(IN) :: Z_minus_N
        REAL(wp), INTENT(IN) :: r_at_region_bndry
        INTEGER, INTENT(IN)  :: max_L_block_size

        IF (i_am_inner_master) THEN
            CALL update_status(previous_stage, &
                               previous_timeindex, &
                               timesteps_per_output, &
                               no_of_pes_to_use, &
                               no_of_pes_to_use_inner, &
                               no_of_pes_to_use_outer, &
                               Z_minus_N, &
                               lplusp, &
                               set_ML_max, &
                               ML_max, &
                               r_at_region_bndry, &
                               delta_t, &
                               deltar, &
                               offset_in_stepsizes, &
                               x_1st, &
                               x_last, &
                               channel_id_1st, &
                               channel_id_last, &
                               max_L_block_size, &
                               propagation_order, &
                               absorb_desired, &
                               absorption_interval, &
                               sigma_factor, &
                               start_factor, &
                               use_2colour_field, &
                               ellipticity(1), &
                               intensity(1), &
                               frequency(1), &
                               periods_of_ramp_on(1), &
                               periods_of_pulse(1), &
                               ellipticity_xuv(1), &
                               intensity_xuv(1), &
                               frequency_xuv(1), &
                               periods_of_ramp_on_xuv(1), &
                               periods_of_pulse_xuv(1), &
                               delay_xuv_in_ir_periods(1),&
                               use_pulse_train,&
                               APT_bursts(1),&
                               APT_ramp_bursts(1),&
                               APT_envelope_power(1),&
                               angle_between_pulses_in_deg(1))
        END IF

    END SUBROUTINE write_status_file

!---------------------------------------------------------------------------

    SUBROUTINE checkpoint_data_files(stage, &
                                     stage_first, &
                                     pop_index, &
                                     my_block_id)

      USE grid_parameters,    ONLY: x_1st, x_last, channel_id_1st, channel_id_last, &
           my_channel_id_1st, my_channel_id_last
        
        USE initial_conditions, ONLY: keep_checkpoints,&
                                      dipole_output_desired,&
                                      dipole_velocity_output,&
                                      binary_data_files,&
                                      keep_popL, &
                                      use_taylor_propagator, &
                                      no_of_pes_per_sector, &
                                      numsols => no_of_field_confs
        USE io_routines,        ONLY: write_real_arrays, &
                                      binary_write_arrays,&
                                      get_array_name,&
                                      write_my_wavefunc_inner, &
                                      write_my_wavefunc_outer, write_my_wavefunc_outer_parallel, &
                                      write_my_wavefunc_inner_checkpt, &
                                      write_my_wavefunc_outer_checkpt, write_my_wavefunc_outer_checkpt_parallel
        USE inner_propagators,  ONLY: update_psi_inner
        USE outer_propagators,  ONLY: update_psi_outer
        USE mpi_communications, ONLY: i_am_outer_master, &
                                      i_am_in_outer_region, &
                                      i_am_in_inner_region, &
                                      all_processor_barrier, &
                                      get_my_block_group_pe_id, receive_cmplx_array, receive_real_array, &
                                      send_cmplx_array, send_real_array, mpi_comm_block, get_my_pe_id
        USE mpi_layer_lblocks,  ONLY: i_am_block_master, &
                                      i_am_gs_master
        USE wavefunction,       ONLY: population_gs_inner, &
                                      population_all_outer_L, &
                                      psi_outer, &
                                      psi_inner, &
                                      data_1st, &
                                      data_last, &
                                      discarded_pop_outer, &
                                      discarded_pop_outer_si, &
                                      time_in_au
        USE rmt_assert,         ONLY: assert

        INTEGER, INTENT(IN) :: stage
        INTEGER, INTENT(IN) :: stage_first
        INTEGER, INTENT(IN) :: pop_index
        INTEGER, INTENT(IN) :: my_block_id

        INTEGER             :: index_of_first_data_pt
        INTEGER             :: index_of_last_data_pt
        INTEGER             :: channel_id
        LOGICAL             :: this_is_the_initial_stage

        COMPLEX(wp), ALLOCATABLE       :: temp_array(:,:)
        REAL(wp), ALLOCATABLE          :: dpop_array(:), dpopsi_array(:)
        INTEGER                        :: loop, i, isol, pe_num, num_of_elements, tag, ierr, my_num_ch
        INTEGER                        :: my_number_channels, number_channels, rem, my_block_group_pe_id  

        CHARACTER(LEN=6)    :: array_name_6

        this_is_the_initial_stage = .false.
        IF (stage == stage_first) THEN
            this_is_the_initial_stage = .true.
        END IF

        index_of_first_data_pt = data_1st
        index_of_last_data_pt = pop_index

        IF (i_am_inner_master) THEN

            ! flush memory buffers to the output files
            FLUSH (37) ! pop_inn
            FLUSH (38) ! pop_all
            FLUSH (46) ! EField

            IF (dipole_output_desired) THEN
                FLUSH (58)  ! expec_z_all
                IF (dipole_velocity_output) FLUSH (63) ! expec_v_all
            END IF

        END IF


        IF (i_am_gs_master) THEN
            array_name_6 = 'pop_GS'
            CALL write_real_arrays &
                (time_in_au, population_gs_inner, &
                 array_name_6, &
                 this_is_the_initial_stage, &
                 data_1st, Data_Last, &
                 index_of_first_data_pt, index_of_last_data_pt)

        END IF

        ! Only continue once the inner master finished writing inner populations.
        ! This is needed to be sure that the outer master will not encounter
        ! locked output directory (i.e. directory in the process of creation)
        ! in the code that follows.
        CALL all_processor_barrier

        IF (i_am_outer_master) THEN

            ! flush memory buffers to the output files
            FLUSH (36) ! pop_out

!           array_name_6  = 'popout'
!           CALL write_real_arrays                      &
!               (time_in_au, population_all_outer,      &
!                array_name_6,                          &
!                this_is_the_initial_stage,             &
!                data_1st, data_last,                   &
!                index_of_first_data_pt, index_of_last_data_pt)
!
!           array_name_6  = 'pop_SI'
!           CALL write_real_arrays                         &
!               (time_in_au, population_all_outer_si,      &
!                array_name_6,                             &
!                this_is_the_initial_stage,                &
!                data_1st, data_last,                      &
!                index_of_first_data_pt, index_of_last_data_pt)

!           array_name_6  = 'pp_Ryd'
!           CALL write_real_arrays                         &
!               (time_in_au, population_all_outer_ryd,     &
!                array_name_6,                             &
!                this_is_the_initial_stage,                &
!                data_1st, data_last,                      &
!                index_of_first_data_pt, index_of_last_data_pt)

!           array_name_6  = 'p_r1st'
!           CALL write_real_arrays                    &
!               (time_in_au, population_at_r1st,      &
!                array_name_6,                        &
!                this_is_the_initial_stage,           &
!                data_1st, data_last,                 &
!                index_of_first_data_pt, index_of_last_data_pt)

            IF (keep_popL) THEN
                IF (binary_data_files) THEN
                    ASSOCIATE (nrec => (index_of_last_data_pt - index_of_first_data_pt + 1))
                    ASSOCIATE (nchan=> (channel_id_last-channel_id_1st+1))
                    array_name_6='popchn'
                    CALL binary_write_arrays(time_in_au(index_of_first_data_pt:index_of_last_data_pt),&
                                             population_all_outer_L(index_of_first_data_pt:index_of_last_data_pt, :, :),&
                                             array_name_6,&
                                             nrec,&
                                             nchan)
                    END ASSOCIATE                                         
                    END ASSOCIATE                                         
                                                         
                ELSE                                         
                    DO channel_id = channel_id_1st, channel_id_last
                       
                        CALL get_array_name(channel_id,array_name_6)
                       
                        CALL write_real_arrays &
                            (time_in_au, population_all_outer_L(:, channel_id, :), &
                             array_name_6, &
                             this_is_the_initial_stage, &
                             data_1st, data_last, &
                             index_of_first_data_pt, index_of_last_data_pt)
                    END DO
                END IF
            END IF

!           SELECT CASE (choice_of_wave_propagator)
!           CASE (arnoldi_series)
!           array_name_6  = 'mxevlo'
!           CALL write_real_arrays               &
!               (time_in_au, max_eval,           &
!                array_name_6,                   &
!                this_is_the_initial_stage,      &
!                data_1st, data_last,            &
!                index_of_first_data_pt, index_of_last_data_pt)

!           array_name_6  = 'GSevlo'
!           CALL write_real_arrays               &
!               (time_in_au, min_eval,           &
!                array_name_6,                   &
!                this_is_the_initial_stage,      &
!                data_1st, data_last,            &
!                index_of_first_data_pt, index_of_last_data_pt)

!           END SELECT

        END IF


        ! Each of us prints:

        ! update and checkpoint outer wave function
        ! + optionally store wave function in stage-labeled directory
        IF (i_am_in_outer_region) THEN
            CALL update_psi_outer(psi_outer)
            call get_my_block_group_pe_id(my_block_group_pe_id)
            IF (no_of_pes_per_sector == 1) THEN
               CALL write_my_wavefunc_outer(psi_outer, my_block_id, discarded_pop_outer, discarded_pop_outer_si)
               IF (keep_checkpoints) CALL write_my_wavefunc_outer_checkpt(psi_outer, my_block_id, &
                    discarded_pop_outer, discarded_pop_outer_si, stage)
            ELSE
               ! code for mpi-parallelised channel distribution within a block.
               ! Rather than gather everything into a high-memory large array on the mpi_block_comm leader,
               ! the leader collects info in turn and writes in turn. The unformatted writes are in order of
               ! wavefunction, discarded_pop and discarded_pop:si, but split into several unformatted lines,
               ! one for each task in the communicator.
               my_number_channels = my_channel_id_last - my_channel_id_1st + 1
               IF (my_block_group_pe_id == 0) THEN
!  the block leader sends psi_outer(:, set of channels) from each pe in turn and writes them in order
                  number_channels = channel_id_last - channel_id_1st + 1
                  rem = MOD(number_channels, no_of_pes_per_sector)
                 ! any remainder has been  allocated 1 to each task from 0 to rem - 1 
                  IF (rem == 0) rem = no_of_pes_per_sector   ! all tasks have the same no of channels
                  ALLOCATE (temp_array(x_1st:x_last, my_number_channels), dpop_array(my_number_channels), &
                           dpopsi_array(my_number_channels), stat=ierr) 
                  DO loop = 1, 3
                     DO isol = 1, numsols
                        ! this preserves the same order of arrays in the file for all channel para/non-para.
                        num_of_elements = my_number_channels * (x_last - x_1st + 1)
                        i = 1
     ! loop, i, isol are passed, needed to tell the routine whether to open/close the file and what to write
                        CALL write_my_wavefunc_outer_parallel(psi_outer(:,:,isol), my_block_id, &
                             discarded_pop_outer(:,isol), discarded_pop_outer_si(:,isol), &
                             loop, i, isol, my_number_channels)
                        IF (keep_checkpoints) CALL write_my_wavefunc_outer_checkpt_parallel &
                             (psi_outer(:,:,isol), my_block_id, discarded_pop_outer(:,isol), &
                             discarded_pop_outer_si(:,isol), stage, loop, i, isol, my_number_channels)
                        DO i = 2, rem
                           pe_num = i - 1
                           if (loop == 1) &
                   CALL receive_cmplx_array(temp_array, num_of_elements, pe_num, mpi_comm_block, ierr) 
                           if (loop == 2) &
                   CALL receive_real_array(dpop_array, my_number_channels, pe_num, mpi_comm_block, ierr) 
                           if (loop == 3) &
                   CALL receive_real_array(dpopsi_array, my_number_channels, pe_num, mpi_comm_block, ierr)
                           CALL write_my_wavefunc_outer_parallel(temp_array, my_block_id, dpop_array, &
                                            dpopsi_array, loop, i, isol, my_number_channels)
                           IF (keep_checkpoints) CALL write_my_wavefunc_outer_checkpt_parallel &
               (temp_array, my_block_id, dpop_array, dpopsi_array, stage, loop, i, isol, my_number_channels)
                        END DO
                        num_of_elements = (my_number_channels - 1) * (x_last - x_1st + 1)
                        my_num_ch = my_number_channels - 1   
                        DO i = rem + 1, no_of_pes_per_sector
                           pe_num = i - 1
                           if (loop == 1) &
                  ! the tag is assumed to be the same as pe_num for recv
                   CALL receive_cmplx_array(temp_array, num_of_elements, pe_num, mpi_comm_block, ierr) 
                           if (loop == 2) &
                   CALL receive_real_array(dpop_array, my_num_ch, pe_num, mpi_comm_block, ierr) 
                           if (loop == 3) &
                   CALL receive_real_array(dpopsi_array, my_num_ch, pe_num, mpi_comm_block, ierr)
                           CALL write_my_wavefunc_outer_parallel(temp_array, my_block_id, dpop_array, &
                                            dpopsi_array, loop, i, isol, my_num_ch)
                           IF (keep_checkpoints) CALL write_my_wavefunc_outer_checkpt_parallel &
                      (temp_array, my_block_id, dpop_array, dpopsi_array, stage, loop, i, isol, my_num_ch)
                        END DO
                     END DO
                  END DO
                  DEALLOCATE (temp_array, dpop_array, dpopsi_array, stat=ierr)
                  CALL assert(ierr==0, 'problem in checkpoint, write_my_wave (outer, recv)')
               ELSE
! my_block_group_pe_id /= 0
                  num_of_elements = my_number_channels * (x_last - x_1st + 1)
                  pe_num = 0               
                  tag = my_block_group_pe_id  ! = 0
                  DO isol = 1, numsols
                     CALL send_cmplx_array(psi_outer(x_1st,my_channel_id_1st,isol), num_of_elements, &
                          pe_num, tag, mpi_comm_block, ierr) 
                  END DO
                  DO isol = 1, numsols
                     CALL send_real_array(discarded_pop_outer(my_channel_id_1st,isol), my_number_channels, &
                      pe_num, tag, mpi_comm_block, ierr) 
                  END DO
                  DO isol = 1, numsols
                     CALL send_real_array(discarded_pop_outer_si(my_channel_id_1st,isol), my_number_channels, &
                      pe_num, tag, mpi_comm_block, ierr) 
                  END DO
                  CALL assert(ierr==0, 'problem in checkpoint, write_my_wave (outer, send)')
               END IF
            END IF
          END IF

        ! update and checkpoint inner wave function
        ! + optionally store wave function in stage-labeled directory
        IF (i_am_in_inner_region) THEN
            CALL update_psi_inner(psi_inner)
            CALL write_my_wavefunc_inner(numrows_blocks, psi_inner, my_block_id, &
                                         i_am_block_master)
            IF (keep_checkpoints) THEN
               CALL write_my_wavefunc_inner_checkpt(numrows_blocks, psi_inner, my_block_id, &
                                                    i_am_block_master, stage)
            END IF
        END IF

    END SUBROUTINE checkpoint_data_files

END MODULE checkpoint
