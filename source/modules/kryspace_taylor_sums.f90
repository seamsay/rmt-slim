! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Routines for checking Krylov subspace.
MODULE kryspace_taylor_sums

    USE precisn,                ONLY: wp
    USE rmt_assert,             ONLY: assert
    USE calculation_parameters, ONLY: kry_order, no_of_kry_sum_segments
    USE initial_conditions,     ONLY: propagation_order

    IMPLICIT NONE

    REAL(wp), SAVE, ALLOCATABLE    :: exp_taylor_coeff(:)
    COMPLEX(wp), SAVE, ALLOCATABLE :: exp_i_taylor_coeff(:)
    REAL(wp), SAVE, ALLOCATABLE    :: cos_taylor_coeff(:)
    REAL(wp), SAVE, ALLOCATABLE    :: sin_taylor_coeff(:)

CONTAINS

    SUBROUTINE init_kryspace_taylor_sums_mod(del_t, krylov_hdt_desired)

        USE global_data, ONLY: one, im

        REAL(wp), INTENT(IN) :: del_t
        LOGICAL, INTENT(IN)  :: krylov_hdt_desired
        REAL(wp)             :: dt
        INTEGER              :: i, ierror

        ALLOCATE (exp_taylor_coeff(0:kry_order), &
                  exp_i_taylor_coeff(0:kry_order), &
                  cos_taylor_coeff(0:kry_order), &
                  sin_taylor_coeff(0:kry_order), &
                  stat=ierror)
        CALL assert(ierror .EQ. 0, "allocation error in kryspace_taylor_sums")

        IF (krylov_hdt_desired) THEN
            ! Krylov subspace spans (H dt), (H dt)^2, ..., (H dt)^n
            ! so take exp(-ih)
            dt = 1.0_wp/REAL(no_of_kry_sum_segments, wp)
        ELSE
            ! Krylov subspace spans (H ), (H)^2, ..., (H)^n
            ! so take exp(-ih dt)
            dt = del_t/REAL(no_of_kry_sum_segments, wp)
        END IF
!       dt = del_t / REAL(no_of_kry_sum_segments, wp)

        exp_taylor_coeff(0) = 1.0_wp
        DO i = 1, kry_order
            exp_taylor_coeff(i) = -dt/REAL(i, wp)
        END DO

        exp_i_taylor_coeff(0) = one
        DO i = 1, kry_order
            exp_i_taylor_coeff(i) = -im*dt/REAL(i, wp)
        END DO

        cos_taylor_coeff(0) = 1.0_wp
        DO i = 1, kry_order
            cos_taylor_coeff(i) = -dt**2/REAL(4*i*i - 2*i, wp)
        END DO

        sin_taylor_coeff(0) = 1.0_wp
        DO i = 1, kry_order
            sin_taylor_coeff(i) = -dt**2/REAL(4*i*i + 2*i, wp)
        END DO

    END SUBROUTINE init_kryspace_taylor_sums_mod

!-----------------------------------------------------------------------

    SUBROUTINE exp_of_minus_ht_times(h, kry_psi, exp_kry_psi)

        INTEGER       :: seg_sum_order, ord
        REAL(wp)      :: h(0:propagation_order, 0:propagation_order)
        COMPLEX(wp)   :: next_psi(0:propagation_order), h_next_psi(0:propagation_order)
        COMPLEX(wp)   :: exp_kry_psi(0:propagation_order)
        COMPLEX(wp), INTENT(IN) :: kry_psi(0:propagation_order)

        seg_sum_order = kry_order/no_of_kry_sum_segments

        next_psi = kry_psi

        DO ord = seg_sum_order, 1, -1

            CALL matrix_h_x_arn_vector(h, next_psi, h_next_psi)

            next_psi = kry_psi + exp_taylor_coeff(ord)*h_next_psi

        END DO ! Sum krylov subspace taylor series loop

        exp_kry_psi = next_psi

    END SUBROUTINE exp_of_minus_ht_times

!-----------------------------------------------------------------------

    SUBROUTINE exp_of_minus_iht_times(h, kry_psi, exp_kry_psi)

        INTEGER     :: seg_sum_order, ord
        REAL(wp)    :: h(0:propagation_order, 0:propagation_order)
        COMPLEX(wp) :: next_psi(0:propagation_order), h_next_psi(0:propagation_order)
        COMPLEX(wp) :: exp_kry_psi(0:propagation_order)
        COMPLEX(wp), INTENT(IN) :: kry_Psi(0:propagation_order)

        seg_sum_order = kry_order/no_of_kry_sum_segments

        next_psi = kry_psi

        DO ord = seg_sum_order, 1, -1

            CALL matrix_h_x_arn_vector(h, next_psi, h_next_psi)

            next_psi = kry_psi + exp_i_taylor_coeff(ord)*h_next_psi

        END DO ! Sum krylov subspace taylor series loop

        exp_kry_psi = next_psi

    END SUBROUTINE exp_of_minus_iht_times

!-----------------------------------------------------------------------

    SUBROUTINE cos_of_ht_times(h, kry_psi, cos_kry_psi)

        COMPLEX(wp), INTENT(IN) :: kry_Psi(0:propagation_order)
        REAL(wp)     :: h(0:propagation_order, 0:propagation_order)
        COMPLEX(wp)  :: cos_kry_psi(0:propagation_order)
        INTEGER      :: seg_sum_order, ord
        COMPLEX(wp)  :: next_psi(0:propagation_order), h_next_psi(0:propagation_order)
        COMPLEX(wp)  :: hh_next_psi(0:propagation_order)

        seg_sum_order = kry_order/no_of_kry_sum_segments

        next_psi = kry_psi

        DO ord = seg_sum_order/2, 1, -1

            CALL matrix_h_x_arn_vector(h, next_psi, h_next_psi)
            CALL matrix_h_x_arn_vector(h, h_next_psi, hh_next_psi)

            next_psi = kry_psi + cos_taylor_coeff(ord)*hh_next_psi

        END DO ! Sum krylov subspace taylor series loop

        cos_kry_psi = next_psi

    END SUBROUTINE cos_of_ht_times

!-----------------------------------------------------------------------

    SUBROUTINE sin_of_ht_times(h, kry_psi, sin_kry_psi, del_t)

        COMPLEX(wp), INTENT(IN) :: kry_psi(0:propagation_order)
        REAL(wp)     :: h(0:propagation_order, 0:propagation_order)
        INTEGER      :: seg_sum_order, ord
        REAL(wp)     :: dt, del_t
        COMPLEX(wp)  :: next_psi(0:propagation_order), h_next_psi(0:propagation_order)
        COMPLEX(wp)  :: hh_next_psi(0:propagation_order), sin_kry_psi(0:propagation_order)

        seg_sum_order = kry_order/no_of_kry_sum_segments

        dt = del_t/REAL(no_of_kry_sum_segments, wp)

        next_psi = kry_psi

        DO ord = seg_sum_order/2, 1, -1

            CALL matrix_h_x_arn_vector(h, next_psi, h_next_psi)
            CALL matrix_h_x_arn_vector(h, h_next_psi, hh_next_psi)

            next_psi = kry_psi + sin_taylor_coeff(ord)*hh_next_psi

        END DO ! Sum krylov subspace taylor series loop

        CALL matrix_h_x_arn_vector(h, next_psi, h_next_psi)

        sin_kry_psi = dt*h_next_psi

    END SUBROUTINE sin_of_ht_times

!-----------------------------------------------------------------------

    SUBROUTINE matrix_h_x_arn_vector(h, V, hV)

        USE global_data, ONLY: zero

        INTEGER     :: i, j1, j2, j
        REAL(wp)    :: h(0:propagation_order, 0:propagation_order)
        COMPLEX(wp) :: V(0:propagation_order), hV(0:propagation_order), sum

        DO i = 0, propagation_order

            j1 = MAX(0, i - 1)         !  h is tridiagonal
            j2 = MIN(propagation_order, i + 1)

            sum = zero
            DO j = j1, j2
                sum = sum + h(i, j)*V(j)
            END DO
            hV(i) = sum

        END DO

    END SUBROUTINE matrix_h_x_arn_vector

!-----------------------------------------------------------------------

    SUBROUTINE hermitian_error(propagation_order, h, error)

        INTEGER, INTENT(IN)     :: propagation_order
        COMPLEX(wp), INTENT(IN) :: h(0:propagation_order, 0:propagation_order)
        REAL(wp), INTENT(INOUT) :: error

        COMPLEX(wp)   :: ht
        REAL(wp)      :: et
        INTEGER       :: i, j

        error = 0.0_wp

        DO j = 0, propagation_order
            DO i = 0, j

                ht = h(i, j) - CONJG(h(j, i))

                et = REAL(ht*CONJG(ht), wp)

                IF (et > error) THEN
                    error = et
                END IF

            END DO
        END DO

        error = SQRT(error)

    END SUBROUTINE hermitian_error

!-----------------------------------------------------------------------

    SUBROUTINE off_diags_error(propagation_order, h, error)

        INTEGER, INTENT(IN)     :: propagation_order
        COMPLEX(wp), INTENT(IN) :: h(0:propagation_order, 0:propagation_order)
        REAL(wp), INTENT(INOUT) :: error

        COMPLEX(wp)   :: ht
        REAL(wp)      :: et
        INTEGER       :: i, j

        error = 0.0_wp

        DO j = 2, propagation_order
            DO i = 0, j - 2

                ht = h(i, j)

                et = REAL(ht*CONJG(ht), wp)

                IF (et > error) THEN
                    error = et
                END IF

            END DO
        END DO

        error = SQRT(error)

    END SUBROUTINE off_diags_error

!-----------------------------------------------------------------------

    SUBROUTINE im_part(propagation_order, h, error)

        INTEGER, INTENT(IN)      :: propagation_order
        COMPLEX(wp), INTENT(IN)  :: h(0:propagation_order, 0:propagation_order)
        REAL(wp), INTENT(INOUT)  :: error

        REAL(wp)  :: ht
        INTEGER   :: i, j

        error = 0.0_wp

        DO j = 0, propagation_order
            DO i = 0, propagation_order

                ht = AIMAG(h(i, j))

                IF (ht > error) THEN
                    error = ht
                END IF

            END DO
        END DO

    END SUBROUTINE im_part

END MODULE kryspace_taylor_sums
