!  boiler plate for unit testing example using the test drive testing framework https://github.com/fortran-lang/test-drive

MODULE test_template
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64    !  Use standard fortran library  *include info*

    USE testdrive, ONLY: check, error_type, new_unittest, unittest_type

    USE testing_utilities, ONLY: compare, format_context

    IMPLICIT NONE   !  all types must be explicitly declared

    PRIVATE
    PUBLIC :: collect_my_Subroutine_name   !  name a public subroutine that collects the information on all tests preformed
CONTAINS
    SUBROUTINE collect_my_Subroutine_name(testsuite)   !  This subroutine will collect the outcomes of tests performed
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite    ! Type of unit test type with member as initialised as testsuite

        !  testsuite is a list of each test performed, it must begin and end in an "&"
        testsuite = [ &
                    new_unittest("Simple Unit Test, Addition: ", my_Test_Name), &
                    new_unittest("Simple Unit Test, Compare: ", my_Compare_Test_Call) &
                    ]      

    END SUBROUTINE

    !  Subroutine that returns an error 
    SUBROUTINE my_Test_Name(error) 
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        !  Simple function from testing_utilities that checks if 1+2=3 and outputs error otherwise
        call check(error, 1 + 2 == 3)

    END SUBROUTINE

    !  Subroutine that compares to numbers that are passed into it's arguements (actual and expected) and outputs error if they are not the same
    SUBROUTINE my_Compare_Test(error, actual, expected)
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        INTEGER, INTENT(IN) :: actual
        INTEGER, INTENT(IN) :: expected
        CHARACTER(len=:), ALLOCATABLE :: context

        context = "" !  context provides extra information should there be an error
        !  Compare is another function from testing_utilities which compares two values and provides an error should they not be the same
        IF (.NOT. compare(actual, expected)) THEN
            CALL format_context(context, "value", actual, expected)
        END IF

    END SUBROUTINE

    !  This subroutine calls the previous one with arguements passed, here there are INTs, in this case they are going to be equal and the test will pass, changing their values to be different will show the outcome of a failed test
    SUBROUTINE my_Compare_Test_Call(error)
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        CALL my_Compare_Test(error=error, &
                        actual=100, &
                        expected=100)
    END SUBROUTINE
    
END MODULE



! needs to be included
IF (context /= "") THEN
    CALL test_failed(error, "test failed", context)
END IF
