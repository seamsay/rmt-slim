PROGRAM tester
    USE, INTRINSIC :: ISO_FORTRAN_ENV, ONLY: ERROR_UNIT
    USE testdrive, ONLY: get_argument, new_testsuite, run_selected, run_testsuite, select_suite, testsuite_type

    USE test_hamiltonian_input_file_read_H_file2, ONLY: collect_hamiltonian_input_file_read_H_file2
    USE test_hamiltonian_input_file_read_H_parameters2, ONLY: collect_hamiltonian_input_file_read_H_parameters2
    USE test_hamiltonian_input_file_parse_H_file, ONLY: collect_hamiltonian_input_file_parse_H_file
    USE test_utilities, ONLY: collect_utilities
    !USE test_read_D_file, ONLY  ! add module to use to the main tester program
    !USE test_unit_test_default, ONLY: collect_my_Subroutine_name

    IMPLICIT NONE

    INTEGER :: status, suite
    CHARACTER(len=:), ALLOCATABLE :: suite_name, test_name
    CHARACTER(len=*), PARAMETER :: fmt = "('#', *(X, A))"
    TYPE(testsuite_type), DIMENSION(:), ALLOCATABLE :: testsuites

    status = 0

    testsuites = [new_testsuite("hamiltonian_input_file::read_H_file2", collect_hamiltonian_input_file_read_H_file2), &
                  new_testsuite("hamiltonian_input_file::read_H_parameters2", collect_hamiltonian_input_file_read_H_parameters2), &
                  new_testsuite("hamiltonian_input_file::parse_H_file", collect_hamiltonian_input_file_parse_H_file), &
                  new_testsuite("utilities", collect_utilities) &
                  !new_testsuite("test_default::example_tests", collect_my_Subroutine_name) &
                 ]
                 !append unit test module to the main test suite
                 ! new_testsuite("readD::read_D_file", collect_read_D_file)
    CALL get_argument(1, suite_name)
    CALL get_argument(2, test_name)

    IF (ALLOCATED(suite_name)) THEN
        suite = select_suite(testsuites, suite_name)
        IF (suite >= 1 .AND. suite <= SIZE(testsuites)) THEN
            IF (ALLOCATED(test_name)) THEN
                WRITE (ERROR_UNIT, fmt) "Testing:", testsuites(suite)%name
                CALL run_selected(testsuites(suite)%collect, test_name, ERROR_UNIT, status)
            ELSE
                WRITE (ERROR_UNIT, fmt) "Testing:", testsuites(suite)%name
                CALL run_testsuite(testsuites(suite)%collect, ERROR_UNIT, status)
            END IF
        ELSE
            WRITE (ERROR_UNIT, "(A)") "Available suites:"
            DO suite = 1, SIZE(testsuites)
                WRITE (ERROR_UNIT, "(A, X, A)") "-", testsuites(suite)%name
            END DO
            ERROR STOP
        END IF
    ELSE
        DO suite = 1, SIZE(testsuites)
            WRITE (ERROR_UNIT, fmt) "Testing:", testsuites(suite)%name
            CALL run_testsuite(testsuites(suite)%collect, ERROR_UNIT, status)
        END DO
    END IF

    IF (status > 0) THEN
        WRITE (ERROR_UNIT, "(I0, X, A)") status, "test(s) failed!"
        ERROR STOP
    ELSE IF (status < 0) THEN
        ERROR STOP
    END IF
END PROGRAM tester
