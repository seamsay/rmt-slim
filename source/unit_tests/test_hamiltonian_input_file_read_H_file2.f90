MODULE test_hamiltonian_input_file_read_H_file2
    USE testdrive, ONLY: check, error_type, new_unittest, test_failed, unittest_type

    USE precisn, ONLY: wp
    USE hamiltonian_input_file, ONLY: read_H_parameters2, read_H_file2

    USE testing_utilities, ONLY: &
        compare, &
        data_file, &
        format_context, &
        range, &
        subarray, &
        subarray_integer => subarray_integer_int32, &
        subarray_wp => subarray_real_real64

    IMPLICIT NONE

    PRIVATE
    PUBLIC :: collect_hamiltonian_input_file_read_H_file2
CONTAINS
    SUBROUTINE collect_hamiltonian_input_file_read_H_file2(testsuite)
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite

        testsuite = [ &
            new_unittest("atomic::small::Ne_4cores", test_atomic_small_Ne_4cores), &
            new_unittest("atomic::small::ar+", test_atomic_small_arx), &
            new_unittest("atomic::small::argon", test_atomic_small_argon), &
            new_unittest("atomic::small::helium", test_atomic_small_helium), &
            new_unittest("atomic::small::iodine", test_atomic_small_iodine), &
            new_unittest("RMatrixI::example", test_RMatrixI_example) &
        ]
    END SUBROUTINE

    SUBROUTINE test_read_H_file_values(error, &
                                       path, &
                                       reduced_L_max, &
                                       coupling_id, &
                                       lplusp, &
                                       set_ML_max, &
                                       original_ML_max, &
                                       xy_plane_desired, &
                                       adjust_gs_energy, &
                                       gs_finast, &
                                       gs_energy_desired, &
                                       remove_eigvals_threshold, &
                                       reduced_lamax, &
                                       neigsrem_1st_sym, &
                                       neigsrem_higher_syms, &
                                       surf_amp_threshold_value, &
                                       expected_lamax, &
                                       expected_L_block_tot_nchan, &
                                       expected_max_L_block_size, &
                                       expected_LML_block_tot_nchan, &
                                       expected_L_block_lrgl, &
                                       expected_L_block_nspn, &
                                       expected_L_block_npty, &
                                       expected_L_block_nchan, &
                                       expected_mnp1, &
                                       expected_neigsrem, &
                                       expected_L_block_post, &
                                       expected_states_per_L_block, &
                                       expected_LML_block_lrgl, &
                                       expected_LML_block_nspn, &
                                       expected_LML_block_npty, &
                                       expected_LML_block_nchan, &
                                       expected_LML_block_ml, &
                                       expected_LML_block_Lblk, &
                                       expected_LML_block_post, &
                                       expected_states_per_LML_block, &
                                       expected_L_block_nconat, &
                                       expected_L_block_l2p, &
                                       expected_lrgt, &
                                       expected_LML_block_nconat, &
                                       expected_LML_block_l2p, &
                                       expected_m2p, &
                                       expected_lstartm1, &
                                       expected_eig, &
                                       expected_wmat, &
                                       expected_L_block_cf, &
                                       expected_LML_block_cf)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        CHARACTER(len=*), INTENT(IN) :: path

        INTEGER, INTENT(IN) :: reduced_L_max, coupling_id, lplusp, original_ML_max, gs_finast, reduced_lamax, neigsrem_1st_sym, &
                               neigsrem_higher_syms
        LOGICAL, INTENT(IN) :: set_ML_max, xy_plane_desired, adjust_gs_energy, remove_eigvals_threshold
        REAL(wp), INTENT(IN) :: gs_energy_desired, surf_amp_threshold_value

        INTEGER, INTENT(IN) :: expected_lamax, expected_L_block_tot_nchan, expected_max_L_block_size, expected_LML_block_tot_nchan
        INTEGER, DIMENSION(:), INTENT(IN) :: expected_L_block_lrgl, expected_L_block_nspn, expected_L_block_npty, &
                                             expected_L_block_nchan, expected_mnp1, expected_neigsrem, &
                                             expected_L_block_post, expected_states_per_L_block, &
                                             expected_LML_block_lrgl, expected_LML_block_nspn, &
                                             expected_LML_block_npty, expected_LML_block_nchan, &
                                             expected_LML_block_ml, expected_LML_block_Lblk, expected_LML_block_post, &
                                             expected_states_per_LML_block
        TYPE(subarray_integer), INTENT(IN) :: expected_L_block_l2p, expected_LML_block_l2p
        INTEGER, DIMENSION(:, :), INTENT(IN) :: expected_L_block_nconat, expected_lrgt, expected_LML_block_nconat, expected_m2p, &
                                                expected_lstartm1
        TYPE(subarray_wp), DIMENSION(:), INTENT(IN) :: expected_eig, expected_wmat, expected_L_block_cf, expected_LML_block_cf

        INTEGER :: actual_lamax, actual_L_block_tot_nchan, actual_max_L_block_size, actual_LML_block_tot_nchan
        INTEGER, DIMENSION(:), ALLOCATABLE :: actual_L_block_lrgl, actual_L_block_nspn, actual_L_block_npty, &
                                              actual_L_block_nchan, actual_mnp1, actual_neigsrem, actual_L_block_post, &
                                              actual_states_per_L_block, actual_LML_block_lrgl, actual_LML_block_nspn, &
                                              actual_LML_block_npty, actual_LML_block_nchan, actual_LML_block_ml, &
                                              actual_LML_block_Lblk, actual_LML_block_post, actual_states_per_LML_block
        INTEGER, DIMENSION(:, :), ALLOCATABLE :: actual_L_block_nconat, actual_L_block_l2p, actual_lrgt, &
                                                 actual_LML_block_nconat, actual_LML_block_l2p, actual_m2p, actual_lstartm1
        REAL(wp), DIMENSION(:, :), ALLOCATABLE :: actual_eig
        REAL(wp), DIMENSION(:, :, :), ALLOCATABLE :: actual_wmat
        REAL(wp), DIMENSION(:, :, :, :), ALLOCATABLE :: actual_L_block_cf, actual_LML_block_cf

        INTEGER :: ignored_nelc, ignored_nz, ignored_lrang2, ignored_ntarg
        REAL(wp) :: ignored_rmatr, ignored_bbloch
        REAL(wp), DIMENSION(:), ALLOCATABLE :: ignored_etarg
        INTEGER, DIMENSION(:), ALLOCATABLE :: ignored_ltarg, ignored_starg
        INTEGER :: ignored_lmaxp1
        INTEGER, DIMENSION(:), ALLOCATABLE :: ignored_kept, ignored_nstk, ignored_nchn
        INTEGER :: ignored_L_max, ignored_last_lrgl, ignored_inast

        CHARACTER(len=:), ALLOCATABLE :: relative_path, H_file_path
        CHARACTER(len=:), ALLOCATABLE :: context
        INTEGER :: lamax, ML_max, no_of_L_blocks, no_of_LML_blocks, nchmx, nstmx

        relative_path = path//"/inputs/H"
        H_file_path = data_file(relative_path)

        ML_max = original_ML_max
        !$OMP CRITICAL
        ! These two functions allocate, set, and deallocate a global variable so need to avoid threading.
        CALL read_H_parameters2(path=H_file_path, &
                                reduced_L_max=reduced_L_max, &
                                coupling_id=coupling_id, &
                                lplusp=lplusp, &
                                set_ML_max=set_ML_max, &
                                xy_plane_desired=xy_plane_desired, &
                                nelc=ignored_nelc, &
                                nz=ignored_nz, &
                                lrang2=ignored_lrang2, &
                                lamax=lamax, &
                                ntarg=ignored_ntarg, &
                                rmatr=ignored_rmatr, &
                                bbloch=ignored_bbloch, &
                                etarg=ignored_etarg, &
                                ltarg=ignored_ltarg, &
                                starg=ignored_starg, &
                                nchmx=nchmx, &
                                nstmx=nstmx, &
                                lmaxp1=ignored_lmaxp1, &
                                no_of_L_blocks=no_of_L_blocks, &
                                no_of_LML_blocks=no_of_LML_blocks, &
                                ML_max=ML_max, &
                                kept=ignored_kept, &
                                nstk=ignored_nstk, &
                                nchn=ignored_nchn, &
                                L_max=ignored_L_max, &
                                last_lrgl=ignored_last_lrgl, &
                                inast=ignored_inast)

        actual_lamax = lamax
        CALL read_H_file2(adjust_gs_energy=adjust_gs_energy, &
                          gs_finast=gs_finast, &
                          gs_energy_desired=gs_energy_desired, &
                          remove_eigvals_threshold=remove_eigvals_threshold, &
                          no_of_L_blocks=no_of_L_blocks, &
                          no_of_LML_blocks=no_of_LML_blocks, &
                          nchmx=nchmx, &
                          nstmx=nstmx, &
                          lplusp=lplusp, &
                          set_ML_max=set_ML_max, &
                          ML_max=ML_max, &
                          reduced_lamax=reduced_lamax, &
                          reduced_L_max = reduced_L_max, &
                          coupling_id=coupling_id, &
                          xy_plane_desired=xy_plane_desired, &
                          neigsrem_1st_sym=neigsrem_1st_sym, &
                          neigsrem_higher_syms=neigsrem_higher_syms, &
                          surf_amp_threshold_value=surf_amp_threshold_value, &
                          lamax=actual_lamax, &
                          L_block_tot_nchan=actual_L_block_tot_nchan, &
                          L_block_lrgl=actual_L_block_lrgl, &
                          L_block_nspn=actual_L_block_nspn, &
                          L_block_npty=actual_L_block_npty, &
                          L_block_nchan=actual_L_block_nchan, &
                          mnp1=actual_mnp1, &
                          neigsrem=actual_neigsrem, &
                          L_block_post=actual_L_block_post, &
                          states_per_L_block=actual_states_per_L_block, &
                          max_L_block_size=actual_max_L_block_size, &
                          L_block_nconat=actual_L_block_nconat, &
                          L_block_l2p=actual_L_block_l2p, &
                          lrgt=actual_lrgt, &
                          eig=actual_eig, &
                          wmat=actual_wmat, &
                          L_block_cf=actual_L_block_cf, &
                          LML_block_lrgl=actual_LML_block_lrgl, &
                          LML_block_nspn=actual_LML_block_nspn, &
                          LML_block_npty=actual_LML_block_npty, &
                          LML_block_nchan=actual_LML_block_nchan, &
                          LML_block_tot_nchan=actual_LML_block_tot_nchan, &
                          LML_block_ml=actual_LML_block_ml, &
                          LML_block_Lblk=actual_LML_block_Lblk, &
                          LML_block_post=actual_LML_block_post, &
                          states_per_LML_block=actual_states_per_LML_block, &
                          LML_block_nconat=actual_LML_block_nconat, &
                          LML_block_l2p=actual_LML_block_l2p, &
                          m2p=actual_m2p, &
                          lstartm1=actual_lstartm1, &
                          LML_block_cf=actual_LML_block_cf)
        !$OMP END CRITICAL

        context = ""

        IF (.NOT. compare(actual_lamax, expected_lamax)) THEN
            CALL format_context(context, "lamax", actual_lamax, expected_lamax)
        END IF

        IF (.NOT. compare(actual_L_block_tot_nchan, expected_L_block_tot_nchan)) THEN
            CALL format_context(context, "L_block_tot_nchan", actual_L_block_tot_nchan, expected_L_block_tot_nchan)
        END IF

        IF (.NOT. compare(actual_max_L_block_size, expected_max_L_block_size)) THEN
            CALL format_context(context, "max_L_block_size", actual_max_L_block_size, expected_max_L_block_size)
        END IF

        IF (.NOT. compare(actual_LML_block_tot_nchan, expected_LML_block_tot_nchan)) THEN
            CALL format_context(context, "LML_block_tot_nchan", actual_LML_block_tot_nchan, expected_LML_block_tot_nchan)
        END IF

        IF (.NOT. compare(actual_L_block_lrgl, expected_L_block_lrgl)) THEN
            CALL format_context(context, "L_block_lrgl", actual_L_block_lrgl, expected_L_block_lrgl)
        END IF

        IF (.NOT. compare(actual_L_block_nspn, expected_L_block_nspn)) THEN
            CALL format_context(context, "L_block_nspn", actual_L_block_nspn, expected_L_block_nspn)
        END IF

        IF (.NOT. compare(actual_L_block_npty, expected_L_block_npty)) THEN
            CALL format_context(context, "L_block_npty", actual_L_block_npty, expected_L_block_npty)
        END IF

        IF (.NOT. compare(actual_L_block_nchan, expected_L_block_nchan)) THEN
            CALL format_context(context, "L_block_nchan", actual_L_block_nchan, expected_L_block_nchan)
        END IF

        IF (.NOT. compare(actual_mnp1, expected_mnp1)) THEN
            CALL format_context(context, "mnp1", actual_mnp1, expected_mnp1)
        END IF

        IF (.NOT. compare(actual_neigsrem, expected_neigsrem)) THEN
            CALL format_context(context, "neigsrem", actual_neigsrem, expected_neigsrem)
        END IF

        IF (.NOT. compare(actual_L_block_post, expected_L_block_post)) THEN
            CALL format_context(context, "L_block_post", actual_L_block_post, expected_L_block_post)
        END IF

        IF (.NOT. compare(actual_states_per_L_block, expected_states_per_L_block)) THEN
            CALL format_context(context, "states_per_L_block", actual_states_per_L_block, expected_states_per_L_block)
        END IF

        IF (.NOT. compare(actual_LML_block_lrgl, expected_LML_block_lrgl)) THEN
            CALL format_context(context, "LML_block_lrgl", actual_LML_block_lrgl, expected_LML_block_lrgl)
        END IF

        IF (.NOT. compare(actual_LML_block_nspn, expected_LML_block_nspn)) THEN
            CALL format_context(context, "LML_block_nspn", actual_LML_block_nspn, expected_LML_block_nspn)
        END IF

        IF (.NOT. compare(actual_LML_block_npty, expected_LML_block_npty)) THEN
            CALL format_context(context, "LML_block_npty", actual_LML_block_npty, expected_LML_block_npty)
        END IF

        IF (.NOT. compare(actual_LML_block_nchan, expected_LML_block_nchan)) THEN
            CALL format_context(context, "LML_block_nchan", actual_LML_block_nchan, expected_LML_block_nchan)
        END IF

        IF (.NOT. compare(actual_LML_block_ml, expected_LML_block_ml)) THEN
            CALL format_context(context, "LML_block_ml", actual_LML_block_ml, expected_LML_block_ml)
        END IF

        IF (.NOT. compare(actual_LML_block_Lblk, expected_LML_block_Lblk)) THEN
            CALL format_context(context, "LML_block_Lblk", actual_LML_block_Lblk, expected_LML_block_Lblk)
        END IF

        IF (.NOT. compare(actual_LML_block_post, expected_LML_block_post)) THEN
            CALL format_context(context, "LML_block_post", actual_LML_block_post, expected_LML_block_post)
        END IF

        IF (.NOT. compare(actual_states_per_LML_block, expected_states_per_LML_block)) THEN
            CALL format_context(context, "states_per_LML_block", actual_states_per_LML_block, expected_states_per_LML_block)
        END IF

        IF (.NOT. compare(actual_L_block_nconat, expected_L_block_nconat)) THEN
            CALL format_context(context, "L_block_nconat", actual_L_block_nconat, expected_L_block_nconat)
        END IF

        IF (.NOT. compare(actual_L_block_l2p, expected_L_block_l2p)) THEN
            CALL format_context(context, "L_block_l2p", actual_L_block_l2p, expected_L_block_l2p)
        END IF

        IF (.NOT. compare(actual_lrgt, expected_lrgt)) THEN
            CALL format_context(context, "lrgt", actual_lrgt, expected_lrgt)
        END IF

        IF (.NOT. compare(actual_LML_block_nconat, expected_LML_block_nconat)) THEN
            CALL format_context(context, "LML_block_nconat", actual_LML_block_nconat, expected_LML_block_nconat)
        END IF

        IF (.NOT. compare(actual_LML_block_l2p, expected_LML_block_l2p)) THEN
            CALL format_context(context, "LML_block_l2p", actual_LML_block_l2p, expected_LML_block_l2p)
        END IF

        IF (.NOT. compare(actual_m2p, expected_m2p)) THEN
            CALL format_context(context, "m2p", actual_m2p, expected_m2p)
        END IF

        IF (.NOT. compare(actual_lstartm1, expected_lstartm1)) THEN
            CALL format_context(context, "lstartm1", actual_lstartm1, expected_lstartm1)
        END IF

        IF (.NOT. compare(actual_eig, expected_eig)) THEN
            CALL format_context(context, "eig", actual_eig, expected_eig)
        END IF

        IF (.NOT. compare(actual_wmat, expected_wmat)) THEN
            CALL format_context(context, "wmat", actual_wmat, expected_wmat)
        END IF

        IF (.NOT. compare(actual_L_block_cf, expected_L_block_cf)) THEN
            CALL format_context(context, "L_block_cf", actual_L_block_cf, expected_L_block_cf)
        END IF

        IF (.NOT. compare(actual_LML_block_cf, expected_LML_block_cf)) THEN
            CALL format_context(context, "LML_block_cf", actual_LML_block_cf, expected_LML_block_cf)
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "H file data set incorrectly!", context)
        END IF
    END SUBROUTINE

    SUBROUTINE test_atomic_small_Ne_4cores(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL test_read_H_file_values( &
            error=error, &
            path="atomic_tests/small_tests/Ne_4cores", &
            reduced_L_max=-1, &
            coupling_id=1, &
            lplusp=0, &
            set_ML_max=.TRUE., &
            original_ML_max=0, &
            xy_plane_desired=.FALSE., &
            adjust_gs_energy=.TRUE., &
            gs_finast=1, &
            gs_energy_desired=-5.000000E-01_WP, &
            remove_eigvals_threshold=.TRUE., &
            reduced_lamax=-1, &
            neigsrem_1st_sym=14, &
            neigsrem_higher_syms=20, &
            surf_amp_threshold_value=1.000000E+00_WP, &
            expected_lamax=4, &
            expected_L_block_tot_nchan=8, &
            expected_max_L_block_size=241, &
            expected_LML_block_tot_nchan=8, &
            expected_L_block_lrgl=[0, 1, 2], &
            expected_L_block_nspn=[1, 1, 1], &
            expected_L_block_npty=[0, 1, 0], &
            expected_L_block_nchan=[2, 3, 3], &
            expected_mnp1=[134, 233, 254], &
            expected_neigsrem=[10, 14, 13], &
            expected_L_block_post=[0, 124, 343, 584], &
            expected_states_per_L_block=[0, 0, 124, 219, 241, 0, 0], &
            expected_LML_block_lrgl=[0, 1, 2], &
            expected_LML_block_nspn=[1, 1, 1], &
            expected_LML_block_npty=[0, 1, 0], &
            expected_LML_block_nchan=[2, 3, 3], &
            expected_LML_block_ml=[0, 0, 0], &
            expected_LML_block_Lblk=[1, 2, 3], &
            expected_LML_block_post=[0, 124, 343, 584], &
            expected_states_per_LML_block=[0, 0, 124, 219, 241, 0, 0], &
            expected_L_block_nconat=RESHAPE([1, 1, 2, 1, 2, 1], [2, 3]), &
            expected_L_block_l2p=subarray([1, 0, 0, 0, 2, 1, 1, 3, 2], [range(1, 3), range(1, 3)], [3, 3]), &
            expected_lrgt=RESHAPE([0, 0, 0, 0, 0, 0, 0, 0, 0], [3, 3]), &
            expected_LML_block_nconat=RESHAPE([1, 1, 2, 1, 2, 1], [2, 3]), &
            expected_LML_block_l2p=subarray([1, 0, 0, 0, 2, 1, 1, 3, 2], [range(1, 3), range(1, 3)], [3, 3]), &
            expected_m2p=RESHAPE([0, 0, 0, 0, 0, 0, 0, 0, 0], [3, 3]), &
            expected_lstartm1=RESHAPE([0, 0, 5, 0, 2, 0], [3, 2]), &
            expected_eig=[ &
                subarray( &
                    [ &
                        9.645431E+05_WP, &
                        0.000000E+00_WP, &
                        -1.202337E+02_WP, &
                        -1.201877E+02_WP, &
                        -1.207701E+02_WP, &
                        -1.207144E+02_WP &
                    ], &
                    [range(134, 135), range(1, 3)], &
                    [254, 3] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        9.645423E+05_WP, &
                        0.000000E+00_WP, &
                        -3.993033E+01_WP, &
                        -3.797012E+01_WP &
                    ], &
                    [range(233, 234), range(1, 3)], &
                    [254, 3] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        9.164918E+04_WP, &
                        2.918260E+05_WP &
                    ], &
                    [range(253, 254), range(1, 3)], &
                    [254, 3] &
                ) &
            ], &
            expected_wmat=[ &
                subarray( &
                    [ &
                        -1.153032E-03_WP, &
                        4.970159E-11_WP, &
                        0.000000E+00_WP, &
                        -2.437117E-12_WP, &
                        7.880722E-04_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(133, 135), range(1, 1)], &
                    [3, 254, 3] &
                ), &
                subarray( &
                    [ &
                        1.231543E-11_WP, &
                        3.534825E-12_WP, &
                        -1.153032E-03_WP, &
                        7.880722E-04_WP, &
                        -2.284284E-12_WP, &
                        -4.695682E-12_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(232, 234), range(2, 2)], &
                    [3, 254, 3] &
                ), &
                subarray( &
                    [ &
                        -1.711013E-03_WP, &
                        1.422209E-09_WP, &
                        6.265766E-11_WP, &
                        -1.068273E-10_WP, &
                        4.314549E-11_WP, &
                        1.665668E-03_WP, &
                        -1.153032E-03_WP, &
                        -3.964434E-12_WP, &
                        1.773673E-11_WP &
                    ], &
                    [range(1, 3), range(252, 254), range(3, 3)], &
                    [3, 254, 3] &
                ) &
            ], &
            expected_L_block_cf=[ &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        -8.069609E-01_WP, &
                        -8.069609E-01_WP, &
                        0.000000E+00_WP, &
                        -8.552971E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(1, 2), range(1, 2), range(1, 1)], &
                    [3, 3, 4, 3] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -4.658991E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -6.588808E-01_WP, &
                        -4.658991E-01_WP, &
                        -6.588808E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -6.047864E-01_WP, &
                        0.000000E+00_WP, &
                        -6.047864E-01_WP, &
                        -4.276485E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(1, 2), range(2, 2)], &
                    [3, 3, 4, 3] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -5.103669E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -6.250693E-01_WP, &
                        -5.103669E-01_WP, &
                        -6.250693E-01_WP, &
                        0.000000E+00_WP, &
                        -8.552971E-02_WP, &
                        -6.285124E-01_WP, &
                        0.000000E+00_WP, &
                        -6.285124E-01_WP, &
                        -3.421188E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(1, 2), range(3, 3)], &
                    [3, 3, 4, 3] &
                ) &
            ], &
            expected_LML_block_cf=[ &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        -8.069609E-01_WP, &
                        -8.069609E-01_WP, &
                        0.000000E+00_WP, &
                        -8.552971E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(1, 2), range(1, 2), range(1, 1)], &
                    [3, 3, 4, 3] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -4.658991E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -6.588808E-01_WP, &
                        -4.658991E-01_WP, &
                        -6.588808E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -6.047864E-01_WP, &
                        0.000000E+00_WP, &
                        -6.047864E-01_WP, &
                        -4.276485E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(1, 2), range(2, 2)], &
                    [3, 3, 4, 3] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -5.103669E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -6.250693E-01_WP, &
                        -5.103669E-01_WP, &
                        -6.250693E-01_WP, &
                        0.000000E+00_WP, &
                        -8.552971E-02_WP, &
                        -6.285124E-01_WP, &
                        0.000000E+00_WP, &
                        -6.285124E-01_WP, &
                        -3.421188E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(1, 2), range(3, 3)], &
                    [3, 3, 4, 3] &
                ) &
            ] &
        )
    END SUBROUTINE

    SUBROUTINE test_atomic_small_arx(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        INTEGER, DIMENSION(3, 31) :: m2p

        m2p = 0

        CALL test_read_H_file_values( &
            error=error, &
            path="atomic_tests/small_tests/ar+", &
            reduced_L_max=-1, &
            coupling_id=1, &
            lplusp=0, &
            set_ML_max=.FALSE., &
            original_ML_max=-1, &
            xy_plane_desired=.FALSE., &
            adjust_gs_energy=.TRUE., &
            gs_finast=6, &
            gs_energy_desired=-1.200000E+00_WP, &
            remove_eigvals_threshold=.TRUE., &
            reduced_lamax=-1, &
            neigsrem_1st_sym=14, &
            neigsrem_higher_syms=20, &
            surf_amp_threshold_value=1.000000E+00_WP, &
            expected_lamax=4, &
            expected_L_block_tot_nchan=14, &
            expected_max_L_block_size=127, &
            expected_LML_block_tot_nchan=70, &
            expected_L_block_lrgl=[0, 1, 1, 2, 2, 3, 3], &
            expected_L_block_nspn=[2, 2, 2, 2, 2, 2, 2], &
            expected_L_block_npty=[0, 0, 1, 0, 1, 0, 1], &
            expected_L_block_nchan=[1, 1, 2, 3, 2, 2, 3], &
            expected_mnp1=[49, 48, 95, 141, 94, 94, 139], &
            expected_neigsrem=[5, 5, 9, 15, 9, 9, 12], &
            expected_L_block_post=[0, 44, 87, 173, 299, 384, 469, 596], &
            expected_states_per_L_block=[0, 0, 44, 43, 86, 126, 85, 85, 127, 0, 0], &
            expected_LML_block_lrgl=[ &
                0, &
                1, &
                1, &
                1, &
                1, &
                1, &
                1, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3 &
            ], &
            expected_LML_block_nspn=[ &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2 &
            ], &
            expected_LML_block_npty=[ &
                0, &
                0, &
                0, &
                0, &
                1, &
                1, &
                1, &
                0, &
                0, &
                0, &
                0, &
                0, &
                1, &
                1, &
                1, &
                1, &
                1, &
                0, &
                0, &
                0, &
                0, &
                0, &
                0, &
                0, &
                1, &
                1, &
                1, &
                1, &
                1, &
                1, &
                1 &
            ], &
            expected_LML_block_nchan=[ &
                1, &
                1, &
                1, &
                1, &
                2, &
                2, &
                2, &
                3, &
                3, &
                3, &
                3, &
                3, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                2, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3, &
                3 &
            ], &
            expected_LML_block_ml=[ &
                0, &
                -1, &
                0, &
                1, &
                -1, &
                0, &
                1, &
                -2, &
                -1, &
                0, &
                1, &
                2, &
                -2, &
                -1, &
                0, &
                1, &
                2, &
                -3, &
                -2, &
                -1, &
                0, &
                1, &
                2, &
                3, &
                -3, &
                -2, &
                -1, &
                0, &
                1, &
                2, &
                3 &
            ], &
            expected_LML_block_Lblk=[ &
                1, &
                2, &
                2, &
                2, &
                3, &
                3, &
                3, &
                4, &
                4, &
                4, &
                4, &
                4, &
                5, &
                5, &
                5, &
                5, &
                5, &
                6, &
                6, &
                6, &
                6, &
                6, &
                6, &
                6, &
                7, &
                7, &
                7, &
                7, &
                7, &
                7, &
                7 &
            ], &
            expected_LML_block_post=[ &
                0, &
                44, &
                87, &
                130, &
                173, &
                259, &
                345, &
                431, &
                557, &
                683, &
                809, &
                935, &
                1061, &
                1146, &
                1231, &
                1316, &
                1401, &
                1486, &
                1571, &
                1656, &
                1741, &
                1826, &
                1911, &
                1996, &
                2081, &
                2208, &
                2335, &
                2462, &
                2589, &
                2716, &
                2843, &
                2970 &
            ], &
            expected_states_per_LML_block=[ &
                0, &
                0, &
                44, &
                43, &
                43, &
                43, &
                86, &
                86, &
                86, &
                126, &
                126, &
                126, &
                126, &
                126, &
                85, &
                85, &
                85, &
                85, &
                85, &
                85, &
                85, &
                85, &
                85, &
                85, &
                85, &
                85, &
                127, &
                127, &
                127, &
                127, &
                127, &
                127, &
                127, &
                0, &
                0 &
            ], &
            expected_L_block_nconat=RESHAPE([1, 1, 2, 3, 2, 2, 3], [1, 7]), &
            expected_L_block_l2p=subarray( &
                [2, 0, 0, 2, 0, 0, 1, 3, 0, 0, 2, 4, 1, 3, 0, 2, 4, 0, 1, 3, 5], &
                [range(1, 3), range(1, 7)], &
                [3, 7] &
            ), &
            expected_lrgt=RESHAPE([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [3, 7]), &
            expected_LML_block_nconat=RESHAPE( &
                [ &
                    1, &
                    1, &
                    1, &
                    1, &
                    2, &
                    2, &
                    2, &
                    3, &
                    3, &
                    3, &
                    3, &
                    3, &
                    2, &
                    2, &
                    2, &
                    2, &
                    2, &
                    2, &
                    2, &
                    2, &
                    2, &
                    2, &
                    2, &
                    2, &
                    3, &
                    3, &
                    3, &
                    3, &
                    3, &
                    3, &
                    3 &
                ], &
                [1, 31] &
            ), &
            expected_LML_block_l2p=subarray([0, 2, 4, 1, 3, 0 ], [range(1, 3), range(12, 13)], [3, 31]), &
            expected_m2p=m2p, &
            expected_lstartm1=RESHAPE( &
                [ &
                    0, &
                    1, &
                    2, &
                    3, &
                    0, &
                    0, &
                    0, &
                    10, &
                    13, &
                    16, &
                    19, &
                    22, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    35, &
                    37, &
                    39, &
                    41, &
                    43, &
                    45, &
                    47, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    4, &
                    6, &
                    8, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    25, &
                    27, &
                    29, &
                    31, &
                    33, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    49, &
                    52, &
                    55, &
                    58, &
                    61, &
                    64, &
                    67 &
                ], &
                [31, 2] &
            ), &
            expected_eig=[ &
                subarray( &
                    [ &
                        5.983334E+05_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -5.118104E+02_WP, &
                        -5.107968E+02_WP &
                    ], &
                    [range(49, 50), range(1, 3)], &
                    [141, 7] &
                ), &
                subarray( &
                    [ &
                        8.806403E+04_WP, &
                        5.983334E+05_WP, &
                        5.983334E+05_WP, &
                        0.000000E+00_WP, &
                        -5.123023E+02_WP, &
                        -5.118104E+02_WP &
                    ], &
                    [range(48, 49), range(1, 3)], &
                    [141, 7] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        1.904531E+06_WP, &
                        0.000000E+00_WP, &
                        -4.862534E+02_WP, &
                        -4.845841E+02_WP &
                    ], &
                    [range(95, 96), range(2, 4)], &
                    [141, 7] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        6.206687E+05_WP, &
                        6.298844E+06_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(140, 141), range(3, 5)], &
                    [141, 7] &
                ), &
                subarray( &
                    [ &
                        -4.923013E+02_WP, &
                        -4.862534E+02_WP, &
                        1.904531E+06_WP, &
                        0.000000E+00_WP, &
                        5.983334E+05_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(94, 95), range(4, 6)], &
                    [141, 7] &
                ), &
                subarray( &
                    [ &
                        1.904531E+06_WP, &
                        0.000000E+00_WP, &
                        5.983334E+05_WP, &
                        0.000000E+00_WP, &
                        -4.891101E+02_WP, &
                        -4.836957E+02_WP &
                    ], &
                    [range(94, 95), range(5, 7)], &
                    [141, 7] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        1.904531E+06_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(139, 140), range(5, 7)], &
                    [141, 7] &
                ) &
            ], &
            expected_wmat=[ &
                subarray( &
                    [ &
                        -4.555207E-04_WP, &
                        0.000000E+00_WP, &
                        3.178392E-04_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(48, 50), range(1, 1)], &
                    [3, 141, 7] &
                ), &
                subarray( &
                    [ &
                        4.555207E-04_WP, &
                        0.000000E+00_WP, &
                        -3.178392E-04_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(47, 49), range(2, 2)], &
                    [3, 141, 7] &
                ), &
                subarray( &
                    [ &
                        -3.243719E-04_WP, &
                        3.396746E-12_WP, &
                        0.000000E+00_WP, &
                        -2.204506E-04_WP, &
                        9.642821E-15_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(94, 96), range(3, 3)], &
                    [3, 141, 7] &
                ), &
                subarray( &
                    [ &
                        -4.314670E-12_WP, &
                        -3.178392E-04_WP, &
                        -4.507444E-13_WP, &
                        -2.447367E-04_WP, &
                        7.307783E-12_WP, &
                        9.934144E-21_WP, &
                        -1.507264E-04_WP, &
                        3.018998E-14_WP, &
                        4.397579E-22_WP &
                    ], &
                    [range(1, 3), range(139, 141), range(4, 4)], &
                    [3, 141, 7] &
                ), &
                subarray( &
                    [ &
                        -3.243719E-04_WP, &
                        8.320297E-12_WP, &
                        0.000000E+00_WP, &
                        -2.204506E-04_WP, &
                        2.361983E-14_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(93, 95), range(5, 5)], &
                    [3, 141, 7] &
                ), &
                subarray( &
                    [ &
                        -4.555208E-04_WP, &
                        2.657190E-11_WP, &
                        0.000000E+00_WP, &
                        -3.178392E-04_WP, &
                        -7.968116E-13_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(93, 95), range(6, 6)], &
                    [3, 141, 7] &
                ), &
                subarray( &
                    [ &
                        -3.243719E-04_WP, &
                        1.089383E-11_WP, &
                        4.667611E-19_WP, &
                        -2.204506E-04_WP, &
                        3.092556E-14_WP, &
                        3.610936E-21_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(138, 140), range(7, 7)], &
                    [3, 141, 7] &
                ) &
            ], &
            expected_L_block_cf=[ &
                subarray( &
                    [ &
                        -2.022752E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -1.011376E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(1, 2), range(2, 2), range(1, 2)], &
                    [3, 3, 4, 7] &
                ), &
                subarray( &
                    [ &
                        -1.415927E+00_WP, &
                        -4.954711E-01_WP, &
                        0.000000E+00_WP, &
                        -4.954711E-01_WP, &
                        -1.618202E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(2, 2), range(3, 3)], &
                    [3, 3, 4, 7] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        -1.692356E+00_WP, &
                        0.000000E+00_WP, &
                        -1.692356E+00_WP, &
                        4.334469E-01_WP, &
                        -7.753734E-01_WP, &
                        0.000000E+00_WP, &
                        -7.753734E-01_WP, &
                        -1.444823E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(2, 2), range(4, 4)], &
                    [3, 3, 4, 7] &
                ), &
                subarray( &
                    [ &
                        1.415927E+00_WP, &
                        -1.213651E+00_WP, &
                        0.000000E+00_WP, &
                        -1.213651E+00_WP, &
                        -4.045504E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(2, 2), range(5, 5)], &
                    [3, 3, 4, 7] &
                ), &
                subarray( &
                    [ &
                        1.155858E+00_WP, &
                        -1.370679E+00_WP, &
                        0.000000E+00_WP, &
                        -1.370679E+00_WP, &
                        -1.444823E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(2, 2), range(6, 6)], &
                    [3, 3, 4, 7] &
                ), &
                subarray( &
                    [ &
                        -4.045504E-01_WP, &
                        -1.589043E+00_WP, &
                        0.000000E+00_WP, &
                        -1.589043E+00_WP, &
                        7.416758E-01_WP, &
                        -9.010054E-01_WP, &
                        0.000000E+00_WP, &
                        -9.010054E-01_WP, &
                        -1.348501E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(2, 2), range(7, 7)], &
                    [3, 3, 4, 7] &
                ) &
            ], &
            expected_LML_block_cf=[ &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -2.022752E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(1, 2), range(1, 2), range(1, 1)], &
                    [3, 3, 4, 31] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -1.011376E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(1, 2), range(1, 2), range(2, 2)], &
                    [3, 3, 4, 31] &
                ), &
                subarray( &
                    [ &
                        -4.045504E-01_WP, &
                        -1.589043E+00_WP, &
                        -1.589043E+00_WP, &
                        7.416758E-01_WP, &
                        -4.045504E-01_WP, &
                        -1.589043E+00_WP, &
                        -1.589043E+00_WP, &
                        7.416758E-01_WP &
                    ], &
                    [range(1, 2), range(1, 2), range(2, 2), range(28, 29)], &
                    [3, 3, 4, 31] &
                ) &
            ] &
        )
    END SUBROUTINE

    SUBROUTINE test_atomic_small_argon(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        INTEGER, DIMENSION(3, 17) :: m2p

        m2p = 0

        CALL test_read_H_file_values( &
            error=error, &
            path="atomic_tests/small_tests/argon", &
            reduced_L_max=-1, &
            coupling_id=1, &
            lplusp=0, &
            set_ML_max=.FALSE., &
            original_ML_max=-1, &
            xy_plane_desired=.FALSE., &
            adjust_gs_energy=.TRUE., &
            gs_finast=1, &
            gs_energy_desired=-5.791500E-01_WP, &
            remove_eigvals_threshold=.TRUE., &
            reduced_lamax=-1, &
            neigsrem_1st_sym=14, &
            neigsrem_higher_syms=20, &
            surf_amp_threshold_value=1.000000E+00_WP, &
            expected_lamax=4, &
            expected_L_block_tot_nchan=10, &
            expected_max_L_block_size=237, &
            expected_LML_block_tot_nchan=34, &
            expected_L_block_lrgl=[0, 1, 1, 2, 2], &
            expected_L_block_nspn=[1, 1, 1, 1, 1], &
            expected_L_block_npty=[0, 0, 1, 0, 1], &
            expected_L_block_nchan=[2, 1, 3, 3, 1], &
            expected_mnp1=[148, 110, 236, 253, 127], &
            expected_neigsrem=[12, 6, 17, 16, 5], &
            expected_L_block_post=[0, 136, 240, 459, 696, 818], &
            expected_states_per_L_block=[0, 0, 136, 104, 219, 237, 122, 0, 0], &
            expected_LML_block_lrgl=[0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2], &
            expected_LML_block_nspn=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], &
            expected_LML_block_npty=[0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1], &
            expected_LML_block_nchan=[2, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1], &
            expected_LML_block_ml=[0, -1, 0, 1, -1, 0, 1, -2, -1, 0, 1, 2, -2, -1, 0, 1, 2], &
            expected_LML_block_Lblk=[1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5], &
            expected_LML_block_post=[ &
                0, &
                136, &
                240, &
                344, &
                448, &
                667, &
                886, &
                1105, &
                1342, &
                1579, &
                1816, &
                2053, &
                2290, &
                2412, &
                2534, &
                2656, &
                2778, &
                2900 &
            ], &
            expected_states_per_LML_block=[ &
                0, &
                0, &
                136, &
                104, &
                104, &
                104, &
                219, &
                219, &
                219, &
                237, &
                237, &
                237, &
                237, &
                237, &
                122, &
                122, &
                122, &
                122, &
                122, &
                0, &
                0 &
            ], &
            expected_L_block_nconat=RESHAPE([1, 1, 1, 0, 2, 1, 2, 1, 1, 0], [2, 5]), &
            expected_L_block_l2p=subarray([1, 0, 0, 1, 0, 0, 0, 2, 1, 1, 3, 2, 2, 0, 0], [range(1, 3), range(1, 5)], [3, 5]), &
            expected_lrgt=RESHAPE([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [3, 5]), &
            expected_LML_block_nconat=RESHAPE( &
                [ &
                    1, &
                    1, &
                    1, &
                    0, &
                    1, &
                    0, &
                    1, &
                    0, &
                    2, &
                    1, &
                    2, &
                    1, &
                    2, &
                    1, &
                    2, &
                    1, &
                    2, &
                    1, &
                    2, &
                    1, &
                    2, &
                    1, &
                    2, &
                    1, &
                    1, &
                    0, &
                    1, &
                    0, &
                    1, &
                    0, &
                    1, &
                    0, &
                    1, &
                    0 &
                ], &
                [2, 17] &
            ), &
            expected_LML_block_l2p=subarray( &
                [ &
                    1, &
                    0, &
                    0, &
                    0, &
                    2, &
                    1, &
                    0, &
                    2, &
                    1, &
                    0, &
                    2, &
                    1, &
                    1, &
                    3, &
                    2 &
                ], &
                [range(1, 3), range(4, 8)], &
                [3, 17] &
            ), &
            expected_m2p=m2p, &
            expected_lstartm1=RESHAPE( &
                [ &
                    0, &
                    2, &
                    3, &
                    4, &
                    0, &
                    0, &
                    0, &
                    14, &
                    17, &
                    20, &
                    23, &
                    26, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    5, &
                    8, &
                    11, &
                    0, &
                    0, &
                    0, &
                    0, &
                    0, &
                    29, &
                    30, &
                    31, &
                    32, &
                    33 &
                ], &
                [17, 2] &
            ), &
            expected_eig=[ &
                subarray( &
                    [ &
                        8.897584E+06_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -5.162032E+02_WP, &
                        -5.156849E+02_WP &
                    ], &
                    [range(148, 149), range(1, 3)], &
                    [253, 5] &
                ), &
                subarray( &
                    [ &
                        -5.004030E+02_WP, &
                        -4.961408E+02_WP, &
                        2.689832E+06_WP, &
                        0.000000E+00_WP, &
                        -5.227307E+02_WP, &
                        -5.227122E+02_WP &
                    ], &
                    [range(110, 111), range(1, 3)], &
                    [253, 5] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        8.897584E+06_WP, &
                        0.000000E+00_WP, &
                        7.169592E+02_WP, &
                        7.174499E+02_WP &
                    ], &
                    [range(236, 237), range(2, 4)], &
                    [253, 5] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        8.452947E+05_WP, &
                        2.689832E+06_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(252, 253), range(3, 5)], &
                    [253, 5] &
                ), &
                subarray( &
                    [ &
                        -5.206452E+02_WP, &
                        -5.201880E+02_WP, &
                        -5.226652E+02_WP, &
                        -5.226530E+02_WP, &
                        8.452943E+05_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(127, 128), range(3, 5)], &
                    [253, 5] &
                ) &
            ], &
            expected_wmat=[ &
                subarray( &
                    [ &
                        -3.169720E-05_WP, &
                        4.677421E-14_WP, &
                        0.000000E+00_WP, &
                        5.559060E-15_WP, &
                        -2.167471E-05_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(147, 149), range(1, 1)], &
                    [3, 253, 5] &
                ), &
                subarray( &
                    [ &
                        4.650315E-05_WP, &
                        0.000000E+00_WP, &
                        -3.169720E-05_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(109, 111), range(2, 2)], &
                    [3, 253, 5] &
                ), &
                subarray( &
                    [ &
                        -1.877219E-14_WP, &
                        -1.811909E-14_WP, &
                        3.169720E-05_WP, &
                        2.167471E-05_WP, &
                        -3.883223E-15_WP, &
                        -4.903246E-15_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(235, 237), range(3, 3)], &
                    [3, 253, 5] &
                ), &
                subarray( &
                    [ &
                        4.650315E-05_WP, &
                        -7.978158E-13_WP, &
                        -1.761504E-13_WP, &
                        1.181547E-13_WP, &
                        4.483205E-14_WP, &
                        -4.566864E-05_WP, &
                        -3.169720E-05_WP, &
                        1.233075E-14_WP, &
                        2.214733E-14_WP &
                    ], &
                    [range(1, 3), range(251, 253), range(4, 4)], &
                    [3, 253, 5] &
                ), &
                subarray( &
                    [ &
                        6.504189E-05_WP, &
                        0.000000E+00_WP, &
                        -4.566863E-05_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(126, 128), range(5, 5)], &
                    [3, 253, 5] &
                ) &
            ], &
            expected_L_block_cf=[ &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        -5.979063E-01_WP, &
                        -5.979063E-01_WP, &
                        0.000000E+00_WP, &
                        -2.236744E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(1, 2), range(1, 2), range(1, 1)], &
                    [3, 3, 4, 5] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -3.452014E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -4.881884E-01_WP, &
                        -3.452014E-01_WP, &
                        -4.881884E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -1.581617E+00_WP, &
                        0.000000E+00_WP, &
                        -1.581617E+00_WP, &
                        -1.118372E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(1, 2), range(3, 3)], &
                    [3, 3, 4, 5] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -3.781491E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -4.631362E-01_WP, &
                        -3.781491E-01_WP, &
                        -4.631362E-01_WP, &
                        0.000000E+00_WP, &
                        -2.236744E-01_WP, &
                        -1.643665E+00_WP, &
                        0.000000E+00_WP, &
                        -1.643665E+00_WP, &
                        -8.946977E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(1, 2), range(4, 4)], &
                    [3, 3, 4, 5] &
                ) &
            ], &
            expected_LML_block_cf=[ &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        -5.979063E-01_WP, &
                        -5.979063E-01_WP, &
                        0.000000E+00_WP, &
                        -2.236744E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 2), range(1, 2), range(1, 2), range(1, 1)], &
                    [3, 3, 4, 17] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -3.452014E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -4.881884E-01_WP, &
                        -3.452014E-01_WP, &
                        -4.881884E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -1.581617E+00_WP, &
                        0.000000E+00_WP, &
                        -1.581617E+00_WP, &
                        -1.118372E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(1, 2), range(5, 5)], &
                    [3, 3, 4, 17] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -3.781491E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -4.631362E-01_WP, &
                        -3.781491E-01_WP, &
                        -4.631362E-01_WP, &
                        0.000000E+00_WP, &
                        -2.236744E-01_WP, &
                        -1.643665E+00_WP, &
                        0.000000E+00_WP, &
                        -1.643665E+00_WP, &
                        -8.946977E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -3.781491E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -4.631362E-01_WP, &
                        -3.781491E-01_WP, &
                        -4.631362E-01_WP, &
                        0.000000E+00_WP, &
                        -2.236744E-01_WP, &
                        -1.643665E+00_WP, &
                        0.000000E+00_WP, &
                        -1.643665E+00_WP, &
                        -8.946977E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(1, 2), range(11, 12)], &
                    [3, 3, 4, 17] &
                ) &
            ] &
        )
    END SUBROUTINE

    SUBROUTINE test_atomic_small_helium(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL test_read_H_file_values( &
            error=error, &
            path="atomic_tests/small_tests/helium", &
            reduced_L_max=-1, &
            coupling_id=1, &
            lplusp=0, &
            set_ML_max=.TRUE., &
            original_ML_max=0, &
            xy_plane_desired=.FALSE., &
            adjust_gs_energy=.TRUE., &
            gs_finast=1, &
            gs_energy_desired=-5.000000E-01_WP, &
            remove_eigvals_threshold=.TRUE., &
            reduced_lamax=-1, &
            neigsrem_1st_sym=14, &
            neigsrem_higher_syms=20, &
            surf_amp_threshold_value=1.000000E+00_WP, &
            expected_lamax=4, &
            expected_L_block_tot_nchan=15, &
            expected_max_L_block_size=135, &
            expected_LML_block_tot_nchan=15, &
            expected_L_block_lrgl=[0, 1, 2, 3], &
            expected_L_block_nspn=[1, 1, 1, 1], &
            expected_L_block_npty=[0, 1, 0, 1], &
            expected_L_block_nchan=[3, 4, 4, 4], &
            expected_mnp1=[118, 154, 152, 148], &
            expected_neigsrem=[15, 19, 17, 15], &
            expected_L_block_post=[0, 103, 238, 373, 506], &
            expected_states_per_L_block=[0, 0, 103, 135, 135, 133, 0, 0], &
            expected_LML_block_lrgl=[0, 1, 2, 3], &
            expected_LML_block_nspn=[1, 1, 1, 1], &
            expected_LML_block_npty=[0, 1, 0, 1], &
            expected_LML_block_nchan=[3, 4, 4, 4], &
            expected_LML_block_ml=[0, 0, 0, 0], &
            expected_LML_block_Lblk=[1, 2, 3, 4], &
            expected_LML_block_post=[0, 103, 238, 373, 506], &
            expected_states_per_LML_block=[0, 0, 103, 135, 135, 133, 0, 0], &
            expected_L_block_nconat=RESHAPE([1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1], [3, 4]), &
            expected_L_block_l2p=subarray([0, 1, 0, 0, 1, 0, 2, 1, 2, 1, 3, 2, 3, 2, 4, 3], [range(1, 4), range(1, 4)], [4, 4]), &
            expected_lrgt=RESHAPE([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [4, 4]), &
            expected_LML_block_nconat=RESHAPE([1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1], [3, 4]), &
            expected_LML_block_l2p=subarray( &
                [0, 1, 0, 0, 1, 0, 2, 1, 2, 1, 3, 2, 3, 2, 4, 3], &
                [range(1, 4), range(1, 4)], &
                [4, 4] &
            ), &
            expected_m2p=RESHAPE([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [4, 4]), &
            expected_lstartm1=RESHAPE([0, 0, 7, 0, 0, 3, 0, 11], [4, 2]), &
            expected_eig=[ &
                subarray( &
                    [ &
                        1.799649E+06_WP, &
                        0.000000E+00_WP, &
                        7.824511E+01_WP, &
                        7.864990E+01_WP, &
                        7.853690E+01_WP, &
                        7.908277E+01_WP &
                    ], &
                    [range(118, 119), range(1, 3)], &
                    [154, 4] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        5.439377E+05_WP, &
                        1.799647E+06_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(153, 154), range(1, 3)], &
                    [154, 4] &
                ), &
                subarray( &
                    [ &
                        5.439342E+05_WP, &
                        5.439377E+05_WP, &
                        5.439356E+05_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(152, 153), range(2, 4)], &
                    [154, 4] &
                ), &
                subarray( &
                    [ &
                        6.744862E+04_WP, &
                        6.745213E+04_WP, &
                        5.748509E+04_WP, &
                        6.745005E+04_WP, &
                        1.712708E+05_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(148, 149), range(2, 4)], &
                    [154, 4] &
                ) &
            ], &
            expected_wmat=[ &
                subarray( &
                    [ &
                        2.832414E-12_WP, &
                        -3.136794E-04_WP, &
                        0.000000E+00_WP, &
                        -6.061079E-13_WP, &
                        -8.419540E-04_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(2, 4), range(117, 119), range(1, 1)], &
                    [4, 154, 4] &
                ), &
                subarray( &
                    [ &
                        -1.401214E-11_WP, &
                        -3.601417E-12_WP, &
                        4.587983E-04_WP, &
                        8.590047E-12_WP, &
                        -6.100100E-12_WP, &
                        1.232473E-03_WP, &
                        -8.984886E-04_WP, &
                        -5.241225E-14_WP, &
                        1.116126E-12_WP &
                    ], &
                    [range(2, 4), range(152, 154), range(2, 2)], &
                    [4, 154, 4] &
                ), &
                subarray( &
                    [ &
                        9.500468E-11_WP, &
                        -9.890961E-11_WP, &
                        1.779333E-03_WP, &
                        1.315100E-03_WP, &
                        -2.788348E-12_WP, &
                        -9.747465E-12_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(2, 4), range(151, 153), range(3, 3)], &
                    [4, 154, 4] &
                ), &
                subarray( &
                    [ &
                        9.132457E-10_WP, &
                        -1.062808E-09_WP, &
                        2.615697E-03_WP, &
                        1.898241E-03_WP, &
                        -4.519559E-11_WP, &
                        -8.205736E-11_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(2, 4), range(147, 149), range(4, 4)], &
                    [4, 154, 4] &
                ) &
            ], &
            expected_L_block_cf=[ &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        5.773503E-01_WP, &
                        8.164966E-01_WP, &
                        0.000000E+00_WP, &
                        5.773503E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -6.666667E-01_WP, &
                        8.164966E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -9.428090E-01_WP, &
                        0.000000E+00_WP, &
                        -6.666667E-01_WP, &
                        -9.428090E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        1.060660E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        1.060660E+00_WP, &
                        7.500000E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 4), range(1, 4), range(1, 2), range(2, 2)], &
                    [4, 4, 4, 4] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        6.324555E-01_WP, &
                        7.745967E-01_WP, &
                        0.000000E+00_WP, &
                        6.324555E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -7.302967E-01_WP, &
                        7.745967E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -8.944272E-01_WP, &
                        0.000000E+00_WP, &
                        -7.302967E-01_WP, &
                        -8.944272E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        1.500000E-01_WP, &
                        1.102270E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        1.102270E+00_WP, &
                        6.000000E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 4), range(1, 4), range(1, 2), range(3, 3)], &
                    [4, 4, 4, 4] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        6.546537E-01_WP, &
                        7.559289E-01_WP, &
                        0.000000E+00_WP, &
                        6.546537E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -7.559289E-01_WP, &
                        7.559289E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -8.728716E-01_WP, &
                        0.000000E+00_WP, &
                        -7.559289E-01_WP, &
                        -8.728716E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        2.142857E-01_WP, &
                        1.113461E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        1.113461E+00_WP, &
                        5.357143E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 4), range(1, 4), range(1, 2), range(4, 4)], &
                    [4, 4, 4, 4] &
                ) &
            ], &
            expected_LML_block_cf=[ &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        5.773503E-01_WP, &
                        8.164966E-01_WP, &
                        0.000000E+00_WP, &
                        5.773503E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -6.666667E-01_WP, &
                        8.164966E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -9.428090E-01_WP, &
                        0.000000E+00_WP, &
                        -6.666667E-01_WP, &
                        -9.428090E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        1.060660E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        1.060660E+00_WP, &
                        7.500000E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 4), range(1, 4), range(1, 2), range(2, 2)], &
                    [4, 4, 4, 4] &
                ), &
                subarray( &
                    [ &
                        0.000000E+00_WP, &
                        6.546537E-01_WP, &
                        7.559289E-01_WP, &
                        0.000000E+00_WP, &
                        6.546537E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -7.559289E-01_WP, &
                        7.559289E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        -8.728716E-01_WP, &
                        0.000000E+00_WP, &
                        -7.559289E-01_WP, &
                        -8.728716E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        2.142857E-01_WP, &
                        1.113461E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        1.113461E+00_WP, &
                        5.357143E-01_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 4), range(1, 4), range(1, 2), range(4, 4)], &
                    [4, 4, 4, 4] &
                ) &
            ] &
        )
    END SUBROUTINE

    SUBROUTINE test_atomic_small_iodine(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL test_read_H_file_values( &
            error=error, &
            path="atomic_tests/small_tests/iodine", &
            reduced_L_max=7, &
            coupling_id=2, &
            lplusp=0, &
            set_ML_max=.TRUE., &
            original_ML_max=3, &
            xy_plane_desired=.FALSE., &
            adjust_gs_energy=.FALSE., &
            gs_finast=12, &
            gs_energy_desired=-0.0_WP, &
            remove_eigvals_threshold=.TRUE., &
            reduced_lamax=-1, &
            neigsrem_1st_sym=14, &
            neigsrem_higher_syms=20, &
            surf_amp_threshold_value=1.0_WP, &
            expected_lamax=10, &
            expected_L_block_tot_nchan=44, &
            expected_max_L_block_size=370, &
            expected_LML_block_tot_nchan=156, &
            expected_L_block_lrgl=[1, 1, 3, 3, 5, 5], &
            expected_L_block_nspn=[0, 0, 0, 0, 0, 0], &
            expected_L_block_npty=[0, 1, 0, 1, 0, 1], &
            expected_L_block_nchan=[5, 5, 8, 8, 9, 9], &
            expected_mnp1=[220, 221, 352, 353, 396, 396], &
            expected_neigsrem=[19, 22, 27, 31, 26, 28], &
            expected_L_block_post=[0, 201, 400, 725, 1047, 1417, 1785], &
            expected_states_per_L_block=[0, 0, 201, 199, 325, 322, 370, 368, 0, 0], &
            expected_LML_block_lrgl=[1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5, 5, 5], &
            expected_LML_block_nspn=SPREAD(0, dim=1, ncopies=20), &
            expected_LML_block_npty=[0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1], &
            expected_LML_block_nchan=[5, 5, 5, 5, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9], &
            expected_LML_block_ml=[-1, 1, -1, 1, -3, -1, 1, 3, -3, -1, 1, 3, -3, -1, 1, 3, -3, -1, 1, 3], &
            expected_LML_block_Lblk=[1, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6], &
            expected_LML_block_post=[ &
                0, &
                201, &
                402, &
                601, &
                800, &
                1125, &
                1450, &
                1775, &
                2100, &
                2422, &
                2744, &
                3066, &
                3388, &
                3758, &
                4128, &
                4498, &
                4868, &
                5236, &
                5604, &
                5972, &
                6340 &
            ], &
            expected_states_per_LML_block=[ &
                0, &
                0, &
                201, &
                201, &
                199, &
                199, &
                325, &
                325, &
                325, &
                325, &
                322, &
                322, &
                322, &
                322, &
                370, &
                370, &
                370, &
                370, &
                368, &
                368, &
                368, &
                368, &
                0, &
                0 &
            ], &
            expected_L_block_nconat=RESHAPE([2, 1, 2, 2, 1, 2, 4, 1, 3, 4, 1, 3, 5, 1, 3, 5, 1, 3], [3, 6]), &
            expected_L_block_l2p=subarray([0, 0, 0, 2, 2, 0, 1, 3, 0], [range(7, 9), range(2, 4)], [9, 6]), &
            expected_lrgt=SPREAD(SPREAD(0, dim=1, ncopies=9), dim=2, ncopies=6), &
            expected_LML_block_nconat=RESHAPE( &
                [ &
                    2, 1, 2, &
                    2, 1, 2,  &
                    2, 1, 2,  &
                    2, 1, 2,  &
                    4, 1, 3, &
                    4, 1, 3, &
                    4, 1, 3, &
                    4, 1, 3, &
                    4, 1, 3, &
                    4, 1, 3, &
                    4, 1, 3, &
                    4, 1, 3, &
                    5, 1, 3, &
                    5, 1, 3, &
                    5, 1, 3, &
                    5, 1, 3, &
                    5, 1, 3, &
                    5, 1, 3, &
                    5, 1, 3, &
                    5, 1, 3 &
                ], &
                [3, 20] &
            ), &
            expected_LML_block_l2p=subarray([0, 0, 0, 0, 0, 0, 0, 0, 0], [range(7, 9), range(2, 4)], [9, 20]), &
            expected_m2p=SPREAD(SPREAD(0, dim=1, ncopies=9), dim=2, ncopies=20), &
            expected_lstartm1=RESHAPE( &
                [ &
                    0, 5, 0, 0, 20, 28, 36, 44, 0, 0, 0, 0, 84, 93, 102, 111, 0, 0, 0, 0, &
                    0, 0, 10, 15, 0, 0, 0, 0, 52, 60, 68, 76, 0, 0, 0, 0, 120, 129, 138, 147 &
                ], &
                [20, 2] &
            ), &
            expected_eig=[ &
                subarray( &
                    [ &
                        1.401304e+04_WP, &
                        1.401307e+04_WP, &
                        1.401308e+04_WP, &
                        -6.760521e+03_WP, &
                        -6.757149e+03_WP, &
                        -6.757116e+03_WP, &
                        -6.793921e+03_WP, &
                        -6.771339e+03_WP, &
                        -6.767251e+03_WP &
                    ], &
                    [range(341, 343), range(4, 6)], &
                    [396, 6] &
                ), &
                subarray( &
                    [ &
                        1.050107e+04_WP, &
                        1.401304e+04_WP, &
                        1.401307e+04_WP, &
                        -6.783473e+03_WP, &
                        -6.760521e+03_WP, &
                        -6.757149e+03_WP, &
                        -6.802497e+03_WP, &
                        -6.793921e+03_WP, &
                        -6.771339e+03_WP &
                    ], &
                    [range(340, 342), range(4, 6)], &
                    [396, 6] &
                ), &
                subarray( &
                    [ &
                        1.050106e+04_WP, &
                        1.401304e+04_WP, &
                        1.401308e+04_WP, &
                        -6.907424e+03_WP, &
                        -6.907415e+03_WP, &
                        -6.907401e+03_WP, &
                        -6.907739e+03_WP, &
                        -6.907727e+03_WP, &
                        -6.907029e+03_WP &
                    ], &
                    [range(211, 213), range(2, 4)], &
                    [396, 6] &
                ), &
                subarray( &
                    [ &
                        1.849458e+03_WP, &
                        1.849459e+03_WP, &
                        1.849489e+03_WP, &
                        -2.296402e+03_WP, &
                        1.050106e+04_WP, &
                        1.050106e+04_WP, &
                        -6.907464e+03_WP, &
                        -6.907438e+03_WP, &
                        -6.907424e+03_WP &
                    ], &
                    [range(209, 211), range(1, 3)], &
                    [396, 6] &
                ), &
                subarray( &
                    [ &
                        1.849459e+03_WP, &
                        1.849487e+03_WP, &
                        1.849490e+03_WP, &
                        -2.296372e+03_WP, &
                        1.050103e+04_WP, &
                        1.050107e+04_WP, &
                        -6.783513e+03_WP, &
                        -6.783480e+03_WP, &
                        -6.783473e+03_WP &
                    ], &
                    [range(338, 340), range(3, 5)], &
                    [396, 6] &
                ), &
                subarray( &
                    [ &
                        1.050103e+04_WP, &
                        1.050107e+04_WP, &
                        1.401304e+04_WP, &
                        -6.783480e+03_WP, &
                        -6.783473e+03_WP, &
                        -6.760521e+03_WP, &
                        -6.802529e+03_WP, &
                        -6.802497e+03_WP, &
                        -6.793921e+03_WP &
                    ], &
                    [range(339, 341), range(4, 6)], &
                    [396, 6] &
                ) &
            ], &
            expected_L_block_cf=[ &
                subarray( &
                    [ &
                        -1.258223e+00_WP, &
                        1.665335e-16_WP, &
                        0.000000e+00_WP, &
                        1.665335e-16_WP, &
                        1.258223e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP &
                    ], &
                    [range(7, 9), range(7, 9), range(2, 2), range(3, 3)], &
                    [9, 9, 10, 6] &
                ), &
                subarray( &
                    [ &
                        2.832882e+00_WP, &
                        -2.313038e+00_WP, &
                        1.249001e-16_WP, &
                        -2.151057e-16_WP, &
                        -2.220446e-16_WP, &
                        -1.266903e+00_WP, &
                        -2.775558e-17_WP, &
                        1.110223e-16_WP, &
                        1.034422e+00_WP &
                    ], &
                    [range(1, 3), range(6, 8), range(2, 2), range(4, 4)], &
                    [9, 9, 10, 6] &
                ), &
                subarray( &
                    [ &
                        2.832882e+00_WP, &
                        -2.151057e-16_WP, &
                        -2.775558e-17_WP, &
                        -2.313038e+00_WP, &
                        -2.220446e-16_WP, &
                        1.110223e-16_WP, &
                        1.249001e-16_WP, &
                        -1.266903e+00_WP, &
                        1.034422e+00_WP &
                    ], &
                    [range(6, 8), range(1, 3), range(2, 2), range(4, 4)], &
                    [9, 9, 10, 6] &
                ), &
                subarray( &
                    [ &
                        2.379397e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        1.443290e-15_WP, &
                        6.661338e-16_WP, &
                        3.330669e-16_WP, &
                        6.661338e-16_WP, &
                        -2.516445e-01_WP, &
                        -1.849202e+00_WP &
                    ], &
                    [range(6, 8), range(5, 7), range(2, 2), range(6, 6)], &
                    [9, 9, 10, 6] &
                ), &
                subarray( &
                    [ &
                        2.379397e+00_WP, &
                        1.443290e-15_WP, &
                        6.661338e-16_WP, &
                        0.000000e+00_WP, &
                        6.661338e-16_WP, &
                        -2.516445e-01_WP, &
                        0.000000e+00_WP, &
                        3.330669e-16_WP, &
                        -1.849202e+00_WP &
                    ], &
                    [range(5, 7), range(6, 8), range(2, 2), range(6, 6)], &
                    [9, 9, 10, 6] &
                ), &
                subarray( &
                    [ &
                        2.111506e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        -1.779395e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP &
                    ], &
                    [range(5, 7), range(2, 4), range(2, 2), range(1, 1)], &
                    [9, 9, 10, 6] &
                ) &
            ], &
            expected_LML_block_cf=[ &
                subarray( &
                    [ &
                        -1.258223e+00_WP, &
                        1.665335e-16_WP, &
                        0.000000e+00_WP, &
                        1.665335e-16_WP, &
                        1.258223e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP &
                    ], &
                    [range(7, 9), range(7, 9), range(2, 2), range(5, 5)], &
                    [9, 9, 10, 20] &
                ), &
                subarray( &
                    [ &
                        -1.258223e+00_WP, &
                        1.665335e-16_WP, &
                        0.000000e+00_WP, &
                        1.665335e-16_WP, &
                        1.258223e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP &
                    ], &
                    [range(7, 9), range(7, 9), range(2, 2), range(8, 8)], &
                    [9, 9, 10, 20] &
                ), &
                subarray( &
                    [ &
                        -1.258223e+00_WP, &
                        1.665335e-16_WP, &
                        0.000000e+00_WP, &
                        1.665335e-16_WP, &
                        1.258223e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP &
                    ], &
                    [range(7, 9), range(7, 9), range(2, 2), range(6, 6)], &
                    [9, 9, 10, 20] &
                ) &
            ], &
            expected_wmat=[ &
                subarray( &
                    [ &
                        8.237033e-05_WP, &
                        2.382205e-03_WP, &
                        3.001152e-02_WP, &
                        -1.020928e-05_WP, &
                        -1.180885e-03_WP, &
                        3.302212e-01_WP, &
                        -2.525214e-03_WP, &
                        -4.073914e-02_WP, &
                        1.589703e-03_WP, &
                        -1.216212e-02_WP, &
                        -3.888838e-02_WP, &
                        -1.387472e-02_WP, &
                        -2.373168e-02_WP, &
                        -1.432652e-01_WP, &
                        1.094364e-01_WP, &
                        -8.882287e-03_WP, &
                        2.898824e-01_WP, &
                        6.460859e-02_WP, &
                        -8.103194e-03_WP, &
                        -6.982352e-02_WP, &
                        -7.348450e-03_WP, &
                        -2.001583e-01_WP, &
                        4.499020e-03_WP, &
                        -4.256523e-02_WP, &
                        -2.510460e-01_WP, &
                        1.992831e-02_WP, &
                        1.154462e-02_WP &
                    ], &
                    [range(4, 6), range(78, 80), range(3, 5)], &
                    [9, 396, 6] &
                ), &
                subarray( &
                    [ &
                        3.748986e-02_WP, &
                        4.985461e-02_WP, &
                        8.598329e-03_WP, &
                        4.170278e-02_WP, &
                        2.107927e-02_WP, &
                        4.836677e-02_WP, &
                        -1.183632e-01_WP, &
                        1.128564e-01_WP, &
                        -2.755954e-01_WP, &
                        -1.758565e-02_WP, &
                        -1.294193e-01_WP, &
                        1.848112e-01_WP, &
                        -7.806852e-02_WP, &
                        -1.185667e-02_WP, &
                        1.266764e-02_WP, &
                        9.441256e-06_WP, &
                        9.824658e-04_WP, &
                        2.094137e-03_WP, &
                        6.091995e-03_WP, &
                        1.031965e-01_WP, &
                        -3.123347e-03_WP, &
                        -7.939192e-05_WP, &
                        -6.910914e-03_WP, &
                        -1.605380e-02_WP, &
                        -1.273582e-04_WP, &
                        -9.277288e-03_WP, &
                        3.691809e-02_WP &
                    ], &
                    [range(5, 7), range(85, 87), range(4, 6)], &
                    [9, 396, 6] &
                ), &
                subarray( &
                    [ &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        0.000000e+00_WP, &
                        2.619411e-02_WP, &
                        -2.631255e-07_WP, &
                        1.316248e-05_WP, &
                        1.201990e-04_WP, &
                        -1.394913e+00_WP, &
                        -2.381609e-02_WP, &
                        1.394619e+00_WP, &
                        1.381501e-04_WP, &
                        -1.060087e-03_WP &
                    ], &
                    [range(5, 7), range(258, 260), range(1, 3)], &
                    [9, 396, 6] &
                ) &
            ] &
        )
    END SUBROUTINE

    SUBROUTINE test_RMatrixI_example(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        ! NOTE: This test is based on data that has not been confirmed to be inputs to a successful calculation, therefore some values may be non-sensical or incorrect.
        CALL test_read_H_file_values( &
            error=error, &
            path="RMatrixI_tests/example", &
            reduced_L_max=-1, &
            coupling_id=2, &
            lplusp=0, &
            set_ML_max=.FALSE., &
            original_ML_max=-1, &
            xy_plane_desired=.TRUE., &
            adjust_gs_energy=.TRUE., &
            gs_finast=1, &
            gs_energy_desired=-5.000000E-01_WP, &
            remove_eigvals_threshold=.TRUE., &
            reduced_lamax=-1, &
            neigsrem_1st_sym=14, &
            neigsrem_higher_syms=20, &
            surf_amp_threshold_value=1.000000E+00_WP, &
            expected_lamax=8, &
            expected_L_block_tot_nchan=26, &
            expected_max_L_block_size=292, &
            expected_LML_block_tot_nchan=47, &
            expected_L_block_lrgl=[0, 0, 2, 2, 4, 4], &
            expected_L_block_nspn=[0, 0, 0, 0, 0, 0], &
            expected_L_block_npty=[0, 1, 0, 1, 0, 1], &
            expected_L_block_nchan=[2, 2, 5, 5, 6, 6], &
            expected_mnp1=[97, 104, 239, 264, 286, 318], &
            expected_neigsrem=[12, 10, 28, 24, 30, 26], &
            expected_L_block_post=[0, 85, 179, 390, 630, 886, 1178], &
            expected_states_per_L_block=[0, 0, 85, 94, 211, 240, 256, 292, 0, 0], &
            expected_LML_block_lrgl=[0, 2, 2, 2, 4, 4, 4, 4, 4], &
            expected_LML_block_nspn=[0, 0, 0, 0, 0, 0, 0, 0, 0], &
            expected_LML_block_npty=[0, 0, 1, 1, 0, 0, 0, 1, 1], &
            expected_LML_block_nchan=[2, 5, 5, 5, 6, 6, 6, 6, 6], &
            expected_LML_block_ml=[0, 0, -2, 2, -4, 0, 4, -2, 2], &
            expected_LML_block_Lblk=[1, 3, 4, 4, 5, 5, 5, 6, 6], &
            expected_LML_block_post=[0, 85, 296, 536, 776, 1032, 1288, 1544, 1836, 2128], &
            expected_states_per_LML_block=[0, 0, 85, 211, 240, 240, 256, 256, 256, 292, 292, 0, 0], &
            expected_L_block_nconat=RESHAPE([1, 1, 1, 1, 3, 2, 3, 2, 4, 2, 4, 2], [2, 6]), &
            expected_L_block_l2p=subarray( &
                [1, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 1, 3, 1, 1, 0], &
                [range(1, 6), range(1, 3)], &
                [6, 6] &
            ), &
            expected_lrgt=RESHAPE([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, &
                                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [6, 6]), &
            expected_LML_block_nconat=RESHAPE([1, 1, 3, 2, 3, 2, 3, 2, 4, 2, 4, 2, 4, 2, 4, 2, 4, 2] , [2, 9]), &
            expected_LML_block_l2p=subarray([1, 1, 0, 0, 0, 0, 1, 1, 3, 1, 1, 0] , [range(1, 6), range(1, 2)], [6, 9]), &
            expected_m2p=RESHAPE([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, &
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, & 
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, & 
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, & 
                                  0, 0, 0, 0, 0, 0], [6, 9]), &
            expected_lstartm1=RESHAPE([0, 2, 0, 0, 17, 23, 29, 0, 0, 0, 0, 7, 12, 0, 0, 0, 35, 41], [9, 2]), &
            expected_eig=[ &
                subarray( &
                    [ &
                        5.352682E+05_WP, &
                        0.000000E+00_WP, &
                        1.199076E+03_WP, &
                        1.199614E+03_WP, &
                        -5.221214E+02_WP, &
                        -5.221043E+02_WP &
                    ], &
                    [range(97, 98), range(1, 3)], &
                    [318, 6] &
                ), &
                subarray( &
                    [ &
                        -5.253159E+02_WP, &
                        -5.253072E+02_WP, &
                        -5.251967E+02_WP, &
                        -5.251704E+02_WP, &
                        -5.260980E+02_WP, &
                        -5.260796E+02_WP &
                    ], &
                    [range(20, 21), range(1, 3)], &
                    [318, 6] &
                ), &
                subarray( &
                    [ &
                        -5.181241E+02_WP, &
                        -5.173896E+02_WP, &
                        -5.205391E+02_WP, &
                        -5.200470E+02_WP, &
                        -5.250745E+02_WP, &
                        -5.250735E+02_WP &
                    ], &
                    [range(55, 56), range(1, 3)], &
                    [318, 6] &
                ) &
            ], &
            expected_wmat=[ &
                subarray( &
                    [ &
                        -5.057795E-09_WP, &
                        -5.342753E-06_WP, &
                        0.000000E+00_WP, &
                        5.342753E-06_WP, &
                        -5.057864E-09_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(96, 98), range(1, 1)], &
                    [6, 318, 6] &
                ), &
                subarray( &
                    [ &
                        -3.668518E-06_WP, &
                        3.780969E-09_WP, &
                        0.000000E+00_WP, &
                        -5.444292E-05_WP, &
                        -3.443145E-05_WP, &
                        -2.941122E-06_WP, &
                        2.778462E-06_WP, &
                        -2.883696E-04_WP, &
                        -2.593396E-08_WP &
                    ], &
                    [range(2, 2), range(263, 265), range(4, 6)], &
                    [6, 318, 6] &
                ) &
            ], &
            expected_L_block_cf=[ &
                subarray( &
                    [ &
                      -1.685209E+00_WP, & 
                       0.000000E+00_WP, & 
                       1.685209E+00_WP, &     
                       0.000000E+00_WP, &     
                      -7.903834E-17_WP, &
                       0.000000E+00_WP, &         
                       3.587625E-16_WP, &
                       2.199846E-16_WP, &
                      -0.7149735E+00_WP, &         
                       2.582434E-17_WP, &
                      -1.191622E+00_WP, &          
                       0.000000E+00_WP &     
                    ], &
                    [range(1, 1), range(2, 3), range(2, 2), range(1, 6)], &
                    [6, 6, 8, 6] &
                ), &
                subarray( &
                    [ &
                        0.000000_WP, &
                       -1.191622_WP, &
                        0.000000_WP, &
                       -1.191622_WP, &
                        0.000000_WP, &
                       -1.191622_WP, &
                        0.000000_WP, &
                       -1.191622_WP, &
                        0.000000_WP, &
                        0.953298_WP, &
                        0.000000_WP, &
                        0.000000_WP &
                    ], &
                    [range(1, 1), range(1, 1), range(1, 2), range(1, 6)], &
                    [6, 6, 8, 6] &
                ) &
            ], &
            expected_LML_block_cf=[ &
                subarray( &
                    [ &
                        -1.191622E+00_WP, &
                        -1.685209E+00_WP, &
                        0.000000E+00_WP, &
                        -1.685209E+00_WP, &
                        -2.902409E-16_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP, &
                        0.000000E+00_WP &
                    ], &
                    [range(1, 3), range(1, 3), range(2, 2), range(1, 1)], &
                    [6, 6, 8, 9] &
                ), &
                subarray( &
                    [ &
                    -1.191622E+00_WP, &
                    -7.903834E-17_WP, &
                     0.000000E+00_WP, &
                    -1.685209E+00_WP, &
                    -1.532113E-17_WP, &
                     0.000000E+00_WP, &
                    -7.903834E-17_WP, &
                     9.532980E-01_WP, &
                    -7.149735E-01_WP, &
                     3.111118E-16_WP, &
                     5.329098E-01_WP, &
                     0.000000E+00_WP, &
                     0.000000E+00_WP, &
                    -7.149735E-01_WP, &
                    -9.532980E-01_WP, &
                     0.000000E+00_WP, &
                     1.598729E+00_WP, &
                     0.000000E+00_WP, &
                    -1.685209E+00_WP, &
                     3.111118E-16_WP, &
                     0.000000E+00_WP, &
                     1.035359E-15_WP, &
                     8.585526E-17_WP, &
                     0.000000E+00_WP, &
                    -1.532113E-17_WP, &
                     5.329098E-01_WP, &
                     1.598729E+00_WP, &
                     8.585526E-17_WP, &
                    -2.736391E-16_WP, &
                     0.000000E+00_WP, &
                     0.000000E+00_WP, &
                     0.000000E+00_WP, &
                     0.000000E+00_WP, &
                     0.000000E+00_WP, &
                     0.000000E+00_WP, &
                     0.000000E+00_WP &
                    ], &
                    [range(1, 6), range(1, 6), range(2, 2), range(2, 2)], &
                    [6, 6, 8, 9] &
                ) &
            ] &
        )
    END SUBROUTINE
END MODULE test_hamiltonian_input_file_read_H_file2
