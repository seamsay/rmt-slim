gen_tas command
***********************

.. automodule:: rmt_utilities.gen_tas
.. argparse::
   :module: rmt_utilities.dipole_cli
   :func: read_command_line
   :prog: RMT_gen_tas

