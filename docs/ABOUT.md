RMT Code: Overview
==================

Welcome to the RMT project.  RMT (R-matrix with time-dependence) is an
application for solving the time-dependent Schrodinger Equation for a single
atom, ion or molecule in a laser field. 

RMT is part of the UK-AMOR suite of codes for tackling problems in <b>A</b>tomic
<b>M</b>olecular and <b>O</b>ptical physics problems with the <b>R</b>matrix
approach. 

UK-AMOR is a High End Computing consortium, funded by the EPSRC under grant
number EP/R029342/1 and supported by CCPQ and ARCHER. 

More information on the UK-AMOR project can be found
[here](http://www.ukamor.com). 

The orginal manuscript describing the method can be found
[here](./RMT.pdf), a manuscript
describing the extension of RMT to describe dynamics in arbitrarily polarised light
fields [here](https://arxiv.org/abs/1812.00234). A PDF overview of the code can
be found [here](https://arxiv.org/abs/1905.06156), although most of the information therein can be found in these
pages

Additional documentation describing various aspects of running the code can be
found below:

* \ref quick_start "Compilation Instructions"
* \ref inputs "Input Parameters"
* \ref run_rmt "Running RMT"
* \ref compinner "Computational Outline"
* \ref rmtout2 "Output Files and Directories"
* <a href="./html/index.html"><b>Extracting Observables</b></a>
* \ref reform_wavefunction "Rebuilding the wavefunction"

License
-------

RMT is licensed with the Gnu Public License: 

 Copyright 2019 
 
> RMT is free software: you can redistribute it and/or modify it under the terms
> of the GNU General Public License as published by the Free Software Foundation,
> either version 3 of the License, or (at your option) any later version.
>
> RMT is distributed in the hope that it will be useful, but WITHOUT ANY
> WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
> PARTICULAR PURPOSE.  See the GNU General Public License for more details.
>
> A copy of the GNU General Public License can be found in all of the
> repositories contained herein (in rmt/COPYING). Alternatively, you can also
> visit [gnu.org](https://www.gnu.org/licenses/).


    +-------------------------------------------------------------------------+
    |                       |                                                 |
    |                       |  Time-dependent R-matrix package                |
    |                       |                                                 |
    |   __          ______  |  Queen's University Belfast (c) 2015 - 2019     |
    |  || \  ||\/||   ||    |                                                 |
    |  || /  ||  ||   ||    |  Greg Armstrong, Jakub Benda, Andrew Brown,     |
    |  || \  ||  ||   ||    |  Daniel Clarke, Kathryn Hamilton,               |
    |                       |  Michael Lysaght, Zdenek Masin, Robert McGibbon,|
    |                       |  Laura Moore, Lampros Nikolopoulos,             |
    |                       |  Jonathan Parker, Martin Plummer, Ken Taylor,   |
    |                       |  Hugo van der Hart, Jack Wragg                  |
    |                       |                                                 |
    +-------------------------------------------------------------------------+

