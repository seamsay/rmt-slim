\page run_rmt Running RMT

Test Suite
==========

To test your version of RMT you may wish to run some calculations and compare
with expected outputs.  A test suite is provided with sample outputs under
`tests`. The exemplar calculations are subdivided into atomic and molecular
categories, with the atomic calculations further divided into small (can be run
on 128 or less cores) and large (requires 256 or more cores) directories.

Atomic Tests
------------

Each atomic test calculation directory is structured identically. Taking the
`tests/atomic_tests/small_tests/helium` directory as an example, the directory
contains subdirectories `inputs`-- containing the necessary input files: 

    H, d, d00, Splinedata, Splinewaves, input.conf

and the output files under the directory `rmt_output`: 

    data/, pop_all, pop_inn, pop_out, expec_z_all, expec_v_all 

If the input data has been generated using the RMatrix-I codes, then the input files will be:

    H, D***, Splinedata, Splinewaves, input.conf

Molecular Tests 
---------------

The molecular test calculation directories contain the input files not only for
the RMT calculations, but also for the UKRmol+ calculations (which generate the
input for RMT in the directory) in directory `UKRmol+`. The relevant inputs for
the RMT calculation are contained in `inputs`,  where files analagous to the
atomic case reside: inputs (`input.conf, molecular_data`) and outputs
(`rmt_output`).  The molecular calculations all run on just 10 cores.


Field Tests
-----------

The field test calculation directorues contain an input file which can be used
to test the electric field input parameters. These tests do not require a full
calculation, fieldcheck can be used instead.


Running a Test Calculation
--------------------------

To run a test calculation, simply change into the chosen test directory and use a 
parallel process launcher to run your version of RMT. For example:

    cd rmt/tests/atomic_tests/small/argon
    mpiexec -n 24 <path_to_repository>/rmt/source/build/bin/rmt.x > log.out


Here `-n 24` means we are using 24 cores for our RMT calculation, and is the sum of the
`no_of_pes_to_use_inner` and `no_of_pes_to_use_outer` variables.

The directory `rmt_output` contains the expected RMT output for the test
calculation.  To check your version of RMT you may wish to compare your output
data with this standard output. Note that it should be expected that the output
files contained in the `rmt_output` directories should be reproduced identically
on all systems with all compilers. It is sufficient to check that the numbers do 
not vary substantially. For reference, the data provided was determined using 
the GCC 10.1.0 compiler on [ARCHER 2](https://www.archer2.ac.uk). Furthermore, 
only some of the output files produced by the RMT calculation are present in the 
`rmt_output` directories, and thus the utility script `compare_rmt_runs.py` will show 
a warning when executed against the test calculations.

Running an RMT Calculation
==========================


Prerequisites
------------

Before running an RMT calculation you should have:

1. A copy of the executable `rmt.x` (see \ref quick_start "README" for compilation instructions).

   Your working directory should contain:

2. The appropriate input files - `H, Splinewaves, Splinedata` and either `d, d00` or `D***`
    for an atomic calculation or `molecular_data` for a molecular calculation.

3. The file `input.conf` populated with the correct parameters (see \ref inputs "/source/modules/initial_conditions").

4. The empty directories `ground, state, data` to house some of the RMT output.

Execution
---------

Once you have all of the prerequisities listed above you will be ready to run an
RMT calculation.  To do this, simply call a parallel process launcher from the
directory containing the input files.  For example:


    mpiexec -n 24 <path_to_repository>/rmt/source/bin/rmt.x > log.out


Here `-n 24` specifies that we want to run over 24 cores - your choice of
`number_of_pes_inner` and `number_of_pes_outer` should add up to this number. 

You can of course symbolically link or copy the executable into your working directory.

Postprocessing
--------------

Upon successful completion of a calculation each processor will print `Reached
finalise` either to the screen or to a chosen output file and your working
directory will contain the following:


    CurrentPosition                             log.out (piped output file)
    d                                           pop_all.<version_number>
    d00                                         pop_inn.<version_number>
    data                                        pop_out.<version_number>
    EField.<version_number>                     rmt.x
    expec_v_all.<version_number> (optional)     Splinewaves
    expec_z_all.<version_number> (optional)     Splinedata
    ground                                      state
    H                                           timing_inner.<version_number>
    hstat.<version_number>                      timing_outer0.<version_number>
    input.conf                                  timing_outer1.<version_number>


The directories `ground` and ` state` will contain the `psi_inner` and
`psi_outer` wavefunction files. `data` will contain the `pop` channel population
files.  Instructions for manipulating the output of RMT calculations can be
found \ref extracting_obs "in /utilities/pylib/README".

The expectation (`expec_*`) and population (`pop_*`) files contain values associated
with start of each iteration. The electric field file (`EField.*`) contains
values of the electric field at the middle of each iteration.

Parameters
==========

To make the tests as thorough as possible, different calculations may test different input
parameters. Thus, some test calculations may produce output files which differ to those 
outlined above. If this is the case, the `README` file for that test will indicate any specific
parameters the calculation is testing, and any changes to the output files produced.

Parameters Tested
-----------------

The following input parameters are tested in the Test Suite:

    small_tests/Ne_4cores ::  Keep_Checkpoints = .true.,
                              Checkpts_Per_Run = 10

    small_tests/ar+ ::        no_of_omp_threads_inner = 2,
                              no_of_omp_threads_outer = 2

    small_tests/argon ::      use_field_from_file = .true.

    small_tests/helium ::     Absorb_Desired             = .true.,
                              Sigma_Factor               = 0.25,
                              Start_Factor               = 0.75,
                              Absorption_Interval        = 25

    big_tests/Ar_LS ::        write_ground = .false.,

    big_tests/Ar_jK ::        write_ground = .false.,

    big_tests/ne+ ::          no_of_field_confs = 2,
                              binary_data_files = .true.

    field_tests/read_field_test :: read_field_from_file = .true.

    field_tests/APT_test ::   use_pulse_train = .true.
                              APT_bursts = 8.0
                              APR_ramp_bursts = 2.0
                              no_of_field_confs = 16

    field_tests/other_tests :: USE_2Colour_Field          = .true.
                               Cross_Polarized            = .true.
                               angle_between_pulses_in_deg = 30
                               xy_plane_desired           = .true.
                               delay_XUV_in_IR_periods    = 0.5


Parameters Not Tested
---------------------

The following input parameters are not tested in the Test Suite:

    use_leakage_pulse                           
    intensity2ir
    surf_amp_threshold_value
    neigsrem_1st_sym
    neigsrem_higher_syms
    hermitian_tolerance
    window_harmonics                        
    window_cutoff                           
    window_FWHM                             

