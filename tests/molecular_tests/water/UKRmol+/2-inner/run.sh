#!/bin/bash

# Calculate eigenstates of the target (water molecule) using the standard
# Gaussian basis. Calculate dipole transition moments between the target
# bound states. Then, calculate also eigenstates of the neutral molecule.

C2v=(A1 B1 B2 A2)

rm -f *.dat fort.* borndat log_file.* molecule.polarisability *.err *.out

for i in 0 1 2
do

    echo "H2O+: congen (sym ${C2v[$i]})"

    congen < inputs/target.congen.doublet.${C2v[$i]}.inp \
                 1> target.congen.doublet.${C2v[$i]}.out \
                 2> target.congen.doublet.${C2v[$i]}.err || exit 1

    mv fort.$(( 720 + $i )) target-configurations-${C2v[$i]}.dat

    echo "H2O+: scatci (sym ${C2v[$i]})"

    ln -s ../1-integrals/moints fort.16
    ln -s target-configurations-${C2v[$i]}.dat fort.72$i

    scatci < inputs/target.scatci.doublet.${C2v[$i]}.inp \
                 1> target.scatci.doublet.${C2v[$i]}.out \
                 2> target.scatci.doublet.${C2v[$i]}.err || exit 1

    rm -f fort.16 fort.72$i fort.82$i

done

mv fort.26 target-ci-coeffs.dat

echo "H2O+: denprop"

ln -s ../1-integrals/moints fort.17
ln -s target-ci-coeffs.dat fort.26
ln -s target-configurations-A1.dat fort.720
ln -s target-configurations-B1.dat fort.721
ln -s target-configurations-B2.dat fort.722

denprop < inputs/target.denprop.inp \
              1> target.denprop.out \
              2> target.denprop.err || exit 1

mv fort.24 target-properties.dat
rm -f fort.17 fort.26 fort.720 fort.721 fort.722

for i in 0 1 2 3
do

    echo "H2O (sym ${C2v[$i]}): congen"

    congen < inputs/scattering.congen.singlet.${C2v[$i]}.inp \
                 1> scattering.congen.singlet.${C2v[$i]}.out \
                 2> scattering.congen.singlet.${C2v[$i]}.err || exit 1

    mv fort.7$i scattering-configurations-${C2v[$i]}.dat

    echo "H2O (sym ${C2v[$i]}): scatci"

    ln -s ../1-integrals/moints fort.16
    ln -s target-ci-coeffs.dat fort.26
    ln -s scattering-configurations-${C2v[$i]}.dat fort.7$i

    scatci < inputs/scattering.scatci.singlet.${C2v[$i]}.inp \
                 1> scattering.scatci.singlet.${C2v[$i]}.out \
                 2> scattering.scatci.singlet.${C2v[$i]}.err || exit 1

    mv fort.2001 scattering-target-vectors-${C2v[$i]}.dat
    rm -f fort.16 fort.26 fort.7$i fort.8$i

done

mv fort.25 scattering-ci-coeffs.dat
