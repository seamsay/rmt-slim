#!/bin/bash

# Calculate dipole transition moment between all inner-region CSFs across all symmetries.
# Then, use the utility "rmt_interface" to write RMT input data file "molecular_data".

rm -f fort.* log_file.* *.out *.dat *.err

echo "H2O: cdenprop (target)"

ln -s ../1-integrals/moints fort.17
ln -s ../2-inner/target-properties.dat fort.24
ln -s ../2-inner/scattering-ci-coeffs.dat fort.25
ln -s ../2-inner/target-ci-coeffs.dat fort.26

ln -s ../2-inner/scattering-configurations-A1.dat fort.70
ln -s ../2-inner/scattering-configurations-B1.dat fort.71
ln -s ../2-inner/scattering-configurations-B2.dat fort.72
ln -s ../2-inner/scattering-configurations-A2.dat fort.73

cdenprop_target < inputs/cdenprop_target.inp \
                      1> cdenprop_target.out \
                      2> cdenprop_target.err || exit 1

echo "H2O: RMT interface"

ln -s ../1-integrals/moints fort.22

rmt_interface < inputs/rmt_interface.inp \
                    1> rmt_interface.out \
                    2> rmt_interface.err || exit 1

rm fort.17 fort.22 fort.24 fort.25 fort.26 fort.7?
