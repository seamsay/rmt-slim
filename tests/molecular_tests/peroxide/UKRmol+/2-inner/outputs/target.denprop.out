+-------------------------------------------------------------------------+
|              _              |                                           |
| || || ||//  | \          |  | University College London (C) 1994 - 2019 |
| || || ||    | / |/\/\ /\ |  | Open University           (C) 2007 - 2019 |
| \\_// ||\\  | \ | | | \/ |  |                                           |
|                             |                                           |
+-------------------------------------------------------------------------+
| Last revision: 2.0.2                                                    |
| Last author:   Jimena Gorfinkiel                                        |
| Last date:     Tue Mar 12 13:22:01 2019 +0000                           |
+-------------------------------------------------------------------------+


                     Density Matrix package
                     PROGRAM DENPROP

                     DATE =20190412


     H2O2 - Transition Moments                                                       


     No. of different wavefunction pairs to be computed =     3


          Assuming UKRMol+ integrals on input.


     Wavefunction location table
     ---------------------------

     Wavefunctions consist of two parts in the Alchemy
     and R-matrix codes. The parts are: 

       (i)  Slater determinants and Clebsch-Gordan 
            coefficients i.e. CSF definitions 
      (ii)  CI expansion coefficient over CSFs     

     Data corresponding to (i) is in a single dataset 
     for each different wavefunctions while data for (ii)
     is stored, for all wavefunctions, on a single library
     dataset

     No. of target states =   2

     No.  Determinants   CI vectors  Set. No.  Start/End
     ---  ------------   ----------  --------  ---------

       1      720           26            1    1   1
       2      721           26            2    1   1

     The print flags, NPFLG, are as follows: 
     --------------------------------------- 

     1. Wavefunction determinant data from SORT file =  0
     2. Diagonal density matrix formulae evaluation  =  0
     3. Off-diagonal density matrix form. evaluation =  0
     4. Summary of unsorted density matrix formulae  =  0
     5. Printing of transformed property integrals   =  0
     6. Sorting procedure for density matrix form.   =  0
     7.                                              =  0
     8.                                              =  0
     9.                                              =  1
     10. Summary of properties for external region   =  0
     11. Polarizibility calculation                  =  0



     Dataset information and logical unit association:
     -------------------------------------------------

     Un-sorted density matrix expressions (NFTD) = 21
     Sorted density matrix expressions    (NFTA) = 30
     Direct access file for sorting       (NFDA) = 20
     Molecular property integrals       (NFTINT) = 17
     Computed moments written to         (NFTMT) = 24
                           with set number NMSET =  1
     Old style moments as in TMTJT/CJG  (NFTMOM) = 61
                          with set number NMSET2 =  1


     Property operator information will be obtained 
     from the file of transformed integrals. To control
     the number evaluated modify your INTS input cards 


     Target Data File Construction for External Region
     -------------------------------------------------


     Logical unit for properties file     =  24
     Set number to be used for properties =   1
     Nuclear center (NUCCEN)  defined     =  -1
     Flag for add. of nuclear terms (ISW) =   0


1-electron integrals read-in.

Number of types of property integrals:    9


     **** &INPUT data has been verified 


          Loading UKRmol+ data...

     Molecule is Abelian using D2h subgroup with  2 elements 



          GROUP MULTIPLICATION TABLE FOR D2H SYMMETRY

                   1   2   3   4   5   6   7   8
                  AG  B3U B2U B1G B1U B2G B3G AU 
               ************************************  
               *                                  *  
           AG  *  AG  B3U B2U B1G B1U B2G B3G AU  *  
               *                                  *  
           B3U *  B3U AG  B1G B2U B2G B1U AU  B3G *  
               *                                  *  
           B2U *  B2U B1G AG  B3U B3G AU  B1U B2G *  
               *                                  *  
           B1G *  B1G B2U B3U AG  AU  B3G B2G B1U *  
               *                                  *  
           B1U *  B1U B2G B3G AU  AG  B3U B2U B1G *  
               *                                  *  
           B2G *  B2G B1U AU  B3G B3U AG  B1G B2U *  
               *                                  *  
           B3G *  B3G AU  B1U B2G B2U B1G AG  B3U *  
               *                                  *  
           AU  *  AU  B3G B2G B1U B1G B2U B3U AG  *  
               *                                  *  
               ************************************  


     Wavefunction symmetry numbers =   0   0

     Wavefunction symmetry difference =   0

     Target vectors read successfully


     Wavefunction symmetry numbers =   1   0

     Wavefunction symmetry difference =   1

     Target vectors read successfully


     Wavefunction symmetry numbers =   1   1

     Wavefunction symmetry difference =   0

     Target vectors read successfully

          Loading UKRmol+ data...

          Details of all     5 centers follows: 

  No.     X        Y        Z      Charge   Symbol    Mass     Spin     Quad  

    1    0.000    1.311   -0.058    8.000       O    15.995    0.000    0.000
    2    0.000   -1.311   -0.058    8.000       O    15.995    0.000    0.000
    3    1.437    1.708    0.916    1.000       H     1.008    0.000    0.000
    4   -1.437   -1.708    0.916    1.000       H     1.008    0.000    0.000
    5    0.000    0.000    0.000    0.000             0.000    0.000    0.000

          Total Molecular Mass =       34.005 Dalton (amu)

          Co-ordinates of the Center of Mass in the present axis system: 

               0.00000      0.00000     -0.00000


          Moment of Inertia Tensor (Lower Half Triangle):

                 62.6518140279
                  4.9460777232         5.9570897031
                  0.0000000000         0.0000000000        65.0142470269


          Principal Moments of Inertia (Atomic units): 

              10078.4255961882    114987.8530991620    118513.6257200774


          Principal Moments of Inertia (amu angstrom^2): 

                  1.5469656001        17.6498056638        18.1909863181


          Principal Axes Transformation Matrix: 

               0.08626 x     -0.99627 y      0.00000 z 
              -0.99627 x     -0.08626 y      0.00000 z 
               0.00000 x      0.00000 y      1.00000 z 

          Rotational Constants (Atomic Units): 

                  0.0000496109         0.0000043483         0.0000042189


          Rotational Constants (cm**(-1)): 

                 10.8883375635         0.9543381935         0.9259466946


          Rotational Constants (eV): 

                  0.0013499951         0.0001183240         0.0001148039




 The following target state and properties tables are now in energy order


          Unique Target State Table
          -------------------------

          There are     2 states in the table

     (2S+1)   Sz    Irrep   Energy (Hartrees)
     ------   --    -----   -----------------

    1     2      0      1   -150.4224319876
    2     2      0      0   -150.3783023909


               ========================

               W A V E F U N C T I O N 
                 P R O P E R T I E S   
                      T A B L E        

               ========================




     No. of Expectation Values (Rows) in table =      15

     No. I-State J-State Property Operator Code Expectation Value
     --- ------- ------- ---------------------- -----------------

     1     2     2     2 0 0 0 0 0 0 0     0.1700D+02
     2     2     2     4 1 0 0 5 1 0 0    -0.8805D-01
     3     2     2     4 2 0 0 5 2 2-1     0.4627D+01
     4     2     2     4 2 0 0 5 2 0 0    -0.1448D+02
     5     2     2     4 2 0 0 5 2 2 1    -0.2457D+02
     6     1     2     2 0 0 0 0 0 0 0     0.0000D+00
     7     1     2     4 1 0 0 5 1 1-1     0.4939D+00
     8     1     2     4 1 0 0 5 1 1 1     0.6730D-02
     9     1     2     4 2 0 0 5 2 1-1     0.3663D+00
    10     1     2     4 2 0 0 5 2 1 1    -0.5561D+00
    11     1     1     2 0 0 0 0 0 0 0     0.1700D+02
    12     1     1     4 1 0 0 5 1 0 0     0.1138D+00
    13     1     1     4 2 0 0 5 2 2-1     0.3662D+01
    14     1     1     4 2 0 0 5 2 0 0    -0.1545D+02
    15     1     1     4 2 0 0 5 2 2 1    -0.2441D+02




           ====> Create/Update target property file <====



          Logical unit for target properties file   =  24
          Set number at which to write target data  =   1
          Format style for the target properties    = FORMATTED  


          No. of nuclear centers in molecular frame =   5
          Sequence number of the scattering center  =   5
          Number of target states in this dataset   =   2
          The number of properties computed         =     15
          Nuclear contribution switch (ISW) is set  =   0


 Output to unit 24 is as follows:

6  1    19  4   2    12  0   0.496109233757D-04  0.434828537558D-05  0.421892416979D-05
8  1 O   8   15.9949        0.0000000000        1.3109030132       -0.0577086557
8  2 O   8   15.9949        0.0000000000       -1.3109030132       -0.0577086557
8  3 H   1    1.0078        1.4365698006        1.7081234448        0.9158782441
8  4 H   1    1.0078       -1.4365698006       -1.7081234448        0.9158782441
5  1  0  0  1  2  0  0 -0.150422431988D+03  State No.   1 Doublet     A"        
5  2  0  0  0  2  0  0 -0.150378302391D+03  State No.   2 Doublet     A'        
1  2  0  2  0  5  1  0 -0.996463858845D+00
1  2  0  2  0  5  2 -2 -0.387320737596D+01
1  2  0  2  0  5  2  0  0.251396884117D+01
1  2  0  2  0  5  2  2  0.716322126793D+00
1  1  1  2  0  5  1 -1  0.493871325742D+00
1  1  1  2  0  5  1  1  0.672998068876D-02
1  1  1  2  0  5  2 -1  0.366253276657D+00
1  1  1  2  0  5  2  1 -0.556060652408D+00
1  1  1  1  1  5  1  0 -0.794637887953D+00
1  1  1  1  1  5  2 -2 -0.483833702946D+01
1  1  1  1  1  5  2  0  0.154659186291D+01
1  1  1  1  1  5  2  2  0.883861884035D+00



 DIPOLE POLARIZIBILTY CODE

 Scan moments on unit NFTMT = 61


     MOMENT SET NO.   1

     - doublet A                                                             

     Wavefunction details : 
     Lambda value or irrep. of state =   0     Number of CSFs =     4
 The value of m is:                      0

     Polarizibility (a.u.) for I states = 
       0.0000000D+00
 The value of m is:                      1

     Polarizibility (a.u.) for I states = 
       0.0000000D+00


     MOMENT SET NO.   2

     - doublet B                         / - doublet A                       

     Wavefunction I details : 
     Lambda value or irrep. of state =   1     Number of CSFs =     3

     Wavefunction J details : 

     Lambda value or irrep. of state =   0     Number of CSFs =     4
 The value of m is:                      0

     Polarizibility (a.u.) for I states = 
       0.0000000D+00

     Polarizibility (a.u.) for J states = 
       0.0000000D+00
 The value of m is:                      1

     Polarizibility (a.u.) for I states = 
       0.2052710D-02

     Polarizibility (a.u.) for J states = 
      -0.2052710D-02


     MOMENT SET NO.   3

     - doublet B                                                             

     Wavefunction details : 
     Lambda value or irrep. of state =   1     Number of CSFs =     3
 The value of m is:                      0

     Polarizibility (a.u.) for I states = 
       0.0000000D+00
 The value of m is:                      1

     Polarizibility (a.u.) for I states = 
       0.0000000D+00


     END OF FILE ON READ OF UNIT  61



     END OF FILE ON READ. NO MORE INPUT


          D E N P R O P   has completed execution 


