#!/bin/bash

mkdir -p outputs
rm -f *.dat fort.* beta_*

# ----------------------------------------------------------------------------------------------------------------------------------
echo "H₂O: dipelm"
# ----------------------------------------------------------------------------------------------------------------------------------

ln -s ../4-dipoles-1-A-2-B-Dxy/outer-partial-wave-dipoles.dat fort.142
ln -s ../4-dipoles-1-A-2-B-Dxy/outer-partial-wave-dipoles.dat fort.143
ln -s ../4-dipoles-1-A-1-A-Dz/outer-partial-wave-dipoles.dat  fort.144

dipelm < inputs/dipelm.inp \
             1> dipelm.out \
             2> dipelm.err || exit 1

mv fort.101 dipelm-photoion-dcs.dat

mv fort.1011 dipelm-pwcs-x.dat
mv fort.1021 dipelm-pwcs-y.dat
mv fort.1031 dipelm-pwcs-z.dat

mv fort.2220 dipelm-photoion-tcs.dat
mv fort.2221 dipelm-photoion-ics-1.dat

rm -f fort.*
