Warning
-------

This is a very simplified model of the hydrogen peroxide molecule,
which serves for testing. The results may strongly differ from the
real experimental data.
