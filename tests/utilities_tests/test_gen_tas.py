import unittest
from rmt_utilities.gen_tas import get_tas_spectrum as gts
from types import SimpleNamespace
import os
import matplotlib.pyplot as plt


args = SimpleNamespace(**{'files': ['tests/utilities_tests/spawn'],
                       'x': False,
                          'y': False,
                          'z': True,
                          'plot': False,
                          'output': True,
                          'pad_factor': 8,
                          'solutions_list': None,
                          'units': 'eV'})


class genhhg(unittest.TestCase):
    @classmethod
    def setUp(self):
        from subprocess import call
        call(['mkdir', 'tests/utilities_tests/spawn'])
        call(['cp', 'tests/utilities_tests/data/expec_z_all.neon00001000', 'tests/utilities_tests/spawn/'])
        call(['cp', 'tests/utilities_tests/data/expec_z_all.neon00001000',
              'tests/utilities_tests/spawn/EField.neon00001000'])

    @classmethod
    def tearDown(self):
        from subprocess import call
        call(['rm', '-rf', 'tests/utilities_tests/spawn'])

    def test_get_tas(self):
        assert gts(args)

    def test_output(self):
        gts(args)
        assert os.path.isfile('tests/utilities_tests/spawn/TAS_0001')
        assert os.path.isfile('tests/utilities_tests/spawn/TAS_0002')

    def test_plot(self):
        args.plot = True
        args.output = False
        plt.ion()
        ax = gts(args)
        assert len(ax.get_lines()) == 2
        assert ax.get_xlabel() == 'Frequency (eV)'
        plt.close()
