import pytest
import unittest
import rmt_utilities.dataobjects as ru
import numpy as np
import pandas as pd
from pathlib import Path
import os
import io
import contextlib


class DataFileBasicTests(unittest.TestCase):
    def setUp(self):
        self.df = ru.DataFile("./tests/utilities_tests/data/heads.csv")
        self.dg = ru.DataFile("./tests/utilities_tests/data/noheads.csv")
        self.dh = ru.DataFile("./tests/utilities_tests/data/Harm_vel_0001_z")

    def test_DataFileName(self):
        assert (self.df.name == "heads.csv")

    def test_DataFilePath(self):
        assert (self.df.path == Path("tests/utilities_tests/data/heads.csv"))

    def test_harvest_heads(self):
        assert not (self.df.empty)

    def test_harvest_noheads(self):
        assert not (self.dg.empty)

    def test_harvest_equivalence(self):
        assert (np.all(self.df.values == self.dg.values))

    def test_harvest_nonequivalence(self):
        assert (self.df.len != self.dh.len)

    def test_xstep(self):
        assert (self.df.xstep == -15)

    def test_datalen(self):
        assert (self.df.len == 4)

    def test_filter(self):
        de = ru.DataFile("./tests/utilities_tests/data/heads.csv", sols=["0002"])
        assert (de.columns == ["Time", "0002"]).all()

    def test_bool(self):
        ob = ru.Data({"a": [1, 2, 3]})
        assert ob

    def test_neq(self):
        df = ru.DataFile("./tests/utilities_tests/data/expec_z_all.neon00001000")
        dg = ru.DataFile("./tests/utilities_tests/data/expec_z_all.neon00001000")
        dg['0001'] += 1e-5
        assert df != dg

    def test_eq(self):
        df = ru.DataFile("./tests/utilities_tests/data/expec_z_all.neon00001000")
        dg = ru.DataFile("./tests/utilities_tests/data/expec_z_all.neon00001000")
        dg['0001'] += 1e-14
        assert df == dg


class DataFileMethodTests(unittest.TestCase):
    def setUp(self):
        self.dz = ru.DataFile("./tests/utilities_tests/data/expec_z_all.neon00001000")

        x1 = np.linspace(0, 4 * np.pi, 128)
        y1 = np.sin(x1)
        y2 = np.sin(2 * x1)
        y3 = np.concatenate((np.zeros(128), y1))
        y4 = np.concatenate((np.zeros(128), y2))
        dx = x1[1] - x1[0]

        f1 = np.fft.fft(y1)[:64]
        f2 = np.fft.fft(y2)[:64]
        f3 = np.fft.fft(y3)[:128]
        f4 = np.fft.fft(y4)[:128]

        fx = np.arange(64) * ((np.pi / 64) / dx)
        fx3 = np.arange(128) * ((np.pi / 128) / dx)

        self.fat = pd.DataFrame()
        self.fat["Freq"] = fx
        self.fat["0001"] = f1
        self.fat["0002"] = f2

        self.fzt = pd.DataFrame()
        self.fzt["Freq"] = fx3
        self.fzt["0001"] = f3
        self.fzt["0002"] = f4

    def test_pad_with_zeros(self):
        y = self.dz._pad_with_zeros([2.0, 4.0, 5.0], factor=2)
        assert (np.all(y == np.array([0., 0., 0., 0., 0., 2., 4, 5.])))

    def test_FFT(self):
        dFT = self.dz.FFT(blackman=False, pad=False)
        x = np.real(self.fat.values)[1:]
        y = np.real(dFT.values)[1:]
        assert np.allclose(x, y)

    def test_FFTpad(self):
        dFT = self.dz.FFT(blackman=False, pad=2)
        x = np.real(self.fzt.values)[1:]
        y = np.real(dFT.values)[1:]
        assert np.allclose(x, y)

    def test_write(self):
        df = {"a": [1, 2, 3], "0001_z": [1, 2, 3], "0002_z": [2, 3, 4]}
        ob = ru.Observable("./tests/utilities_tests/data", df)
        ob.write("Harm_len_", units="au")
        assert os.path.isfile("tests/utilities_tests/data/Harm_len_0001_z")
        assert os.path.isfile("tests/utilities_tests/data/Harm_len_0002_z")
        ndf = pd.read_csv("tests/utilities_tests/data/Harm_len_0001_z", delim_whitespace=True,
                          header=None)
        assert all([x == y for x, y in zip(df["a"], ndf[0])])
        assert all([x == y for x, y in zip(df["0001_z"], ndf[1])])
        ob.write("Harm_len_", units="eV")
        ndf = pd.read_csv("tests/utilities_tests/data/Harm_len_0002_z", delim_whitespace=True,
                          header=None)
        from rmt_utilities.atomicunits import eV
        assert all([x == y * eV for x, y in zip(df["a"], ndf[0])])
        assert all([x == y for x, y in zip(df["0002_z"], ndf[1])])
        from subprocess import call
        call(["rm", "tests/utilities_tests/data/Harm_len_0001_z"])
        call(["rm", "tests/utilities_tests/data/Harm_len_0002_z"])

    def test_unitfail(self):
        df = {"a": [1, 2, 3], "0001_z": [1, 2, 3], "0002_z": [2, 3, 4]}
        ob = ru.Observable("./tests/utilities_tests/data", df)
        self.assertRaises(ru.UnitError, ob.write, "op", units="ff")

    def test_plotfail(self):
        df = {"a": [1, 2, 3], "0001_z": [1, 2, 3], "0002_z": [2, 3, 4]}
        ob = ru.Observable("./tests/utilities_tests/data", df)
        self.assertRaises(ru.UnitError, ob.plot, units="ff")

    def test_plot(self):
        df = {"a": [1, 2, 3], "0001_z": [1, 2, 3], "0002_z": [2, 3, 4]}
        ob = ru.Observable("./tests/utilities_tests/data", df)
        ax = ob.plot(units="au")
        assert ax.get_xlabel() == "Frequency (au)"
        assert ax.get_ylabel() == "Amplitude (arb. units)"
        ax = ob.plot(units="eV")
        assert ax.get_xlabel() == "Frequency (eV)"


class helperfunctests(unittest.TestCase):
    def test_convertpath(self):
        with pytest.raises(TypeError) as E:
            ru.convertpath(1)
        assert str(E.value).startswith('The path supplied')
        assert '(1) is of type' in str(E.value)

    def test_noHfile(self):
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            H = ru.Hfile('.')
        optext = f.getvalue()
        assert optext.startswith("Error reading H file")
        assert H.rmatr == 20.0

    def test_Hfile(self):
        H = ru.Hfile('tests/utilities_tests/data')
        assert H.rmatr == 15.0


class popdata_Tests(unittest.TestCase):
    @classmethod
    def setUp(self):
        import utilities_tests.setup as ts
        ts.build_popchn()
        self.pp = ru.popdata('./tests/utilities_tests/data/popchn.dat')

    @classmethod
    def tearDown(self):
        import utilities_tests.setup as ts
        ts.erase_popchn()

    def test_ColumnHeader(self):
        pp = ru.popdata()
        pp.numsol = 1
        expected = "Time                  0001                 \n"
        assert expected == pp._ColumnHeader()
        pp.numsol = 2
        expected = "Time                  0001                  0002                 \n"
        assert expected == pp._ColumnHeader()

    def test_build_fname(self):
        pp = ru.popdata()
        pp.path = Path('./tests/utilities_tests/data/popchn.googlyboo')
        assert pp._build_fname(1) == Path('./tests/utilities_tests/data/popL01.googlyboo')
        assert pp._build_fname(99) == Path('./tests/utilities_tests/data/popL99.googlyboo')
        assert pp._build_fname(9999) == Path('./tests/utilities_tests/data/pL9999.googlyboo')

    def test_popdata_path(self):
        assert self.pp.path == Path('./tests/utilities_tests/data/popchn.dat')

    def test_popdata_params(self):
        assert self.pp.nchan == 5
        assert self.pp.steps == 1

    def test_popdata_times(self):
        assert self.pp.deltat == 0.01
        assert all(self.pp.times == np.arange(0.01, 0.13, 0.01))

    def test_read_binary_data(self):
        results = self.pp._read_binary_data()
        expected = np.linspace(0, 1, 120)
        assert results.shape == (3, 2, 5, 4)
        assert all(results[0][0][0][:] == expected[:4])
        assert all(results[-1][-1][-1][:] == expected[-4:])

    def test_noread_binary_data(self):
        pp = ru.popdata()
        assert pp._read_binary_data() is None

    def test_alldfs(self):
        expected = np.linspace(0, 1, 120)
        for keys, exp in zip(self.pp.alldfs, ['0001', '0002']):
            assert (keys == exp)
        for col, exp in zip(self.pp.alldfs['0001'], range(1, 6)):
            assert col == str(exp)
        exp = (np.concatenate([expected[24:28], expected[64:68], expected[104:108]]))
        assert all(self.pp.alldfs['0002']['2'] == exp)

    def test_write_error(self):
        pp = ru.popdata('./tests/utilities_tests/data/popchn.dat')
        pp.path = Path('this/isnt/a/directory')
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            with pytest.raises(FileNotFoundError):
                pp._write_recon_data(range(3), 7)
        optext = f.getvalue()
        assert optext == "unable to write to file this/isnt/a/directory\n"

    def test_noinit(self):
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            ru.popdata('thisandthat')
        optext = f.getvalue()
        assert optext == "failed to compile population data\n"

    def test_recon(self):
        from subprocess import call
        ru.popdata('./tests/utilities_tests/data/popchn.dat', recon=True)
        assert Path('./tests/utilities_tests/data/popL01.dat').is_file()
        assert Path('./tests/utilities_tests/data/popL05.dat').is_file()
        for ii in range(1, 6):
            call(["rm", f"tests/utilities_tests/data/popL0{ii}.dat"])

    def test_read_formatted_files(self):
        from subprocess import call
        unform = ru.popdata('./tests/utilities_tests/data/popchn.dat')
        ru.popdata('./tests/utilities_tests/data/popchn.dat', recon=True)
        form = ru.popdata('./tests/utilities_tests/data')
        assert unform.nchan == form.nchan
        assert unform.numsol == form.numsol
        for key in unform.alldfs.keys():
            assert (unform.alldfs[key].round(13).equals(form.alldfs[key].round(13)))
        for ii in range(1, 6):
            call(["rm", f"tests/utilities_tests/data/popL0{ii}.dat"])

    def test_get_channelID(self):
        c = ru.popdata()
        testdict = {1: Path("./popL01.googlyboo"),
                    19: Path("/googlyboo/popL19.test"),
                    111: Path("/Users/test/ppL111.test"),
                    9999: Path("testdir/pL9999.googlyboo"),
                    10000: Path("p10000.test")}
        for key in testdict:
            assert (c._get_channelID(testdict[key]) == int(key))

    def test_cross_sec(self):
        pdat = ru.popdata('./tests/utilities_tests/data/popchn.dat')
        exp = 409.7443147246781
        assert pdat.cross_sec('0001', ['1', '2'], 1, 1, 1, 1) == exp
        exp = 1786.1539221372232
        assert pdat.cross_sec('0001', ['1', '2'], 1e12, 1, 1, 2) == exp
