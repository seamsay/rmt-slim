#!/bin/sh
# simulate an calculation by creating a selection of output files

touch tests/utilities_tests/spawn/pop_all.out
touch tests/utilities_tests/spawn/pop_inn.out
touch tests/utilities_tests/spawn/pop_out.out
touch tests/utilities_tests/spawn/data/pop_GS.out
touch tests/utilities_tests/spawn/state/psi_inner.out
touch tests/utilities_tests/spawn/state/psi_outer.out
touch tests/utilities_tests/spawn/ground/psi_outer.out
touch tests/utilities_tests/spawn/ground/psi_inner.out
