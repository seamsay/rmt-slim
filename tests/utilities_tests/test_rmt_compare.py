import unittest
import rmt_utilities.rmt_compare as rmc
from types import SimpleNamespace
import io
import contextlib


goodargs = {'fileA': 'tests/utilities_tests/goodcalc',
            'fileB': 'tests/utilities_tests/data',
            'tolerance': 9,
            'quiet': False}

badargs = {'fileA': 'tests/utilities_tests/badcalc',
           'fileB': 'tests/utilities_tests/data',
           'tolerance': 9,
           'quiet': False}


class rmt_compare_tests(unittest.TestCase):
    @classmethod
    def setUp(self):
        from subprocess import call
        call(['cp', '-r', 'tests/utilities_tests/data/', 'tests/utilities_tests/goodcalc'])
        call(['cp', '-r', 'tests/utilities_tests/data/', 'tests/utilities_tests/badcalc'])
        with open('tests/utilities_tests/goodcalc/expec_z_all.neon00001000', 'r') as f:
            with open('tests/utilities_tests/badcalc/expec_z_all.neon00001000', 'w') as g:
                for line in f.readlines():
                    g.writelines(line.replace(' 0.0 ', ' 0.0000001 '))

    @classmethod
    def tearDown(self):
        from subprocess import call
        call(['rm', '-rf', 'tests/utilities_tests/goodcalc/'])
        call(['rm', '-rf', 'tests/utilities_tests/badcalc/'])

    def test_goodcalc(self):
        nargs = SimpleNamespace(**goodargs)
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            assert rmc.compare(nargs)
        assert f.getvalue().startswith('RMT calculation in:')
        assert 'tests/utilities_tests/goodcalc' in f.getvalue()

    def test_badcalc(self):
        nargs = SimpleNamespace(**badargs)
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            try:
                rmc.compare(nargs)
            except SystemExit as e:
                assert e.__str__().startswith('Calculations do not agree')
        assert f.getvalue().startswith('RMT calculation in:')

    def test_calcs_equal(self):
        from rmt_utilities.rmtutil import RMTCalc
        calcA = RMTCalc('tests/utilities_tests/data')
        calcB = RMTCalc('tests/utilities_tests/goodcalc')
        calcC = RMTCalc('tests/utilities_tests/badcalc')
        assert calcA == calcB
        assert calcA != calcC

    def test_quiet(self):
        nargs = SimpleNamespace(**badargs)
        nargs.quiet = True
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            try:
                rmc.compare(nargs)
            except SystemExit as e:
                assert e.__str__().startswith('Calculations do not agree')
        assert f.getvalue() == ""

    def test_tolerance(self):
        nargs = SimpleNamespace(**badargs)
        nargs.tolerance = 6
        assert rmc.compare(nargs)

    def test_args(self):
        parser = rmc.read_command_line()
        assert parser
        args = parser.parse_args(['tests/utilities_tests/goodcalc', 'tests/utilities_tests/data'])
        for arg in goodargs:
            assert getattr(args, arg) == goodargs[arg]
