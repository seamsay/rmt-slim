import pytest
import unittest
import rmt_utilities.rmtutil as ru
import pandas as pd
from pathlib import Path
from shutil import which
import subprocess


DUMMY_EXEC = Path('tests/utilities_tests/bin/rmt.sh')
TEMPLATE = Path('tests/utilities_tests/data')
DUMMY_mpiRun = which('mpirun')


class RMTCalcBasicTests(unittest.TestCase):
    @classmethod
    def setup_class(self):
        subprocess.call(["mkdir", "./tests/utilities_tests/data/data"])
        tmpdf = pd.DataFrame({'Time': [17, 2, 3, 4],
                              '0001': [1, 4, 9, 16],
                              '0002': [1, 8, 27, 64]})
        for fl in ["pthis", "pand", "pthat"]:
            tmpdf.to_csv(f"./tests/utilities_tests/data/data/{fl}", index=False, sep=" ")
        self.calc = ru.RMTCalc(path="tests/utilities_tests/data/", target="Argon", description="testing")

    @classmethod
    def teardown_class(self):
        subprocess.call(["rm", "-rf", "./tests/utilities_tests/data/data"])

    def test_path(self):
        assert self.calc.path == Path("tests/utilities_tests/data").absolute()

    def test_target(self):
        assert self.calc.target == "Argon"

    def test_description(self):
        assert self.calc.description == "testing"

    def test_conffile(self):
        assert self.calc.conffile == Path("tests/utilities_tests/data/input.conf").absolute()

    def test_config_xlast(self):
        assert self.calc.config["x_last_others"] == 480

    def test_config_frequency(self):
        assert self.calc.config["frequency"] == 12 * [1.0]

    def test_config_deltar_default(self):
        assert self.calc.config["deltar"] == 0.08

    def test_suffix(self):
        assert self.calc._suffix == "neon00001000"

    def test_rootfiles(self):
        complist = [Path("tests/utilities_tests/data/" + f).absolute() for f in
                    ["expec_v_all.neon00001000",
                     "expec_z_all.neon00001000"]]
        assert all([x in self.calc.rootfiles for x in complist])
        assert complist == self.calc.rootfiles

    def test_datalist(self):
        complist = [Path("tests/utilities_tests/data/data/" + f).absolute() for f in ["pthis", "pand", "pthat"]]
        assert all([x in self.calc.datalist for x in complist])
        complist.sort()
        assert self.calc.datalist == complist

    def test_emptydatalist(self):
        calc = ru.RMTCalc("tests/utilities_tests/data/data")
        assert calc.datalist == []

    def test_statelist(self):
        complist = [Path("tests/utilities_tests/data/state/" + f).absolute() for f in ["pthose", "pand", "pthese"]]
        assert all([x in self.calc.statelist for x in complist])
        complist.sort()
        assert self.calc.statelist == complist

    def test_eq(self):
        assert self.calc == self.calc

    def test_neq(self):
        assert self.calc != ru.RMTCalc("./")

    def test_agreesWith(self):
        assert self.calc.agreesWith(self.calc)

    def test_atomicunits(self):
        from rmt_utilities import atomicunits
        assert atomicunits.au2eV(1) == 27.211386245988


class RMTCalcMethodTests(unittest.TestCase):
    @classmethod
    def setup_class(self):
        self.calc = ru.RMTCalc(path="tests/utilities_tests/data/", target="Argon", description="testing")
        self.calc._initExpecFiles(sols=["0001"])
        self.spawn = ru.RMTCalc(path="tests/utilities_tests/spawn", template=TEMPLATE,
                                rmtexec=DUMMY_EXEC)

    @classmethod
    def teardown_class(self):
        subprocess.run(['rm', '-rf', "tests/utilities_tests/spawn"])

    def test_initExpecFilesField(self):
        assert (self.calc.field is None)

    def test_initExpecFilessols(self):
        df = pd.read_csv("tests/utilities_tests/data/expec_z_all.neon00001000", delim_whitespace=True)
        df = df[["Time", "0001"]]
        a = self.calc.expec_z
        assert (a.equals(df))

    def test_initExpecFilesRead(self):
        self.calc2 = ru.RMTCalc(path="tests/utilities_tests/data/", target="Argon", description="testing")
        self.calc2._initExpecFiles(sols=None)
        df = pd.read_csv("tests/utilities_tests/data/expec_z_all.neon00001000", delim_whitespace=True)
        a = self.calc2.expec_z
        assert (a.equals(df))

    def test__FFT_cutoffnum(self):
        assert len(self.calc._FFT(self.calc.expec_z, cutoff=9, pad=1)) == 19

    def test__FFT_cutoffmax(self):
        assert (self.calc._FFT(self.calc.expec_z, cutoff=7, pad=1)["Freq"].values < 7).all()

    def test_execute(self):
        assert self.calc.execute(mpirun="echo")
        subprocess.call(["rm", "./tests/utilities_tests/data/log.out"])

    def test_execute_fail(self):
        assert not self.calc.execute(mpirun="return")

    def test_templatesetup(self):
        assert self.spawn.path.is_dir()
        gdir = self.spawn.path/'ground'
        exc = self.spawn.path/'rmt.sh'
        assert gdir.is_dir()
        assert exc.is_symlink()

    @pytest.mark.skipif(not DUMMY_mpiRun, reason="requires mpirun")
    def test_mpi_execute(self):
        import os
        pre = self.spawn.rootfiles
        if os.geteuid() == 0:
            mpiopts = "--allow-run-as-root"
        else:
            mpiopts = ""
        assert self.spawn.execute(mpirun=DUMMY_mpiRun, rmtexec=DUMMY_EXEC,
                                  mpiopts=mpiopts)
        post = self.spawn.rootfiles
        assert pre != post

    def test_template_with_exec(self):
        from os import readlink
        ncalc = ru.RMTCalc(path="tests/utilities_tests/newspawn", template="tests/utilities_tests/bin")
        assert ncalc.path.is_dir()
        assert Path(readlink(ncalc.path/"rmt.x")) == Path("tests/utilities_tests/bin/rmt.x").absolute()
        subprocess.run(['rm', '-rf', "tests/utilities_tests/newspawn"])

    def test_remove_exec_from_list(self):
        ncalc = ru.RMTCalc(path="tests/utilities_tests/newspawn", template="tests/utilities_tests/bin",
                           rmtexec="tests/utilities_tests/bin/rmt.sh")
        assert ncalc.path.is_dir()
        assert not (ncalc.path/'rmt.x').is_symlink()
        subprocess.run(['rm', '-rf', "tests/utilities_tests/newspawn"])

    def test_samefile_errors(self):
        calcA = ru.RMTCalc("tests/utilities_tests/data")
        calcB = ru.RMTCalc("tests/utilities_tests/data")
        calcA.rootfiles = [Path(x) for x in ["this"]]
        calcB.rootfiles = [Path(x) for x in ["this"]]
        calcA.datalist = [Path(x) for x in ["that"]]
        calcB.datalist = [Path(x) for x in ["that"]]
        calcA._samefiles(calcB)
        calcB.datalist = [Path(x) for x in ["that", "and"]]
        with pytest.raises(FileNotFoundError):
            calcA._samefiles(calcB)
        calcB.datalist = [Path(x) for x in ["tht"]]
        with pytest.raises(IOError):
            calcA._samefiles(calcB)

    def test_agreesWith_errors(self):
        calcA = ru.RMTCalc("tests/utilities_tests/data")
        calcB = ru.RMTCalc("tests/utilities_tests/data")
        calcA.rootfiles = [Path(x) for x in ["this"]]
        calcB.rootfiles = [Path(x) for x in ["this"]]
        calcA.datalist = [Path(x) for x in ["that"]]
        calcB.datalist = [Path(x) for x in ["that"]]
        with pytest.raises(IOError):
            calcA.agreesWith(calcB)

    def test_effective_cycles(self):
        calc = ru.RMTCalc("./")
        calc.config = {'periods_of_ramp_on': 4,
                       'periods_of_pulse': 10}
        assert calc._effective_cycles(sol_id=2, nphotons=1) == 5.
        calc.config = {'periods_of_ramp_on': [3, 4],
                       'periods_of_pulse': [7, 10]}
        assert calc._effective_cycles(sol_id=2, nphotons=1) == 5.

    def test_select_params(self):
        calc = ru.RMTCalc("./")
        calc.config = {'value': 4,
                       'list': [10, 12]}
        assert calc._select_params(param='value') == 4
        assert calc._select_params(param='list') == 10
        assert calc._select_params(param='list', sol=2) == 12
        with pytest.raises(KeyError):
            calc._select_params(param='googlyboo')

    def test_cross_sec_from_popchn(self):
        import utilities_tests.setup as ts
        ts.build_popchn()
        calc = ru.RMTCalc("./tests/utilities_tests/")
        calc.config = {'frequency': 1,
                       'intensity': 1,
                       'periods_of_ramp_on': 0,
                       'periods_of_pulse': 1}
        calc._suffix = "dat"
        exp = 409.7443147246781
        assert calc.cross_sec(channel_list=[1, 2])['0001'] == exp
        assert calc.cross_sec(channel_list=[1, 2], sols=[1])['0001'] == exp
        ts.erase_popchn()

    def test_cross_sec_from_formatted(self):
        import utilities_tests.setup as ts
        from rmt_utilities.dataobjects import popdata
        ts.build_popchn()
        popdata("./tests/utilities_tests/data/popchn.dat", recon=True)
        ts.erase_popchn()
        calc = ru.RMTCalc("./tests/utilities_tests/")
        calc.config = {'frequency': 1,
                       'intensity': 1,
                       'periods_of_ramp_on': 0,
                       'periods_of_pulse': 1}
        calc._suffix = "dat"
        exp = 409.7443147246781
        assert calc.cross_sec(channel_list=[1, 2])['0001'] == exp
        assert calc.cross_sec(channel_list=[1, 2], sols=[1])['0001'] == exp
        for ii in range(1, 6):
            subprocess.call(["rm", f"tests/utilities_tests/data/popL0{ii}.dat"])

    def test_noexecutable(self):
        import io
        import contextlib

        exp = "no suitable rmt executable provided, templated calculation may be incomplete\n"
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            ru.RMTCalc(path="tests/utilities_tests/newspawn",
                       template="tests/utilities_tests/data"),
        assert f.getvalue() == exp
        subprocess.run(['rm', '-rf', "tests/utilities_tests/newspawn"])
